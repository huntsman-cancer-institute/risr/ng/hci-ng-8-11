
var async = require("async");
var childProcess = require("child_process");
var gulp = require("gulp");
var exit = require("gulp-exit");
var debug = require("gulp-debug")
var gulpSequence = require("gulp-sequence")
var gulpLoad = require("gulp-load-plugins")
var install = require("gulp-install");
var clean = require("gulp-clean");
var del = require("del");
var jeditor = require("gulp-json-editor");
//var jmodify = require("gulp-json-modify");
var jmodify = require("./bin/local-jmodify");
var fs = require("fs-extra");
var rename = require("gulp-rename");
var rimraf = require("rimraf");
var spawn = require("cross-spawn");
var path = require("path");
var argv = require("yargs").argv;
var logger = require("node-color-log");

/**
 * Contains tasks to create and build the centralized demo.  By default the demo has no dependencies on the libraries.
 * There is no package.json, instead we have a template with an empty optionalDependencies.  When we create the demo,
 * we inject the dependencies and other information in to a copied package.json.
 * Because dependencies are listed as optional, npm install skips them.  What we must do is copy the transpiled code from
 * each library in to the demo's node_modules as if they were placed there by npm install.
 *
 * Steps
 * 1. npm install the demo
 * 2. npm build all projects
 * 3. npm push all compiled code to demo node_modules
 * 4. build demo
 * 5. run demo
 */

var version = "0.0.0";
var packages = [];
var allPackages = [];

var watch = "./demo/package.json";

/**
 * Get the version from the root package.json.
 */
gulp.task("root-version", gulp.series(function() {
  let packageJson = JSON.parse(fs.readFileSync("./package.json"));
  version = packageJson.version;

  logger.color("green").log("Root Version: " + version);

  return new Promise(function(resolve, reject) {
    logger.color("green").log("Task 'root-version' Done.");
    resolve();
  });
}));

/**
 * For each package, get the published version from npm and compare the latest artifact version with the source version.
 */
gulp.task("check-versions", gulp.series(["root-version"], function() {
  packages = getPackages(false);

  logger.log("\r\ncheck-versions:");
  for (let package of packages) {
    let child = spawn.sync("npm", ["view", "@huntsman-cancer-institute/" + package.name, "versions"], {});
    let out = child.stdout.toString();
    let versions = JSON.parse(out.replace(/'/g, '"'));
    
    //Check latest of same major version rather than just latest
    var filteredVersions = [];
    filteredVersions = versions.filter(function(version) {
    	let major = package.version.substring(0, version.indexOf("."));
		return version.startsWith(major);
	});
    
    let version = filteredVersions[filteredVersions.length - 1];

    if (version == package.version) {
      logger.color("green").log("\t" + package.name);
      logger.color("green").log("\t\tPublished: " + version + "\tSource: " + package.version);
    } else {
      logger.color("red").log("\t" + package.name);
      logger.color("red").log("\t\tPublished: " + version + "\tSource: " + package.version);
    }
  }

  return new Promise(function(resolve, reject) {
    logger.color("green").log("Task 'version' Done.");
    resolve();
  });
}));

/**
 * Get a list of all packages and any package specified by the --packages argument.  These packages contain the name,
 * version, dependency list and more.
 */
gulp.task("packages", gulp.series(["root-version"], function() {
  logger.color("yellow").log("Task: packages");

  try {
    allPackages = getPackages(true);
    packages = getPackages(false);
  } catch (e) {
    return Promise.reject(e);
  }

  return Promise.resolve("Task: 'packages' Done.");
}));

/**
 * Generates the list of dependencies of a package and the packages that depend on it.  Sorts the packages in dependency
 * order so they can be built in an order that allows compilation.
 */
gulp.task("find-dependencies", gulp.series("packages", function() {
  try {
    logger.color("yellow").log("AllPackages in find-dependencies: " + allPackages);
    for (let package of allPackages) {
      let deps = [];
      let otherDeps = [];

      logger.log( package.name);

      for (let dep in package.pkgDeps) {
    	logger.log("pkgPeerDep: " + dep);
        if (dep.indexOf("@huntsman-cancer-institute/") == 0) {
          deps.push(dep.substr(27));
        }
      }

      for (let dep in package.pkgPeerDeps) {
    	logger.log("pkgDep: " + dep);
        if (dep.indexOf("@huntsman-cancer-institute/") != 0) {
          otherDeps.push(dep);
        }
      }

      package.deps = deps;
      package.otherDeps = otherDeps;
    }

    for (let package of allPackages) {
      package.deps = findDependencies(package.deps);
    }

    // Assign the packages (B) to push package A to because package B depends on package A.
    for (let package of allPackages) {
      for (let dep of package.deps) {
        for (let depPkg of allPackages) {
          if (dep == depPkg.name) {
            depPkg.pushDeps.push(package.name);
            break;
          }
        }
      }
    }

    // Sort packages in order of no dependencies to requiring one or more previous packages.
    allPackages.sort((a, b) => {
      if (a.deps.length > 0 || b.deps.length > 0) {
        let flag = 0;

        for (let bDep of b.deps) {
          if (bDep == a.name) {
            flag = -1;
          }
        }
        for (let aDep of a.deps) {
          if (aDep == b.name) {
            if (flag == -1) {
              logger.color("red").log("Circular dependency? " + a.name + " and " + b.name);
            }
            flag = 1;
          }
        }

        if (flag != 0) {
          return flag;
        }

        // If no dependency to each other, sort based on the smallest number of dependencies.
        if (a.deps.length < b.deps.length) {
          return -1;
        } else if (a.deps.length > b.deps.length) {
          return 1;
        } else {
          return 0;
        }
      } else if (a.pushDeps.length < b.pushDeps.length) {
        return -1;
      } else if (a.pushDeps.length > b.pushDeps.length) {
        return 1;
      } else {
        return 0;
      }
    });

    logger.color("green").log("Packages in dependency order:");
    for (let package of allPackages) {
      logger.color("green").log("\t" + package.name);
      logger.log("\t\tDependencies:\t" + JSON.stringify(package.deps));
      logger.log("\t\tPush To:\t" + JSON.stringify(package.pushDeps));
    }

    temp = [];
    for (let package of allPackages) {
      for (let pkg of packages) {
        if (package.name == pkg.name) {
          temp.push(package);
          break;
        }
      }
    }
    packages = temp;
  } catch (e) {
    return Promise.reject(e);
  }

  return Promise.resolve("Task 'find-dependencies' Done.");
}));

/**
 * Recursive function to find dependencies.  When we build auth, we need to push it to header, but header only has
 * navigation as a dependency.  We need to find that header requires navigation which requires authentication.
 *
 * @param deps
 * @returns {any[]}
 */
function findDependencies(deps) {
  for (let dep of deps) {
    for (let package of allPackages) {
      if (package.name == dep) {
        if (package.deps && package.deps.length > 0) {
          deps = deps.concat(package.deps).concat(findDependencies(package.deps));
        }
      }
    }
  }

  return Array.from(new Set(deps));
}

/**
 * Use del.sync to delete the path specified in the parameter.
 *
 * @param path
 * @returns {*}
 */
function deletePath(path) {
  logger.color("yellow").log("deletePath: " + path);

  return del.sync([path]);
}

/**
 * Update this package with the root version along with all its @huntsman-cancer-institute dependencies with that version.
 *
 * @param package
 */
function pushVersion(package, value) {
  return gulp.src("./modules/@huntsman-cancer-institute/" + package.name + "/package.json", {base: "./"})
    .pipe(jeditor(function(json) {
      json.version = version;

      for (let dep of package.deps) {
      	if (json.optionalDependencies != undefined && json.optionalDependencies["@huntsman-cancer-institute/" + dep] != undefined) {
  		  json.optionalDependencies["@huntsman-cancer-institute/" + dep] = version;
  		}
  		if (json.peerDependencies != undefined && json.peerDependencies["@huntsman-cancer-institute/" + dep] != undefined) {
  		  json.peerDependencies["@huntsman-cancer-institute/" + dep] = version;
  		}
  		if (json.devDependencies != undefined && json.devDependencies["@huntsman-cancer-institute/" + dep] != undefined) {
  		  json.peerDependencies["@huntsman-cancer-institute/" + dep] = version;
  		}
  		if (json.dependencies != undefined && json.dependencies["@huntsman-cancer-institute/" + dep] != undefined) {
  		  json.dependencies["@huntsman-cancer-institute/" + dep] = version;
  		}
      }

      return json;
    }))
    .pipe(gulp.dest("./"));
}

/**
 * Update the package.json files to change the value of the postinstall ("ngcc" vs "")
 *
 * @param package
 */
function editPostinstall(name, value) {
	logger.color("cyan").log("Task: editPostinstall");
	logger.color("yellow").log(name + ": postinstall: \"" + value + "\"");
	
	  return gulp.src("./modules/@huntsman-cancer-institute/" + name + "/package.json", {base: "./"})
	    .pipe(jeditor(function(json) {
	      json.scripts.postinstall = value;

	      return json;
	    }))
	    .pipe(gulp.dest("./"));
	}


/**
 * Based on the root-version, push that version to all packages.  Also, update all those package @huntsman-cancer-institute dependencies
 * with this version.
 */
gulp.task("push-versions", gulp.series(["find-dependencies"], function() {
  logger.color("cyan").log("push-versions: " + version);

  try {
    for (let package of packages) {
      logger.color("green").log("\tpush-version: " + package.name);
      pushVersion(package);
    }
  } catch (e) {
    logger.color("red").log(e);
    return Promise.reject(e);
  }

  return Promise.resolve("Task 'push-versions' Done.");
}));

/**
 * Function for deleting a package's demo reference and dependency reference.
 *
 * @param package
 * @returns {*}
 */
function deleteLib(package, deleteDemo) {
  try {
    if (deleteDemo) {
      logger.color("yellow").log("deleteLib: ./demo/node_modules/@huntsman-cancer-institute/" + package.name);
      deletePath("./demo/node_modules/@huntsman-cancer-institute/" + package.name);
    }

    for (let pushDep of package.pushDeps) {
      logger.color("yellow").log("deleteLib Pushed: ./modules/@huntsman-cancer-institute/" + pushDep + "/node_modules/@huntsman-cancer-institute/" + package.name);
      deletePath("./modules/@huntsman-cancer-institute/" + pushDep + "/node_modules/@huntsman-cancer-institute/" + package.name);
    }
  } catch (e) {
    logger.color("red").log(e);
    return Promise.reject(e);
  }

  return Promise.resolve("Function 'buildLib' Done.");
}

/**
 * Task to delete all references of all packages in node_modules.
 */
gulp.task("delete-libs", gulp.series("find-dependencies", function(done) {
  logger.color("cyan").log("\r\nTask: delete-libs");

  for (let package of packages) {
    let deleteDemo = true;
    if (argv.preserveDemo) {
      deleteDemo = false;
    }
    deleteLib(package, deleteDemo);
  }

  done();
}));

/**
 * Task to delete all references of all packages in node_modules except in the demo.
 */
gulp.task("delete-libs-exclude-demo", gulp.series("find-dependencies", function() {
  logger.color("cyan").log("\r\nTask: delete-libs");

  try {
    for (let package of packages) {
      deleteLib(package, false);
    }
  } catch (e) {
    logger.color("red").log(e);
    return Promise.reject(e);
  }

  return Promise.resolve("Task 'delete-libs' Done.");
}));

/**
 * Create the tasks to push the already built packages to the demo's node_modules.
 *
 * @param done
 * @returns {*}
 */
function createPushTasks(done) {
  let tasks = [];
  for (let package of packages) {
    let packagePath = "./modules/@huntsman-cancer-institute/" + package.name;

    // Push the built library to the demo's node_modules.
    function pushDemoFunction() {
      return gulp.src(package.files, {base: packagePath + "/dist", cwd: packagePath + "/dist"})
        .pipe(gulp.dest("./demo/node_modules/@huntsman-cancer-institute/" + package.name + "/"));
    }
    pushDemoFunction.displayName = "push-" + package.name + "-to-demo";
    tasks.push(pushDemoFunction);

    // Push the compodoc output to the demo.
    function pushDocFunction() {
      return gulp.src(["**/*"], {base: packagePath + "/docs", cwd: packagePath + "/docs"})
        .pipe(gulp.dest("./demo/compodoc/" + package.name + "/"));
    }
    pushDocFunction.displayName = "push-" + package.name + "-docs-to-demo";
    tasks.push(pushDocFunction);
  }

  return gulp.series(...tasks, (seriesDone) => {
    seriesDone();
    done();
  })();
}

function createPushDocTasks(done) {
  let tasks = [];
  for (let package of packages) {
    let packagePath = "./modules/@huntsman-cancer-institute/" + package.name;

    // Push the compodoc output to the demo.
    function pushDocFunction() {
      return gulp.src(["**/*"], {base: packagePath + "/docs", cwd: packagePath + "/docs"})
        .pipe(gulp.dest("./demo/compodoc/" + package.name + "/"));
    }
    pushDocFunction.displayName = "push-" + package.name + "-docs-to-demo";
    tasks.push(pushDocFunction);
  }

  return gulp.series(...tasks, (seriesDone) => {
    seriesDone();
    done();
  })();
}

/**
 * Push libraries to any external destination.  For example, "npm run push-libs-dest -- --packages crud --dest ../core/core-ng".
 * This is so you can modify hci-ng modules and push them to an application without publishing anything.
 *
 * @param done
 * @returns {*}
 */
function createPushDestTasks(done) {
  let destPath = argv.dest;
  let tasks = [];
  for (let package of packages) {
    let packagePath = "./modules/@huntsman-cancer-institute/" + package.name + "/dist";

    // Push the built library to the demo's node_modules.
    function pushDestFunction() {
      logger.color("yellow").log(destPath + "/node_modules/@huntsman-cancer-institute/" + package.name + "/");
      return gulp.src(package.files, {base: packagePath, cwd: packagePath})
        .pipe(gulp.dest(destPath + "/node_modules/@huntsman-cancer-institute/" + package.name + "/"));
    }
    pushDestFunction.displayName = "push-" + package.name + "-to-dest";
    tasks.push(pushDestFunction);
  }

  return gulp.series(...tasks, (seriesDone) => {
    seriesDone();
    done();
  })();
}

/**
 * Builds an array of tasks in the order that they must execute and returns as a gulp.series.  This iterates each
 * package and performs several steps in each one.
 *
 * @param done
 * @returns {*}
 */
function createBuildTasks(done) {
  let tasks = [];
  for (let package of packages) {
    let packagePath = "./modules/@huntsman-cancer-institute/" + package.name;
    
    function postinstallFunction() {
    	return editPostinstall(package.name, "ngcc");
    }
    postinstallFunction.displayName = "ngcc";
    tasks.push(postinstallFunction);

    if (argv.install) {
      function installFunction(done) {
        spawn.sync("npm", ["install"], {stdio: "inherit", cwd: packagePath});
        done();
      }
      installFunction.displayName = "install-" + package.name;
      tasks.push(installFunction);
    } 
    
    function postinstallFunction2() {
    	return editPostinstall(package.name, "");
    }
    postinstallFunction2.displayName = "nongcc";
    tasks.push(postinstallFunction2);
    
	function buildFunction(done) {
	  spawn.sync("npm", ["run", "build"], {stdio: "inherit", cwd: packagePath});
	  done();
	}
	buildFunction.displayName = "build-" + package.name;
	tasks.push(buildFunction);


    for (let pushDep of package.pushDeps) {
      // Push the package to other packages that depend on it.
      function pushDepFunction() {
        return gulp.src(package.files, {base: packagePath + "/dist", cwd: packagePath + "/dist"})
          .pipe(gulp.dest("./modules/@huntsman-cancer-institute/" + pushDep + "/node_modules/@huntsman-cancer-institute/" + package.name));
      }
      pushDepFunction.displayName = "push-" + package.name + "-to-" + pushDep;
      tasks.push(pushDepFunction);

      // Push third party dependencies such as @auth0 for authentication.
      for (let otherDep of package.otherDeps) {
        let otherPath = "./modules/@huntsman-cancer-institute/" + package.name + "/node_modules/" + otherDep;

        function pushOtherDepFunction() {
          return gulp.src(package.files, {base: otherPath, cwd: otherPath})
            .pipe(gulp.dest("./modules/@huntsman-cancer-institute/" + pushDep + "/node_modules/" + otherDep));
        }
        pushOtherDepFunction.displayName = "push-" + package.name + "-to-" + pushDep;
        tasks.push(pushOtherDepFunction);
      }
    }

    // Push the build library to the demo's node_modules
    function pushDemoFunction() {
      return gulp.src(package.files, {base: packagePath + "/dist", cwd: packagePath + "/dist"})
        .pipe(gulp.dest("./demo/node_modules/@huntsman-cancer-institute/" + package.name + "/"));
    }
    pushDemoFunction.displayName = "push-" + package.name + "-to-demo";
    tasks.push(pushDemoFunction);

    // Push the compodoc output to the demo.
    function pushDocFunction() {
      return gulp.src(["**/*"], {base: packagePath + "/docs", cwd: packagePath + "/docs"})
        .pipe(gulp.dest("./demo/compodoc/" + package.name + "/"));
    }
    pushDocFunction.displayName = "push-" + package.name + "-docs-to-demo";
    tasks.push(pushDocFunction);
  }

  return gulp.series(...tasks, (seriesDone) => {
    seriesDone();
    done();
  })();
}

/**
 * Create tasks to install then publish each module.
 *
 * @param done
 * @returns {*}
 */
function createPublishTasks(done) {
  let tasks = [];
  for (let package of packages) {
    let packagePath = "./modules/@huntsman-cancer-institute/" + package.name;
    
    function postinstallFunction() {
    	return editPostinstall(package.name, "ngcc");
    }
    postinstallFunction.displayName = "ngcc";
    tasks.push(postinstallFunction);
    

    function installFunction(done) {
      spawn.sync("npm", ["install"], {stdio: "inherit", cwd: packagePath});
      done();
    }
    installFunction.displayName = "install-" + package.name;
    tasks.push(installFunction);
    
    function postinstallFunction2() {
    	return editPostinstall(package.name, "");
    }
    postinstallFunction2.displayName = "nongcc";
    tasks.push(postinstallFunction2);

    function publishFunction(done) {
      spawn.sync("npm", ["run", "npmPublish"], {stdio: "inherit", cwd: packagePath});
      done();
    }
    publishFunction.displayName = "publish-" + package.name;
    tasks.push(publishFunction);
  }

  return gulp.series(...tasks, (seriesDone) => {
    seriesDone();
    done();
  })();
}

/**
 * Delete the demo's node_modules.
 */
gulp.task("delete-demo-modules", gulp.series(function() {
  logger.color("yellow").log("Task: delete-demo-modules");

  return del.sync(["./demo/node_modules"]);
}));

/**
 * For each package, run the clean command and delete the node_modules.  This should return to a state as though we just
 * checked out for the first time.
 */
gulp.task("clean-all", gulp.series([], function(done) {
  let packages = getPackages(true);

  for (var package of packages) {
    let path = "./modules/@huntsman-cancer-institute/" + package.name;

    logger.color("yellow").log("npm run clean: " + path);
    spawn.sync("npm", ["run", "clean"], { stdio: "inherit", cwd: path});

    logger.color("yellow").log("Cleaning node_modules: " + path);
    del.sync([path + "/node_modules"], {force: true});
  }

  done();
}));

/**
 * Builds and runs the demo.  This assumes the node_modules/@huntsman-cancer-institute already exists.
 */
gulp.task("run-demo", gulp.series([], function() {
  spawn.sync("npm", ["run", "run-demo"], { stdio: "inherit", cwd: "demo"});
}));

/**
 * Builds and runs the production demo.  This assumes the node_modules/@huntsman-cancer-institute already exists.
 */
gulp.task("run-demo-prod", gulp.series([], function() {
  spawn.sync("npm", ["run", "run-demo-prod"], { stdio: "inherit", cwd: "demo"});
}));

/**
 * Gets the package name and version for everything in modules/@huntsman-cancer-institute.  Or, if a "--packages pgk1,pgk2" argument is passed,
 * will only operate on those packages.
 */
function getPackages(all) {
  logger.color("cyan").log("\r\ngetPackages(): " + all);

  var packageNames = [];

  if (argv.packages && !all) {
    // If a package argument, use that.

    packageNames = argv.packages.split(",");
  } else {
    // If no packages argument, find all packages.

    packageNames = fs.readdirSync("modules/@huntsman-cancer-institute/").filter(function(file) {
      return fs.statSync(path.join("modules/@huntsman-cancer-institute/", file)).isDirectory();
    });

    // With git change branch, some directories might still exist, but VCed files won't be there.  Skip these.
    for (var i = packageNames.length - 1; i >= 0; i--) {
      try {
        fs.readFileSync("./modules/@huntsman-cancer-institute/" + packageNames[i] + "/package.json")
      } catch (e) {
        packageNames.splice(i, 1);
      }
    }
  }

  return packageNamesToObject(packageNames);
}

/**
 * Take an array of package names and convert it in to an array of objects with the name, version and target files.
 *
 * @param packageNames
 * @returns {Array}
 */
function packageNamesToObject(packageNames) {
  let packageArray = [];

  for (var package of packageNames) {
    logger.color("white").log("\t" + package);

    var packageJson = {};

    try {
      packageJson = JSON.parse(fs.readFileSync("./modules/@huntsman-cancer-institute/" + package + "/package.json"));
    } catch (e) {
      logger.color("red").warn("Package " + package + " doesn't exist, skipping...");
      continue;
    }

    packageArray.push({
      name: package,
      files: packageJson.files,
      version: packageJson.version,
      pkgDeps: packageJson.dependencies,
      pkgPeerDeps: packageJson.peerDependencies,
      deps: [],
      pushDeps: [],
      otherDeps: []
    })
  }

  return packageArray;
}

gulp.task("push-tsconfig", gulp.series("find-dependencies", function(done) {
  for (var package of allPackages) {
    logger.color("cyan").log("Copying tsconfig.json to ./modules/@huntsman-cancer-institute/" + package.name + "/tsconfig.json");
    gulp.src("./tsconfig.json").pipe(gulp.dest("./modules/@huntsman-cancer-institute/" + package.name, {overwrite: true}));
  }

  done();
}));

gulp.task("push-dependencies", gulp.series("find-dependencies", function(done) {
  var rootDependencies = JSON.parse(fs.readFileSync("./package.json")).pushDependencies;

  // Push Dependencies to the Modules
  for (var package of allPackages) {
    var dependencies = JSON.parse(fs.readFileSync("./modules/@huntsman-cancer-institute/" + package.name + "/package.json")).dependencies;
    var devDependencies = JSON.parse(fs.readFileSync("./modules/@huntsman-cancer-institute/" + package.name + "/package.json")).devDependencies;
    var peerDependencies = JSON.parse(fs.readFileSync("./modules/@huntsman-cancer-institute/" + package.name + "/package.json")).peerDependencies;
    var optionalDependencies = JSON.parse(fs.readFileSync("./modules/@huntsman-cancer-institute/" + package.name + "/package.json")).optionalDependencies;

    var stream = gulp.src("./modules/@huntsman-cancer-institute/" + package.name + "/package.json", { base: "./" });

    for (var rootKey of Object.keys(rootDependencies)) {
      if (dependencies) {
        for (var pkgKey of Object.keys(dependencies)) {
          if (pkgKey == rootKey) {
            if (dependencies[pkgKey] == rootDependencies[rootKey]) {
              continue;
            } else if (pkgKey.indexOf(".") != -1) {
              pkgKey = pkgKey.replace(".", "\\.");
            }
            stream = stream.pipe(jmodify({key: "dependencies." + pkgKey, value: rootDependencies[rootKey]}));
          }
        }
      }

      if (devDependencies) {
        for (var pkgKey of Object.keys(devDependencies)) {
          if (pkgKey == rootKey) {
            if (devDependencies[pkgKey] == rootDependencies[rootKey]) {
              continue;
            } else if (pkgKey.indexOf(".") != -1) {
              pkgKey = pkgKey.replace(".", "\\.");
            }
            stream = stream.pipe(jmodify({key: "devDependencies." + pkgKey, value: rootDependencies[rootKey]}));
          }
        }
      }

      if (peerDependencies) {
        for (var pkgKey of Object.keys(peerDependencies)) {
          if (pkgKey == rootKey) {
            if (peerDependencies[pkgKey] == rootDependencies[rootKey]) {
              continue;
            } else if (pkgKey.indexOf(".") != -1) {
              pkgKey = pkgKey.replace(".", "\\.");
            }
            stream = stream.pipe(jmodify({key: "peerDependencies." + pkgKey, value: rootDependencies[rootKey]}));
          }
        }
      }

      if (optionalDependencies) {
        for (var pkgKey of Object.keys(optionalDependencies)) {
          if (pkgKey == rootKey) {
            if (optionalDependencies[pkgKey] == rootDependencies[rootKey]) {
              continue;
            } else if (pkgKey.indexOf(".") != -1) {
              pkgKey = pkgKey.replace(".", "\\.");
            }
            stream = stream.pipe(jmodify({key: "optionalDependencies." + pkgKey, value: rootDependencies[rootKey]}));
          }
        }
      }
    }



    /**
     * Not pushing the exact current package version of each module to each dependency, any more.
     * This was problematic, since when building, the projects being depended on are built in order, but not yet published, when you bumped up to a new packag version.
     * So going straight to publish-libs worked, since they were published in order
     * But build-libs would not work after push-dependencies, since the required projects were not yet published
     * 
     * So, now we will have a minimum necessary compatible version in the push-dependencies (^ style), and have that pushed out to each project's dependencies
     */
     
	/*
    for (var package of allPackages) {
      if (dependencies) {
	    for (var pkgKey of Object.keys(dependencies)) {
	      if (pkgKey == "@huntsman-cancer-institute/" + package.name && dependencies[pkgKey] != package.version) {
	        stream = stream.pipe(jmodify({key: "dependencies." + pkgKey, value: package.version}));
	      }
	    }
	  }	
    	
      if (devDependencies) {
        for (var pkgKey of Object.keys(devDependencies)) {
          if (pkgKey == "@huntsman-cancer-institute/" + package.name && devDependencies[pkgKey] != package.version) {
            stream = stream.pipe(jmodify({key: "devDependencies." + pkgKey, value: package.version}));
          }
        }
      }

      if (peerDependencies) {
        for (var pkgKey of Object.keys(peerDependencies)) {
          if (pkgKey == "@huntsman-cancer-institute/" + package.name && peerDependencies[pkgKey] != package.version) {
            stream = stream.pipe(jmodify({key: "peerDependencies." + pkgKey, value: package.version}));
          }
        }
      }

      if (optDependencies) {
        for (var pkgKey of Object.keys(optDependencies)) {
          if (pkgKey == "@huntsman-cancer-institute/" + package.name && optDependencies[pkgKey] != package.version) {
            stream = stream.pipe(jmodify({key: "optDependencies." + pkgKey, value: package.version}));
          }
        }
      }
    }*/

    stream.pipe(gulp.dest("."));
  }
/*
  for (var package of allPackages) {
    if (devDependencies) {
      for (var pkgKey of Object.keys(devDependencies)) {
        if (pkgKey == "@huntsman-cancer-institute/" + package.name && devDependencies[pkgKey] != package.version) {
          stream = stream.pipe(jmodify({key: "devDependencies." + pkgKey, value: package.version}));
        }
      }
    }

    if (dependencies) {
      for (var pkgKey of Object.keys(dependencies)) {
        if (pkgKey == "@huntsman-cancer-institute/" + package.name && dependencies[pkgKey] != package.version) {
          stream = stream.pipe(jmodify({key: "dependencies." + pkgKey, value: package.version}));
        }
      }
    }

    if (optDependencies) {
      for (var pkgKey of Object.keys(optDependencies)) {
        if (pkgKey == "@huntsman-cancer-institute/" + package.name && optDependencies[pkgKey] != package.version) {
          stream = stream.pipe(jmodify({key: "optDependencies." + pkgKey, value: package.version}));
        }
      }
    }
  }

  stream.pipe(gulp.dest("."));
*/
  done();
}));

/**
 * Run npm install on the demo.
 */
gulp.task("demo-npm-install", function(done) {
  logger.color("cyan").log("Task: demo-npm-install");

  spawn.sync("npm", ["install"], {stdio: "inherit", cwd: "./demo"});
  done();
});

gulp.task("push-docs", gulp.series("find-dependencies", createPushDocTasks));

/**
 * Creates the demo's package.json from the template and then runs npm install.
 *
 * Note: This does not allow you to run the demo as you still need to build and or push the packages.
 */
gulp.task("install-demo", gulp.series("demo-npm-install"));

/**
 * Build all libraries in the correct dependency order.  After each one is built, push it to the demo and all of the
 * libraries that require it as a dependency.  Otherwise there would be build errors if a dependency didn't exist.
 */
const buildLibSeries = gulp.series("delete-libs", createBuildTasks);
gulp.task("build-libs", gulp.series(buildLibSeries));

/**
 * Push all libraries to the demo's node_modules.  This assumes each library is already built.
 */
const pushLibSeries = gulp.series("find-dependencies", createPushTasks);
gulp.task("push-libs", gulp.series(pushLibSeries));

gulp.task("push-libs-dest", gulp.series("find-dependencies", createPushDestTasks));

/**
 * Delete pushed libraries, then install and publish in order.
 */
const publishLibSeries = gulp.series("delete-libs-exclude-demo", createPublishTasks);
gulp.task("publish-libs", gulp.series(publishLibSeries));
