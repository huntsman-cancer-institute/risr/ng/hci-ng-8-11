# hci-ng #

## [12.0.0](https://ri-source.hci.utah.edu/hcidevs/hci-ng/tree/v12.0.0)
    - Refactor the search list.
    - Add utils package with common component injection library.
    - ComponentType decorator for preserving class names.
    - Apply new theme.

## [11.0.0](https://ri-source.hci.utah.edu/hcidevs/hci-ng/tree/v11.0.0)
    - To Angular 8 and rxjs 6.
    - Improved compiling which uses dist/.
    - Centralize dependencies in the root package.json and push them to modules.
    - Push centralized tsconfig.json to modules.
    - Header, footer, sidebar to navigation.
    - Paging in search list.

## [10.0.0](https://ri-source.hci.utah.edu/hcidevs/hci-ng/tree/v10.0.0)
    - Dictionary editor.
    - Configure on demand editor.
    - CRUD service and components.
    - Improved layout styling.

## [9.0.0](https://ri-source.hci.utah.edu/hcidevs/hci-ng/tree/v9.0.0)
    - Libraries unified to one version.
    - Gulp refactored for new centralized commands

## [2.1.0](https://ri-source.hci.utah.edu/hcidevs/hci-ng/tree/v2.1.0)
    - Dictionary Servies
    - COD
    - Minor styling updates

## [2.0.0](https://ri-source.hci.utah.edu/hcidevs/hci-ng/tree/v2.0.0)
    - Misc components
    - Inputs with dictionary
    - Styling theme update
@huntsman-cancer-institute
## [1.1.0](https://ri-source.hci.utah.edu/hcidevs/hci-ng/tree/v1.1.0)
    - Embedded styling moved to @huntsman-cancer-institute/style
    - Refactored navigation.
    - Navigation has global service.
    - Updated styling with look and feel.

## [1.0.0](https://ri-source.hci.utah.edu/hcidevs/hci-ng/tree/v1.0.0)
    - Initial Release
