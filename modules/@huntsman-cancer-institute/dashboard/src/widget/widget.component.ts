/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, isDevMode, Output, EventEmitter} from "@angular/core";

import {BehaviorSubject, Subject} from "rxjs";

import {Widget, WidgetAttributeChoice, WidgetAttributeContainer, WidgetAttributeValue, WidgetInstance, WidgetAttribute} from "../model/model";
import {DashboardService} from "../services/dashboard.service";

const CARD_CLASS: number = -1;
const SHOW_TITLE: number = -2;

/**
 * @since 1.0.0
 */
@Component({
  selector: "widget",
  template: `
    <ng-template></ng-template>
  `
})
export class WidgetComponent {

  @Output() cardClassEvent: EventEmitter<string> = new EventEmitter<string>();

  public showTitle: boolean = true;
  public cardClass: string = "one-third";
  public cardPaddingClass: string;

  public widget: Widget;
  public widgetInstance: WidgetInstance;

  public rendered: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public attributeContainers: BehaviorSubject<Map<string, WidgetAttributeContainer>> = new BehaviorSubject<Map<string, WidgetAttributeContainer>>(undefined);

  protected dashboardService: DashboardService;

  private initializedSubject: Subject<boolean> = new Subject<boolean>();

  constructor(dashboardService: DashboardService) {
    if (isDevMode()) {
      console.debug("WidgetComponent()");
    }

    this.dashboardService = dashboardService;

    this.rendered.subscribe((rendered: boolean) => {
      if (rendered) {
	      this.attributeContainers.next(this.getAttributeContainers());
	    }
	  });
  }

  config(attributeValues: WidgetAttributeValue[]): void {
    let commonAttributes: WidgetAttribute[] = this.dashboardService.getCommonAttributes().get("Common").attributes;

    if (isDevMode()) {
      console.debug("Widget.config");
      console.debug(commonAttributes);
    }

    if (!commonAttributes) {
      return;
    }

    if (this.widgetInstance && this.widgetInstance.widget.attributeValueSet && this.widgetInstance.widget.attributeValueSet.attributeValues) {
      for (let commonAttribute of commonAttributes) {
        for (var v = 0; v < this.widgetInstance.widget.attributeValueSet.attributeValues.length; v++) {
          if (commonAttribute.idWidgetAttribute === this.widgetInstance.widget.attributeValueSet.attributeValues[v].idWidgetAttribute) {
            if (commonAttribute.name === "cardClass" || commonAttribute.name === "showTitle") {
              this.setValues(commonAttribute.name, [this.widgetInstance.widget.attributeValueSet.attributeValues[v]]);
            }
          }
        }
      }
    }
    if (this.widgetInstance && this.widgetInstance.attributeValueSet && this.widgetInstance.attributeValueSet.attributeValues) {
      for (let commonAttribute of commonAttributes) {
        for (var v = 0; v < this.widgetInstance.attributeValueSet.attributeValues.length; v++) {
          if (commonAttribute.idWidgetAttribute === this.widgetInstance.attributeValueSet.attributeValues[v].idWidgetAttribute) {
            if (commonAttribute.name === "cardClass" || commonAttribute.name === "showTitle") {
              this.setValues(commonAttribute.name, [this.widgetInstance.attributeValueSet.attributeValues[v]]);
            }
          }
        }
      }
    }
  }

  getInitializedSubject(): Subject<boolean> {
    return this.initializedSubject;
  }

  setWidget(widget: Widget) {
    this.widget = widget;
  }

  render()  {
    this.rendered.next(true);
  }

  tryRender() {
    // Override
  }

  setValues(name: string, attributeValues: WidgetAttributeValue[]): boolean {
    if (isDevMode()) {
      console.debug("WidgetComponent.setValues: " + name);
    }

    if (!attributeValues || attributeValues.length === 0) {
      return false;
    } if (name === "cardClass") {
      this.cardClass = attributeValues[0].valueString;
      this.cardClassEvent.emit(this.cardClass);
    } else if (name === "showTitle") {
      this.showTitle = attributeValues[0].valueBoolean;
    }

    return false;
  }

  getWidgetChoices(name: string): Subject<WidgetAttributeChoice[]> {
    return undefined;
  }

  setWidgetInstance(widgetInstance: WidgetInstance) {
    this.widgetInstance = widgetInstance;
  }

  getWidgetInstance() {
    return this.widgetInstance;
  }

  getCardClass() {
    return this.cardClass;
  }

  getCardPaddingClass() {
    return this.cardPaddingClass;
  }

  getParentDimension(htmlElement: HTMLElement, type: string) {
    var x = 300;
    let parent: any = htmlElement.parentElement;
    while (parent) {
      if (parent.className.includes("widget-host")) {
        break;
      }
      parent = parent.parentElement;
    }

    if (parent) {
      if (type === "width") {
        x = parent.offsetWidth;
      } else {
        x = parent.offsetHeight;
      }
    }
    return (x <= 0) ? 300 : x;
  }

  getAttributeContainers(): Map<string, WidgetAttributeContainer> {
    if (this.widgetInstance) {
      if (isDevMode()) {
        console.debug("WidgetComponent.getAttributeContainers from " + this.widgetInstance.widget.name);
      }
      return this.widgetInstance.widget.attributeContainerMap;
    } else if (this.widget) {
      if (isDevMode()) {
        console.debug("WidgetComponent.getAttributeContainers from " + this.widget.name);
      }
      return this.widget.attributeContainerMap;
    } else {
      if (isDevMode()) {
        console.debug("WidgetComponent.getAttributeContainers from none");
      }
      return new Map<string, WidgetAttributeContainer>();
    }
  }

}
