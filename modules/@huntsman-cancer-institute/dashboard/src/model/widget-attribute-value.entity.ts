export class WidgetAttributeValue {
  public idWidgetAttributeValue: number;
  //public idWidgetAttributeValueSet: number;
  public idWidgetAttribute: number;
  public valueString: string;
  public valueBoolean: boolean;
  public valueDate: number;
  public valueInteger: number;
  public valueNumeric: number;

  deserialize(object): WidgetAttributeValue {
    this.idWidgetAttributeValue = object.idWidgetAttributeValue;
    //this.idWidgetAttributeValueSet = object.idWidgetAttributeValueSet;
    this.idWidgetAttribute = object.idWidgetAttribute;
    this.valueString = object.valueString;
    this.valueBoolean = object.valueBoolean;
    this.valueDate = object.valueDate;
    this.valueInteger = object.valueInteger;
    this.valueNumeric = object.valueNumeric;

    return this;
  }
}
