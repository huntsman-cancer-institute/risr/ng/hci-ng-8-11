import {WidgetAttributeChoice} from "./widget-attribute-choice.entity";

export class WidgetAttribute {
  public idWidgetAttribute: number;
  public idWidgetAttributeContainer: number;
  public name: string;
  public display: string;
  public dataType: string;
  public multiValue: boolean;
  public userEditable: boolean;
  public sortOrder: number;
  public defaultValue: string;
  public widgetAttributeChoices: WidgetAttributeChoice[];

  deserialize(object): WidgetAttribute {
    this.idWidgetAttribute = object.idWidgetAttribute;
    this.idWidgetAttributeContainer = object.idWidgetAttributeContainer;
    this.name = object.name;
    this.display = object.display;
    this.dataType = object.dataType;
    this.multiValue = object.multiValue;
    this.userEditable = object.userEditable;
    this.sortOrder = object.sortOrder;
    this.defaultValue = object.defaultValue;

    if (object.widgetAttributeChoices) {
      this.widgetAttributeChoices = [];
      for (var i = 0; i < object.widgetAttributeChoices.length; i++) {
        this.widgetAttributeChoices.push(new WidgetAttributeChoice().deserialize(object.widgetAttributeChoices[i]));
      }
    }

    return this;
  }

  deserializeArray(list: Object[]): WidgetAttribute[] {
    let attributes: WidgetAttribute[] = [];
    for (var i = 0; i < list.length; i++) {
      attributes.push(new WidgetAttribute().deserialize(list[i]));
    }
    return attributes;
  }

}
