import {WidgetInstance} from "./widget-instance.entity";

export class Dashboard {
  public idDashboard: number;
  public idUserProfile: number;
  public name: string;
  public sortOrder: number;
  public idDashboardType: number;
  public widgetInstances: WidgetInstance[] = [];

  deserialize(object): Dashboard {
    this.idDashboard = object.idDashboard;
    this.idUserProfile = object.idUserProfile;
    this.name = object.name;
    this.sortOrder = object.sortOrder;
    this.idDashboardType = object.idDashboardType;

    if (object.widgetInstances) {
      this.widgetInstances = [];
      for (var i = 0; i < object.widgetInstances.length; i++) {
        this.widgetInstances.push(new WidgetInstance().deserialize(object.widgetInstances[i]));
      }
    }

    return this;
  }

  deserializeArray(list: Object[]): Dashboard[] {
    let dashboards: Dashboard[] = [];
    for (var i = 0; i < list.length; i++) {
      dashboards.push(new Dashboard().deserialize(list[i]));
    }
    return dashboards;
  }

}
