export class WidgetCategory {
  public idWidgetCategory: number;
  public category: string;
  public icon: string;

  deserialize(object): WidgetCategory {
    this.idWidgetCategory = object.idWidgetCategory;
    this.category = object.category;
    this.icon = object.icon;

    return this;
  }

  deserializeArray(list: Object[]): WidgetCategory[] {
    let widgetCategories: WidgetCategory[] = [];
    for (var i = 0; i < list.length; i++) {
      widgetCategories.push(new WidgetCategory().deserialize(list[i]));
    }
    return widgetCategories;
  }
}
