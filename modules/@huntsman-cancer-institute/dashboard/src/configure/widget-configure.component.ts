/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, isDevMode} from "@angular/core";

import {Subscription} from "rxjs";

import {WidgetAttribute, WidgetAttributeContainer, WidgetInstance, Widget} from "../model/model";
import {DashboardService} from "../services/dashboard.service";
import {WidgetComponent} from "../widget/widget.component";

/**
 * @since 1.0.0
 */
@Component({
  selector: "widget-configure",
  template: `
    <ng-container *ngIf="widgetInstance && selectedAttributeContainer">
      <div class="attribute-container-header-parent">
        <ng-template ngFor let-attributeContainer let-i="index" [ngForOf]="attributeContainers | hasEditableAttributes">
          <div (click)="selectedAttributeContainer = attributeContainer; iSelected = i;"
               class="attribute-container-header"
               [ngClass]="selectedAttributeContainer.name === attributeContainer.name ? 'selected' : ''"
               [ngbTooltip]="attributeContainer.name">
            <span *ngIf="selectedAttributeContainer.name === attributeContainer.name">{{selectedAttributeContainer.name}}</span>
          </div>
        </ng-template>
      </div>
      <div class="d-flex flex-column flex-grow-1 attribute-container">
        <div class="d-flex flex-column flex-grow-1 y-auto">
          <ng-template ngFor let-attribute [ngForOf]="selectedAttributeContainer.attributes">
            <widget-attribute [attribute]="attribute"
                              [idWidgetAttributeValueSet]="widgetInstance.attributeValueSet.idWidgetAttributeValueSet"
                              [widgetComponent]="widgetComponent"
                              *ngIf="attribute.userEditable"></widget-attribute>
          </ng-template>
        </div>
      </div>
      <div *ngIf="attributeContainers.length === 0">
        There are no configuration options for this widget.
      </div>
    </ng-container>
  `,
  styles: [`
    
    .attribute-container-header-parent {
      border-bottom: black 2px solid;
      padding-bottom: 5px;
      margin-bottom: 5px;
    }
    
    .attribute-container-header {
      display: inline-block;
      width: 35px;
      background-color: #66aaff;
      border: black 1px solid;
      border-radius: 20px;
      padding: 4px;
      font-size: large;
      font-weight: bold;
      text-align: center;
      min-height: 35px;
      vertical-align: top;
    }
    
    .attribute-container-header.selected {
      width: 45%;
    }
    
    .attribute-container {
      overflow-y: auto;
      overflow-x: hidden;
    }
    
  `]
})
export class WidgetConfigureComponent {

  @HostBinding("class") classList: string = "outlet-column";

  public iSelected: number;
  public selectedAttributeContainer: WidgetAttributeContainer;
  public attributeContainers: WidgetAttributeContainer[] = [];

  public widgetInstance: WidgetInstance;
  private widgetComponent: WidgetComponent;
  private attributeContainerMap: Map<string, WidgetAttributeContainer>;

  private widgetComponentSubscription: Subscription;
  private attributeContainersSubscription: Subscription;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.dashboardService.configurationWidgetInstanceSubject.subscribe((widgetInstance: WidgetInstance) => {
      if (isDevMode()) {
        console.debug("WidgetConfigureComponent.configurationWidgetInstanceSubject.subscribe:");
        console.debug(widgetInstance);
      }

      if (!widgetInstance) {
        this.widgetInstance = undefined;
        this.widgetComponent = undefined;
        this.selectedAttributeContainer = undefined;
        this.iSelected = undefined;

		    this.unsubscribe();
      } else {
        this.widgetInstance = widgetInstance;

        this.widgetComponentSubscription = this.dashboardService.getWidgetComponentSubject(widgetInstance.idWidgetInstance)
            .subscribe((widgetComponent: WidgetComponent) => {
          if (isDevMode()) {
            console.debug("WidgetConfigureComponent.getWidgetComponentSubject.subscribe");
            console.debug(widgetComponent);
          }

          if (this.attributeContainersSubscription) {
            this.attributeContainersSubscription.unsubscribe();
          }

          this.widgetComponent = widgetComponent;
          if (this.widgetComponent) {
			      this.widgetComponent.config(undefined);

            this.attributeContainersSubscription = this.widgetComponent.attributeContainers.subscribe((attributeContainers: Map<string, WidgetAttributeContainer>) => {
              if (isDevMode()) {
                console.debug("WidgetConfigureComponent.attributeContainersSubscription");
                console.debug(attributeContainers);
              }

              if (attributeContainers) {
                this.attributeContainerMap = Widget.merge(attributeContainers, this.dashboardService.getCommonAttributes());
                this.generateAttributeContainers();
              } else {
                this.attributeContainerMap = this.dashboardService.getCommonAttributes();
                this.generateAttributeContainers();
              }
			      });
          }
        });
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe();
  }

  unsubscribe() {
    if (this.widgetComponentSubscription) {
      this.widgetComponentSubscription.unsubscribe();
    }
    if (this.attributeContainersSubscription) {
      this.attributeContainersSubscription.unsubscribe();
    }
  }

  /**
   * Though a map is used to override attributes easily, transfer back to an array for easy sorting and to make
   * the ngFor happy.
   */
  generateAttributeContainers() {
    if (isDevMode()) {
      console.debug("WidgetConfigureComponent.generateAttributeContainers: " + this.attributeContainerMap.size);
    }

    let tempContainers: WidgetAttributeContainer[] = [];
    for (let attributeContainer of this.attributeContainerMap.values()) {
      if (attributeContainer.hidden) {
        continue;
      }

      attributeContainer.attributes = [];
      for (let attribute of attributeContainer.attributeMap.values()) {
        /*if (attribute.userEditable) {
          attributeContainer.attributes.push(attribute);
        }*/
        attributeContainer.attributes.push(attribute);
      }
      attributeContainer.attributes.sort((a: WidgetAttribute, b: WidgetAttribute) => {
        if (a.sortOrder < b.sortOrder) {
          return -1;
        } else if (a.sortOrder > b.sortOrder) {
          return 1;
        } else {
          return 0;
        }
      });

      tempContainers.push(attributeContainer);
    }

    tempContainers.sort((a: WidgetAttributeContainer, b: WidgetAttributeContainer) => {
      if (a.sortOrder < b.sortOrder) {
        return -1;
      } else if (a.sortOrder > b.sortOrder) {
        return 1;
      } else {
        return 0;
      }
    });

    if (this.attributeContainers.length === 0 || !this.wacEquals(tempContainers, this.attributeContainers)) {
      this.attributeContainers = tempContainers;

      if (this.iSelected && this.iSelected < this.attributeContainers.length && this.selectedAttributeContainer.name === this.attributeContainers[this.iSelected].name) {
        this.selectedAttributeContainer = this.attributeContainers[this.iSelected];
      } else if (this.attributeContainers.length > 0) {
        this.selectedAttributeContainer = this.attributeContainers[0];
        this.iSelected = 0;
      } else {
        this.selectedAttributeContainer = undefined;
        this.iSelected = undefined;
      }
      this.dashboardService.setConfigurationAttributeContainers(this.attributeContainers);

      if (isDevMode()) {
        console.debug(this.attributeContainers);
      }
    }
  }

  wacEquals(a: WidgetAttributeContainer[], b: WidgetAttributeContainer[]): boolean {
    if (a.length !== b.length) {
      return false;
    } else {
      for (var i = 0; i < a.length; i++) {
        if (a[i].name !== b[i].name) {
          return false;
        } else if (a[i].attributes.length !== b[i].attributes.length) {
          return false;
        } else {
          for (var j = 0; j < a[i].attributes.length; j++) {
            if (a[i].attributes[j].name !== b[i].attributes[j].name) {
              return false;
            }
          }
        }
      }
    }
    return true;
  }
}
