/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A barrel file for the HCI ng dashboard package.
 *
 * @since 1.0.0
 */
export {DashboardModule} from "./src/dashboard.module";
export {DashboardComponent} from "./src/dashboard.component";
export {DashboardService, USER_PROFILE_ENDPOINT, DASHBOARD_TYPE} from "./src/services/dashboard.service";
export {DashboardGlobalService} from "./src/services/dashboard-global.service";
export {WidgetHostComponent} from "./src/widget/widget-host.component";
export {WidgetComponent} from "./src/widget/widget.component";

export {Dashboard} from "./src/model/dashboard.entity";
export {UserProfile} from "./src/model/user-profile.entity";
export {Widget} from "./src/model/widget.entity";
export {WidgetAttribute} from "./src/model/widget-attribute.entity";
export {WidgetAttributeChoice} from "./src/model/widget-attribute-choice.entity";
export {WidgetAttributeContainer} from "./src/model/widget-attribute-container.entity";
export {WidgetAttributeValue} from "./src/model/widget-attribute-value.entity";
export {WidgetAttributeValueSet} from "./src/model/widget-attribute-value-set.entity";
export {WidgetCategory} from "./src/model/widget-category.entity";
export {WidgetInstance} from "./src/model/widget-instance.entity";
