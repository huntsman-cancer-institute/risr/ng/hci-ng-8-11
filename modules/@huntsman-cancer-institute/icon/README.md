# Angular Icon Library

This is a collection of our custom icons.  These icons are designed to be used in the same fashion as font awesome 5.
They are loaded as a collection in the same way font awesome libraries are loaded.

## Usage

Icons are created in the same way as font awesome but with the hci group.

    <i class="hci fa-core fa-sm"></i>
    
One thing to note is that our icon "core" still needs to have the "fa-" prepended.
