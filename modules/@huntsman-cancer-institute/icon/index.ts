
var WINDOW = window;
var NAMESPACE_IDENTIFIER = "___FONT_AWESOME___";

var PRODUCTION = function () {
  try {
    return process.env.NODE_ENV === "production";
  } catch (e) {
    return false;
  }
}();

function bunker(fn) {
  try {
    fn();
  } catch (e) {
    if (!PRODUCTION) {
      throw e;
    }
  }
}

var w = WINDOW || {};

if (!w[NAMESPACE_IDENTIFIER]) {
  w[NAMESPACE_IDENTIFIER] = {};
}
if (!w[NAMESPACE_IDENTIFIER].styles) {
  w[NAMESPACE_IDENTIFIER].styles = {};
}
if (!w[NAMESPACE_IDENTIFIER].hooks) {
  w[NAMESPACE_IDENTIFIER].hooks = {};
}
if (!w[NAMESPACE_IDENTIFIER].shims) {
  w[NAMESPACE_IDENTIFIER].shims = [];
}

var namespace = w[NAMESPACE_IDENTIFIER];

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

function define(prefix, icons) {
  var normalized = Object.keys(icons).reduce(function (acc, iconName) {
    var icon = icons[iconName];
    var expanded = !!icon.icon;

    if (expanded) {
      acc[icon.iconName] = icon.icon;
    } else {
      acc[iconName] = icon;
    }
    return acc;
  }, {});

  if (typeof namespace.hooks.addPack === "function") {
    namespace.hooks.addPack(prefix, normalized);
  } else {
    namespace.styles[prefix] = _extends({}, namespace.styles[prefix] || {}, normalized);
  }

  /**
   * Font Awesome 4 used the prefix of `fa` for all icons. With the introduction
   * of new styles we needed to differentiate between them. Prefix `fa` is now an alias
   * for `fas` so we"ll easy the upgrade process for our users by automatically defining
   * this as well.
   */
  if (prefix === "hci") {
    define("fa", icons);
  }
}


import {hciAccountingUnits} from "./src/accounting-units";
import {hciAccounts} from "./src/accounts";
import {hciAdditionalDetailsSolid} from "./src/additional-details-solid";
import {hciAndOpenCaps} from "./src/and-open-caps";
import {hciAndOpenLower} from "./src/and-open-lower";
import {hciAndSolidCaps} from "./src/and-solid-caps";
import {hciAndSolidLower} from "./src/and-solid-lower";
import {hciAndWordCaps} from "./src/and-word-caps";
import {hciAndWordLower} from "./src/and-word-lower";
import {hciArrowLongDownOutline} from "./src/arrow-long-down-outline";
import {hciArrowLongLeftOutline} from "./src/arrow-long-left-outline";
import {hciArrowLongRightOutline} from "./src/arrow-long-right-outline";
import {hciArrowLongUpOutline} from "./src/arrow-long-up-outline";
import {hciAwsCube} from "./src/aws-cube";
import {hciBackArrowLeft} from "./src/back-arrow-left";
import {hciBackArrowRight} from "./src/back-arrow-right";
import {hciBiopsy} from "./src/biopsy";
import {hciBooleanYesNoReg} from "./src/boolean-yesno-reg";
import {hciBooleanYesNoSolid} from "./src/boolean-yesno-solid";
import {hciBooleanYesNoUnknownReg} from "./src/boolean-yesnounknown-reg";
import {hciBooleanYesNoUnknownSolid} from "./src/boolean-yesnounknown-solid";
import {hciBreadcrumbCapRightSolid} from "./src/breadcrumb-cap-right-solid";
import {hciBreadcrumbTailRightSolid} from "./src/breadcrumb-tail-right-solid";
import {hciBucket} from "./src/bucket";
import {hciCaretAngleLeftHollow} from "./src/caret-angle-left-hollow";
import {hciCaretAngleLeftShadow} from "./src/caret-angle-left-shadow";
import {hciCaretAngleRightHollow} from "./src/caret-angle-right-hollow";
import {hciCaretAngleRightShadow} from "./src/caret-angle-right-shadow";
import {hciCaretDownHollow} from "./src/caret-down-hollow";
import {hciCaretDownShadow} from "./src/caret-down-shadow";
import {hciCaretLeftHollow} from "./src/caret-left-hollow";
import {hciCaretLeftShadow} from "./src/caret-left-shadow";
import {hciCaretRightHollow} from "./src/caret-right-hollow";
import {hciCaretRightShadow} from "./src/caret-right-shadow";
import {hciCaretUpHollow} from "./src/caret-up-hollow";
import {hciCaretUpShadow} from "./src/caret-up-shadow";
import {hciCCRlogo} from "./src/ccr-logo";
import {hciClipboardRegular} from "./src/clipboard-regular";
import {hciConsentHexOpen} from "./src/consent-hex-open";
import {hciConsentSolid} from "./src/consent-solid";
import {hciCore} from "./src/core";
import {hciCourses} from "./src/courses";
import {hciCreate} from "./src/create";
import {hciCreates} from "./src/creates";
import {hciDateTimeSolid} from "./src/date-time-solid";
import {hciDesktopArrowUp} from "./src/desktop-arrow-up";
import {hciDiagnosis} from "./src/diagnosis";
import {hciCoreGradient} from "./src/core-gradient";
import {hciDictionaryHexOpen} from "./src/dictionary-hex-open";
import {hciDxRegular} from "./src/dx-regular";
import {hciEdwClipboard} from "./src/edw-clipboard";
import {hciEdwHexOpen} from "./src/edw-hex-open";
import {hciEventSolid} from "./src/event-solid";
import {hciFileCvs} from "./src/file-cvs";
import {hciFileData} from "./src/file-data";
import {hciFileObject} from "./src/file-object";
import {hciFileView} from "./src/file-view";
import {hciFilterByDateDown} from "./src/filter-bydate-down";
import {hciFilterByDateUp} from "./src/filter-bydate-up";
import {hciFolderAnalysis} from "./src/folder-analysis";
import {hciFolderExperiment} from "./src/folder-experiment";
import {hciFolderProject} from "./src/folder-project";
import {hciGnomexSolid} from "./src/gnomex-solid";
import {hciGridViewRegular} from "./src/grid-view-regular";
import {hciGridViewSolid} from "./src/grid-view-solid";
import {hciGrimDelete} from "./src/grim-delete";
import {hciGrimError} from "./src/grim-error";
import {hciGrimRisr} from "./src/grim-risr";
import {hciGripLinesLongHorizontal} from "./src/grip-lineslong-horizontal";
import {hciGripLinesLongVertical} from "./src/grip-lineslong-vertical";
import {hciHorizontalRule} from "./src/horizontal-rule";
import {hciImportHex} from "./src/import-hex";
import {hciIqHexOpen} from "./src/iq-hex-open";
import {hciLab} from "./src/lab";
import {hciLogoEdwUhealthHex} from "./src/logo-edw-uhealth-hex";
import {hciLogoHCI} from "./src/logo-hci";
import {hciLogoRad} from "./src/logo-rad";
import {hciLogoRISR} from "./src/logo-risr-2022";
import {hciLogoRISRsolid} from "./src/logo-risr-2022solid";
import {hciLogoRSR} from "./src/logo-rsr";
import {hciLogoUpdb} from "./src/logo-updb";
import {hciMember} from "./src/member";
import {hciMetastasis} from "./src/metastasis";
import {hciMicroscopeRegular} from "./src/microscope-regular";
import {hciNlpLettersReg} from "./src/nlp-letters-reg";
import {hciObjectExplorerSolid} from "./src/object-explorer-solid";
import {hciObjectExplorer} from "./src/object-explorer";
import {hciOrOpenCaps} from "./src/or-open-caps";
import {hciOrOpenLower} from "./src/or-open-lower";
import {hciOrSolidCaps} from "./src/or-solid-caps";
import {hciOrSolidLower} from "./src/or-solid-lower";
import {hciOrWordCaps} from "./src/or-word-caps";
import {hciOrWordLower} from "./src/or-word-lower";
import {hciOverviewDx} from "./src/overview-dx";
import {hciOverviewStage} from "./src/overview-stage";
import {hciOverviewTx} from "./src/overview-tx";
import {hciPathologySolid} from "./src/pathology-solid";
import {hciPhiView} from "./src/phi-view";
import {hciPhiViewIdent} from "./src/phi-view-ident";
import {hciPhiViewLimited} from "./src/phi-view-limited";
import {hciProject} from "./src/project";
import {hciProjects} from "./src/projects";
import {hciPurchaseCard} from "./src/purchase-card";
import {hciPurchaseGroups} from "./src/purchase-groups";
import {hciPurchaseGroupUser} from "./src/purchase-group-user";
import {hciPurchasingAdmin} from "./src/purchasing-admin";
import {hciPushpinSolid} from "./src/pushpin-solid";
import {hciRadiationRegular} from "./src/radiation-regular";
import {hciRadiologyRegular} from "./src/radiology-regular";
import {hciRecent} from "./src/recent";
import {hciRecentProjects} from "./src/recent-projects";
import {hciRecurrence} from "./src/recurrence";
import {hciRelatedSamplesHex} from "./src/related-samples-hex";
import {hciRelatedSamplesSolid} from "./src/related-samples-solid";
import {hciSampleAliases} from "./src/sample-aliases";
import {hciSampleHexOpen} from "./src/sample-hex-open";
import {hciSettings} from "./src/settings";
import {hciShare} from "./src/share";
import {hciSiteSpecific} from "./src/site-specific";
import {hciStage} from "./src/stage";
import {hciStagingClinical} from "./src/staging-clinical";
import {hciStagingDetails} from "./src/staging-details";
import {hciStagingDominant} from "./src/staging-dominant";
import {hciStagingPathologic} from "./src/staging-pathologic";
import {hciStorageContainer} from "./src/storage-container";
import {hciStudyHexOpen} from "./src/study-hex-open";
import {hciSubAccount} from "./src/sub-account";
import {hciSubAccountSet} from "./src/sub-account-set";
import {hciSubjectHexOpen} from "./src/subject-hex-open";
import {hciSubjectSolid} from "./src/subject-solid";
import {hciSurgery} from "./src/surgery";
import {hciTabsetAddSolid} from "./src/tabset-add-solid";
import {hciTabsetSolid} from "./src/tabset-solid";
import {hciTesttubeRegular} from "./src/testtube-regular";
import {hciTextViewRegular} from "./src/text-view-regular";
import {hciTextViewSolid} from "./src/text-view-solid";
import {hciText} from "./src/text";
import {hciTherapyChemo} from "./src/therapy-chemo";
import {hciTherapyHormone} from "./src/therapy-hormone";
import {hciTherapyImmuno} from "./src/therapy-immuno";
import {hciTherapyRadiation} from "./src/therapy-radiation";
import {hciTherapySystemic} from "./src/therapy-systemic";
import {hciTrashToss} from "./src/trash-toss";
import {hciTreatment} from "./src/treatment";
import {hciTimeRegular} from "./src/time-regular";
import {hciTumorRegistryHex} from "./src/tumor-registry-hex";
import {hciTumorRegistry} from "./src/tumor-registry";
import {hciUHealth} from "./src/uhealth";
import {hciUnArchiveRegular} from "./src/un-archive-regular";
import {hciUnArchiveSolid} from "./src/un-archive-solid";
import {hciVendor} from "./src/vendor";
import {hciVersions} from "./src/versions";

var iconList = {
  hciAccountingUnits: hciAccountingUnits,
  hciAccounts: hciAccounts,
  hciAdditionalDetailsSolid: hciAdditionalDetailsSolid,
  hciAndOpenCaps: hciAndOpenCaps,
  hciAndOpenLower: hciAndOpenLower,
  hciAndSolidCaps: hciAndSolidCaps,
  hciAndSolidLower: hciAndSolidLower,
  hciAndWordCaps: hciAndWordCaps,
  hciAndWordLower: hciAndWordLower,
  hciArrowLongDownOutline: hciArrowLongDownOutline,
  hciArrowLongLeftOutline: hciArrowLongLeftOutline,
  hciArrowLongRightOutline: hciArrowLongRightOutline,
  hciArrowLongUpOutline: hciArrowLongUpOutline,
  hciAwsCube: hciAwsCube,
  hciBackArrowLeft: hciBackArrowLeft,
  hciBackArrowRight: hciBackArrowRight,
  hciBiopsy: hciBiopsy,
  hciBooleanYesNoReg: hciBooleanYesNoReg,
  hciBooleanYesNoSolid: hciBooleanYesNoSolid,
  hciBooleanYesNoUnknownReg: hciBooleanYesNoUnknownReg,
  hciBooleanYesNoUnknownSolid: hciBooleanYesNoUnknownSolid,
  hciBreadcrumbCapRightSolid: hciBreadcrumbCapRightSolid,
  hciBreadcrumbTailRightSolid: hciBreadcrumbTailRightSolid,
  hciBucket: hciBucket,
  hciCaretAngleLeftHollow: hciCaretAngleLeftHollow,
  hciCaretAngleLeftShadow: hciCaretAngleLeftShadow,
  hciCaretAngleRightHollow: hciCaretAngleRightHollow,
  hciCaretAngleRightShadow: hciCaretAngleRightShadow,
  hciCaretDownHollow: hciCaretDownHollow,
  hciCaretDownShadow: hciCaretDownShadow,
  hciCaretLeftHollow: hciCaretLeftHollow,
  hciCaretLeftShadow: hciCaretLeftShadow,
  hciCaretRightHollow: hciCaretRightHollow,
  hciCaretRightShadow: hciCaretRightShadow,
  hciCaretUpHollow: hciCaretUpHollow,
  hciCaretUpShadow: hciCaretUpShadow,
  hciCCRlogo: hciCCRlogo,
  hciClipboardRegular: hciClipboardRegular,
  hciConsentHexOpen: hciConsentHexOpen,
  hciConsentSolid: hciConsentSolid,
  hciCore: hciCore,
  hciCourses: hciCourses,
  hciCreate: hciCreate,
  hciCreates: hciCreates,
  hciDateTimeSolid: hciDateTimeSolid,
  hciDesktopArrowUp: hciDesktopArrowUp,
  hciDiagnosis: hciDiagnosis,
  hciCoreGradient: hciCoreGradient,
  hciDictionaryHexOpen: hciDictionaryHexOpen,
  hciDxRegular: hciDxRegular,
  hciEdwClipboard: hciEdwClipboard,
  hciEdwHexOpen: hciEdwHexOpen,
  hciEventSolid: hciEventSolid,
  hciFileCvs: hciFileCvs,
  hciFileData: hciFileData,
  hciFileObject: hciFileObject,
  hciFileView: hciFileView,
  hciFilterByDateDown: hciFilterByDateDown,
  hciFilterByDateUp: hciFilterByDateUp,
  hciFolderAnalysis: hciFolderAnalysis,
  hciFolderExperiment: hciFolderExperiment,
  hciFolderProject: hciFolderProject,
  hciGnomexSolid: hciGnomexSolid,
  hciGridViewRegular: hciGridViewRegular,
  hciGridViewSolid: hciGridViewSolid,
  hciGrimDelete: hciGrimDelete,
  hciGrimError: hciGrimError,
  hciGrimRisr: hciGrimRisr,
  hciGripLinesLongHorizontal: hciGripLinesLongHorizontal,
  hciGripLinesLongVertical: hciGripLinesLongVertical,
  hciHorizontalRule: hciHorizontalRule,
  hciImportHex: hciImportHex,  
  hciIqHexOpen: hciIqHexOpen,
  hciLab: hciLab,
  hciLogoEdwUhealthHex: hciLogoEdwUhealthHex,
  hciLogoHCI: hciLogoHCI,
  hciLogoRad: hciLogoRad,
  hciLogoRISR: hciLogoRISR,
  hciLogoRISRsolid: hciLogoRISRsolid,
  hciLogoRSR: hciLogoRSR,
  hciLogoUpdb: hciLogoUpdb,
  hciMember: hciMember,
  hciMetastasis: hciMetastasis,
  hciMicroscopeRegular: hciMicroscopeRegular,
  hciNlpLettersReg: hciNlpLettersReg,
  hciObjectExplorerSolid: hciObjectExplorerSolid,
  hciObjectExplorer: hciObjectExplorer,
  hciOrOpenCaps: hciOrOpenCaps,
  hciOrOpenLower: hciOrOpenLower,
  hciOrSolidCaps: hciOrSolidCaps,
  hciOrSolidLower: hciOrSolidLower,
  hciOrWordCaps: hciOrWordCaps,
  hciOrWordLower: hciOrWordLower,
  hciOverviewDx: hciOverviewDx,
  hciOverviewStage: hciOverviewStage,
  hciOverviewTx: hciOverviewTx,
  hciPathologySolid: hciPathologySolid,
  hciPhiView: hciPhiView,
  hciPhiViewIdent: hciPhiViewIdent,
  hciPhiViewLimited: hciPhiViewLimited,
  hciProject: hciProject,
  hciProjects: hciProjects,
  hciPurchaseCard: hciPurchaseCard,
  hciPurchaseGroups: hciPurchaseGroups,
  hciPurchaseGroupUser: hciPurchaseGroupUser,
  hciPurchasingAdmin: hciPurchasingAdmin,
  hciPushpinSolid: hciPushpinSolid,
  hciRadiationRegular: hciRadiationRegular,
  hciRadiologyRegular: hciRadiologyRegular,
  hciRecent: hciRecent,
  hciRecentProjects: hciRecentProjects,
  hciRecurrence: hciRecurrence,
  hciRelatedSamplesHex: hciRelatedSamplesHex,
  hciRelatedSamplesSolid: hciRelatedSamplesSolid,
  hciSampleAliases: hciSampleAliases,
  hciSampleHexOpen: hciSampleHexOpen,
  hciSettings: hciSettings,
  hciShare: hciShare,
  hciSiteSpecific: hciSiteSpecific,
  hciStage: hciStage,
  hciStagingClinical: hciStagingClinical,
  hciStagingDetails: hciStagingDetails,
  hciStagingDominant: hciStagingDominant,
  hciStagingPathologic: hciStagingPathologic,
  hciStorageContainer: hciStorageContainer,
  hciStudyHexOpen: hciStudyHexOpen,
  hciSubAccount: hciSubAccount,
  hciSubAccountSet: hciSubAccountSet,
  hciSubjectHexOpen: hciSubjectHexOpen,
  hciSubjectSolid: hciSubjectSolid,
  hciSurgery: hciSurgery,
  hciTabsetAddSolid: hciTabsetAddSolid,
  hciTabsetSolid: hciTabsetSolid,
  hciTesttubeRegular: hciTesttubeRegular,
  hciTextViewRegular: hciTextViewRegular,
  hciTextViewSolid: hciTextViewSolid,
  hciText: hciText,
  hciTherapyChemo: hciTherapyChemo,
  hciTherapyHormone: hciTherapyHormone,
  hciTherapyImmuno: hciTherapyImmuno,
  hciTherapyRadiation: hciTherapyRadiation,
  hciTherapySystemic: hciTherapySystemic,
  hciTrashToss: hciTrashToss,
  hciTreatment: hciTreatment,
  hciTimeRegular: hciTimeRegular,
  hciTumorRegistryHex: hciTumorRegistryHex,
  hciTumorRegistry: hciTumorRegistry,
  hciUHealth: hciUHealth,
  hciUnArchiveRegular: hciUnArchiveRegular,
  hciUnArchiveSolid: hciUnArchiveSolid,
  hciVendor: hciVendor,
  hciVersions: hciVersions,
};

bunker(function () {
  define("hci", iconList);
});
