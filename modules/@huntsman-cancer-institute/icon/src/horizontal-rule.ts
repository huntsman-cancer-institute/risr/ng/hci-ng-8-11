export const hciHorizontalRule = {
    prefix: "hci",
    iconName: "horizontal-rule",
    icon: [512, 512, [], "f378", "M493 237H19c-10.73 0-19 8.74-19 19s8.32 19 19 19h474c10.49 0 19-8.51 19-19s-8.51-19-19-19z"]
};