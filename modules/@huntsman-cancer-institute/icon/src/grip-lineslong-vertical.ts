export const hciGripLinesLongVertical = {
  prefix: "hci",
  iconName: "grip-lineslong-vertical",
  icon: [512, 512, [], "f204", "M512 276v8c0 2.2-1.8 4-4 4H4c-2.2 0-4-1.8-4-4v-8c0-2.2 1.8-4 4-4h504c2.2 0 4 1.8 4 4zm0-48v8c0 2.2-1.8 4-4 4H4c-2.2 0-4-1.8-4-4v-8c0-2.2 1.8-4 4-4h504c2.2 0 4 1.8 4 4z"]
};
