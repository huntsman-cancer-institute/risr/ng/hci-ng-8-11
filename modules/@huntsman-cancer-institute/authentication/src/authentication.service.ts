/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Injectable, InjectionToken, Inject, Optional, isDevMode} from "@angular/core";
import {LocationStrategy} from "@angular/common";
import {Router} from "@angular/router";
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from "@angular/common/http";

import {interval, Observable, BehaviorSubject, Subscription, throwError, of} from "rxjs";
import {catchError, first, map} from "rxjs/operators";
import {JwtHelperService} from "@auth0/angular-jwt";

import {AuthenticationProvider} from "./authentication.provider";
import { CoolLocalStorage } from "@angular-cool/storage";

/**
 * The token used for injection of the server side endpoint for the currently authenticated subject.
 *
 * @type {InjectionToken}
 */
export let AUTHENTICATION_SERVER_URL = new InjectionToken<string>("authentication_server_rest_api");
export let AUTHENTICATION_LOGOUT_PATH = new InjectionToken<string>("authentication_logout_path");
export let AUTHENTICATION_DIRECT_ENDPOINT = new InjectionToken<string>("authentication_direct_endpoint");
export let AUTHENTICATION_TOKEN_ENDPOINT = new InjectionToken<string>("authentication_token_endpoint");
export let AUTHENTICATION_ROUTE = new InjectionToken<string>("authentication_route");
export let AUTHENTICATION_MAX_INACTIVITY_MINUTES = new InjectionToken<number>("authentication_max_inactivity");
export let AUTHENTICATION_USER_COUNTDOWN_SECONDS = new InjectionToken<number>("authentication_user_countdown_seconds");
export let AUTHENTICATION_IDP_INACTIVITY_MINUTES = new InjectionToken<number>("authentication_idp_inactivity_minutes");

/**
 * @since 1.0.0
 */
@Injectable()
export class AuthenticationService {

  /**
   * The generic error message used when a server error is thrown without a status.
   *
   * @type {string}
   */
  public static GENERIC_ERR_MSG: string = "Server error";

  private static CONTENT_TYPE: string = "Content-Type";
  private static SEC_GOV_CLASS_HEADER: string = "SecurityGovernorClass";
  private static SEC_GOV_ID_HEADER: string = "SecurityGovernorId";
  private static DEIDENT_HEADER: string = "DeidentifiedContext";
  private static LIMITED_HEADER: string = "LimitedContext";

  public userCountdownSeconds: number = 60;
  public idpInactivityMinutes: number = 5;

  public contentType: string = "application/json";
  public securityGovernorClass: string;
  public securityGovernorId: number;
  public limitedContext: boolean = false;
  public deidentifiedContext: boolean = false;

  private maxViewPermission: BehaviorSubject<"view" | "viewident" | "viewlimited"> = new BehaviorSubject<"view" | "viewident" | "viewlimited">("viewident");
  private _isAuthenticatedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private _userIsAboutToTimeOut: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private _redirectUrl: string;
  private _refreshSubscription: Subscription;
  private _lastUserInteraction: Date;
  private _maxInactivityMinutes: number = 120;

  private baseUrl: string;
  private contextRoot: string = "";

  constructor(private _http: HttpClient,
              private _router: Router,
              private _localStorageService: CoolLocalStorage,
              private _jwtHelper: JwtHelperService,
              private authenticationProvider: AuthenticationProvider,
              @Inject(AUTHENTICATION_ROUTE) private _authenticationRoute: string,
              @Inject(AUTHENTICATION_LOGOUT_PATH) private _logoutPath: string,
              @Inject(AUTHENTICATION_TOKEN_ENDPOINT) private _tokenEndpoint: string,
              @Optional() @Inject(AUTHENTICATION_SERVER_URL) private _serverUrl: string,
              @Optional() @Inject(AUTHENTICATION_DIRECT_ENDPOINT) private _directEndpoint: string,
              @Optional() @Inject(AUTHENTICATION_MAX_INACTIVITY_MINUTES) private _maxInactivity: number,
              @Optional() @Inject(AUTHENTICATION_USER_COUNTDOWN_SECONDS) private _userCountdownSeconds: number,
              @Optional() @Inject(AUTHENTICATION_IDP_INACTIVITY_MINUTES) private _idpInactivityMinutes: number,
              @Optional() @Inject(LocationStrategy) private locationStrategy: LocationStrategy) {
    if (isDevMode()) {
      console.debug("window.location.href: " + window.location.href);
    }

    if (window.location) {
      let parts: string[] = window.location.href.split("/");
      this.baseUrl = parts[0] + "//" + parts[2];
      if (parts.length > 3) {
        this.contextRoot = parts[3];
      }
    }

    if (this._localStorageService.getItem("maxViewPermission")) {
      this.maxViewPermission.next(<"view" | "viewident" | "viewlimited">this._localStorageService.getItem("maxViewPermission"));
    }

    if (_maxInactivity) {
      this._maxInactivityMinutes = _maxInactivity;
    }

    if (_userCountdownSeconds) {
      this.userCountdownSeconds = _userCountdownSeconds;
    }

    if (_idpInactivityMinutes) {
      this.idpInactivityMinutes = _idpInactivityMinutes;
    }

    this.hasValidConfig();

    //There could be a non-expired token in local storage.
    let token: string = this.authenticationProvider.authToken;
    this.storeToken(token);
  }

  getBaseUrl(): string {
    return (this.baseUrl) ? this.baseUrl : "";
  }

  getContextRoot(): string {
    return this.contextRoot;
  }

  getHeaders(req: HttpRequest<any>): HttpHeaders {
    let headers: HttpHeaders = req.headers;

    //Don't set content type if already set
    if (!req.headers.get(AuthenticationService.CONTENT_TYPE)) {
      headers = headers.set(AuthenticationService.CONTENT_TYPE, this.contentType.toString());
    }

    if (headers.get(AuthenticationService.SEC_GOV_CLASS_HEADER) === "") {
      headers = headers.delete(AuthenticationService.SEC_GOV_CLASS_HEADER);
    } else if (this.securityGovernorClass && !headers.get(AuthenticationService.SEC_GOV_CLASS_HEADER)) {
      headers = headers.set(AuthenticationService.SEC_GOV_CLASS_HEADER, this.securityGovernorClass);
    }

    if (headers.get(AuthenticationService.SEC_GOV_ID_HEADER) === "") {
      headers = headers.delete(AuthenticationService.SEC_GOV_ID_HEADER);
    } else if (this.securityGovernorId && !headers.get(AuthenticationService.SEC_GOV_ID_HEADER)) {
      headers = headers.set(AuthenticationService.SEC_GOV_ID_HEADER, this.securityGovernorId.toString());
    }

    headers = headers.set(AuthenticationService.DEIDENT_HEADER, this.deidentifiedContext.toString());
    headers = headers.set(AuthenticationService.LIMITED_HEADER, this.limitedContext.toString());

    return headers;
  }

  get authenticationTokenKey(): string {
    return this.authenticationProvider.authenticationTokenKey;
  }

  get authToken(): string {
    return this.authenticationProvider.authToken;
  }

  public updateUserActivity(): void {
    if (this._isAuthenticatedSubject.value) {
      this._lastUserInteraction = new Date();
      this._userIsAboutToTimeOut.next(false);
    }
  }

  /**
   * A mutator for identifying the clients original request location. Setting this value will influence the end location
   * navigated to by {@link #navigateToPath}.
   *
   * @param redirectUrl location of the users request before authentication
   */
  set redirectUrl(redirectUrl: string) {
    this._redirectUrl = redirectUrl;
  }

  get redirectUrl() {
    return this._redirectUrl;
  }

  requestAccessToken(redirectOnSuccess: boolean): void {

    this._http.get(this.tokenLocation(), {withCredentials: true})
      .subscribe(
        (response: any) => {
          this.storeToken(response.auth_token);
          if (redirectOnSuccess) {
            this.proceedIfAuthenticated();
          }
        },
        (error) => {
          //Token refresh failed.
          this.logout(true);
        }
      );
  }

  /**
   * Verifies whether or not a current user session exists.
   *
   * @returns {Observable<boolean>} evaluates to true if the user is authenticated, false otherwise.
   */
  isAuthenticated(): Observable<boolean> {
    return this._isAuthenticatedSubject.asObservable();
  }

  isAboutToTimeOut(): Observable<boolean> {
    return this._userIsAboutToTimeOut.asObservable();
  }

  getTimeoutStart(): number {
    if (this._lastUserInteraction) {
      return this._lastUserInteraction.valueOf() + (((this._maxInactivityMinutes * 60) - this.userCountdownSeconds) * 1000);
    }
  }

  tokenLocation(): string {
    if (this._serverUrl) {
      return this._serverUrl + this._tokenEndpoint;
    } else {
      return this._tokenEndpoint;
    }
  }

  directLoginLocation(): string {
    if (this._serverUrl) {
      return this._serverUrl + this._directEndpoint;
    } else {
      return this._directEndpoint;
    }
  }

  logoutLocation(): string {
    if (this._serverUrl) {
      return this._serverUrl + this._logoutPath;
    } else {
      return this._logoutPath;
    }
  }

  /**
   * A function to authenticated the user with the provided credentials. Failure results in an error that describes the
   * server response (status and status message) and should be actionable by the client application.
   *
   * @param username of the authenticating user to verify
   * @param password of the authenticating user to verify
   * @returns {Observable<R>} describing the result of the login action, true or an error
   */
  login(_username: string, _password: string): Observable<boolean> {
    return this._http.post(
      this.directLoginLocation(),
      {username: _username, password: _password},
      {observe: "response"}
    ).pipe(map((resp: HttpResponse<any>) => {
      if (resp.status === 201) {
        return true;
      } else {
        throw new Error("Authentication failed. " + resp.status + ": " + resp.statusText);
      }
    }),
      catchError(this.handleError));
  }


  clearLogin(): Observable<Response> {
    //Front-end logout
    try {
      this._localStorageService.removeItem(this.authenticationProvider.authenticationTokenKey);
      this.unsubscribeFromTokenRefresh();
      this._isAuthenticatedSubject.next(false);
      this._userIsAboutToTimeOut.next(false);
    } catch (Error) {
    }

    //Back-end logout
    let headers = new HttpHeaders().set(AuthenticationService.CONTENT_TYPE, "text/plain");
    return <Observable<Response>>this._http.get(this.logoutLocation(), {headers: headers});
  }

  /**
   * A function to signal the termination of the current session. Invoking this function will clean up any relevant state
   * related to the last active session.
   */
  logout(keepCurrentRoute: boolean = false): void {
    //Prevent logout if already on authentication route. Doing otherwise screws up SAML
    if (! this._router.routerState || this._router.routerState.snapshot.url !== this._authenticationRoute) {
      this._redirectUrl = (keepCurrentRoute && this._router.routerState && this._router.routerState.snapshot) ? this._router.routerState.snapshot.url : "";

      if (this._redirectUrl.startsWith("/")) {
        this._redirectUrl = this._redirectUrl.substring(1);
      }

      this.clearLogin().subscribe(
        (response) => {
          window.location.replace(this._redirectUrl);
          },
        (error) => {
          window.location.replace(this._redirectUrl);
        }
      );
    }
  }

  storeToken(token: string): void {
    let valid = this.validateToken(token);

    // unsubscribe from refesh before we decide wether to resubscribe
    this.unsubscribeFromTokenRefresh();

    if (valid) {
      this._localStorageService.setItem(this.authenticationProvider.authenticationTokenKey, token);
      this.subscribeToTokenRefresh(token);

      //Change the BehaviorSubject if the user was not previously authenticated.
      //Since other code may be subscribing to this observable, we don't want to cause new events to fire if just refreshing the JWT.
      if (! this._isAuthenticatedSubject.value) {
        this._isAuthenticatedSubject.next(true);
      }
    } else {
      this._localStorageService.removeItem(this.authenticationProvider.authenticationTokenKey);
      this._isAuthenticatedSubject.next(false);
    }
  }

  proceedIfAuthenticated(): boolean {
    if (isDevMode()) {
      console.debug("AuthenticationService.proceedIfAuthenticated: " + this._redirectUrl);
    }

    if (this._isAuthenticatedSubject.value) {
      //Login counts as user activity, too
      this.updateUserActivity();

      if (this._redirectUrl && this._redirectUrl && this._redirectUrl !== "") {
        this._router.navigateByUrl(this._redirectUrl);
      } else {
        this._router.navigate([""]);
      }

      return true;
    } else {
      return false;
    }
  }

  validateToken(token: string): boolean {
    return (token && !this._jwtHelper.isTokenExpired(token));
  }

  subscribeToTokenRefresh(token: any): void {
    let exp = this._jwtHelper.getTokenExpirationDate(token);

    // Use a timer to periodically check timeouts
    this._refreshSubscription = interval(1000)
      .subscribe(() => {

        // If a tab is inactive we can't know if our timer is accurate
        // so when the interval hits check against timestamps
        if (this._isAuthenticatedSubject.value && Date.now() > this.getTimeoutStart()) {
          //Don't update the subject more than once! Doing so initializes more than one countdown timer!
          if (this._userIsAboutToTimeOut.getValue() !== true) {
            this._userIsAboutToTimeOut.next(true);
          }
        }

        // check for refresh token
        let msToExpiry = (exp.valueOf() - new Date().valueOf());

        // Refresh 60 seconds before expiry
        if (msToExpiry <= 60000) {
          this.refreshTokenIfUserIsActive();
        }
      });
  }

  unsubscribeFromTokenRefresh(): void {
    if (this._refreshSubscription && ! this._refreshSubscription.closed) {
      this._refreshSubscription.unsubscribe();
    }
  }

  getMaxViewPermission(): "view" | "viewident" | "viewlimited" {
    return this.maxViewPermission.getValue();
  }

  getMaxViewPermissionSubject(): BehaviorSubject<"view" | "viewident" | "viewlimited"> {
    return this.maxViewPermission;
  }

  setMaxViewPermission(maxViewPermission: "view" | "viewident" | "viewlimited"): void {
    this._localStorageService.setItem("maxViewPermission", maxViewPermission);
    this.maxViewPermission.next(maxViewPermission);
  }

  private refreshTokenIfUserIsActive(): void {
    //Only refresh if the user has been active
    if (this._lastUserInteraction && ((new Date().valueOf() - this._lastUserInteraction.valueOf()) <= (this._maxInactivityMinutes * 60 * 1000))) {
      this.requestAccessToken(false);
    }
  }

  private hasValidConfig(): void {
    if (this._tokenEndpoint == null && (this._serverUrl === null || this._logoutPath === null)) {
      throw new Error("BUG ALERT! Invalid AuthenticationService configuration. No valid configuration for authentication endpoint(s).");
    }
    if (this._localStorageService === null || this.authenticationProvider.authenticationTokenKey === null) {
      throw new Error("BUG ALERT! Invalid AuthenticationService configuration. No valid configuration for local storage");
    }
  }

  private handleError(error: any) : Observable<never> {
    let errMsg = (error.message) ? error.message : AuthenticationService.GENERIC_ERR_MSG;
    return throwError(errMsg);
  }
}
