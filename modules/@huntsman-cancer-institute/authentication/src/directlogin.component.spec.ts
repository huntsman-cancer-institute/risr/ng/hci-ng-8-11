/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */

import {TestBed, async, ComponentFixture} from "@angular/core/testing";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {Router} from "@angular/router";
import {By} from "@angular/platform-browser";

import {} from "jasmine";
import {Observable, of} from "rxjs";

import {DirectLoginComponent} from "./directlogin.component";
import {
  AuthenticationService, AUTHENTICATION_LOGOUT_PATH, AUTHENTICATION_DIRECT_ENDPOINT, AUTHENTICATION_TOKEN_ENDPOINT, AUTHENTICATION_ROUTE
} from "./authentication.service";
import {AUTHENTICATION_TOKEN_KEY} from "./authentication.provider";
import {AuthenticationModule} from "./authentication.module";
import {CoolLocalStorage} from "@angular-cool/storage";

let fixture: ComponentFixture<DirectLoginComponent>;

class MockRouter {
  navigate(config: any) {
    return;
  }
}

class MockCoolLocalStorage {
  getItem(key: string) {
    return null;
  }

  removeItem(key: string) {
    return;
  }
}

describe("LoginComponent Tests", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        AuthenticationModule.forRoot()
      ],
      providers: [
        {provide: Router, useValue: new MockRouter()},
        {provide: AUTHENTICATION_LOGOUT_PATH, useValue: "https://localhost:8080/auth/logout"},
        {provide: AUTHENTICATION_DIRECT_ENDPOINT, useValue: "https://localhost:8080/core/api/user/user-session/active"},
        {provide: AUTHENTICATION_TOKEN_ENDPOINT, useValue: "https://localhost:8080/core/api/token"},
        {provide: AUTHENTICATION_ROUTE, useValue: "/authentication"},
        {provide: AUTHENTICATION_TOKEN_KEY, useValue: "jwt_token"},
        {provide: CoolLocalStorage, useValue: new MockCoolLocalStorage()}
      ]
    });
    fixture = TestBed.createComponent(DirectLoginComponent);
  });

  it("Should call Authenticaiton Service login() and then requestAccessToken() on successful login.", async(() => {
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      let authenticationService: AuthenticationService = TestBed.get(AuthenticationService);
      let loginSpy = spyOn(authenticationService, "login").and.returnValue(of(true));
      let requestAccessTokenSpy = spyOn(authenticationService, "requestAccessToken");

      let password = fixture.debugElement.query(By.css("#password")).nativeElement;
      let username = fixture.debugElement.query(By.css("#username")).nativeElement;
      let button = fixture.debugElement.query(By.css("#hci-login-form-submit-button")).nativeElement;

      password.value = "goodPassword";
      username.value = "someUser";

      password.dispatchEvent(new Event("input"));
      username.dispatchEvent(new Event("input"));

      fixture.detectChanges();

      button.click();

      fixture.detectChanges();

      let loginError = fixture.debugElement.query(By.css("#hci-login-error"));

      expect(loginSpy).toHaveBeenCalledWith(username.value, password.value);
      expect(loginError).toBeNull();
      expect(requestAccessTokenSpy).toHaveBeenCalledTimes(1);
    });
  }));

  it("Should call Authentication Service login() and then display an error on unsuccessful login.", async(() => {
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      let userService: AuthenticationService = TestBed.get(AuthenticationService);
      let loginSpy = spyOn(userService, "login").and.returnValue(Observable.throw("Not a chance."));
      let requestAccessTokenSpy = spyOn(userService, "requestAccessToken");

      let password = fixture.debugElement.query(By.css("#password")).nativeElement;
      let username = fixture.debugElement.query(By.css("#username")).nativeElement;
      let button = fixture.debugElement.query(By.css("#hci-login-form-submit-button")).nativeElement;

      password.value = "badPassword";
      username.value = "someUser";

      password.dispatchEvent(new Event("input"));
      username.dispatchEvent(new Event("input"));

      fixture.detectChanges();

      button.click();

      fixture.detectChanges();

      let loginError = fixture.debugElement.query(By.css("#hci-login-error")).nativeElement;

      expect(loginSpy).toHaveBeenCalledWith(username.value, password.value);
      expect(loginError).toBeDefined();
      expect(requestAccessTokenSpy).toHaveBeenCalledTimes(0);
    });
  }));

  it("Should have a disabled login button if username is empty", async(() => {
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      let password = fixture.debugElement.query(By.css("#password")).nativeElement;
      let button = fixture.debugElement.query(By.css("#hci-login-form-submit-button")).nativeElement;

      password.value = "badPassword";

      password.dispatchEvent(new Event("input"));

      fixture.detectChanges();

      expect(button.disabled).toBeTruthy();
    });
  }));

  it("Should have a disabled login button if password is empty", async(() => {
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      let username = fixture.debugElement.query(By.css("#username")).nativeElement;
      let button = fixture.debugElement.query(By.css("#hci-login-form-submit-button")).nativeElement;

      username.value = "foo";

      username.dispatchEvent(new Event("input"));

      fixture.detectChanges();

      expect(button.disabled).toBeTruthy();
    });
  }));

  it("Should have a disabled login button if username and password are empty", async(() => {
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();

      let button = fixture.debugElement.query(By.css("#hci-login-form-submit-button")).nativeElement;

      expect(button.disabled).toBeTruthy();
    });
  }));
});
