/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */

/**
 * A barrel file for the HCI ng2 user package.
 *
 * @since 1.0.0
 */
export {UserModule} from "./src/user.module"
export {UserEntity} from "./src/user.entity"
export {RoleEntity} from "./src/authorization/role.entity"
export {PermissionEntity} from "./src/authorization/permission.entity"

/**
 * The opaque tokens for service configuration.
 */
export {
  AUTHENTICATED_USER_ENDPOINT
} from "./src/user.service"

export {UserService} from "./src/user.service"

export {RoleCheckDirective} from "./src/authorization/role-check.directive";
export {RoleCheckUnlessNullDirective} from "./src/authorization/role-check-unless-null.directive";
