import {Component, forwardRef, HostBinding, Input} from "@angular/core";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {HttpClient} from "@angular/common/http";

import {DictionaryService, DropdownEntry} from "@huntsman-cancer-institute/dictionary-service";

@Component({
  selector: "hci-md-select",
  template: `
    <mat-form-field>
      <mat-label>{{label}}</mat-label>
      <mat-select [(value)]="value" [required]="required">
        <mat-option *ngIf="!required" [value]="undefined"></mat-option>
        <mat-option *ngFor="let row of entries" [value]="row[idKey]">{{ row[displayKey] }}</mat-option>
      </mat-select>
    </mat-form-field>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MdSelectComponent),
      multi: true
    }
  ]
})
export class MdSelectComponent implements ControlValueAccessor {

  @HostBinding("class") @Input("class") classList: string = "";

  @Input() name: string;
  @Input() label: string;
  @Input() url: string;
  @Input() className: string;
  @Input() entries: DropdownEntry[];
  @Input() required: boolean = false;
  @Input() idKey: string = "id";
  @Input() displayKey: string = "display";

  private _value: number;
  private onChangeCallback: (_: number) => void = () => {};
  private onTouchedCallback: () => void = () => {};

  constructor(private dictionaryService: DictionaryService, private http: HttpClient) {}

  ngOnInit(): void {
    if (this.url && this.url.indexOf("/") === -1) {
      this.dictionaryService.getDictionaryDropdownEntries(this.url).subscribe((entries: DropdownEntry[]) => {
        this.entries = entries;
      });
    } else if (this.url) {
      this.http.get(this.url).subscribe((entries: any[]) => {
        this.entries = entries;
      });
    }

    if(this.classList.length > 0) {
      this.classList += " material-input";
    }else {
      this.classList = "material-input";
    }
  }

  public get value(): number {
    return this._value;
  }

  public set value(newValue: number) {
    this._value = newValue;
    this.onChangeCallback(newValue);
  }

  public onBlur() {
    this.onTouchedCallback();
  }

  public writeValue(value: number) {
    if (value !== this.value) {
      this.value = value;
    }
  }

  public registerOnChange(callback: (_: number) => void) {
    this.onChangeCallback = callback;
  }


  public registerOnTouched(callback: () => void) {
    this.onTouchedCallback = callback;
  }
}
