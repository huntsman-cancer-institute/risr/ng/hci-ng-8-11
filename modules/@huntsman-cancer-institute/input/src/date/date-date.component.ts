import {
  Component, ElementRef, EventEmitter, Input, OnInit, DoCheck, AfterViewInit, ViewChild, Output,
  ChangeDetectorRef
} from "@angular/core";
import {DatePipe} from "@angular/common";
import {FormBuilder, FormGroup, FormControl} from "@angular/forms";

import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";

import {DateValidator} from "./date-validator";
import {DateBase} from "./date-base";

import * as moment from "moment";
import {emit} from "cluster";

/**
 * TODO: Confirm timezone behavior.
 *
 * @since 4.0.0
 */
@Component({
  selector: "hci-date",
  providers: [DatePipe, DateValidator],
  template: `
      <div class="form-group">
         <div class="input-group d-flex">
           <form [formGroup]="dateForm" class="flex-grow-1">
             <div class="date-wrapper">
               <div class="input-group-append">
                 <input type="text" (keyup)="onKeyUpInput($event)"
                        (ngModelChange)="onChange($event)" class="form-control"
                        formControlName="dateInput" placeholder="{{label}}"
                        aria-label="date" aria-describedby="basic-addon2" />
                 <span class="input-group-text" id="basic-addon2" (click)="clear()">
                   <i class="fas fa-times"></i>
                 </span>
                 <button class="btn btn-outline-secondary" type="button" (click)="startEdit();">
                   <i class="fas fa-calendar-alt"></i>
                 </button>
               </div>
               <div *ngIf="validate && dateForm.get('dateInput').hasError('dateFormatValidator')"
                    class="invalid-date">Enter date as M/D/YYYY or MM/DD/YYYY</div>
               <div *ngIf="validate && dateForm.get('dateInput').hasError('dateRangeValidator')"
                    class="invalid-date">Invalid Date Range</div>
               <div *ngIf="validate && dateForm.get('dateInput').hasError('dateInvalidValidator')"
                    class="invalid-date">Not a real date</div>
             </div>
           </form>
           <!--
             We will replace the nbg date picker with material date picker
           -->
<!--           <form [formGroup]="dateForm" class="flex-grow-1">-->
<!--             <div class="date-wrapper">-->
<!--               <div class="input-group-append">-->
<!--                  <mat-form-field appearance="outline" [style.padding-top.em]="0.5"-->
<!--                                  [style.padding-bottom.em]="0.5">-->
<!--                    <input matInput (keypress)="onKeyPressInput($event)"-->
<!--                           [min]="minDate" [max]="maxDate" [matDatepicker]="datePicker"-->
<!--                           formControlName="dateInput" placeholder="{{label}}" aria-label="date"-->
<!--                           aria-describedby="basic-addon2">-->
<!--                    <mat-datepicker-toggle matSuffix [for]="datePicker"></mat-datepicker-toggle>-->
<!--                    <mat-datepicker #datePicker></mat-datepicker>-->
<!--                    <mat-error *ngIf="validate && dateForm.get('dateInput').hasError('dateFormatValidator')"-->
<!--                               class="invalid-date">-->
<!--                      Enter date as M/D/YYYY or MM/DD/YYYY-->
<!--                    </mat-error>-->
<!--                    <mat-error *ngIf="validate && dateForm.get('dateInput').hasError('dateRangeValidator')"-->
<!--                               class="invalid-date">-->
<!--                      Invalid Date Range-->
<!--                    </mat-error>-->
<!--                  </mat-form-field>-->
<!--               </div>-->
<!--             </div>-->
<!--          </form>-->
         </div>
      </div>
      <div *ngIf="state === 1" class="inline-editing">
        <ngb-datepicker [ngModel]="modifiedData" #dp [startDate]="startDate" [minDate]="minDate"
                        [maxDate]="maxDate" (keydown)="onKeyDown($event)"
                        (ngModelChange)="onChange($event)" (dateSelect)="save()"></ngb-datepicker>
        <div class="inline-hover-save-options">
<!--          <a class="btn ii-so-btn save" (click)="save()"><i class="fas fa-check"></i></a>-->
<!--          <a class="btn ii-so-btn cancel" (click)="cancel()"><i class="fas fa-times"></i></a>-->
        </div>
      </div>

  `,
  styles: [`
    /*::ng-deep mat-form-field .mat-form-field-flex > .mat-form-field-infix{*/
    /*  padding: 0.25em 0 0.5em 0;*/
    /*}*/
  `],
  host: {
    "(document:click)": "handleOutsideEvent($event)"
  }
})

export class DateComponent extends DateBase implements OnInit, DoCheck, AfterViewInit {

  @ViewChild("dp", {static: false}) dp: any;

  // Binded data from the parent component.
  @Input() dateFormat: string = "shortDate";
  startDate;
  @Input() minDate;
  @Input() maxDate;
  @Input() label: string;
  labelDate: string;
  @Input() inputData: Object;
  // The data used in our input.  We stored this as well as the inputData so we can cancel the edit and preserve the original data.
  @Input() modifiedData: Object;
  @Output() inputDataChange = new EventEmitter<any>();
  @Output() dateValid = new EventEmitter<boolean>();
  dateForm: FormGroup;
  dateInput: FormControl;
  // only validate if user is typing
  validate: boolean = false;
  private focused: boolean = false;

  constructor(elementRef: ElementRef, private formBuilder: FormBuilder, private datePipe: DatePipe, private changeDetectorRef: ChangeDetectorRef) {
    super(elementRef);
  }

  /**
   *
   */
  ngOnInit() {
    this.dateForm = this.formBuilder.group({
       dateInput : new FormControl("", [DateValidator.dateFormatValidator, DateValidator.dateRangeValidator, DateValidator.dateInvalidValidator])
    });
    this.onChanges();

    this.updateInternalDataFromModel();
    // set the minDate to 20 years in the past by default
    let today = new Date();
    if (!this.minDate) {
        this.minDate = {year: today.getFullYear() - 20, month: today.getMonth() + 1, day: today.getDate()};
    }
    // set max date to today by default
    if (!this.maxDate) {
        this.maxDate = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};
    }
  }

  ngAfterViewInit() {
    // Fill the input with the bound data set in the parent component.
    if (this.inputData) {
      let formattedDate = this.datePipe.transform(<string>this.inputData, this.dateFormat);
      this.dateForm.controls["dateInput"].setValue(formattedDate);
    }
    this.changeDetectorRef.detectChanges();
  }

  onChanges(): void {}


  ngDoCheck() {
    // this is the only way I found to have the focus on the startDate day when the user opens
    // the popup. We only want to focus once otherwise user can not click anywhere else in the popup
    if (this.dp && !this.focused) {
        this.dp.focus();
        this.focused = true;
    }
    // mark picker for focusing next time it is open
    if (this.state === 0) {
        this.focused = false;
    }
  }

  public updateInternalDataFromModel() {
     if (this.inputData) {
         this.modifiedData = new Date(<string>this.inputData);
         let md: Date = new Date(<string>this.inputData);
         this.startDate = {year: md.getFullYear(), month: md.getMonth() + 1, day: md.getDate()};
     } else {
         let today: Date = new Date();
         this.startDate = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};
     }
  }

  /**
   * Updates the bound data with the working data.
   */
  public updateModelFromInternalData() {
     if (this.modifiedData) {
         let dsDate = this.ngbDateToString(<NgbDateStruct>this.modifiedData);
         if (dsDate) {
            this.inputData = dsDate;
         } else {
            this.inputData = this.modifiedData;
         }

         const formattedModifiedDate = this.datePipe.transform(<string>this.inputData, this.dateFormat);
         this.dateForm.controls["dateInput"].setValue(formattedModifiedDate, { emitEvent: false});

     }
     this.inputDataChange.emit(this.inputData);
     this.updateInternalDataFromModel();
  }

  public clear() {
      this.modifiedData = null;
      this.inputData = null;
      this.dateForm.controls["dateInput"].setValue("");
      this.validate = false;
      let today: Date = new Date();
      this.startDate = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};
      this.state = 0;
      this.dateForm.controls["dateInput"].setValue("");
      this.inputDataChange.emit(this.inputData);
      this.dateValid.emit(true);
  }

  public onKeyUpInput(event: KeyboardEvent) {
    // Check for when the input is deleted before being saved
    if(event.key === "Delete" || event.key === "Backspace") {
      if(this.dateForm.controls["dateInput"].value.length === 0) {
        this.clear();
      }
    }

     // update range so that the date range validator can run accurately
     DateValidator.minValue = this.minDate;
     DateValidator.maxValue = this.maxDate;

    // Get the updated dateForm value
     let eventTarget = event.target as HTMLInputElement;
     let inputValue = eventTarget.value;
     this.dateForm.controls["dateInput"].patchValue(inputValue);

     // if we have a good date and is within the range update the picker
     if (DateValidator.inRange(this.dateForm.controls["dateInput"])) {
         this.validate = false;
         this.getDateValid();
         this.inputData = this.datePipe.transform(this.dateForm.controls["dateInput"].value, this.dateFormat);
         this.updateInternalDataFromModel();
         this.dateForm.controls["dateInput"].setValue(this.inputData);
         this.inputDataChange.emit(this.inputData);
     } else {
       this.validate = true;
       this.getDateValid();
     }

      // // only validate if user is typing
      // if (!this.validate) {
      //   this.validate = true;
      //   this.dateForm.controls["dateInput"].setValue("");
      //
      //   // clear the date picker
      //   this.modifiedData = null;
      //   this.inputData = null;
      //   let today: Date = new Date();
      //   this.startDate = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};
      //   this.state = 0;
      //   this.inputDataChange.emit(this.inputData);
      // }
  }

  /**
   * NgbDateStruct stores day, month and year.  Convert this to ISO8601.
   *
   * @param date
   * @returns {any}
   */
  private ngbDateToString(date: NgbDateStruct): string {
    if (date === undefined || date === null) {
      return null;
    } else if (date.year === undefined || date.month === undefined || date.day === undefined) {
      return null;
    }

    return date.year + "-" + ((date.month < 10) ? "0" : "") + date.month + "-" + ((date.day < 10) ? "0" : "") + date.day + "T12:00-06:00";
  }

  public getDateValid(): boolean {
    if (this.dateForm.get("dateInput").hasError("dateFormatValidator")
      || this.dateForm.get("dateInput").hasError("dateRangeValidator")
      || this.dateForm.get("dateInput").hasError("dateInvalidValidator")) {
      this.dateValid.emit(false);
      return false;
    } else {
      this.dateValid.emit(true);
      return true;
    }
  }

}
