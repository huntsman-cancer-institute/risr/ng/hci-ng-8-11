import {TestBed, async} from "@angular/core/testing";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {DateComponent} from "./date-date.component";
import {DateRangeComponent} from "./date-date-range.component";

describe("InlineDateRangeComponent Tests", () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule
      ],
      declarations: [
        DateComponent,
        DateRangeComponent
      ]
    });
  });

  it ("Should show startDate datepicker in state 1.",
    async(() => {
      let fixture = TestBed.createComponent(DateRangeComponent);
      let cmpt: DateRangeComponent = fixture.componentInstance;
      cmpt.ngAfterViewInit();
      cmpt.sp.startEdit();
      let element = fixture.nativeElement;
      fixture.detectChanges();

      expect(element.querySelectorAll("div > ngb-datepicker").length).toBe(1);
    })
  );

  it("Should update startDate inputData upon save().",
    async(() => {
      let fixture = TestBed.createComponent(DateRangeComponent);
      let cmpt = fixture.componentInstance;

      cmpt.inputStartData = "2018-01-18T12:00-06:00";
      cmpt.sp.ngOnInit();
      cmpt.ngAfterViewInit();
      cmpt.sp.modifiedData = {year: 2018, month: 1, day: 18};
      cmpt.sp.save();

      expect(cmpt.sp.inputData).toEqual("2018-01-18T12:00-06:00");
    })
  );

  it("Should not update startDate inputData upon cancel().",
    async(() => {
      let fixture = TestBed.createComponent(DateRangeComponent);
      let cmpt = fixture.componentInstance;

      cmpt.inputStartData = "2018-01-18T12:00-06:00";
      cmpt.ngAfterViewInit();
      cmpt.sp.modifiedData = new Date("2017-01-18T12:00-06:00");
      cmpt.sp.cancel();

      expect(cmpt.sp.inputData).toEqual(undefined);
    })
  );

  it ("Should show endDate datepicker in state 1.",
     async(() => {
       let fixture = TestBed.createComponent(DateRangeComponent);
       let cmpt = fixture.componentInstance;
       cmpt.ngAfterViewInit();
       cmpt.ep.startEdit();
       let element = fixture.nativeElement;
       fixture.detectChanges();

       expect(element.querySelectorAll("div > ngb-datepicker").length).toBe(1);
     })
  );

  it("Should update endDate inputData upon save().",
    async(() => {
      let fixture = TestBed.createComponent(DateRangeComponent);
      let cmpt = fixture.componentInstance;

      cmpt.inputEndData = "2008-01-18T12:00-06:00";
      cmpt.ep.ngOnInit();
      cmpt.ngAfterViewInit();
      cmpt.ep.modifiedData = {year: 2008, month: 1, day: 18};
      cmpt.ep.save();

      expect(cmpt.ep.inputData).toEqual("2008-01-18T12:00-06:00");
    })
  );

  it("Should not update endDate inputData upon cancel().",
    async(() => {
       let fixture = TestBed.createComponent(DateRangeComponent);
       let cmpt = fixture.componentInstance;

       cmpt.inputEndData = "2008-01-08T12:00-06:00";
       cmpt.ngAfterViewInit();
       cmpt.ep.modifiedData = new Date("2017-01-18T12:00-06:00");
       cmpt.ep.cancel();

       expect(cmpt.ep.inputData).toEqual(undefined);
    })
   );

  it("If both start and end dates are null/empty the startDate for both pickers should be today's date.",
    async(() => {
      let fixture = TestBed.createComponent(DateRangeComponent);
      let cmpt = fixture.componentInstance;

      cmpt.inputStartData = null;
      cmpt.inputEndData = null;
      cmpt.ngAfterViewInit();
      let today: Date = new Date();
      let todayStruct = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};
      cmpt.sp.ngOnInit();
      cmpt.sp.startEdit();
      expect(cmpt.sp.startDate).toEqual(todayStruct);
      cmpt.ep.ngOnInit();
      cmpt.ep.startEdit();
      expect(cmpt.ep.startDate).toEqual(todayStruct);
     })
   );

  it("If the start date is null/empty but the endDate is not null, the startDate for the start picker should be the " +
  		"inputDate of the end picker and the startDate for the end picker should be todays date.",
    async(() => {
      let fixture = TestBed.createComponent(DateRangeComponent);
      let cmpt = fixture.componentInstance;

      cmpt.inputStartData = null;
      cmpt.inputEndData = "2008-01-18T12:00-06:00";
      let endDate: Date = new Date(<string>cmpt.inputEndData);
      let endStruct = {year: endDate.getFullYear(), month: endDate.getMonth() + 1, day: endDate.getDate()};
      cmpt.sp.ngOnInit();
      cmpt.sp.startEdit();
      cmpt.ngAfterViewInit();
      expect(cmpt.sp.startDate).toEqual(endStruct);
      let today: Date = new Date();
      let todayStruct = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};
      cmpt.ep.ngOnInit();
      cmpt.ep.startEdit();
      cmpt.ngAfterViewInit();
      expect(cmpt.ep.startDate).toEqual(todayStruct);
    })
   );

  it("If the start date is not null but the endDate is null, the startDate for both pickers should be today's date",
    async(() => {
      let fixture = TestBed.createComponent(DateRangeComponent);
      let cmpt = fixture.componentInstance;

      cmpt.inputStartData = "2008-01-18T12:00-06:00";
      cmpt.inputEndData = null;
      let today: Date = new Date();
      let todayStruct = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};
      cmpt.ep.ngOnInit();
      cmpt.ep.startEdit();
      cmpt.ngAfterViewInit();
      expect(cmpt.ep.startDate).toEqual(todayStruct);
      cmpt.sp.ngOnInit();
      cmpt.sp.startEdit();
      cmpt.ngAfterViewInit();
      expect(cmpt.sp.startDate).toEqual(todayStruct);
     })
   );

});
