import {Component, EventEmitter, Input, AfterViewInit, Output, ViewChild} from "@angular/core";

import {DateComponent} from "./date-date.component";

/**
 * TODO: Confirm timezone behavior.
 *
 * @since 4.0.0
 */
@Component({
  selector: "hci-date-range",
  template: `  
    <div class="d-flex flex-wrap date-range">
      <hci-date #startdate
                class="date"
                [label]="startLabel"
                [(inputData)]="inputStartData"
                (inputDataChange)="onClickSubmitStartDate($event)"
                [dateFormat]="dateFormat"
                [minDate]="minStartDate"
                [maxDate]="maxStartDate">
      </hci-date>
      <hci-date #enddate
                class="date"
                [label]="endLabel"
                [(inputData)]="inputEndData"
                (inputDataChange)="onClickSubmitEndDate($event)"
                [dateFormat]="dateFormat"
                [minDate]="minEndDate"
                [maxDate]="maxEndDate">
      </hci-date>
    </div>
  `
})
export class DateRangeComponent implements AfterViewInit {

    @ViewChild("startdate", {static: true})
    sp: DateComponent;

    @ViewChild("enddate", {static: true})
    ep: DateComponent;

   // Binded data from the parent component.
   @Input() dateFormat: string = "shortDate";
   @Input() inputStartData: Object;
   @Input() inputEndData: Object;
   @Input() startLabel: string;
   @Input() endLabel: string;
   @Output() inputDataChange = new EventEmitter<any[]>();

   @Input() minStartDate: any;
   public maxStartDate;
   public minEndDate;
   public maxEndDate;

   ngOnInit() {
     this.setRange();
   }

   /**
   *
   */
  ngAfterViewInit() {
     // if there is no start date but there is an end date, make the start date go to the end date
     if (!this.inputStartData && this.inputEndData) {
       let dt: Date = new Date(<string>this.inputEndData);
       this.sp.startDate = {year: dt.getFullYear(), month: dt.getMonth() + 1, day: dt.getDate()};
     }
  }

  onClickSubmitStartDate(startData) {
    this.setRange();
    // if there is no start date but there is an end date, make the start date go to the end date
    if (!startData && this.inputEndData) {
      let dt: Date = new Date(<string>this.inputEndData);
      this.sp.startDate = {year: dt.getFullYear(), month: dt.getMonth() + 1, day: dt.getDate()};
    }
    this.inputDataChange.emit([this.inputStartData, this.inputEndData]);
  }

  onClickSubmitEndDate(endData) {
    this.setRange();
    this.inputDataChange.emit([this.inputStartData, this.inputEndData]);
  }

  private setRange() {
    // set the minDate to 20 years in the past by default
    let today = new Date();
    if (!this.minStartDate) {
        this.minStartDate = {year: today.getFullYear() - 20, month: today.getMonth() + 1, day: today.getDate()};
    }
    this.minEndDate = this.minStartDate;
    // set the maxDate to today by default
    this.maxStartDate = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};
    this.maxEndDate = this.maxStartDate;
    if (this.inputEndData) {
        let endDate = new Date(<string>this.inputEndData);
        this.maxStartDate = {year: endDate.getFullYear(), month: endDate.getMonth() + 1, day: endDate.getDate()};
    }
    if (this.inputStartData) {
        let startDate = new Date(<string>this.inputStartData);
        this.minEndDate = {year: startDate.getFullYear(), month: startDate.getMonth() + 1, day: startDate.getDate()};
    }
  }
}
