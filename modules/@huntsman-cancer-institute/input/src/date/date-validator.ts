import {AbstractControl} from "@angular/forms";

import * as moment from "moment";

export class DateValidator {
  static minValue;
  static maxValue;

  static dateFormatValidator(ac: AbstractControl) {
    let dateArr = ac.value.split("/");
    if (dateArr.length != 3 || dateArr[0].length > 3 || dateArr[0].length < 1 || dateArr[1].length > 3 ||
      dateArr[1].length < 1 || dateArr[2].length !== 4) {
      return { "dateFormatValidator": true };
    }
    return null;
  }

  static dateRangeValidator(ac: AbstractControl) {
    if (ac && ac.value && DateValidator.minValue && DateValidator.maxValue && (moment(ac.value, "M/D/YYYY", true).isValid() || moment(ac.value, "MM/DD/YYYY", true).isValid())) {
      let dateValue = moment(ac.value);
      let minDate = moment(DateValidator.convertNgbStructToDate(DateValidator.minValue));
      let maxDate = moment(DateValidator.convertNgbStructToDate(DateValidator.maxValue));

      if (dateValue.isBefore(minDate, "day") || dateValue.isAfter(maxDate, "day")) {
        return { "dateRangeValidator": true };
      }
    }

    return null;
  }

  static dateInvalidValidator(ac: AbstractControl) {
    if (ac && ac.value && !moment(ac.value, "M/D/YYYY", true).isValid() && !moment(ac.value, "MM/DD/YYYY", true).isValid()) {
      return { "dateInvalidValidator": true };
    }
    return null;
  }

   static convertNgbStructToDate(struct) {
     return {year: struct.year, month: struct.month - 1, day: struct.day};
   }

   static inRange(ac: AbstractControl) {
     if (ac && ac.value && DateValidator.minValue && DateValidator.maxValue && (moment(ac.value, "M/D/YYYY", true).isValid() || moment(ac.value, "MM/DD/YYYY", true).isValid())) {
       let dateValue = moment(ac.value);
       let minDate = moment(DateValidator.convertNgbStructToDate(DateValidator.minValue));
       let maxDate = moment(DateValidator.convertNgbStructToDate(DateValidator.maxValue));

       if (dateValue.isBefore(minDate, "day") || dateValue.isAfter(maxDate, "day")) {
          return false;
       } else {
          return true;
       }
     }
     return false;
   }
}
