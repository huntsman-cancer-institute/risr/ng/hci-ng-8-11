/**
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 *
 * Extending class works like in Java except that decorators are only scanned in the child class.  In this case,
 * we want variables like inputData to be available to all inline components that extend this class; however, we want
 * this variable to be an @Input.
 * What we can do here is define "inputData: Object" here and in the extending class, define "@Input inputData: Object".
 *
 * The two way binding to the component is inputData.  To allow non-saving modification to this data, we copy
 * it to modifiedData.  The wrapped component such as input/select/... then use modifiedData as their ngModel.
 * When we cancel out of editing, modifiedData is ignored and replaced with inputData.  When editing is saved, inputData
 * is replaced with modifiedData.
 *
 * @since 1.0.0
 */
import {ElementRef, Injectable} from "@angular/core";

@Injectable()
export abstract class DateBase {

  inputData: Object;
  modifiedData: Object;
  state: number = 0;

  _el: HTMLElement;

  constructor(elementRef: ElementRef) {
    this._el = elementRef.nativeElement;
  }

  /**
   * Listen to clicks on the document.  If the state is in edit mode and there is a click outside the element,
   * then save it.
   *
   * @param event
   */
  public handleOutsideEvent(event: Event) {
    if (this.state !== 1) {
      return;
    }
    let targetCmpt: HTMLElement = null;

    try {
      // Catch exception if target is not an element
      targetCmpt = (<HTMLElement>event.target);
    } catch (e) {
      this.save();
      return;
    }

    let inside: boolean = false;
    do {
      if (targetCmpt === this._el) {
        inside = true;
        break;
      }
      targetCmpt = targetCmpt.parentElement;
    } while (targetCmpt);
    if (!inside) {
      this.save();
    }
  }

  /**
   * Updates the modifiedData (working data) to the bound data.
   */
  public updateInternalDataFromModel() {
    this.modifiedData = this.inputData;
  }

  /**
   * Updates the bound data with the working data.
   */
  public updateModelFromInternalData() {
    this.inputData = this.modifiedData;
  }

  /**
   * When user chooses to edit, change state to show edit dialog.
   */
  public startEdit() {
    this.modifiedData = this.inputData;
    this.state = 1;
  }

  /**
   * Generic actions to change state based on key input.  Escape will cancel out of the edit
   * without saving, enter will save and exit.
   *
   * @param event
   */
  public onKeyDown(event: KeyboardEvent) {
    if (event.key === "escape") {
      this.cancel();
    } else if (event.key === "enter") {
      this.save();
    }
  }

  /**
   *
   * @param newValue
   */
  public onChange(newValue: Object) {
    this.modifiedData = newValue;
  }

  /**
   * Replace the bound data with the working data and exit editing mode.
   */
  public save() {
    this.updateModelFromInternalData();
    this.state = 0;
  }

  /**
   * Don't update the bound data and exit the editing mode.
   */
  public cancel() {
    this.updateInternalDataFromModel();
    this.state = 0;
  }

}
