/**
 * @description: A model of common data for dropdown components.
 * @since 5.0.0
 */

export interface SelectItem {
  id: any;
  text: string;
  entity?: any;
}

export const KEY_CODE = Object.freeze({
  DOWN_ARROW: 40,
  UP_ARROW: 38,
  ENTER: 13,
  TAB: 9,
  DELETE: 8
});


