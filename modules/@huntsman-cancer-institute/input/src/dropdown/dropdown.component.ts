import {Component, EventEmitter, Input, isDevMode, OnInit, Output, ViewChild} from "@angular/core";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";

import {Observable} from "rxjs";
import {map, tap} from "rxjs/operators";

import {DropdownService} from "./dropdown.service";
import {SelectItem} from "./select-item";
import {DropdownSelectComponent} from "./dropdown-select.component";

@Component({
  selector: "hci-dropdown",
  template: `
    <form novalidate [formGroup]="dropdownForm">
      <div class="form-group">
        <hci-dropdown-select #dropdownSingle *ngIf="dropdownType === 0"
                             [placeholder]="placeholder"
                             formControlName="dropdownSingleTypeAhead"
                             [multiple]="false"
                             [dataProvider]="listItems"
                             [selectItemAdapter]="entityToSelectItem"
                             referenceMode="entity"
                             (select)="onSelect($event)"
                             (remove)="onRemove($event)"
                             [disabled]="isDisabled"
                             [selectedProvider]="getItems"
                             [clientMode]="true"></hci-dropdown-select>
        <hci-dropdown-select *ngIf="dropdownType === 1"
                             [placeholder]="placeholder"
                             formControlName="dropdownMultipleTypeAhead"
                             [dataProvider]="listItems"
                             [selectItemAdapter]="entityToSelectItem"
                             [clientMode]="true"
                             referenceMode="entity"
                             [multiple]="true"
                             (select)="onSelect($event)"
                             (remove)="onRemove($event)"
                             [disabled]="isDisabled"
                             [selectedProvider]="getItems"
                             [clientMode]="true"></hci-dropdown-select>
        <hci-dropdown-select *ngIf="dropdownType === 2"
                             [placeholder]="placeholder"
                             formControlName="dropdownSingle"
                             [dataProvider]="listItems"
                             [selectItemAdapter]="entityToSelectItem"
                             [inputState]="0"
                             referenceMode="entity"
                             [multiple]="false"
                             (select)="onSelect($event)"
                             (remove)="onRemove($event)"
                             [disabled]="isDisabled"
                             [selectedProvider]="getItems"
                             [clientMode]="true"></hci-dropdown-select>
        <hci-dropdown-select *ngIf="dropdownType === 3"
                             [placeholder]="placeholder"
                             formControlName="dropdownMultiple"
                             [dataProvider]="listItems"
                             [selectItemAdapter]="entityToSelectItem"
                             [inputState]="0"
                             referenceMode="entity"
                             [multiple]="true"
                             (select)="onSelect($event)"
                             (remove)="onRemove($event)"
                             [selectedProvider]="getItems"
                             [disabled]="isDisabled"
                             [clientMode]="true"></hci-dropdown-select>
        <hci-dropdown-select *ngIf="dropdownType === 4"
                             [placeholder]="placeholder"
                             formControlName="dropdownSingle"
                             [dataProvider]="listItems"
                             [selectItemAdapter]="entityToSelectItem"
                             [inputState]="0"
                             referenceMode="entity"
                             (select)="onSelect($event)"
                             (remove)="onRemove($event)"
                             [selectedProvider]="getItems"
                             [disabled]="isDisabled"
                             [clientMode]="true">
          <div *appTemplateDropdown="let item = $entity; let i = $index">
            <span>[{{item.id}}]</span> {{item[displayField]}}
          </div>
        </hci-dropdown-select>
      </div>
    </form>
  `,
  providers: [DropdownService]
})
export class DropdownComponent implements OnInit {

  public dropdownForm: FormGroup;
  public getItems: (ids: any[]) => Observable<Object[]>;
  public listItems: (item: string) => Observable<Object[]>;
  public listItemsMax: (item: string, ids: any[]) => Observable<Object[]>;
  public entityToSelectItem: (entity: Object) => SelectItem;
  public count: number;

  @Input() inputSingleData: Object;
  @Input() inputMultiData: Object[];
  @Input() modifiedData: Object[] = [];
  @Input() optionData: Object[];
  @Input() displayField: string;
  @Input() displayId: any;
  @Input() dataUrl: string;
  @Input() dropdownType: any;
  @Input() placeholder: string;
  @Input() isDisabled = false;
  @Output() inputSingleDataChange = new EventEmitter<any>();
  @Output() inputMultiDataChange = new EventEmitter<any>();

  @ViewChild("dropdownSingle", {static: false}) dropdownSingle: DropdownSelectComponent;

  constructor(private dropdownService: DropdownService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.dropdownForm = this.formBuilder.group({
      option: new FormControl(""),
      dropdownSingleTypeAhead: new FormControl(""),
      dropdownMultipleTypeAhead: new FormControl(""),
      dropdownSingle: new FormControl(""),
      dropdownMultiple: new FormControl(""),
      habilitado: true
    });

    this.initDropdownForm();
    this.initDropdownItem();

    this.dropdownForm.valueChanges.subscribe(() => {});
  }

  onSelect(item: SelectItem) {
    if (this.dropdownType === 0 || this.dropdownType === 2 || this.dropdownType === 4) {
      this.modifiedData.length = 0;
      this.modifiedData.push(item.entity);
      this.inputSingleData = this.modifiedData[0];
      this.inputSingleDataChange.emit(this.inputSingleData);
    } else if (this.dropdownType === 1 || this.dropdownType === 3) {
      this.modifiedData.push(item.entity);
      this.inputMultiData = this.modifiedData;
      this.inputMultiDataChange.emit(this.inputMultiData);
    }
  }

  onRemove(item: SelectItem) {
    if (this.modifiedData.length > 0) {
      const index = this.modifiedData.indexOf(item.entity, 0);
      if (index > -1) {
        this.modifiedData.splice(index, 1);
      }
    }

    if (this.inputSingleData) {
      this.inputSingleData = "";
      this.inputSingleDataChange.emit(this.inputSingleData);
    }

    if (this.inputMultiData) {
      this.inputMultiData = this.modifiedData;
      this.inputMultiDataChange.emit(this.inputMultiData);
    }
  }

  reset() {
    this.dropdownForm.reset();
  }

  send(formJson: string) {}

  private initDropdownItem() {
    let dataSet: Object[] = [];

    // Set dropdown data list from a given array or a json file
    if (this.optionData && this.optionData.length > 0) {
      // Get dropdown list data from an object
      if (isDevMode()) {
        console.debug("optionData=", this.optionData);
      }
      dataSet = this.dropdownService.setData(undefined, this.optionData, this.displayField);
    } else if (this.dataUrl && this.dataUrl !== "") {
      // Get dropdown list data from an url with json file
      if (isDevMode()) {
        console.debug("dataUrl=", this.dataUrl);
      }
      dataSet = this.dropdownService.setData(this.dataUrl, undefined, this.displayField);
    } else {
      console.error("Neither optionData or dataUrl were defined.");
      if (isDevMode()) {
        console.debug("dataUrl=", this.dataUrl);
        console.debug("optionData=", this.optionData);
      }
    }

    // Filter and sort data
    this.listItems = (term: string) => this.dropdownService.listData(term, dataSet);
    this.listItemsMax = (term: string, ids: any[]) => {
      const selectedCount = ids ? ids.length : 0;
      return this.dropdownService.listDataMax(term, 3 + selectedCount, dataSet)
        .pipe(
          tap(response => this.count = response.count),
          map((response) => response.results)
        );
    };

    // Get items and set them to the dropdown entiry
    this.getItems = (ids: any[]) => this.dropdownService.getItems(ids, dataSet);
    this.entityToSelectItem = (entity: any) => {
      return {
        id: entity[this.displayId],
        text: entity[this.displayField],
        entity: entity
      };
    };
  }

  private initDropdownForm() {
    this.modifiedData.length = 0;

    if (this.dropdownType === 0 && this.inputSingleData) {
      this.dropdownForm.controls["dropdownSingleTypeAhead"].setValue(this.inputSingleData);
      this.modifiedData.push(this.inputSingleData);
    }

    if (this.dropdownType === 1 && this.inputMultiData) {
      this.dropdownForm.controls["dropdownMultipleTypeAhead"].setValue(this.inputMultiData);
      this.modifiedData = this.inputMultiData;
    }

    if (this.dropdownType === 2 && this.inputSingleData) {
      this.dropdownForm.controls["dropdownSingle"].setValue(this.inputSingleData);
      this.modifiedData.push(this.inputSingleData);
    }

    if (this.dropdownType === 3 && this.inputMultiData) {
      this.dropdownForm.controls["dropdownMultiple"].setValue(this.inputMultiData);
      this.modifiedData = this.inputMultiData;
    }

    if (this.dropdownType === 4 && this.inputSingleData) {
      this.dropdownForm.controls["dropdownSingle"].setValue(this.inputSingleData);
      this.modifiedData.push(this.inputSingleData);
    }
  }
}
