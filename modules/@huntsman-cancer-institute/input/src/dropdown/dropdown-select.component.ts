import {AfterViewInit, Component, Input, TemplateRef, EventEmitter, ViewChild, forwardRef, Output} from "@angular/core";
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from "@angular/forms";

import {Observable, of} from "rxjs";
import {debounceTime, distinctUntilChanged, filter, map, mergeMap, switchMap, tap} from "rxjs/operators";

import {DropdownSelectResultComponent} from "./dropdown-select-result.component";
import {SelectItem, KEY_CODE} from "./select-item";
import {Messages} from "./messages";

const VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DropdownSelectComponent),
  multi: true
};

const noop = () => {};

@Component({
  selector: "hci-dropdown-select",
  template: `
    <div class="select-container">
      <ul class="form-control"
          [class.simple-selection]="!multiple"
          [class.multiple-selection]="multiple"
          [class.search-focused]="searchFocused"
          [class.readonly]="disabled">

        <ng-container *ngIf="!searchFocused || multiple">
          <li *ngFor="let item of selectedItems" class="select-selected label label-info" [class.label]="multiple"
              [class.label-info]="multiple">
            <span class="selected-item-text">{{item.text}}</span>
            <a class="select-selection-remove" (click)="removeItem(item)" *ngIf="!disabled">
              <i [class]="deleteIcon" [class.text-info]="!multiple"></i>
            </a>
          </li>
        </ng-container>

        <li class="select-input">
          <input #termInput type="text"
                 [placeholder]="getPlaceholder()"
                 [formControl]="term"
                 [class.hideable]="isHideable()"
                 [style.width]="getInputWidth()"
                 (focus)="onFocus()"
                 (blur)="onBlur()"
                 (keyup)="onKeyUp($event)"
                 (keydown)="onKeyDown($event)"
                 (keypress)="onKeyPress($event)"
                 (click)="showResults()" *ngIf="!disabled" />
        </li>
      </ul>
      <span class="icon-drop-menu" *ngIf="inputState===0" (click)="showResults()">
        <span class="after"></span>
      </span>
      <div class="results-container" *ngIf="resultsVisible">
        <span class="results-msg" *ngIf="listedData && (listedData.length + selectedItems.length) < resultsCount">
          {{getCountMessage()}}
        </span>
        <span class="results-msg no-results-msg" *ngIf="searchFocused && listedData && listedData.length === 0">
          {{messages && messages.noResultsMsg ? messages.noResultsMsg : NO_RESULTS_MSG}}
        </span>
        <hci-dropdown-select-result #results
                                    [selectedItems]="selectedItems"
                                    [items]="listedData"
                                    (itemSelectedEvent)="onItemSelected($event)"
                                    [templateRef]="templateRef"
                                    [searchFocused]="searchFocused"></hci-dropdown-select-result>
      </div>
    </div>
  `,
  providers: [VALUE_ACCESSOR],
  host: {class: "flex-grow-1"}
})
export class DropdownSelectComponent implements AfterViewInit, ControlValueAccessor  {

  MORE_RESULTS_MSG = "Showing " + Messages.PARTIAL_COUNT_VAR + " of " + Messages.TOTAL_COUNT_VAR + " results.";
  NO_RESULTS_MSG = "No results found";

  @Input() messages: Messages = {
    moreResultsMsg: this.MORE_RESULTS_MSG,
    noResultsMsg: this.NO_RESULTS_MSG
  };

  @Input() dataProvider: (item: string, selected?: any[]) => Observable<any[]>;
  @Input() selectedProvider: (ids: any[]) => Observable<any[]>;
  @Input() selectItemAdapter: (entity: any) => SelectItem;
  @Input() referenceMode: "id" | "entity" = "id";
  @Input() placeholder = "";
  @Input() disabled = false;
  @Input() multiple = false;
  @Input() clientMode = false;
  @Input() deleteIcon = "fas fa-times-circle";
  @Input() inputState = 2; // selectIcon controller: 0->with selectIcon 2->without selectIcon
  @Input() resultsCount;
  @Input() searchDelay = 100;

  @Output() select: EventEmitter<SelectItem> = new EventEmitter<SelectItem>();
  @Output() remove: EventEmitter<SelectItem> = new EventEmitter<SelectItem>();

  @ViewChild("results", {static: false}) results: DropdownSelectResultComponent;

  onTouchedCallback: () => void = noop;
  onChangeCallback: (_: any) => void = noop;
  templateRef: TemplateRef<any>;
  term = new FormControl();
  searchFocused = false;
  resultsVisible = false;
  listedData: SelectItem[];
  modifiedData: SelectItem[];
  selectedItems: SelectItem[] = [];

  @ViewChild("termInput", {static: false}) private termInput;

  private placeholderSelected = "";

  ngAfterViewInit() {
    this.subscribeToChangesAndLoadDataFromObservable();
  }

  onItemSelected(item: SelectItem) {
    if (this.multiple) {
      this.selectedItems.push(item);
      const index = this.listedData.indexOf(item, 0);
      if (index > -1) {
        this.listedData.splice(index, 1);
      }
    } else {
      this.selectedItems.length = 0;
      this.selectedItems.push(item);
    }

    this.onChangeCallback(this.buildValue());
    this.term.patchValue("", {emitEvent: false});
    setTimeout(() => this.focusInput(), 1);
    this.resultsVisible = false;
    this.select.emit(item);
    if (!this.multiple) {
      this.placeholderSelected = item.text;
    }
  }

  focusInput() {
    if (!this.disabled) {
      this.termInput.nativeElement.focus();
      this.resultsVisible = false;
    }
    this.searchFocused = !this.disabled;
  }

  writeValue(selectedValues: any): void {
    if (selectedValues) {
      if (this.referenceMode === "id") {
        this.populateItemsFromIds(selectedValues);
      } else {
        this.populateItemsFromEntities(selectedValues);
      }
    } else {
      this.placeholderSelected = "";
      this.selectedItems = [];
    }
  }

  getCountMessage(): string {
    let msg = this.messages && this.messages.moreResultsMsg ? this.messages.moreResultsMsg : this.MORE_RESULTS_MSG;
    msg = msg.replace(Messages.PARTIAL_COUNT_VAR, String(this.listedData.length));
    msg = msg.replace(Messages.TOTAL_COUNT_VAR, String(this.resultsCount - this.selectedItems.length));
    return msg;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  removeItem(item: SelectItem) {
    const index = this.selectedItems.indexOf(item, 0);

    if (index > -1) {
      this.selectedItems.splice(index, 1);
    }

    this.onChangeCallback(this.buildValue());
    this.remove.emit(item);
    if (!this.multiple) {
      this.placeholderSelected = "";
    }
  }

  getPlaceholder(): string {
    return this.selectedItems.length > 0 ? this.placeholderSelected : this.placeholder;
  }

  getInputWidth(): string {
    const searchEmpty = this.selectedItems.length === 0 && (this.term.value === null || this.term.value.length === 0);
    const length = this.term.value === null ? 0 : this.term.value.length;
    if (!this.multiple) {
      return "100%";
    } else {
      return searchEmpty ? "100%" : (1 + length * .6) + "em";
    }
  }

  isHideable(): boolean {
    return !this.multiple && this.placeholderSelected !== "";
  }

  onFocus() {
    this.searchFocused = true;
  }

  onBlur() {
    this.term.patchValue("", {emitEvent: false});
    this.searchFocused = false;
    this.resultsVisible = false;
    this.onTouchedCallback();
  }

  onKeyUp(ev: KeyboardEvent) {
    if (this.results) {
      if (ev.keyCode === KEY_CODE.DOWN_ARROW) {
        this.results.activeNext();
      } else if (ev.keyCode === KEY_CODE.UP_ARROW) {
        this.results.activePrevious();
      } else if (ev.keyCode === KEY_CODE.ENTER) {
        this.results.selectCurrentItem();
      }
    } else {
      if (this.inputState === 0) {
        if (ev.keyCode === KEY_CODE.ENTER || ev.keyCode === KEY_CODE.DOWN_ARROW) {
          this.showResults();
        }
      }
    }
  }

  onKeyDown(ev: KeyboardEvent) {
    if (this.results) {
      if (ev.keyCode === KEY_CODE.TAB) {
        this.results.selectCurrentItem();
      }
    }

    if (ev.keyCode === KEY_CODE.DELETE) {
      if ((!this.term.value || this.term.value.length === 0) && this.selectedItems.length > 0) {
        this.removeItem(this.selectedItems[this.selectedItems.length - 1]);
      }
    }
  }

  onKeyPress(ev: KeyboardEvent) {
    if (ev.keyCode === KEY_CODE.ENTER) {
      ev.preventDefault();
    }
  }

  showResults() {
    if (!this.disabled) {
      this.termInput.nativeElement.focus();
      this.subscribeToResults(of(""));
    }
    this.searchFocused = !this.disabled;
  }

  focus(): void {
    this.termInput.nativeElement.focus();
  }

  private subscribeToChangesAndLoadDataFromObservable() {
    const observable = this.term.valueChanges.pipe(
      debounceTime(this.searchDelay),
      distinctUntilChanged()
    );
    this.subscribeToResults(observable);
  }

  private subscribeToResults(observable: Observable<string>): void {
    observable.pipe(
      tap(() => this.resultsVisible = false),
      filter((term) => term.length >= this.inputState),
      switchMap(term => this.loadDataFromObservable(term)),
      map(items => items.filter(item => !(this.multiple && this.alreadySelected(item)))),
      tap(() => this.resultsVisible = this.searchFocused)
    ).subscribe((items) => this.listedData = items);
  }

  private loadDataFromObservable(term: string): Observable<SelectItem[]> {
    return this.clientMode ? this.fetchAndfilterLocalData(term) : this.fetchData(term);
  }

  private alreadySelected(item: SelectItem): boolean {
    let result = false;
    this.selectedItems.forEach(selectedItem => {
      if (selectedItem.id === item.id) {
        result = true;
      }
    });
    return result;
  }

  private fetchAndfilterLocalData(term: string): Observable<SelectItem[]> {
    if (!this.modifiedData) {
      return this.fetchData("").pipe(
        mergeMap((items) => {
          this.modifiedData = items;
          return this.filterLocalData(term);
        })
      );
    } else {
      return this.filterLocalData(term);
    }
  }

  private fetchData(term: string): Observable<SelectItem[]> {
    return this
      .dataProvider(term, this.buildValue())
      .pipe(map((items: any[]) => this.adaptItems(items)));
  }

  private filterLocalData(term: string): Observable<SelectItem[]> {
    return of(this.modifiedData.filter((item) => this.containsText(item, term)));
  }

  private containsText(item, term: string) {
    return item.text.toUpperCase().indexOf(term.toUpperCase()) !== -1;
  }

  private adaptItems(items: any[]): SelectItem[] {
    const convertedItems = [];
    items.map((item) => this.selectItemAdapter(item))
      .forEach((selectItem) => convertedItems.push(selectItem));
    return convertedItems;
  }

  private buildValue() {
    return "id" === this.referenceMode ? this.getSelectedIds() : this.getEntities();
  }

  private getSelectedIds(): any {
    if (this.multiple) {
      const ids: any[] = [];
      this.selectedItems.forEach(item => ids.push(item.id));
      return ids;
    } else {
      return this.selectedItems.length === 0 ? null : this.selectedItems[0].id;
    }
  }

  private getEntities(): any[] {
    if (this.multiple) {
      const entities = [];
      this.selectedItems.forEach(item => {
        entities.push(item.entity);
      });
      return entities;
    } else {
      return this.selectedItems.length === 0 ? null : this.selectedItems[0].entity;
    }
  }

  private populateItemsFromEntities(selectedValues: any) {
    if (this.multiple) {
      this.handleMultipleWithEntities(selectedValues);
    } else {
      const selectItem = this.selectItemAdapter(selectedValues);
      this.selectedItems = [selectItem];
      this.placeholderSelected = selectItem.text;
    }
  }

  private handleMultipleWithEntities(selectedValues: any) {
    this.selectedItems = [];
    selectedValues.forEach((entity) => {
      const item = this.selectItemAdapter(entity);
      const ids = this.getSelectedIds();

      if (ids.indexOf(item.id) === -1) {
        this.selectedItems.push(item);
      }
    });
  }

  private populateItemsFromIds(selectedValues: any) {
    if (this.multiple) {
      this.handleMultipleWithIds(selectedValues);
    } else {
      this.handleSingleWithId(selectedValues);
    }
  }

  private handleMultipleWithIds(selectedValues: any) {
    if (selectedValues !== undefined && this.selectedProvider !== undefined) {
      const uniqueIds = [];
      selectedValues.forEach((id) => {
        if (uniqueIds.indexOf(id) === -1) {
          uniqueIds.push(id);
        }
      });

      this.selectedProvider(uniqueIds).subscribe((items: any[]) => {
        this.selectedItems = items.map(this.selectItemAdapter);
      });
    }
  }

  private handleSingleWithId(id: any) {
    if (id !== undefined && this.selectedProvider !== undefined) {
      this.selectedProvider([id]).subscribe((items: any[]) => {
        items.forEach((item) => {
          const selectItem = this.selectItemAdapter(item);
          this.selectedItems = [selectItem];
          this.placeholderSelected = selectItem.text;
        });
      });
    }
  }
}
