# Angular Misc Components

This package contains useful components that don't belong to any particular category.

## Busy

The busy component will take an array of booleans or boolean subjects and if any of those are true, grey out the entire
parent with a spinner icon.


      <div class="flex-grow-1">
        <hci-busy [busy]="[busy]"></hci-busy>
        </div>

## Accordion Controller

The @ng-bootstrap accordion is our standard layout component.  This controller offers a quick way to navigate through an
accordion when it becomes large.

    <ngb-accordion id="test-accordion" #accordion="ngbAccordion" class="y-auto">
      <ngb-panel id="test-1" title="Test 1">
        <ng-template ngbPanelContent>
          <div class="d-flex flex-wrap">
            <div class="col-md-4">
              #1
            </div>
            <ng-container *ngFor="let n of counter">
              <input type="text" class="col-md-4 mb-2" />
            </ng-container>
          </div>
        </ng-template>
      </ngb-panel>
    </ngb-accordion>

    <hci-accordion-nav [accordion]="accordion"
                       [panels]="accordion.panels"
                       accordionId="test-accordion"
                       class="mt-auto mb-0">
    </hci-accordion-nav>
