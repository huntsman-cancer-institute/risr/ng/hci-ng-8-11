# Angular Notification Library

This module is designed to be a single spot for an application to post errors/warnings to and for the user to be notified
about those.

## NotificationService

This service should be a singleton in the application.  It manages notifications and allows any other part of the application
to push notifications.  Notifications can be pushed as info, warnings or errors.

    notificationService.addNotification({
      notification: "Some event occurred.",
      type: "INFO"
    })

## NotificationPopup

The notification component is supposed to be added to the root of the application.  This is due to popups being fixed to
the screen.  By default when a new notification appears, it popups on the top right of the screen and disappears after
5 seconds.  After that you need to go to the notification dropdown to view the message.

    <hci-notification-popup></hci-notification-popup>

## NotificationDropdown

The dropdown is a specialized nav component intended to be placed in the app header.  This is a bell icon that shows a
number count when there are unread notifications.  Clicking on the icon brings up a viewer for browsing the notifications.

