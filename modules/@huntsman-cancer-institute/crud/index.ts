/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A barrel file for the crud service package.
 *
 * @since 9.0.0
 */
export {CrudModule} from "./src/crud.module";
export {CrudService} from "./src/services/crud.service";

export {CRUD_ENDPOINT} from "./src/services/crud.service";

export {CrudContainerComponent} from "./src/components/crud-container.component";
export {CrudComponent} from "./src/components/crud.component";
