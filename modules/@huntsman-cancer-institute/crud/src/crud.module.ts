/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {ModuleWithProviders, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {SelectModule} from "@huntsman-cancer-institute/input";
import {MiscModule} from "@huntsman-cancer-institute/misc";

import {CRUD_ENDPOINT, CrudService} from "./services/crud.service";
import {CrudComponent} from "./components/crud.component";
import {FieldViewablePipe} from "./pipes/field-viewable.pipe";
import {CrudContainerComponent} from "./components/crud-container.component";

/**
 * The module providing the crud service.
 *
 * @since 9.0.0
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    SelectModule,
    MiscModule
  ],
  declarations: [
    CrudContainerComponent,
    CrudComponent,
    FieldViewablePipe
  ],
  exports: [
    CrudContainerComponent,
    CrudComponent,
    FieldViewablePipe
  ]
})
export class CrudModule {
  static forRoot(config: any): ModuleWithProviders<CrudModule> {
    return {
      providers: [
        {provide: CRUD_ENDPOINT, useValue: config.crudEndpoint},
        CrudService
      ],
      ngModule: CrudModule
    };
  }
}
