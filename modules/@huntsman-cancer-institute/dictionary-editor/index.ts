/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A barrel file for the dictionary editor package.
 *
 * @since 1.0.0
 */
export {DictionaryEditorModule} from "./src/dictionary-editor.module";
