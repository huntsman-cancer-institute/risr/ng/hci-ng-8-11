/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {MiscModule} from "@huntsman-cancer-institute/misc";
import {NavigationModule} from "@huntsman-cancer-institute/navigation";

import {DICTIONARY_EDITOR_ROUTES} from "./dictionary-editor.routes";
import {DictionaryEditorComponent} from "./dictionary-editor.component";
import {DictionaryEditorHomeComponent} from "./components/home.component";
import {DictionaryEditorDetailComponent} from "./components/detail.component";
import {AgGridModule} from "ag-grid-angular";
import {AgGridEditorModule} from "./grid-editors/ag-grid-editor.module";
import {AgGridRendererModule} from "./grid-renderers/ag-grid-renderer.module";
import {SelectEditor} from "./grid-editors/select.editor";
import {SelectRenderer} from "./grid-renderers/select.renderer";
import {NewRowComponent} from "./components/new-row.component";
import {AgGridDateCellEditorComponent} from "./grid-editors/ag-grid-date-cell-editor.component";
import {DateRenderer} from "./grid-renderers/date.renderer";
/**
 * The module defining the dictionary editor.
 *
 * @since 1.0.0
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DICTIONARY_EDITOR_ROUTES,
    NgbModule,
    NavigationModule,
    MiscModule,
    AgGridEditorModule,
    AgGridRendererModule,
    AgGridModule.withComponents([
      SelectEditor,
      AgGridDateCellEditorComponent,
      SelectRenderer,
      DateRenderer
    ]),
  ],
  declarations: [
    DictionaryEditorComponent,
    DictionaryEditorHomeComponent,
    DictionaryEditorDetailComponent,
    NewRowComponent
  ],
  exports: [
    DictionaryEditorComponent,
    DictionaryEditorHomeComponent,
    DictionaryEditorDetailComponent
  ],
  entryComponents: [
    NewRowComponent
  ]
})
export class DictionaryEditorModule {}
