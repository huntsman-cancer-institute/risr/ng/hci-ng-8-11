import {ModuleWithProviders} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import {RouteGuardService} from "@huntsman-cancer-institute/authentication";

import {DictionaryEditorComponent} from "./dictionary-editor.component";
import {DictionaryEditorHomeComponent} from "./components/home.component";
import {DictionaryEditorDetailComponent} from "./components/detail.component";

export const ROUTES: Routes = [
  {path: "", component: DictionaryEditorComponent,
    children: [
      {path: "", redirectTo: "home", pathMatch: "full"},
      {path: "home", component: DictionaryEditorHomeComponent},
      {path: "detail/:className", component: DictionaryEditorDetailComponent}
    ]
  }
];
export const DICTIONARY_EDITOR_ROUTES: ModuleWithProviders<RouterModule> = RouterModule.forChild(ROUTES);
