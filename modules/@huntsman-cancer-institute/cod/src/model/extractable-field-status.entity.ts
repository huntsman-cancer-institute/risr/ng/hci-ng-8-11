
export interface ExtractableFieldStatus {
  idExtractableFieldStatus: number;
  idAttributeValueSet: number;
  idExtractableField: number;
  extractionStatus: string;
  extractReportAlias: string;
  extractionDate: string;
}
