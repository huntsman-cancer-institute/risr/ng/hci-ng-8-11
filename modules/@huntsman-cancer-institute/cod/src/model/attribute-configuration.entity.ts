import {AttributeContainer} from "./attribute-container.entity";
import {Attribute} from "./attribute.entity";

export interface AttributeConfiguration {
  idAttributeConfiguration: number;
  codeAttributeContext: string;
  codeAttributeSecurityContext: string;
  idFilter1: number;
  idFilter2: number;
  baseWindowTemplate: string;
  preconfigTabTemplate: string;
  commandName: string;
  attributeContainers: AttributeContainer[];
}
