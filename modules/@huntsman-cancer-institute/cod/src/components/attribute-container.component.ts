import {
  ChangeDetectorRef, Component, ContentChildren, ElementRef, Input, isDevMode,
  QueryList, Renderer2, ViewChildren, SimpleChanges, HostBinding
} from "@angular/core";
import { Subject, Subscription } from "rxjs";
import {NgbModal, NgbModalRef, NgbPanel} from "@ng-bootstrap/ng-bootstrap";
import {AccordionNavComponent} from "@huntsman-cancer-institute/misc";
import {AttributeService} from "../services/attribute.service";
import {AttributeConfiguration} from "../model/attribute-configuration.entity";
import {AttributeValueSet} from "../model/attribute-value-set.entity";
import {AttributeContainer} from "../model/attribute-container.entity";

/**
 * This component should be added on to any screen that displays a container with COD content.  This container
 * represents the one of the containers, which in turn contain attributes which display those values in the
 * attributeValueSet.
 */
@Component({
  selector: "hci-attribute-container",
  template: `
    <hci-busy [getBusySubjects]="getBusySubjects"></hci-busy>
  
    <ng-container *ngIf="attributeContainer">
      <div *ngIf="editPopup" [id]="'id-attribute-container-' + attributeContainer.idAttributeContainer + '-header'"
           class="d-flex attribute-container-header {{'sort-order-' + attributeContainer.sortOrder}}">
        <div class="ml-auto mr-0" (click)="edit(editModal, attributeContainer)">
          <i class="fas fa-pencil-alt"></i>
        </div>
      </div>
      <div class="d-flex flex-grow-1 y-auto" style="flex: 1 1 1px;">
        <div class="attribute-container"
             [class.col-md-12]="attributeContainer.isAutoLayout && attributeContainer.isAutoLayout === 'Y'"
             [class.flex]="attributeContainer.isAutoLayout && attributeContainer.isAutoLayout === 'Y'"
             [class.flex-wrap]="attributeContainer.isAutoLayout && attributeContainer.isAutoLayout === 'Y'"
             [class.absolute]="!attributeContainer.isAutoLayout || attributeContainer.isAutoLayout === 'N'">
          <ng-container *ngFor="let attribute of attributeContainer.graphicalAttributes | isGroupAttribute: false">
            <ng-container *ngIf="attributeContainer.isAutoLayout && attributeContainer.isAutoLayout === 'Y' && attribute.codeAttributeDataType !== 'LINE'">
              <hci-attribute-flex [id]="'id-attribute-' + attribute.idAttribute"
                                  [attribute]="attribute"
                                  [editInline]="editInline"
                                  [class.attribute]="true"
                                  [class.col-4]="attribute.codeAttributeDataType !== 'GA' && attribute.codeAttributeDataType !== 'LINE'"
                                  [class.col-12]="attribute.codeAttributeDataType === 'GA' || attribute.codeAttributeDataType === 'LINE'"></hci-attribute-flex>
            </ng-container>
            <ng-container *ngIf="!attributeContainer.isAutoLayout || attributeContainer.isAutoLayout === 'N'">
              <hci-attribute-absolute [id]="'id-attribute-' + attribute.idAttribute"
                                      [attribute]="attribute"
                                      [editInline]="editInline"
                                      [class.attribute]="true"></hci-attribute-absolute>
            </ng-container>
          </ng-container>
        </div>
      </div>
    </ng-container>

    <ng-template #editModal let-close="close">
      <div class="modal-header">
        {{editContainer.containerName}}
      </div>
      <div class="modal-body d-flex flex-column hci-cod-edit">
        <ng-container *ngFor="let attribute of editContainer.graphicalAttributes | isGroupAttribute: false">
          <hci-attribute-edit [id]="'edit-id-attribute-' + attribute.idAttribute"
                              [attribute]="attribute"
                              class="attribute"></hci-attribute-edit>
        </ng-container>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" (click)="close('Save')">Save</button>
        <button class="btn btn-primary" (click)="close('Cancel')">Cancel</button>
      </div>
    </ng-template>
  `
})
export class AttributeContainerComponent {
  @HostBinding("class") classList: string = "hci-attribute-configuration hci-cod d-flex flex-column flex-grow-1";

  @Input() idAttributeValueSet: number;
  @Input() idAttributeConfiguration: number;
  @Input() idAttributeContainer: number;
  @Input() indexOfContainer: number;
  @Input() idParentObject: number;

  @Input() attributeContainer: AttributeContainer;
  @Input() attributeConfiguration: AttributeConfiguration;
  @Input() attributeValueSet: AttributeValueSet;
  
  @Input() editInline: boolean = true;
  @Input() editPopup: boolean =  false;
  @Input() editable: boolean = true;

  editContainer: AttributeContainer;

  windowDimension: any = {};

  subscriptions: Subscription = new Subscription();
  
  getBusySubjects: Function = () => {
    let subjects: any[] = [];
  
    //subjects.push(this.attributeService.getAttributeConfigurationLoadingSubject());
    subjects.push(this.attributeService.getLoadingSubject());
    
    return subjects;
  };

  constructor(private attributeService: AttributeService,
              private elementRef: ElementRef,
              private renderer: Renderer2,
              private changeDetectorRef: ChangeDetectorRef,
              private modalService: NgbModal) {}

  /**
   * Upon init, subscribe to the configuration and value set.
   */
  ngOnInit() {
    if (! this.editable) {
      this.editInline = false;
      this.editPopup = false;
    }
    
    this.attributeService.setAttributeConfigurationById(this.idAttributeConfiguration, this.idAttributeValueSet, this.idParentObject);

    this.subscriptions.add(this.attributeService.getAttributeConfigurationSubject().subscribe((attributeConfiguration: AttributeConfiguration) => {
      if(attributeConfiguration) {
        this.attributeConfiguration = attributeConfiguration;

        if (this.idAttributeContainer !== undefined) {
          this.attributeContainer = this.getAttributeContainerById(this.idAttributeContainer);
        }

        if (this.indexOfContainer !== undefined) {
          if (this.attributeConfiguration.attributeContainers[this.indexOfContainer]) {
            this.attributeContainer = this.attributeConfiguration.attributeContainers[this.indexOfContainer];
          }
        }
      }
    }));

    this.subscriptions.add(this.attributeService.attributeConfigurationDimensionSubject.subscribe((windowDimension: any) => {
      if(windowDimension){
        this.windowDimension = windowDimension;
      }
    }));

    this.subscriptions.add(this.attributeService.getAttributeValueSet().subscribe((attributeValueSet: AttributeValueSet) => {
      this.attributeValueSet = attributeValueSet;

      if (this.attributeValueSet) {
        this.attributeService.notifyAttributes();
      }
    }));
  }

  @Input() set boundData(value: any) {
    this.attributeService.setBoundData(value);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (! this.editable) {
      this.editInline = false;
      this.editPopup = false;
    }
    
    if (changes.idAttributeContainer) {
      this.idAttributeContainer = changes.idAttributeContainer.currentValue;

      this.attributeContainer = this.getAttributeContainerById(this.idAttributeContainer);
    }

    if (changes.indexOfContainer) {
      this.indexOfContainer = changes.indexOfContainer.currentValue;
      if (this.attributeConfiguration && this.attributeConfiguration.attributeContainers[this.indexOfContainer]) {
        this.attributeContainer = this.attributeConfiguration.attributeContainers[this.indexOfContainer];
      }
    }
  }

  getAttributeService(): AttributeService {
    return this.attributeService;
  }

  edit(modal: NgbModalRef, editContainer: AttributeContainer): void {
    this.editContainer = editContainer;

    this.modalService.open(modal, {windowClass: "modal-lg"}).result.then((result) => {
      if (result === "Save") {
        this.attributeService.updateAttributeValueSet();
      } else if (result === "Cancel") {
        this.attributeService.clearUpdatedAttributeValues();
      }
    }, (reason) => {});
  }

  post(): void {
    this.attributeService.updateAttributeValueSet();
  }

  getAttributeContainerById(idAttributeContainer: number): AttributeContainer {
    if (this.attributeConfiguration) {
      return this.attributeConfiguration.attributeContainers.find(x => x.idAttributeContainer === idAttributeContainer);
    }
  }
}
