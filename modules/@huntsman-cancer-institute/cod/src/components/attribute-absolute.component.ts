import {Component,
  ElementRef,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from "@angular/core";

import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AttributeBase} from "./attribute-base";
import {AttributeService} from "../services/attribute.service";
import {NativeSelectComponent} from "@huntsman-cancer-institute/input";

/**
 * The attribute for absolute positioning.  This component has its width, height, x and y positioning set based upon
 * the attribute definition.
 */
@Component(
  {
    selector: "hci-attribute-absolute",
    encapsulation: ViewEncapsulation.None,
    template:
      `
        <div class="drag-select" style=""></div>

        <!-- Line -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'LINE'">
          <div #attributeRef
               class="d-flex flex-row cod-type-line i-height">
            <div class="d-flex cod-line mr-1 mt-auto mb-auto flex-shrink-0"></div>
            <div *ngIf="attribute.displayName" class="d-flex mr-1 cod-label flex-shrink-0">
              {{attribute.displayName}}
            </div>
            <div class="d-flex cod-line grow mt-auto mb-auto"></div>
          </div>
        </ng-container>

        <!-- Label -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'LABEL'">
          <div #inputRef #attributeRef
               class="d-flex flex-column">
            <div *ngIf="attribute.displayName" class="d-flex cod-label mt-auto mb-auto">
              {{attribute.displayName}}
            </div>
          </div>
        </ng-container>

        <!-- String -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'S'">
          <div #attributeRef
               class="d-flex flex-column cod-type-s i-height">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div *ngIf="(attribute.h <= 25)" class="d-flex flex-grow-1">
              <input
                #inputRef
                type="text"
                [ngModel]="attributeValues[0].valueString"
                (ngModelChange)="valueStringChange($event)"
                [disabled]="!editInline || attribute.isCalculated === 'Y'"
                class="form-control"/>
            </div>
            <div *ngIf="(attribute.h > 25)" class="d-flex flex-grow-1">
              <textarea
                #inputRef
                type="text"
                spellcheck="spellcheck"
                lang="en"
                [ngModel]="attributeValues[0].valueString"
                (ngModelChange)="valueStringChange($event)"
                [disabled]="!editInline || attribute.isCalculated === 'Y'"
                class="form-control">
              </textarea>
            </div>
          </div>
        </ng-container>

        <!-- Text -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'TXT'">
          <div #attributeRef
               class="d-flex flex-column cod-type-txt i-height">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-grow-1">
          <textarea
            #inputRef
            type="text"
            spellcheck="spellcheck"
            lang="en"
            [ngModel]="attributeValues[0].valueLongText.textData"
            (ngModelChange)="valueTextChange($event)"
            [disabled]="!editInline || attribute.isCalculated === 'Y'"
            class="form-control">
          </textarea>
            </div>
          </div>
        </ng-container>

        <!-- Checkbox -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'CB'">
          <div #attributeRef
               class="d-flex flex-row cod-type-cb i-height align-items-center">
            <div *ngIf="attribute.displayName" class="d-flex cod-right-label" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div #inputRef class="d-flex flex-grow-1 align-items-center mr-1">
              <input type="checkbox"
                     [checked]="attributeValues[0].valueString === 'Y'"
                     (change)="valueCheckboxChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     class="form-control"/>
            </div>
          </div>
        </ng-container>

        <!-- Numeric -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'N'">
          <div #attributeRef
               class="d-flex flex-column cod-type-n i-height">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-grow-1">
              <input #inputRef
                     type="number"
                     [ngModel]="attributeValues[0].valueNumeric"
                     (ngModelChange)="valueNumericChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     class="form-control"/>
            </div>
          </div>
        </ng-container>

        <!-- Integer -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'I'">
          <div #attributeRef
               class="d-flex flex-column cod-type-i i-height">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-grow-1">
              <input #inputRef
                     type="number"
                     [ngModel]="attributeValues[0].valueInteger"
                     (ngModelChange)="valueIntegerChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     class="form-control"/>
            </div>
          </div>
        </ng-container>

        <!-- Date -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'D'">
          <div #attributeRef
               class="d-flex flex-column cod-type-d i-height">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-grow-1">
              <input #inputRef
                     matInput
                     name="valueDate"
                     [(ngModel)]="attributeValues[0].valueDate"
                     (ngModelChange)="valueDateChange($event)"
                     [matDatepicker]="valueDate"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     class="form-control">
              <mat-datepicker-toggle matSuffix [for]="valueDate" class="cod-dp-toggle"></mat-datepicker-toggle>
              <mat-datepicker #valueDate></mat-datepicker>
            </div>
          </div>
        </ng-container>

        <!-- Date Time -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'DT'">
          <div #attributeRef
               class="d-flex flex-column cod-type-dt i-height">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-grow-1">
              <input #inputRef
                     matInput
                     name="valueDateTime"
                     class="form-control"
                     [(ngModel)]="attributeValues[0].date"
                     (ngModelChange)="valueDateChange($event)"
                     [disabled]="!editInline || attribute.isCalculated === 'Y'"
                     [ngxMatDatetimePicker]="dtpicker">
              <mat-datepicker-toggle matSuffix [for]="dtpicker" class="cod-dp-toggle"></mat-datepicker-toggle>
              <ngx-mat-datetime-picker #dtpicker [showSeconds]="true"></ngx-mat-datetime-picker>
            </div>
          </div>
        </ng-container>

        <!-- Boolean -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'B'">
          <div #attributeRef
               class="d-flex flex-column cod-type-b i-height">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-grow-1">
              <select #inputRef
                      [ngModel]="attributeValues[0].valueString"
                      (ngModelChange)="valueStringChange($event)"
                      [disabled]="!editInline || attribute.isCalculated === 'Y'"
                      class="form-control edit-renderer">
                <option [ngValue]="undefined"></option>
                <option [ngValue]="'Y'" [selected]="attributeValues[0].valueString === 'Y'">Yes</option>
                <option [ngValue]="'N'" [selected]="attributeValues[0].valueString === 'N'">No</option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Extended Boolean -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'EB'">
          <div #attributeRef
               class="d-flex flex-column cod-type-eb i-height">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-grow-1">
              <select #inputRef
                      [ngModel]="attributeValues[0].valueString"
                      (ngModelChange)="valueStringChange($event)"
                      [disabled]="!editInline || attribute.isCalculated === 'Y'"
                      class="form-control edit-renderer">
                <option [ngValue]="undefined"></option>
                <option [ngValue]="'Y'">Yes</option>
                <option [ngValue]="'N'">No</option>
                <option [ngValue]="'U'">Unknown</option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Choice: Single -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'AC' && attribute.isMultiValue === 'N'">
          <div #attributeRef
               class="d-flex flex-column cod-type-ac-n i-height">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-grow-1">
              <select #inputRef
                      [ngModel]="(attributeValues[0].valueAttributeChoice)?attributeValues[0].valueAttributeChoice.idAttributeChoice:undefined"
                      (ngModelChange)="valueChoiceChange($event)"
                      [disabled]="!editInline || attribute.isCalculated === 'Y'"
                      class="form-control edit-renderer">
                <option [ngValue]="undefined"></option>
                <option *ngFor="let attributeChoice of attributeChoices"
                        [ngValue]="attributeChoice.idAttributeChoice">
                  {{ attributeChoice.choice }}
                </option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Choice: Multi -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'AC' && attribute.isMultiValue === 'Y'">
          <div #attributeRef
               class="d-flex flex-column cod-type-ac-y i-height"
               style="height: inherit;">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-column y-auto" style="row-gap: 5px">
              <ng-container *ngFor="let attributeChoice of attributeChoices">
                <div class="d-flex flex-shrink-0">
                  <input type="checkbox"
                         [checked]="attributeChoice.value"
                         [disabled]="!editInline || attribute.isCalculated === 'Y'"
                         (change)="valueMultiChoiceChange(attributeChoice)"
                         [disabled]="!editInline || attribute.isCalculated === 'Y'"
                         class="form-control checkbox"/>
                  <div class="cod-label pl-1">
                    {{attributeChoice.choice}}
                  </div>
                </div>
              </ng-container>
            </div>
          </div>
        </ng-container>

        <!-- Dictionary: Multi -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'DICT' && attribute.isMultiValue === 'Y'">
          <div #attributeRef
               class="d-flex flex-column cod-type-ac-y i-height"
               style="height: inherit;">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-column y-auto" style="row-gap: 5px">
              <ng-container *ngFor="let entry of dictionaryEntries">
                <div class="d-flex flex-shrink-0">
                  <input type="checkbox"
                         [checked]="entry.checked"
                         [disabled]="!editInline || attribute.isCalculated === 'Y'"
                         (change)="valueMultiDictChange(entry)"
                         [disabled]="!editInline || attribute.isCalculated === 'Y'"
                         class="form-control checkbox" />
                  <div class="cod-label pl-1">
                    {{entry.display}}
                  </div>
                </div>
              </ng-container>
            </div>
          </div>
        </ng-container>

        <!-- Dictionary -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'DICT' && attribute.isMultiValue === 'N'">
          <div #attributeRef
               class="d-flex flex-column cod-type-dict i-height">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              {{attribute.displayName}}
            </div>
            <div class="d-flex flex-grow-1">
              <select #inputRef
                      [ngModel]="attributeValues[0].valueIdDictionary"
                      (ngModelChange)="valueDictChange($event)"
                      [disabled]="!editInline || attribute.isCalculated === 'Y'"
                      class="form-control edit-renderer">
                <option [ngValue]="undefined"></option>
                <option *ngFor="let entry of dictionaryEntries"
                        [ngValue]="entry.value">
                  {{ entry.display }}
                </option>
              </select>
            </div>
          </div>
        </ng-container>

        <!-- Grid -->
        <ng-container *ngIf="attribute.codeAttributeDataType === 'GA'">
          <div #attributeRef class="d-flex flex-column cod-type-ga i-height">
            <div *ngIf="attribute.displayName" class="d-flex cod-top-label mr-1" [style.width]="'max-content'">
              <div>
                {{attribute.displayName}}
              </div>
              <div *ngIf="editInline" style="margin-left: auto;" class="pl-2 pb-2">
                <button class="btn-ga" (click)="addGridRow(editGridModal, attribute.idAttribute)">
              <span class="ga-icon">
                <i class="fas fa-plus fa-xs"></i>
              </span>
                </button>
                <button class="btn-ga" (click)="removeGridRow(attribute.idAttribute)">
              <span class="ga-icon">
                <i class="fas fa-minus fa-xs"></i>
              </span>
                </button>
              </div>
            </div>
            <div class="d-flex flex-grow-1">
              <ag-grid-angular #gridComponent
                               class="ag-theme-balham"
                               (gridReady)="this.onGridReady($event)"
                               (modelUpdated)="onModelUpdated($event)"
                               (gridSizeChanged)="onGridSizeChanged($event)"
                               (rowDoubleClicked)="editGridRow(editGridModal, attribute.idAttribute, $event)"
                               [gridOptions]="this.gridOptions"
                               [rowSelection]="'single'"
                               [columnDefs]="gridColumns"
                               [rowData]="gridData"
                               [style.width]="attribute.w + 'px'"
                               [style.height]="attribute.h + 'px'">
              </ag-grid-angular>
            </div>
          </div>
        </ng-container>

        <ng-template #editGridModal let-close="close">
          <div class="modal-header">
            {{attribute.displayName}} Grid Row
          </div>
          <div class="modal-body d-flex flex-column hci-cod-edit">
            <ng-container *ngFor="let attribute of editGroupRowAttributes">
              <hci-attribute-edit [id]="'edit-id-attribute-' + attribute.idAttribute"
                                  [groupAttributeRowId]="editGroupAttributeRowId"
                                  [attribute]="attribute"
                                  class="attribute"></hci-attribute-edit>
            </ng-container>
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary" (click)="close('Save')">Save</button>
            <button class="btn btn-primary" (click)="close('Cancel')">Cancel</button>
          </div>
        </ng-template>
      `
    ,
    styles: [
      `
        .hci-cod button.mat-icon-button.mat-button-base {
          height: 20px;
          width: 20px;
          line-height: unset;
        }

        .btn-ga {
          padding: 0px;
          height: 18px;
          width: 18px;
        }

        .ga-icon {
          font-size: .9em;
          vertical-align: top;
        }

        .hci-cod .mat-datepicker-toggle-default-icon {
          height: 20px;
          width: 20px;
        }
      `
    ]
  }
)
export class AttributeAbsoluteComponent extends AttributeBase {

  @ViewChild("nativeSelectRef", {static: true})
  nativeSelectComponent: NativeSelectComponent;

  constructor(
    attributeService: AttributeService,
    elementRef: ElementRef,
    renderer: Renderer2,
    modalService: NgbModal
  ) {
    super(attributeService, elementRef, renderer, modalService);
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (this.editInline) {
      this.renderer.listen(this.elementRef.nativeElement, "mousemove", (event) => this.onMouseMove(event));
    }
  }

  ngAfterViewInit(): void {
    this.refresh();
  }

  /**
   * In the absolute case, upon refresh reset the position and size of the attribute.
   */
  refresh(): void {
    super.refresh();

    this.renderer.setStyle(this.elementRef.nativeElement, "position", "absolute");

    if (this.attribute.tabOrder !== undefined) {
      this.renderer.setStyle(this.elementRef.nativeElement, "z-index", 10 + this.attribute.tabOrder);
    }

    if (this.attribute.x !== undefined) {
      this.renderer.setStyle(this.elementRef.nativeElement, "left", this.attribute.x + "px");
    }
    if (this.attribute.y !== undefined) {
      if (this.attribute.codeAttributeDataType.toUpperCase() === "LINE") {
        // It appears the legacy metabuilder offset lines by 8 pixels
        this.renderer.setStyle(this.elementRef.nativeElement, "top", (this.attribute.y - 8) + "px");
      } else {
        this.renderer.setStyle(this.elementRef.nativeElement, "top", this.attribute.y + "px");
      }
    }

    // Checkboxes don't need a width, the select border should be the size of its label
    if (this.attribute.w !== undefined && this.attribute.codeAttributeDataType.toUpperCase() !== "CB") {
      this.renderer.setStyle(this.elementRef.nativeElement, "width", this.attribute.w + "px");
    }

    if (this.attribute.codeAttributeDataType === "LINE") {
      this.renderer.setStyle(this.elementRef.nativeElement, "height", "16px");
    } else if (this.attribute.h !== undefined) {
      if (!this.editInline) {
        this.renderer.setStyle(this.elementRef.nativeElement, "height", this.attribute.h + "px");
      }
      if (this.inputRef && this.inputRef.nativeElement) {
        this.renderer.setStyle(this.inputRef.nativeElement, "height", this.attribute.h + "px");
      }
      if (this.nativeSelectRef) {
        this.nativeSelectRef.setHeight(this.attribute.h);
      }
      if (this.attribute.isMultiValue === "Y") {
        this.renderer.setStyle(this.elementRef.nativeElement, "height", this.attribute.h + "px");
      }
    }
  }

  /**
   * Add cursor styling when the mouse is over the selected attribute.
   *
   * @param {MouseEvent} event
   */
  onMouseMove(event: MouseEvent): void {
    if (this.elementRef.nativeElement.classList.contains("selected")) {
      let sx: number = this.elementRef.nativeElement.getBoundingClientRect().left;
      let sy: number = this.elementRef.nativeElement.getBoundingClientRect().top;

      if (Math.abs(event.clientX - (sx + this.elementRef.nativeElement.offsetWidth)) <= 10) {
        this.renderer.addClass(this.elementRef.nativeElement, "e-resize");
        return;
      } else if (Math.abs(event.clientY - (sy + this.elementRef.nativeElement.offsetHeight)) <= 6) {
        this.renderer.addClass(this.elementRef.nativeElement, "n-resize");
        return;
      }
    }

    this.renderer.removeClass(this.elementRef.nativeElement, "e-resize");
    this.renderer.removeClass(this.elementRef.nativeElement, "n-resize");
  }
}
