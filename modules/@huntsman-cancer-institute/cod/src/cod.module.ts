/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {ModuleWithProviders, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";

import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule} from "@angular-material-components/datetime-picker";

import {AgGridModule} from "ag-grid-angular";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {DropdownModule, SelectModule} from "@huntsman-cancer-institute/input";
import {MiscModule} from "@huntsman-cancer-institute/misc";

import {AttributeConfigurationComponent} from "./components/attribute-configuration.component";
import {IsGroupAttributePipe} from "./pipes/is-group-attribute.pipe";
import {AttributeAbsoluteComponent} from "./components/attribute-absolute.component";
import {AttributeFlexComponent} from "./components/attribute-flex.component";
import {AttributeEditComponent} from "./components/attribute-edit.component";
import {AttributeContainerComponent} from "./components/attribute-container.component";
import {AttributeBase} from "./components/attribute-base";
import {AttributeService} from "./services/attribute.service";
import {DictionaryServiceModule} from "@huntsman-cancer-institute/dictionary-service";


/**
 * The main @huntsman-cancer-institute/cod module.  Custom components to be used by the grid are passed in here.
 *
 * @since 1.0.0
 */
@NgModule({
  imports: [
    AgGridModule,
    CommonModule,
    FormsModule,
    RouterModule,
    NgbModule,
    DropdownModule,
    SelectModule,
    MiscModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    DictionaryServiceModule
  ],
  declarations: [
    AttributeBase,
    AttributeConfigurationComponent,
    AttributeAbsoluteComponent,
    AttributeContainerComponent,
    AttributeFlexComponent,
    AttributeEditComponent,
    IsGroupAttributePipe
  ],
  exports: [
    AttributeConfigurationComponent,
    AttributeAbsoluteComponent,
    AttributeFlexComponent,
    AttributeEditComponent,
    AttributeContainerComponent,
    IsGroupAttributePipe
  ]
})
export class CodModule {
  static forRoot(): ModuleWithProviders<CodModule> {
    return {
      providers: [
        AttributeService
      ],
      ngModule: CodModule
    };
  }
}
