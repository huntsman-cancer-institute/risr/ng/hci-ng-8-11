import {ComponentFixtureAutoDetect, TestBed, async} from "@angular/core/testing";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {APP_BASE_HREF, CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";

import {} from "jasmine";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {DICTIONARY_ENDPOINT} from "@huntsman-cancer-institute/dictionary-service";
import {MiscModule} from "@huntsman-cancer-institute/misc";

import {CodModule} from "../cod.module";
import {AttributeConfigurationComponent} from "../components/attribute-configuration.component";
import {AttributeConfiguration} from "../model/attribute-configuration.entity";
import {AttributeValueSet} from "../model/attribute-value-set.entity";
import {AttributeContainer} from "../model/attribute-container.entity";
import {AttributeValue} from "../model/attribute-value.entity";
import {ATTRIBUTE_ENDPOINT} from "../services/attribute.service";
import {GraphicalAttribute} from "../model/graphical-attribute.entity";

describe("AttributeConfigurationComponent Tests", () => {

  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterModule.forRoot([]),
        CommonModule,
        FormsModule,
        NgbModule,
        MiscModule,
        CodModule
      ],
      providers: [
        {provide: ComponentFixtureAutoDetect, useValue: true},
        {provide: DICTIONARY_ENDPOINT, useValue: "/api/dictionaries"},
        {provide: ATTRIBUTE_ENDPOINT, useValue: "/api/attr/"},
        {provide: APP_BASE_HREF, useValue: "/"}
      ]
    });

    http = TestBed.get(HttpTestingController);
  });

  /*it("Component should have 1 container and 1 attribute rendered.", () => {
    let fixture = TestBed.createComponent(AttributeConfigurationComponent);
    let cmpt = fixture.componentInstance;

    cmpt.attributeConfiguration = <AttributeConfiguration>{
      idAttributeConfiguration: 1,
      attributeContainers: [
        <AttributeContainer>{
          idAttributeContainer: 1,
          idAttributeConfiguration: 1,
          containerName: "Test",
          graphicalAttributes: [
            <GraphicalAttribute>{
              idAttribute: 1,
              idGroupAttribute: undefined,
              attributeName: "test",
              displayName: "Test",
              reportingName: "Test",
              codeAttributeDataType: "S",
              isMultiValue: undefined,
              isAjcc: undefined,
              isCap: undefined,
              isActive: undefined,
              isCalculated: undefined,
              isRequired: undefined,
              defaultValue: undefined,
              idAjccParameterName: undefined,
              attributeDictionary: undefined,
              groupAttribute: undefined,
              attributes: undefined,
              attributeChoices: undefined,
              idAttributeContainer: 1,
              x: 0,
              y: 0,
              h: 20,
              w: 100,
              tabOrder: 1,
              altioFormat: undefined,
              dropFilterControlName: undefined,
              dropFilterControlProperty: undefined,
              dropFilterAttribute: undefined
            }
          ]
        }
      ]
    };

    cmpt.attributeValueSet = <AttributeValueSet>{
      idAttributeValueSet: 1,
      attributeValues: [
        <AttributeValue>{
          idAttributeValue: 1,
          idAttributeValueSet: 1,
          idAttribute: 1,
          valueString: "Test"
        }
      ]
    };

    let el: HTMLElement = fixture.nativeElement.querySelector("#id-attribute-container-1");
    expect(el).toBeDefined();

    el = fixture.nativeElement.querySelector("#id-attribute-1");
    expect(el).toBeDefined();
  });*/

});
