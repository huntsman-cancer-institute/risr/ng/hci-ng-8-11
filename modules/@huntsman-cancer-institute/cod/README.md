# Angular Configure on Demand

This module contains the components needed to dynamically generate and layout components as defined by our meta
attribute model (attr-persistence).

## COD
The main CodModule contains all the COD components for implementation by the application.  Each COD component instance
provides its own service.

## Editor
There is a sub directory here which is the editor which is itself a separate lazy loaded module.  This is intended to
be accessed via a route in (for example) CORE to edit any attribute configuration that the user has access to.  The editor
relies on the same attribute service and models in the main source.
