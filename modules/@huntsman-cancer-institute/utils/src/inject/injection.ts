import {ComponentFactoryResolver, ComponentRef, Type, ViewContainerRef} from "@angular/core";

/**
 * Helper classes that will dynamically create a component from a Type or string name.
 *
 * One important consideration is that types are uglified during production packing.  This means that if we search for
 * a type with a string, it won't find it because the type name is now obfuscated.  To get around this, I introduce a
 * new decorator that you must pass in the original type as a string.  The decorator then places that as the
 * "componentType" on the class.
 */
export class ComponentInjector {

  static getComponentRef(resolver: ComponentFactoryResolver, container: ViewContainerRef, type: Type<any> | string): ComponentRef<any> {
    let factory = null;
    let factories = null;

    try {
      if (typeof type === "string") {
        let factories = Array.from(resolver["_factories"].keys());
        let factoryClass = <Type<any>>factories.find((factory: any) => factory.componentType === type);
        if (!factoryClass) {
          factoryClass = <Type<any>>factories.find((factory: any) => factory.name === type);
        }
        factory = resolver.resolveComponentFactory(factoryClass);
      } else {
        factory = resolver.resolveComponentFactory(type);
      }
      return container.createComponent(factory);
    } catch (error) {
      console.error("getComponentRef: " + type);
      console.error(error);
    }
  }

  static getComponent(resolver: ComponentFactoryResolver, container: ViewContainerRef, type: Type<any> | string): any {
    let factory = null;
    if (typeof type === "string") {
      let factories = Array.from(resolver["_factories"].keys());
      let factoryClass = <Type<any>>factories.find((factory: any) => factory.componentType === type);
      if (!factoryClass) {
        factoryClass = <Type<any>>factories.find((factory: any) => factory.name === type);
      }
      factory = resolver.resolveComponentFactory(factoryClass);
    } else {
      factory = resolver.resolveComponentFactory(type);
    }
    let componentRef = container.createComponent(factory);
    return componentRef.instance;
  }

}
