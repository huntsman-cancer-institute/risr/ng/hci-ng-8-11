/**
 * We do a lot of dynamic component creation and use the class name to reference the component.  The problem is that
 * uglification obfuscates the component name so it no longer matches the component name stored as a string in the typescript
 * or database.  Use this decorator to auto specify the componentType on that class.  This is the standard by which
 * our injection util will find the appropriate component.
 *
 * @param {string} componentType
 * @returns {any}
 * @constructor
 */
export function ComponentType(componentType: string) {
  function decorator(target, n, descriptor) {
    target.componentType = componentType;
  };

  return <any>decorator;
}
