/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */

/**
 * A barrel file for the @huntsman-cancer-institute/utils package.
 *
 * @since 12.0.0
 */
export {ComponentType} from "./src/inject/component-type.decorator";
export {ComponentInjector} from "./src/inject/injection";
