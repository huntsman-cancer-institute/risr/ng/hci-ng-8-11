/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */

export {ReportModule} from "./src/report.module";
export {ReportingFrameworkComponent} from "./src/reporting-framework.component";
