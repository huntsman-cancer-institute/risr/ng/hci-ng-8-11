# Angular Reporting Framework

This package contains a component that is the main window
 for the reporting framework in angular. It will display the
 reports in one or multiple, tabbed lists. Dynamically display
 the parameters for the selected report, and calls the report
 when the run button is clicked.

The component takes a few inputs to set up, with one being optional.

    <hci-reporting-framework
        manageReportUrl="/artadmin/legacy/api/hci.artadmin.controller.ManageReports"
        commandPrefix="/artadmin/legacy/api/jsp"
        dictAPILeadIn="/artadmin/api/dictionaries"
        reportsByType="global"
    >
    </hci-reporting-framework>

The input reportsByType is optional (usual choices are "global"
and "patient"). If it is not specified,
and the Reports.xml specifies tabs, the framework will
look at either the "type" or "tab" attribute of the report
when matching a report to the Report Tabs. Otherwise,
it will only look at the new "tab" attribute.

At the moment, owners (Cancer Groups) and permissions
are handled entirely by the back end and the Security Advisor.

