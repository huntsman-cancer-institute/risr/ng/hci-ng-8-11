import {Injectable, TemplateRef} from "@angular/core";
import { MatDialogRef, MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { SpinnerDialogComponent } from "./spinner-dialog.component";

export enum DialogType {
    ERROR = "Error",
    WARNING = "Warning",
    ALERT = "Alert",
    SUCCESS = "Succeed",
    FAILED = "Failed",
    CONFIRM = "Confirm",
    INFO = "Info",
    VALIDATION = "Validation Error",
}

@Injectable()
export class DialogsService {

    private _spinnerDialogIsOpen: boolean = false;
    private spinnerWorkItemCount: number = 0;
    private checkingSpinnerWorkItems: boolean = false;

    public spinnerDialogRefs: MatDialogRef<SpinnerDialogComponent>[] = [];

    public get spinnerDialogIsOpen(): boolean {
        return this._spinnerDialogIsOpen;
    }

    constructor(private dialog: MatDialog) {
    }

    public startDefaultSpinnerDialog(): MatDialogRef<SpinnerDialogComponent> {
        return this.startSpinnerDialog("Please wait...", 3, 30);
    }

    public startSpinnerDialog(message: string, strokeWidth: number, diameter: number): MatDialogRef<SpinnerDialogComponent> {
        if (this._spinnerDialogIsOpen) {
            return null;
        }

        this._spinnerDialogIsOpen = true;

        let configuration: MatDialogConfig = new MatDialogConfig();
        configuration.data = {
            message: message,
            strokeWidth: strokeWidth,
            diameter: diameter
        };
        configuration.width = "13em";
        configuration.disableClose = true;

        let dialogRef: MatDialogRef<SpinnerDialogComponent> = this.dialog.open(SpinnerDialogComponent, configuration);

        dialogRef.afterClosed().subscribe(() => {
            this._spinnerDialogIsOpen = false;
        });

        this.spinnerDialogRefs.push(dialogRef);

        return dialogRef;
    }

    // Let there be an alternative, global way to stop all active spinner dialogs.
    public stopAllSpinnerDialogs(): void {
        this.spinnerWorkItemCount = 0;
        for (let dialogRef of this.spinnerDialogRefs) {
            setTimeout(() => {
                dialogRef.close();
            });
        }
        this.spinnerDialogRefs = [];
    }

    public addSpinnerWorkItem(): void {
        this.spinnerWorkItemCount++;
        if (!this.checkingSpinnerWorkItems) {
            this.checkingSpinnerWorkItems = true;
            setTimeout(() => {
                this.checkSpinnerWorkItems();
            });
        }
    }

    public removeSpinnerWorkItem(): void {
        this.spinnerWorkItemCount--;
        if (this.spinnerWorkItemCount < 0) {
            this.spinnerWorkItemCount = 0;
        }
        if (!this.checkingSpinnerWorkItems) {
            this.checkingSpinnerWorkItems = true;
            setTimeout(() => {
                this.checkSpinnerWorkItems();
            });
        }
    }

    private checkSpinnerWorkItems(): void {
        this.checkingSpinnerWorkItems = false;
        if (this.spinnerWorkItemCount) {
            if (!this._spinnerDialogIsOpen) {
                this.startDefaultSpinnerDialog();
            }
        } else if (this._spinnerDialogIsOpen) {
            this.stopAllSpinnerDialogs();
        }
    }

}
