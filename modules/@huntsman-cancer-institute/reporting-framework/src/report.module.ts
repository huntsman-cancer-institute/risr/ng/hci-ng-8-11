import {NgModule} from "@angular/core";
import {CommonModule, DatePipe} from "@angular/common";

import {ReportingFrameworkComponent} from "./reporting-framework.component";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {ReportDatepickerComponent} from "./datepicker/report-datepicker.component";
import {FormatComponent} from "./format/format.component";
import {ParameterComponent} from "./parameter/parameter.component";
import {FormsModule} from "@angular/forms";
import {SelectModule} from "@huntsman-cancer-institute/input";
import {SpinnerDialogComponent} from "./spinner/spinner-dialog.component";
import {AgGridModule} from "ag-grid-angular";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { MatDialogModule } from "@angular/material/dialog";
import {DialogsService} from "./spinner/dialogs.service";
import {ResizableSectionDividerComponent} from "./resizable-divider/resizable-section-divider.component";
import {MatTabsModule} from "@angular/material/tabs";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    MatDatepickerModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    SelectModule,
    AgGridModule.withComponents([])
  ],
  providers: [
    DatePipe,
    DialogsService
  ],
  declarations: [
    ReportingFrameworkComponent,
    ReportDatepickerComponent,
    ParameterComponent,
    FormatComponent,
    SpinnerDialogComponent,
    ResizableSectionDividerComponent
  ],
  entryComponents: [
    ParameterComponent,
    FormatComponent,
    SpinnerDialogComponent
  ],
  exports: [
    ReportingFrameworkComponent
  ]
})
export class ReportModule {}
