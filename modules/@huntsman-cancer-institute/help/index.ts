/*
 *  Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A barrel file for the HCI ng2 dynamic help package
 *
 * @since 1.0.0
 */
export {HelpModule} from "./src/help.module"
export {HelpComponent} from "./src/help.component"
export {HelpService} from "./src/help.service"
export {HelpEntity} from "./src/help.entity"
export {TOOLTIP_BASE_ENDPOINT, HELP_BASE_ENDPOINT, HELP_CACHE_SIZE} from "./src/help.service"
