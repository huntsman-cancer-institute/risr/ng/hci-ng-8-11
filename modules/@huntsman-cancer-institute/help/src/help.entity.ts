/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A common entity intended to be used to define the api used in client/server interactions and as a transient client
 * representation. This entity is defined by a composite key that is made up of a server side entity identifier and an
 * application context enumeration. The help entity includes:
 * <ul>
 *   <li>tooltip - the content provided for tooltip hover</li>
 *   <li>modalTitle - the content provided for the modal help title</li>
 *   <li>modalBody - the content provided for the modal help body</li>
 * </ul>
 * <p>
 * @example <caption>Example of the {@code HelpEntity} JSON representation</caption>
 * <pre>
 *   {
 *     "tooltip":"foo",
 *     "modalTitle":"foo title",
 *     "modalBody":"foo body"
 *   }
 * </pre>
 *
 * @since 1.0.0
 */
export class HelpEntity {
  constructor(private tooltip?: string, private title?: string, private body?: string) {
  }

  public get Tooltip(): string {
    return this.tooltip;
  }

  public set Tooltip(tooltip: string) {
    this.tooltip = tooltip;
  }

  public get Title(): string {
    return this.title;
  }

  public set Title(title: string) {
    this.title = title;
  }

  public get Body(): string {
    return this.body;
  }

  public set Body(body: string) {
    this.body = body;
  }
}

export module HelpEntity {
  /**
   * An inner class to {@link HelpEntity} to describe the composite key of this entity which is made up of an {@code integer}
   * id and a {@link ApplicationContextEnum}.
   */
  export class Key {
    constructor(private id: number, private applicationContext: string) {
    }

    /**
     * An accessor for this {@link HelpEntity} object's server side identifier.
     *
     * @returns {number}
     * @constructor
     */
    public get Id(): number {
      return this.id;
    }

    /**
     * An accessor for this {@link HelpEntity} object's application context as described by the {@link ApplicationContextEnum}
     *
     * @returns {string}
     * @constructor
     */
    public get ApplicationContext(): string {
      return this.applicationContext;
    }

    /**
     * A method for deep comparison of two {@code} Key objects via JSON representation.
     *
     * @param that the key to compare to
     * @returns {boolean} true if equal, false otherwise
     */
    public equals(that: Key): boolean {
      return JSON.stringify(this) === JSON.stringify(that);
    }
  }
}
