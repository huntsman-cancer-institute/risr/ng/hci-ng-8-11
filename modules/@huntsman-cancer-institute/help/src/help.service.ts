/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Injectable, InjectionToken, Inject} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";

import { Observable, of, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";

import {ApplicationContextEnum} from "./application-context.enum";
import {HelpCache} from "./help-cache.class";
import {HelpEntity} from "./help.entity";

/**
 * @type {OpaqueToken} defining the base endpoint for the tooltip resource.
 */
export let TOOLTIP_BASE_ENDPOINT = new InjectionToken("tooltip_base_endpoint");

/**
 * @type {OpaqueToken} defining the base endpoint for the help resource
 */
export let HELP_BASE_ENDPOINT = new InjectionToken("help_base_endpoint");

/**
 * If set to null, the cache will be initialize with its default size.
 *
 * @type {OpaqueToken} defining the maximum size of the application help cache.
 */
export let HELP_CACHE_SIZE = new InjectionToken("help_cache_size");

/**
 * A service for accessing help and tooltip information from the server and maintaining the application client state as
 * it is related to this help detail. All server interactions related to the user should be encapsulated in this service
 * and this service should protect related state from unwanted modification.
 * <p>
 * This service leverages an application caching strategy that is intended to supplement any client caching that may exist.
 * This cache will, of course, be cleared if the client is refreshed, but, ideally, it should also be cleared when a users
 * session is terminated.
 * <p>
 * This service is expected to be injected with known server endpoints and. This injection can be achieved through the host
 * applications root module providers configuration.
 * <p>
 * @example <caption>Example of module configuration for {@code HelpService}</caption>
 * <pre>
 *   &commat;NgModule({
 *     imports: [BrowserModule, HttpModule],
 *     declarations: [DemoAppComponent],
 *     bootstrap: [DemoAppComponent],
 *     providers: [
 *       {provide: HELP_CACHE_SIZE, useValue: 20},
 *       {provide: TOOLTIP_BASE_ENDPOINT, useValue: "https://localhost:8080/core/api/tooltips"},
 *       {provide: HELP_BASE_ENDPOINT, useValue: "https://localhost:8080/core/api/helps"}
 *     ]
 *   })
 * </pre>
 *
 * @since 1.0.0
 */
@Injectable()
export class HelpService {
  /**
   * The generic error message used when a server error is thrown without a status.
   *
   * @type {string}
   */
  public static readonly GENERIC_ERR_MSG: string = "Failure interacting with dynamic help REST API";
  public static readonly INVALID_ERR_MSG: string = "Unexpected response received with status: ";

  /**
   * A readonly reference to the {@link ApplicationContextEnum}.
   *
   * @type {ApplicationContextEnum}
   */
  public readonly applicationContexts = ApplicationContextEnum;

  private _helpCache: HelpCache;

  constructor(private _http: HttpClient,
              @Inject(TOOLTIP_BASE_ENDPOINT) private _tooltipEndpoint: string,
              @Inject(HELP_BASE_ENDPOINT) private _helpEndpoint: string,
              @Inject(HELP_CACHE_SIZE) helpCacheSize?: number) {
    this._helpCache = helpCacheSize ? new HelpCache(helpCacheSize) : new HelpCache();
  }

  /**
   * Retrieves the tooltip given the submitted server side identifier and application context
   * (see {@link ApplicationContextEnum}). The resource will be retrieved from the server if it does not already exist in the
   * application or client cache.
   *
   * @param id of the tooltip resource
   * @param appCtxt of the tooltip resource
   * @returns {any}
   */
  public lookupTooltip(id: number, appCtxt: string): Observable<String> {
    let lookupKey: HelpEntity.Key = new HelpEntity.Key(id, this.applicationContexts[<any>appCtxt]);
    let cacheEntry: [HelpEntity.Key, HelpEntity] = this._helpCache.get(lookupKey);

    if (cacheEntry && cacheEntry[1].Tooltip) {
      return of(cacheEntry[1].Tooltip);
    } else {
      return this._http.get(this.buildTooltipEndpointUrl(id, appCtxt)).pipe(map((resp: any) => {
        this.cacheTooltip(resp.tooltip, cacheEntry ? cacheEntry[1] : null, lookupKey);
        return resp.tooltip;
      }, (error: HttpErrorResponse) => {
        this.handleError(error);
      }));
    }
  }

  /**
   * Retrieves the help detail given the submitted server side identifier and application context
   * (see {@link ApplicationContextEnum}). The resource will be retrieved from the server if it does not already exist in the
   * application or client cache.
   *
   * @param id of the tooltip resource
   * @param appCtxt of the tooltip resource
   * @returns {any}
   */
  public lookupHelp(id: number, appCtxt: string): Observable<any> {
    let lookupKey: HelpEntity.Key = new HelpEntity.Key(id, this.applicationContexts[<any>appCtxt]);
    let cacheEntry: [HelpEntity.Key, HelpEntity] = this._helpCache.get(lookupKey);

    if (cacheEntry && cacheEntry[1].Body) {
      return of(cacheEntry[1]);
    } else {
      return this._http.get(this.buildHelpEndpointUrl(id, appCtxt)).pipe(map((resp: any) => {
        this.cacheHelp(resp, cacheEntry ? cacheEntry[1] : null, lookupKey);
        return resp;
      }, (error: HttpErrorResponse) => {
        this.handleError(error);
      }));
    }
  }

  /**
   * Allows an idempotent update of help detail to the server. Specifically, this will update the title and body that is
   * displayed in the {@link HelpComponent} modal help dialog. This service method expects that the submitted user input
   * provided in {@link HelpEntity} has been sanitized by the component in form submission.
   *
   * @param id of the help resource
   * @param appCtxt of the help resource
   * @param helpDetail defines the complete help detail (title and body) regardless of whether one or both were changed
   * @returns {Observable<boolean>} true if submitted successfully
   */
  public submitHelpUpdate(id: number, appCtxt: string, helpDetail: HelpEntity): Observable<boolean> {
    return this._http.put(this.buildHelpEndpointUrl(id, appCtxt),
      JSON.stringify(helpDetail), {withCredentials: true}).pipe(map((resp: any) => {
      let lookupKey: HelpEntity.Key = new HelpEntity.Key(id, this.applicationContexts[<any>appCtxt]);

      this._helpCache.put(lookupKey, helpDetail);

      return true;
    }),
      catchError(this.handleError));
  }

  /**
   * Allows an idempotent update of a tooltip to the server. Specifically, this will update the tooltip that is
   * displayed in the {@link HelpComponent} when hovering on a help icon. This service method expects that the submitted
   * user input provided in {@link HelpEntity} has been sanitized by the component in form submission.
   *
   * @param id of the tooltip resource
   * @param appCtxt of the tooltip resource
   * @param helpDetail defines the complete tooltip, regardless of whether one or both were changed
   * @returns {Observable<boolean>} true if submitted successfully
   */
  public submitTooltipUpdate(id: number, appCtxt: string, helpDetail: HelpEntity): Observable<boolean> {
    return this._http.put(this.buildTooltipEndpointUrl(id, appCtxt),
      JSON.stringify(helpDetail), {withCredentials: true}).pipe(map((resp: Response) => {
      if (resp.status === 204) {
        let lookupKey: HelpEntity.Key = new HelpEntity.Key(id, this.applicationContexts[<any>appCtxt]);

        this._helpCache.put(lookupKey, helpDetail);

        return true;
      } else {
        throw new Error(HelpService.INVALID_ERR_MSG + resp.status);
      }
    }),
      catchError(this.handleError));
  }

  private buildHelpEndpointUrl(id: number, appCtxt: string) {
    return this._helpEndpoint + "/" + appCtxt + "/" + id;
  }

  private buildTooltipEndpointUrl(id: number, appCtxt: string) {
    return this._tooltipEndpoint + "/" + appCtxt + "/" + id;
  }

  private cacheHelp(json: any, helpEntity: HelpEntity, key: HelpEntity.Key): void {
    if (helpEntity) {
      helpEntity.Title = json.title;
      helpEntity.Body = json.body;
    } else {
      this._helpCache.put(key, new HelpEntity(
        null,
        json.title,
        json.body));
    }
  }

  private cacheTooltip(tooltip: string, helpEntity: HelpEntity, key: HelpEntity.Key): void {
    if (helpEntity) {
      helpEntity.Tooltip = tooltip;
    } else {
      this._helpCache.put(key, new HelpEntity(tooltip));
    }
  }

  private handleError(error: any): Observable<any> {
    let errMsg = (error.message) ? error.message : HelpService.GENERIC_ERR_MSG;

    return throwError(errMsg);
  }
}
