import {Directive, ElementRef, HostListener, Input, isDevMode, OnInit, Renderer2} from "@angular/core";

import {BehaviorSubject, interval, Subject} from "rxjs";
import {takeWhile} from "rxjs/operators";

import {NavigationGlobalService} from "../services/navigation-global.service";

/**
 * This directive allows you to auto calculate a property such as margin-top or top based on heights of other components.
 * Using [topMargin]="['header', 'sub-header']" [topMarginStyle]="'margin-top'" means that the margin-top of the component
 * with this directive placed on it will be equal to the height of the header plus sub-header.  The header and sub-header
 * and ids.
 *
 * @since 7.0.0
 */
@Directive({
  selector: "[topMargin]"
})
export class TopMarginDirective implements OnInit {

  @Input() topMargin: string[];
  @Input() topMarginStyle: string = "margin-top";
  @Input() topMarginDefault: number = 0;
  @Input() topMarginStyleAppend: string = "px";
  @Input() topMarginOffset: number = 0;
  @Input() topMarginTarget: string;

  private targetsFound: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private heightSubject: Subject<number> = new Subject<number>();

  constructor(private navigationGlobalService: NavigationGlobalService,
              private el: ElementRef,
              private renderer: Renderer2) {}

  ngOnInit() {
    if (isDevMode()) {
      console.debug("TopMarginDirective.ngOnInit: " + this.topMargin + ", " + this.topMarginStyle);
    }

    this.heightSubject.subscribe((height: number) => {
      this.setHeight(height);
    });

    this.targetsFound.subscribe((targetsFound: boolean) => {
      if (targetsFound) {
        this.getHeight();
      }
    });

    this.navigationGlobalService.getNotificationEventSubject().subscribe((event: boolean) => {
      this.findTargets();
    });

    this.heightSubject.next(this.topMarginDefault);

    this.findTargets();
  }

  /**
   * Sets the style to the sum of the heights.
   *
   * @param {number} height
   */
  setHeight(height: number): void {
    if (isDevMode()) {
      console.debug("TopMarginDirective.setHeight: " + height);
    }
    if (this.topMarginTarget) {
      let el: HTMLElement = this.el.nativeElement.querySelector("#" + this.topMarginTarget);

      if (el) {
        this.renderer.setStyle(el, this.topMarginStyle, height + this.topMarginStyleAppend);
      } else {
        el = this.el.nativeElement.querySelector("." + this.topMarginTarget);
        if (el) {
          this.renderer.setStyle(el, this.topMarginStyle, height + this.topMarginStyleAppend);
        } else if (isDevMode()) {
          console.warn("TopMarginDirective.setHeight: Could not find id or class target: " + this.topMarginTarget);
        }
      }
    } else {
      this.renderer.setStyle(this.el.nativeElement, this.topMarginStyle, height + this.topMarginStyleAppend);
    }
  }

  /**
   * Loops until the DOM is fully created.
   */
  findTargets(): void {
    let count: number = 0;
    this.targetsFound.next(false);
    interval(50)
      .pipe(takeWhile(() => !this.targetsFound.getValue() || count === 1000))
      .subscribe(() => {
        let exists: boolean = true;
        for (let target of this.topMargin) {
          if (!document.querySelector("#" + target)) {
            exists = false;
            break;
          }
        }
        if (exists) {
          this.targetsFound.next(true);
        }
        count = count + 1;
      });
  }

  /**
   * Finds the sum of the heights based on the components passed in.
   */
  getHeight() {
    if (isDevMode()) {
      console.debug("TopMarginDirective.getHeight");
    }

    if (this.topMargin && this.targetsFound) {
      let x: number = this.topMarginOffset;
      for (let target of this.topMargin) {
        let e: HTMLElement = <HTMLElement>document.querySelector("#" + target);
        if (e) {
          let o: number = 0;
          if (e.offsetHeight.toString().match("[a-z]")) {
            o = +e.offsetHeight.toString().replace("px", "").replace("rem", "").replace("em", "");
          } else {
            o = +e.offsetHeight.toString();
          }
          x = +x + o;
        }
      }

      this.heightSubject.next(x);
    }
  }

  @HostListener("window:resize", ["$event"])
  private windowResize(event) {
    this.getHeight();
  }
}
