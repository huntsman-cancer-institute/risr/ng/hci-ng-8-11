import {ChangeDetectorRef, Component, ComponentFactoryResolver, ElementRef, Input, Renderer2} from "@angular/core";
import {Subject} from "rxjs";
import {ComponentType} from "@huntsman-cancer-institute/utils";
import {NavComponent} from "./nav.component";
import {NavigationService} from "../services/navigation.service";

@ComponentType("LiNavComponent")
@Component(
  {
    selector: "hci-li",
    template:
      `
        <ng-container>
          <li [id]="id + '-li'" class="hci-li {{liClass}}" (click)="handleLiClick($event)">
            <a *ngIf="!href && !route" class="hci-li-a {{aClass}}">
              <i *ngIf="!imgSrc && iClass && !iRight" class="hci-li-i">
                <span [class]="iClass"></span>
                {{iText}}
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
              <i *ngIf="imgSrc && !iRight" class="hci-li-i-img">
                <img src="{{imgSrc}}" [class]="iClass" [class.hci-li-img]="true"/>
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
              <span class="hci-li-span {{spanClass}}">{{title}}</span>
              <i *ngIf="!imgSrc && iClass && iRight" class="hci-li-i">
                <span class="{{iClass}}"></span>
                {{iText}}
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
              <i *ngIf="imgSrc && iRight" class="hci-li-i-img">
                <img src="{{imgSrc}}" [class]="iClass" [class.hci-li-img]="true"/>
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
            </a>

            <a *ngIf="href" [href]="href" [target]="target" class="hci-li-a {{aClass}}">
              <i *ngIf="!imgSrc && iClass && !iRight" class="hci-li-i">
                <span [class]="iClass"></span>
                {{iText}}
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
              <i *ngIf="imgSrc && !iRight" class="hci-li-i-img">
                <img src="{{imgSrc}}" [class]="iClass" [class.hci-li-img]="true"/>
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
              <span class="hci-li-span {{spanClass}}">{{title}}</span>
              <i *ngIf="!imgSrc && iClass && iRight" class="hci-li-i">
                <span class="{{iClass}}"></span>
                {{iText}}
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
              <i *ngIf="imgSrc  && iRight" class="hci-li-i-img">
                <img src="{{imgSrc}}" [class]="iClass" [class.hci-li-img]="true"/>
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
            </a>

            <a *ngIf="route"
               routerLink="{{route}}"
               routerLinkActive="active-link"
               [queryParams]="queryParams"
               queryParamsHandling="{{queryParamsHandling}}"
               class="hci-li-a {{aClass}}">
              <i *ngIf="!imgSrc && iClass && !iRight" class="hci-li-i">
                <span [class]="iClass"></span>
                {{iText}}
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
              <i *ngIf="imgSrc && !iRight" class="hci-li-i-img">
                <img src="{{imgSrc}}" [class]="iClass" [class.hci-li-img]="true"/>
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
              <span class="{{spanClass}}">{{title}}</span>
              <i *ngIf="!imgSrc && iClass && iRight" class="hci-li-i">
                <span [class]="iClass"></span>
                {{iText}}
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
              <i *ngIf="imgSrc && iRight" class="hci-li-i-img">
                <img src="{{imgSrc}}" [class]="iClass" [class.hci-li-img]="true"/>
                <hci-badge *ngIf="badgeCount" [countSubject]="badgeCount" [countLoading]="badgeLoading"></hci-badge>
              </i>
            </a>
          </li>

          <p class="{{showValueClass}}" *ngIf="showValueFlag">
            {{showValue}}
          </p>
        </ng-container>
      `,
    styles: [
      `
        .hci-dic-li-l {
          float: left;
          border: 0 var(--greywarm-meddark) !important;
          padding: 0;
          margin: 0;
        }

        .hci-dic-li-r {
          float: right;
          border: 0 var(--greywarm-meddark) !important;
          padding-right: 10px;
          margin: 0;
        }

        .hci-dic-span-1 {
          border-radius: 5px;
          padding-right: 5px;
          padding-left: 2px !important;
          padding-top: 3px;
          background: linear-gradient(0deg, var(--bluewarm-dark) 50%, var(--bluewarm-meddark) 100%);
        }

        .hci-dic-span-3 {
          color: var(--pure-yellow);
          border-radius: 5px;
          padding-right: 5px;
          padding-left: 5px;
          padding-top: 3px;
          background: linear-gradient(0deg, var(--bluewarm-dark) 50%, var(--bluewarm-meddark) 100%);
        }

        #filterReadOnly {
          float: left;
        }

        .hci-dic-show-value {
          padding-left: 10px;
          padding-top: 3px;
          font-size: 15px !important;
          float: left;
        }
      `
    ]
  }
)
export class LiNavComponent extends NavComponent {

  @Input() liClass: string = "nav-item";
  @Input() href: string;
  @Input() target: string;
  @Input() route: string;
  @Input() queryParams: Object;
  @Input() queryParamsHandling: string = "merge";
  @Input() aClass: string = "nav-link";
  @Input() spanClass: string;
  @Input() iClass: string;
  @Input() imgSrc: string;
  @Input() iText: string;
  @Input() iRight: boolean = false;
  @Input() badgeCount: Subject<number>;
  @Input() badgeLoading: Subject<boolean>;
  @Input() showValueFlag: boolean = false;
  @Input() showValue: string = "";
  @Input() showValueClass: string = "";

  @Input() liClick: Function = () => {
  }

  constructor(elementRef: ElementRef, renderer: Renderer2, resolver: ComponentFactoryResolver, navigationService: NavigationService, changeDetectorRef: ChangeDetectorRef) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);
  }

  handleLiClick(event: Event) {
    this.liClick(this, this.navigationService, event);
  }

  setConfig(config) {
    super.setConfig(config);
  }

  updateConfig(config) {
    if (!config) {
      return;
    }

    if (config.liClass) {
      this.liClass = config.liClass;
    }

    if (config.href) {
      this.href = config.href;
    }

    if (config.target) {
      this.target = config.target;
    }

    if (config.route) {
      this.route = config.route;
    }

    if (config.queryParams) {
      this.queryParams = config.queryParams;
    }

    if (config.queryParamsHandling) {
      this.queryParamsHandling = config.queryParamsHandling;
    }

    if (config.aClass) {
      this.aClass = config.aClass;
    }

    if (config.spanClass) {
      this.spanClass = config.spanClass;
    }

    if (config.iClass) {
      if (this.iClass !== config.iClass) {
        this.iClass = undefined;
        this.detectChanges();
      }

      this.iClass = config.iClass;
    }

    if (config.imgSrc) {
      this.imgSrc = config.imgSrc;
    }

    if (config.iText) {
      this.iText = config.iText;
    }

    if (config.iRight !== undefined) {
      this.iRight = config.iRight;
    }

    if (config.liClick) {
      this.liClick = config.liClick;
    }

    if (config.badgeCount) {
      this.badgeCount = config.badgeCount;
    }

    if (config.badgeLoading) {
      this.badgeLoading = config.badgeLoading;
    }

    if (config.showValueFlag) {
      this.showValueFlag = config.showValueFlag;
    }

    if (config.showValue) {
      this.showValue = config.showValue;
    }

    if (config.showValueClass) {
      this.showValueClass = config.showValueClass;
    }

    super.updateConfig(config);
  }
}
