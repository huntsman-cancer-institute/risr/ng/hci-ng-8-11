import {Component, Input, SimpleChanges} from "@angular/core";

import {BehaviorSubject, Subscription} from "rxjs";

import {ComponentType} from "@huntsman-cancer-institute/utils";

@ComponentType("BadgeComponent")
@Component({
  selector: "hci-badge",
  template: `
    <div class="hci-badge" [class.spinner]="loading">{{(loading) ? "|" : ((count > 0) ? count : "")}}</div>
  `
})
export class BadgeComponent {

  public count: number;
  public loading: boolean = false;

  @Input() countSubject: BehaviorSubject<number>;
  @Input() countLoading: BehaviorSubject<boolean>;

  public countSubscription: Subscription;
  public countLoadingSubscription: Subscription;

  ngOnDestroy() {
    if (this.countSubscription) {
      this.countSubscription.unsubscribe();
    }
    if (this.countLoadingSubscription) {
      this.countLoadingSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["countSubject"]) {
      if (this.countSubscription) {
        this.countSubscription.unsubscribe();
      }

      if (this.countSubject) {
        this.countSubscription = this.countSubject.subscribe((count: number) => {
          this.count = count;
        });
      }
    }
    if (changes["countLoading"]) {
      if (this.countLoadingSubscription) {
        this.countLoadingSubscription.unsubscribe();
      }

      if (this.countLoading) {
        this.countLoadingSubscription = this.countLoading.subscribe((loading: boolean) => {
          this.loading = loading;
        });
      }
    }
  }
}
