import {
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ElementRef,
  EventEmitter,
  Input,
  isDevMode,
  Optional,
  Output,
  QueryList,
  Renderer2,
  SimpleChange,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewChildren
} from "@angular/core";

import {BehaviorSubject, Observable, of, Subject} from "rxjs";
import {delay, finalize} from "rxjs/operators";
import {NgbDropdown} from "@ng-bootstrap/ng-bootstrap";

import {HciDataDto, HciFilterDto, HciGridDto, HciGroupingDto, HciPagingDto, HciSortDto} from "hci-ng-grid-dto";
import {ComponentType} from "@huntsman-cancer-institute/utils";

import {NavComponent} from "../nav.component";
import {NavigationService} from "../../services/navigation.service";
import {RowGroup} from "./row-group";
import {NavigationGlobalService} from "../../services/navigation-global.service";
import {SearchListControllerComponent} from "./search-list-controller.component";

/**
 * Shows a list of items that can be grouped or not grouped.  The data can be generated from an array, or from an external
 * data call using jpa-services.
 */
@ComponentType("SearchListComponent")
@Component(
  {
    selector: "hci-search-list",
    styles: [
      `
        .results-count {
          background-color: var(--greywarm-meddark);
          color: white;
          font-size: 17.3px;
          padding-left: 18px;
        }

        .format-bottom-padding {
          padding-bottom: 25px;
        }

        :host::ng-deep div[id^='row-']:nth-child(even) {
          background-color: var(--white-medium) !important;
        }

        :host::ng-deep div[id^='row-'] {
          border: 0 !important;
        }

        :host::ng-deep div.row-parent.active {
          color: white !important;
          background-color: var(--bluewarmvivid-lightest) !important;
        }

        :host::ng-deep .ri-sidebar-cell {
          background-color: transparent !important;
        }
      `
    ],
    template:
      `
        <div *ngIf="showResultsCount && searchResultMessage"
          class="results-count" [ngClass]="{ 'format-bottom-padding': this.searchResultMessage === '' }">
          {{ this.searchResultMessage }}
        </div>

        <div class="d-flex scrollable">
          <div id="no-results-found"></div>
          <div id="search-title" class="d-flex flex-column">
            <div class="d-flex pl-3">
            </div>
          </div>
          <div id="result-list">
            <div class="d-flex flex-column" [class.y-hidden]="selectedRowGroup">
              <hci-busy [busySubjects]="loadingSubjects" parentSelector=".scrollable" [icon]="busyIcon"></hci-busy>
              <!-- No grouping -->
              <ng-container *ngIf="!grouping.fields || grouping.fields.length === 0">
                <ng-container *ngFor="let rowGroup of viewData">
                  <ng-container *ngIf="route">
                    <div #rowParent
                         [id]="getRowId(rowGroup.rgRows[0])"
                         class="row-parent nav-item"
                         [routerLink]="route(rowGroup, rowGroup.rgRows[0])"
                         routerLinkActive="active"
                         [routerLinkActiveOptions]="{changeDetection: rowRefresh}"
                         (click)="onRowClick($event, cmpt, rowGroup, rowGroup.rgRows[0])">
                      <ng-container *ngTemplateOutlet="itemTemplate; context: {row: rowGroup.rgRows[0]}"></ng-container>
                    </div>
                  </ng-container>
                  <ng-container *ngIf="!route">
                    <div #rowParent
                         [id]="getRowId(rowGroup.rgRows[0])"
                         class="row-parent nav-item"
                         (click)="onRowClick($event, cmpt, rowGroup, rowGroup.rgRows[0])">
                      <ng-container *ngTemplateOutlet="itemTemplate; context: {row: rowGroup.rgRows[0]}"></ng-container>
                    </div>
                  </ng-container>
                </ng-container>
              </ng-container>

              <!-- Grouping with inline expansion -->
              <ng-container *ngIf="grouping.fields && grouping.fields.length > 0 && !selectedRowGroup">
                <ng-container *ngFor="let rowGroup of viewData; let i = index;">
                  <div [id]="getRowGroupId(rowGroup)"
                       class="group-parent nav-item group {{groupClass}}"
                       (click)="onGroupClick($event, cmpt, rowGroup)">
                    <ng-container
                      *ngTemplateOutlet="groupTemplate; context: {rowGroup: rowGroup, cmpt: cmpt}"></ng-container>
                  </div>
                  <ng-container *ngIf="rowGroup.rgExpanded">
                    <div class="nav-container" (click)="onGroupClick($event, cmpt, rowGroup)">
                      <ng-container *ngFor="let row of rowGroup.rgRows">
                        <ng-container *ngIf="route">
                          <div #rowParent
                               [id]="getRowId(row)"
                               class="row-parent nav-item"
                               [routerLink]="route(rowGroup, row)"
                               routerLinkActive="active"
                               [routerLinkActiveOptions]="{changeDetection: rowRefresh}"
                               (click)="onRowClick($event, cmpt, rowGroup, row)">
                            <ng-container *ngTemplateOutlet="itemTemplate; context: {row: row}"></ng-container>
                          </div>
                        </ng-container>
                        <ng-container *ngIf="!route">
                          <div #rowParent
                               [id]="getRowId(row)"
                               class="row-parent nav-item"
                               (click)="onRowClick($event, cmpt, rowGroup, row)">
                            <ng-container *ngTemplateOutlet="itemTemplate; context: {row: row}"></ng-container>
                          </div>
                        </ng-container>
                      </ng-container>
                    </div>
                  </ng-container>
                </ng-container>
              </ng-container>

              <!-- Grouping with separate individual expansion -->
              <div *ngIf="grouping.fields && grouping.fields.length > 0 && selectedRowGroup"
                   #groupSelectedParent
                   class="group-selected-parent flex-column {{groupClass}}">
                <div [id]="getRowGroupId(selectedRowGroup)"
                     class="group-parent nav-item group active"
                     (click)="onGroupClick($event, cmpt, selectedRowGroup)">
                  <ng-container
                    *ngTemplateOutlet="groupSelectedTemplate; context: {rowGroup: selectedRowGroup, cmpt: cmpt}"></ng-container>
                </div>

                <div #selectedRowGroupContainer id="selected-group-container" class="d-flex flex-column y-auto">
                  <div id="innerRowGroupContainer" class="d-flex flex-column">
                    <ng-container *ngFor="let row of selectedRowGroup.rgRows">
                      <ng-container *ngIf="route">
                        <div #rowParent
                             [id]="getRowId(row)"
                             class="row-parent nav-item"
                             [routerLink]="route(rowGroup, row)"
                             routerLinkActive="active"
                             (click)="onRowClick($event, cmpt, rowGroup, row)">
                          <ng-container *ngTemplateOutlet="itemTemplate; context: {row: row}"></ng-container>
                        </div>
                      </ng-container>
                      <ng-container *ngIf="!route">
                        <div #rowParent
                             [id]="getRowId(row)"
                             class="row-parent nav-item"
                             (click)="onRowClick($event, cmpt, rowGroup, row)">
                          <ng-container *ngTemplateOutlet="itemTemplate; context: {row: row}"></ng-container>
                        </div>
                      </ng-container>
                    </ng-container>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Paging footer -->
        <div *ngIf="paging.pageSize > 0" class="d-flex flex-shrink-0 paging-footer justify-content-between pl-3 pr-3"
             style="min-height: 1rem; align-items: center;">
          <ng-container *ngIf="!selectedRowGroup">
            <div (click)="doPageFirst()" class="d-flex mt-1 mb-1" [class.disabled]="paging.page === 0"><i
              class="fas fa-fast-backward fa-xs"></i></div>
            <div (click)="doPagePrevious()" class="d-flex mt-1 mb-1" [class.disabled]="paging.page === 0"><i
              class="fas fa-backward fa-xs"></i></div>
            <div style="font-size: 0.8rem;">{{paging.page + 1}} of {{paging.numPages}}</div>
            <div (click)="doPageNext()" class="d-flex mt-1 mb-1" [class.disabled]="paging.page === paging.numPages - 1">
              <i
                class="fas fa-forward fa-xs"></i></div>
            <div (click)="doPageLast()" class="d-flex mt-1 mb-1" [class.disabled]="paging.page === paging.numPages - 1">
              <i
                class="fas fa-fast-forward fa-xs"></i></div>
          </ng-container>
        </div>

        <!-- Default template for a group -->
        <ng-template #defaultGroupTemplate let-rowGroup="rowGroup">
          <div class="d-flex flex-grow-1 pl-2" style="align-items: center;">
            <div class="font-weight-bold mr-auto">{{getGroupDisplay(rowGroup)}}</div>
            <ng-container class="d-flex mr-1" style="align-items: center;">
              <div *ngIf="rowGroup.rgExpanded === true; else plusTemplate">
                <i class="fas fa-minus fa-sm mr-1"></i>
              </div>
              <ng-template #plusTemplate>
                <div>
                  <i class="fas fa-plus fa-sm mr-1"></i>
                </div>
              </ng-template>
              ({{rowGroup.rgCount}})
            </ng-container>
          </div>
        </ng-template>

        <!-- Default template for an individual selected group -->
        <ng-template #defaultGroupSelectedTemplate let-rowGroup="rowGroup">
          <div class="d-flex flex-grow-1 pl-2" style="align-items: center;">
            <div class="mr-3">
              <i class="fas fa-level-up-alt" data-fa-transform="flip-h"></i>
            </div>
            <div class="font-weight-bold mr-auto">{{getGroupDisplay(rowGroup)}}</div>
          </div>
        </ng-template>
      `
  }
)
export class SearchListComponent extends NavComponent {

  searchResultMessage = "";

  // The data that is actually rendered.
  viewData: RowGroup[];

  // A reference to the search list controller if applicable.
  @Input() controller: SearchListControllerComponent;

  // An array of data.
  @Input("data") boundData: Object[];

  // A subject that represents an array of data.
  @Input("dataSubject") dataSubject: BehaviorSubject<Object[]>;

  // A function which takes in a HciGridDto and returns a HciDataDto which wraps an array of data.
  @Input("dataCall") externalDataCall: (dto: HciGridDto) => Observable<HciDataDto>;

  // An array of subjects with booleans to control the hci-busy overlaying the search-list.
  @Input() loadingSubjects: Subject<boolean>[];

  // An array of booleans to control the hci-busy overlaying the search-list.
  @Input() loading: boolean[] = [false];

  // If clicking a row changes a route, this function specifies the route to use.
  @Input() route: (rowGroup: any, row: any) => any[];

  // Expand the selected row group as its own screen, or show rows under selected row groups in the same view.
  @Input() inlineExpand: boolean = false;

  // Without any other metadata for what a row is, provide the field to be used for the id.
  // This is actually the field that has the (defined) rowId -- whatever it is under different
  // structures: idPatient, idMedicalEvent etc
  @Input() rowId: any;

  // Css class applied to the row group parent.
  @Input() groupClass: string = "";

  // Css class applied to the row parent.
  @Input() rowClass: string = "";

  // A template for how to display row group info if using the default template.  e.g. "lastName, firstName" field names get replaced with values.
  @Input() groupDisplay: string;

  // TODO: Reimplement
  @Input() closeOthers: boolean = true;

  // The list of fields to group by.
  @Input() groupingFields: string[] = [];

  // delimiter for getGroupDisplay
  @Input() delimiter: string = ", ";

  // TODO: Reimplement if we want users to be able to control grouping.
  @Input() groupByAllowedFields: string[] = [];

  // The page size of row groups or rows if no grouping.
  @Input() pageSize: number;

  // The page size of rows in a selected group.
  @Input() groupPageSize: number = 50;

  // The number of pages of rows to cache internally before an external call is made.
  @Input() groupCacheNumPages: number;

  // A reference to a parent dropdown if applicable.
  @Input() dropdown: NgbDropdown;

  //Which icon to use for the busy spinner
  @Input() busyIcon: string = "hci fa-core fa-spin";

  @Input() showResultsCount: boolean = false;

  // If no onGroupClick function is provided, this is used.  The overriding function should still call this.
  defaultGroupClick: (event: MouseEvent, cmpt: SearchListComponent, rowGroup: RowGroup) => void = (event: MouseEvent, cmpt: SearchListComponent, rowGroup: RowGroup) => {
    cmpt.setGroupExpanded(event, rowGroup);

    this.groupClick.emit({
      event: event,
      rowGroup: rowGroup
    });
  };

  @Input() onGroupClick: (event: MouseEvent, cmpt: SearchListComponent, rowGroup: RowGroup) => void;

  // If no onRowClick function is provided, this is used.  The overriding function should still call this.
  defaultRowClick: (event: MouseEvent, cmpt: SearchListComponent, rowGroup: RowGroup, row: any) => void = (event: MouseEvent, cmpt: SearchListComponent, rowGroup: RowGroup, row: any) => {
    if (this.dropdown) {
      this.dropdown.close();
    }

    this.selectedRow = row;

    this.rowClick.emit({
      event: event,
      row: row,
      rowGroup: rowGroup
    });

    event.stopPropagation();
  };
  @Input() onRowClick: (event: MouseEvent, cmpt: SearchListComponent, rowGroup: RowGroup, row: any) => void;

  @Input() groupTemplate: TemplateRef<any>;
  @Input() groupSelectedTemplate: TemplateRef<any>;
  @Input() itemTemplate: TemplateRef<any>;

  @Input() onGroupClose: Function;

  @Output() groupClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() rowClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() dataChange: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild("defaultGroupTemplate", {read: TemplateRef, static: true}) defaultGroupTemplate: TemplateRef<any>;
  @ViewChild("defaultGroupSelectedTemplate", {
    read: TemplateRef,
    static: true
  }) defaultGroupSelectedTemplate: TemplateRef<any>;

  selectedRow: any;
  selectedRowGroup: RowGroup;
  selectedRowGroupContainer: HTMLElement;
  innerRowGroupContainer: HTMLElement;

  // Grid DTO info needed to build external data requests.
  sorts: HciSortDto[] = [];
  filters: HciFilterDto[] = [];
  paging: HciPagingDto = new HciPagingDto(0, -1);
  grouping: HciGroupingDto = new HciGroupingDto([]);

  //globalRowGroupMap: Map<string, number> = new Map<string, number>();
  groupedDataSet = [];

  pagingSubject: Subject<HciPagingDto> = new BehaviorSubject<HciPagingDto>(this.paging);

  // A dummy number that increments based on selection changes.  Forces routerLinkActive to recheck.
  rowRefresh: number = 0;

  busy: boolean = false;

  lastScrollTop: number = -1;
  lastScrollPosition: number = 1;
  setScrollTop: boolean = false;

  cmpt: SearchListComponent = this;

  lastEvent: any;

  constructor(elementRef: ElementRef,
              renderer: Renderer2,
              resolver: ComponentFactoryResolver,
              navigationGlobalService: NavigationGlobalService,
              @Optional() navigationService: NavigationService,
              changeDetectorRef: ChangeDetectorRef) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);

    if (!navigationService) {
      this.navigationService = new NavigationService(navigationGlobalService);
    }

    if (this.navigationService.getId() === undefined) {
      this.isRoot = true;
      this.setConfig({});
    }
  }

  /**
   * Return the id for the row group based upon the group key or if undefined, the first row's id.  e.g. "row-group-bob-smith"
   *
   * @param {RowGroup} rowGroup
   * @returns {string}
   */
  getRowGroupId(rowGroup: RowGroup): string {
    if (rowGroup.rgKey) {
      return "row-group-" + rowGroup.rgKey;
    } else {
      let rowId: string = this.getRowId(rowGroup.rgRows[0]);
      return rowId.replace("row-", "row-group-");
    }
  }

  /**
   * Return the row id.  e.g. "row-1".
   *
   * @param row
   * @returns {string}
   */
  getRowId(row: any): string {
    if (this.rowId) {
      return "row-" + <string><any>row[this.rowId];
    } else {
      return "row-undefined";
    }
  }

  /**
   * On init, listen for loading subjects and listen for page changes.
   */
  ngOnInit(): void {
    super.ngOnInit();

    if (this.loadingSubjects) {
      this.loadingSubjects[0].subscribe((busy: boolean) => {
        this.busy = busy;
      });
    }

    this.pagingSubject.subscribe((paging: HciPagingDto) => {
      this.paging = paging;
      this.setData();
    });
  }

  /**
   * Called when inputs change.
   *
   * @param {SimpleChanges} changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
    if (changes["groupingFields"]) {
      let change: SimpleChange = <SimpleChange>changes["groupingFields"];
      if (change.currentValue) {
        this.grouping.setFields(change.currentValue);
      } else {
        this.grouping.setFields([]);
      }
    }
  }

  /**
   * Set data only when the component has fully initialized and is rendered.
   */
  checkInitialized() {
    if (this.afterViewInit) {
      this.initialized = true;
      this.updateConfig(this.config);
      this.postInit();

      if (this.dataSubject) {
        //May as well make it spin while processing, as well as loading. Otherwise it looks frozen
        if (this.loadingSubjects) {
          this.loadingSubjects[0].next(true);
        }

        this.dataSubject
          .pipe(delay(0))
          .subscribe((data: Object[]) => {
            this.boundData = data;
            this.setResultMessage(this.boundData);
            this.setData();

            //Now done loading
            if (this.loadingSubjects) {
              this.loadingSubjects[0].next(false);
            }
          });
      } else {
        this.setData();
      }
    }
  }

  /**
   * Upon new rowParent being set, add any classes if rowClass is defined.
   *
   * @param {QueryList<ElementRef>} rowParents
   */
  @ViewChildren("rowParent", {read: ElementRef}) set rowParents(rowParents: QueryList<ElementRef>) {
    if (rowParents && this.rowClass) {
      rowParents.forEach((e: ElementRef) => {
        let classes: string[] = this.rowClass.split(" ");
        for (let className of classes) {
          this.renderer.addClass(e.nativeElement, className);
        }
      });
      this.rowRefresh += 1;
    }
  }

  postInit(): void {
    if (isDevMode()) {
      console.debug("SearchListComponent.postInit: " + this.id);
    }

    if (this.getParent()) {
      this.controller = <SearchListControllerComponent>this.getParent().getComponentByType("SearchListControllerComponent");
    }

    if (this.controller) {
      if (isDevMode()) {
        console.debug("SearchListComponent: using controller");
      }

      this.controller.addSearchList(this.title);

      this.controller.filtersSubject.subscribe((filters: HciFilterDto[]) => {
        this.filters = filters;
        this.setData();
        this.resetPaging();
      });

      this.controller.sortsSubject.subscribe((sorts: HciSortDto[]) => {
        this.sorts = sorts;
        this.setData();
      });

      this.controller.selectedSearchListSubject.subscribe((title: string) => {
        if (isDevMode()) {
          console.debug("selectedSearchListSubject.subscribe: " + this.title + ", " + title);
        }

        let visible: boolean = true;

        if (this.title && title && this.title !== title) {
          visible = false;
        }

        if (visible) {
          this.renderer.setStyle(this.elementRef.nativeElement, "display", "flex");
        } else {
          this.renderer.setStyle(this.elementRef.nativeElement, "display", "none");
        }
      });
    }
  }

  /**
   * This function would reset paging and fires
   */
  resetPaging() {
    this.paging.setPage(0);
    this.pagingSubject.next(this.paging);
  }

  /**
   * Updates the config with options and re-fetches data.
   *
   * @param config
   */
  updateConfig(config) {
    if (isDevMode()) {
      console.debug("updateConfig");
    }

    if (config) {
      this.config = Object.assign({}, this.config, config);

      if (config.route) {
        this.route = config.route;
      }
      if (config.groupByAllowedFields) {
        this.groupByAllowedFields = config.groupByAllowedFields;
      }
      if (config.sorts) {
        this.sorts = config.sorts;
      }
      if (config.groupingFields) {
        this.groupingFields = config.groupingFields;
        this.grouping.setFields(config.groupingFields);
      }
      if (config.pageSize) {
        this.pageSize = config.pageSize;
        this.paging.setPageSize(config.pageSize);
      }
      if (config.groupDisplay) {
        this.groupDisplay = config.groupDisplay;
      }
      if (config.delimiter) {
        this.delimiter = config.delimiter;
      }
      if (config.closeOthers !== undefined) {
        this.closeOthers = config.closeOthers;
      }
      if (config.groupTemplate) {
        this.groupTemplate = config.groupTemplate;
        this.groupSelectedTemplate = config.groupTemplate;
      }
      if (config.groupSelectedTemplate) {
        this.groupSelectedTemplate = config.groupSelectedTemplate;
      }
      if (config.itemTemplate) {
        this.itemTemplate = config.itemTemplate;
      }
      if (config.data) {
        this.boundData = config.data;
        this.setResultMessage(this.boundData);
      }
      if (config.dataSubject) {
        this.dataSubject = config.dataSubject;
      }
      if (config.dataCall) {
        this.externalDataCall = config.dataCall;
      }
      if (config.onGroupClick) {
        this.onGroupClick = config.onGroupClick;
      }
      if (config.inlineExpand !== undefined) {
        this.inlineExpand = config.inlineExpand;
      }
      if (config.groupClass) {
        this.groupClass = config.groupClass;
      }
      if (config.rowClass) {
        this.rowClass = config.rowClass;
      }
      if (config.rowId) {
        this.rowId = config.rowId;
      }
      if (config.onGroupClick) {
        this.onGroupClick = config.onGroupClick;
      }
      if (config.onRowClick) {
        this.onRowClick = config.onRowClick;
      }
      if (config.loadingSubjects) {
        this.loadingSubjects = config.loadingSubjects;
      }
      if (config.groupPageSize) {
        this.groupPageSize = config.groupPageSize;
      }
      if (config.groupCacheNumPages) {
        if (config.groupCacheNumPages >= 2) {
          this.groupCacheNumPages = config.groupCacheNumPages;
        } else {
          console.warn("groupCacheNumPages must be 2 or more.");
        }
      }
      if (config.onGroupClose) {
        this.onGroupClose = config.onGroupClose;
      }
      if (config.busyIcon) {
        this.busyIcon = config.busyIcon;
      }

      if (config.showResultsCount) {
        this.showResultsCount = config.showResultsCount;
      }
    }

    if (!this.onGroupClick) {
      this.onGroupClick = this.defaultGroupClick;
    }
    if (!this.onRowClick) {
      this.onRowClick = this.defaultRowClick;
    }

    if (!this.itemTemplate) {
      console.warn("SearchListComponent: An itemTemplate must be provided!");
    }

    if (!this.groupTemplate) {
      this.groupTemplate = this.defaultGroupTemplate;
    }
    if (!this.groupSelectedTemplate) {
      this.groupSelectedTemplate = this.defaultGroupSelectedTemplate;
    }

    if (this.externalDataCall) {
      this.loadingSubjects = [new BehaviorSubject<boolean>(false)];
    }


    super.updateConfig(config);

    this.setData();
  }

  /**
   * Generates the data if an array, calls the data if external call.
   */
  setData(data?: Object[]): void {
    if (isDevMode()) {
      console.debug("setData");
    }

    if (data) {
      this.boundData = data;
      this.setResultMessage(this.boundData);
    }

    this.selectedRowGroup = undefined;
    this.selectedRowGroupContainer = undefined;

    if (this.externalDataCall) {
      if (!this.busy) {
        this.busy = true;
        this.loadingSubjects[0].next(true);

        this.externalDataCall(new HciGridDto(this.filters, this.sorts, this.paging, new HciGroupingDto(this.grouping.fields, this.grouping.fields && this.grouping.fields.length > 0)))
          .pipe(finalize(() => {
            this.loadingSubjects[0].next(false);
            this.busy = false;
          }))
          .subscribe((externalData: HciDataDto) => {
            this.generateData(externalData);
          });
      }
    } else {
      this.generateData();
    }
  }

  /**
   * Generates the data array that the interface uses to render (regardless of source).
   *
   * @param {HciDataDto} externalData
   */
  generateData(externalData?: HciDataDto): void {
    if (isDevMode()) {
      console.debug("generateData");
      console.debug(externalData);
    }

    if (externalData) {
      this.boundData = externalData.data;
      this.setResultMessage(this.boundData);
      this.paging.setDataSize(externalData.gridDto.paging.dataSize);

      // Create row groups.
      this.viewData = [];
      let rowGroup: RowGroup;

      for (let i = 0; i < this.boundData.length; i++) {
        rowGroup = new RowGroup(undefined, this.boundData[i], (externalData.dataCounts && externalData.dataCounts !== null) ? externalData.dataCounts[i] : 0);

        if (this.grouping.fields.length > 0) {
          for (let groupingField of this.grouping.fields) {
            rowGroup[groupingField] = this.boundData[i][groupingField];
          }
        }

        this.viewData.push(rowGroup);
      }
    } else {
      // Create and populate row groups.
      this.populateFromBoundData();
    }

    this.detectChanges();

    this.dataChange.emit(Object.assign({type: "none"}, this.lastEvent, {
      paging: this.paging,
      nViewData: this.viewData.length
    }));
  }

  /**
   * Generates the data if the data is an array (not external).
   */
  populateFromBoundData(): void {
    if (isDevMode()) {
      console.debug("populateFromBoundData");
      console.debug(this.boundData);
    }

    if (!this.boundData) {
      this.viewData = [];
      this.paging.setPage(0);
      return;
    }

    if (this.boundData.length === 0) {
      console.debug("Bound Data length found as 0");
      this.paging.setPage(0);
      this.paging.setNumPages(1);
      this.paging.setDataSize(0);
    }

    let preparedData = [];
    this.groupedDataSet = [];

    let rowGroupMap: Map<string, number> = new Map<string, number>();

    let doRowGrouping: boolean = this.grouping.fields && this.grouping.fields.length > 0;
    for (let row of this.boundData) {
      let groupKey: string;
      if (doRowGrouping) {
        for (let field of this.grouping.fields) {
          groupKey = (groupKey) ? groupKey + ":" + row[field] : row[field];
        }
      }

      if (this.filters && this.filters.length > 0) {
        let include: boolean = true;

        for (let filter of this.filters) {
          if (!filter.valid) {
            continue;
          } else if (this.controller.advancedSearch && this.controller.advancedSearch.length > 0 && !this.controller.advancedSearchAllowFilter) {
          //  Advanced search does not auto filter by default
            continue;
          } else {
            let filterValues = filter.getValue().split(",");
            let filterFields = filter.getField().split(" or ");

            let innerInclude: boolean = false;
            for (let field of filterFields) {
              if (row[field] && typeof row[field] == "number") {
                for (let value of filterValues) {
                  if (row[field] == value) {
                    innerInclude = true;
                    break;
                  }
                }
              } else if (row[field] && row[field] instanceof Date || filter.getDataType() == "date") {
                for (let value of filterValues) {
                  if (value instanceof Date) {
                    if ((new Date(row[field])).getTime() == (new Date(value)).getTime()) {
                      innerInclude = true;
                      break;
                    }
                  }
                }
              } else if (row[field] && typeof row[field] == "string") {
                // Check if it is multiple search strings
                for (let value of filterValues) {
                  if (typeof value == "string") {
                    if (row[field].toLowerCase().indexOf(value.toLowerCase()) !== -1) {
                      innerInclude = true;
                      break;
                    }
                  }
                }
              }
              if (innerInclude) {
                break;
              }
            }

            if (!innerInclude) {
              include = false;
            }

          }

          if(include) {
            break;
          }
        }

        if (!include) {
          continue;
        }
      }

      if (doRowGrouping) {
        if (rowGroupMap.get(groupKey) >= 0) {
          preparedData[rowGroupMap.get(groupKey)].rgRows.push(row);
        } else {
          rowGroupMap.set(groupKey, preparedData.length);

          let rowGroup: RowGroup = new RowGroup(groupKey, row);
          for (let field of this.grouping.fields) {
            rowGroup[field] = row[field];
          }

          preparedData.push(rowGroup);
        }
      } else {
        preparedData.push(new RowGroup(groupKey, row));
      }

      //this.globalRowGroupMap = rowGroupMap;
    }

    if (this.sorts.length > 0) {
      preparedData.sort((a: RowGroup, b: RowGroup) => {
        let v: number = 0;

        for (let sort of this.sorts) {
          if (this.isSortFieldGroupField(sort.getField())) {
            if (sort.getAsc()) {
              if (a[sort.getField()] < b[sort.getField()]) {
                v = -1;
              } else if (a[sort.getField()] > b[sort.getField()]) {
                v = 1;
              }
            } else {
              if (a[sort.getField()] > b[sort.getField()]) {
                v = -1;
              } else if (a[sort.getField()] < b[sort.getField()]) {
                v = 1;
              }
            }

            if (v != 0) {
              return v;
            }
          } else if (this.grouping.getFields().length === 0) {
            if (sort.getAsc()) {
              if (a.rgRows[0][sort.getField()] < b.rgRows[0][sort.getField()]) {
                v = -1;
              } else if (a.rgRows[0][sort.getField()] > b.rgRows[0][sort.getField()]) {
                v = 1;
              }
            } else {
              if (a.rgRows[0][sort.getField()] > b.rgRows[0][sort.getField()]) {
                v = -1;
              } else if (a.rgRows[0][sort.getField()] < b.rgRows[0][sort.getField()]) {
                v = 1;
              }
            }

            if (v != 0) {
              return v;
            }
          }
        }

        return v;
      });

      if (this.grouping.getFields().length > 0) {
        for (let rowGroup of preparedData) {
          rowGroup.rgRows.sort((a: Object, b: Object) => {
            let v: number = 0;

            for (let sort of this.sorts) {
              if (!this.isSortFieldGroupField(sort.getField())) {
                if (sort.getAsc()) {
                  if (a[sort.getField()] < b[sort.getField()]) {
                    v = -1;
                  } else if (a[sort.getField()] > b[sort.getField()]) {
                    v = 1;
                  }
                } else {
                  if (a[sort.getField()] > b[sort.getField()]) {
                    v = -1;
                  } else if (a[sort.getField()] < b[sort.getField()]) {
                    v = 1;
                  }
                }

                if (v != 0) {
                  return v;
                }
              }
            }

            return v;
          });
        }
      }
    }

    this.paging.setDataSize(preparedData.length);

    this.groupedDataSet = preparedData;

    if (this.paging.getPageSize() > 0) {
      this.viewData = [];
      for (var i = this.paging.getPage() * this.paging.getPageSize(); i < Math.min(preparedData.length, (this.paging.getPage() + 1) * this.paging.getPageSize()); i++) {
        this.viewData.push(preparedData[i]);
      }
    } else {
      this.paging.setDataSize(preparedData.length);
      this.viewData = preparedData;
    }

    for (let rowGroup of this.viewData) {
      rowGroup.rgCount = rowGroup.rgRows.length;
    }
  }


  /**
   * Return true if the sort field is a grouping field.
   *
   * @param sortField
   */
  isSortFieldGroupField(sortField: string): boolean {
    if (!this.grouping || !this.grouping.fields || this.grouping.fields.length === 0) {
      return false;
    }

    for (let groupField of this.grouping.fields) {
      if (sortField === groupField) {
        return true;
      }
    }
    return false;
  }

  /**
   * Select a group of data.  Call the first page of data upon expanding.  If already expanded, close and go back to the
   * list of groups.
   *
   * @param {RowGroup} rowGroup
   */
  setGroupExpanded(event: MouseEvent, rowGroup: RowGroup) {
    if (isDevMode()) {
      console.debug("setGroupExpanded: " + rowGroup.rgExpanded);
    }

    if (this.externalDataCall && !rowGroup.rgExpanded) {
      if (!this.busy) {
        this.busy = true;
        this.loadingSubjects[0].next(true);

        let groupFilters: HciFilterDto[] = this.filters.slice();
        for (let groupField of this.grouping.fields) {
          groupFilters.push(new HciFilterDto(groupField, "string", rowGroup[groupField], undefined, "E", true));
        }

        rowGroup.rgPageMin = 0;
        rowGroup.rgPageMax = 0;

        this.externalDataCall(new HciGridDto(groupFilters, this.sorts, new HciPagingDto(0, this.groupPageSize), new HciGroupingDto(this.grouping.fields, false)))
          .pipe(finalize(() => {
            this.loadingSubjects[0].next(false);
            this.busy = false;
          }))
          .subscribe((externalData: HciDataDto) => {
            rowGroup.rgRows = externalData.data;

            if (!this.inlineExpand) {
              this.selectedRowGroup = rowGroup;
            }

            this.detectChanges();
          });
      }
    } else if (!rowGroup.rgExpanded) {
      rowGroup.rgPageMin = 0;
      rowGroup.rgPageMax = 0;
      if (!this.inlineExpand) {
        this.selectedRowGroup = rowGroup;
      }
    }

    if (rowGroup.rgExpanded) {
      if (this.inlineExpand) {
        let groupSelectedParent: HTMLElement;
        if (event) {
          groupSelectedParent = <HTMLElement>(<HTMLElement>event.target).closest(".group-parent");
        } else {
          groupSelectedParent = <HTMLElement><unknown>document.getElementById(this.getRowGroupId(rowGroup));
          //groupSelectedParent.click();
        }
        this.renderer.removeClass(groupSelectedParent, "active");
      }

      of(null).pipe(delay(250)).subscribe(() => {
        this.selectedRowGroup = undefined;
        this.selectedRowGroupContainer = undefined;
      });

      if (this.onGroupClose) {
        this.onGroupClose();
      }
    } else if (!rowGroup.rgExpanded && this.inlineExpand) {
      let groupSelectedParent: HTMLElement;
      if (event) {
        groupSelectedParent = <HTMLElement>(<HTMLElement>event.target).closest(".group-parent");
      } else {
        groupSelectedParent = <HTMLElement><unknown>document.getElementById(this.getRowGroupId(rowGroup));
        //groupSelectedParent.click();
      }
      this.renderer.addClass(groupSelectedParent, "active");
    }

    if (isDevMode() && !this.inlineExpand) {
      console.debug("this.selectedRowGroup");
      console.debug(this.selectedRowGroup);
    }

    rowGroup.rgExpanded = !rowGroup.rgExpanded;

    this.detectChanges();
  }

  /**
   * Add the scroll listener to the group container.
   */
  addGroupScrollListener(): void {
    this.removeGroupScrollListener();

    this.lastScrollTop = -1;

    if (this.selectedRowGroup && this.selectedRowGroup.rgCount !== this.selectedRowGroup.rgRows.length) {
      this.selectedRowGroupContainer = this.elementRef.nativeElement.querySelector("#selected-group-container");
      this.selectedRowGroupContainer.addEventListener("scroll", this.onGroupScroll.bind(this), true);
    }
  }

  /**
   * Remove the scroll listener from the group container.
   */
  removeGroupScrollListener(): void {
    if (this.selectedRowGroupContainer) {
      this.selectedRowGroupContainer.removeEventListener("scroll", this.onGroupScroll.bind(this));
    }
  }

  /**
   * Listen for the scroll event, determine if the scroll hit the top or bottom of the container.
   *
   * @param {Event} event
   */
  onGroupScroll(event: Event): void {
    if (!this.busy) {
      let newScrollTop: number = Math.round(this.selectedRowGroupContainer.scrollTop);
      let scrollPosition: number = 1;
      if (newScrollTop === 0 && scrollPosition !== 0) {
        scrollPosition = 0;
      } else if (scrollPosition !== 2 && Math.abs(this.selectedRowGroupContainer.scrollHeight - (newScrollTop + this.selectedRowGroupContainer.offsetHeight)) < 3) {
        scrollPosition = 2;
      } else {
        scrollPosition = 1;
      }

      if (this.lastScrollPosition !== scrollPosition && this.lastScrollPosition === 1) {
        if (scrollPosition === 0) {
          this.groupPreviousPage();
        } else if (scrollPosition === 2) {
          this.groupNextPage();
        }
      }

      this.lastScrollTop = newScrollTop;
      this.lastScrollPosition = scrollPosition;
    }
  }

  /**
   * When scrolling hits the top, load the previous page of group data.
   */
  groupPreviousPage(): void {
    if (this.selectedRowGroup.rgPageMin === 0) {
      console.warn("First page already loaded.");
      return;
    }

    this.removeGroupScrollListener();

    let groupFilters: HciFilterDto[] = this.filters.slice();
    for (let groupField of this.grouping.fields) {
      groupFilters.push(new HciFilterDto(groupField, "string", this.selectedRowGroup[groupField], undefined, "E", true));
    }

    if (!this.busy) {
      this.busy = true;
      this.loadingSubjects[0].next(true);
      this.externalDataCall(new HciGridDto(groupFilters, this.sorts, new HciPagingDto(this.selectedRowGroup.rgPageMin - 1, this.groupPageSize), new HciGroupingDto(this.grouping.fields, false)))
        .pipe(finalize(() => {
          this.loadingSubjects[0].next(false);
          this.busy = false;

          of(undefined).pipe(delay(50)).subscribe(() => {
            if (this.selectedRowGroupContainer.scrollTop === 0) {
              this.renderer.setStyle(this.selectedRowGroupContainer, "scrollTop", "1");
            }
          });

          this.addGroupScrollListener();
        }))
        .subscribe((externalData: HciDataDto) => {
          this.selectedRowGroup.rgRows = externalData.data.concat(this.selectedRowGroup.rgRows);
          this.selectedRowGroup.rgPageMin -= 1;

          if (this.groupCacheNumPages && this.selectedRowGroup.rgPageMax - this.selectedRowGroup.rgPageMin >= this.groupCacheNumPages) {
            this.selectedRowGroup.rgRows.splice(this.groupCacheNumPages * this.groupPageSize);
            this.selectedRowGroup.rgPageMax -= 1;
          }
        });
    }
  }

  /**
   * When scrolling hits the bottom, load the next page of group data.
   */
  groupNextPage(): void {
    if ((this.selectedRowGroup.rgPageMax + 1) * this.groupPageSize >= this.selectedRowGroup.rgCount) {
      console.warn("All rows already loaded");
      return;
    }

    this.removeGroupScrollListener();

    let groupFilters: HciFilterDto[] = this.filters.slice();
    for (let groupField of this.grouping.fields) {
      groupFilters.push(new HciFilterDto(groupField, "string", this.selectedRowGroup[groupField], undefined, "E", true));
    }

    if (!this.busy) {
      this.busy = true;
      this.loadingSubjects[0].next(true);
      this.externalDataCall(new HciGridDto(groupFilters, this.sorts, new HciPagingDto(this.selectedRowGroup.rgPageMax + 1, this.groupPageSize), new HciGroupingDto(this.grouping.fields, false)))
        .pipe(finalize(() => {
          this.loadingSubjects[0].next(false);
          this.busy = false;

          of(undefined).pipe(delay(50)).subscribe(() => {
            if (this.selectedRowGroupContainer.scrollTop === 0) {
              this.renderer.setStyle(this.selectedRowGroupContainer, "scrollTop", "1");
            }
          });

          this.addGroupScrollListener();
        }))
        .subscribe((externalData: HciDataDto) => {
          this.selectedRowGroup.rgRows = this.selectedRowGroup.rgRows.concat(externalData.data);
          this.selectedRowGroup.rgPageMax += 1;

          if (this.groupCacheNumPages && this.selectedRowGroup.rgPageMax - this.selectedRowGroup.rgPageMin >= this.groupCacheNumPages) {
            this.selectedRowGroup.rgRows.splice(0, this.groupPageSize);
            this.selectedRowGroup.rgPageMin += 1;
          }
        });
    }
  }

  /**
   * Go to the first page.
   */
  public doPageFirst(): void {
    if (!this.busy) {
      this.paging.setPage(0);
      this.pagingSubject.next(this.paging);
      this.checkActiveRowId();
    }
  }

  /**
   * Go to the previous page.
   */
  public doPagePrevious(): void {
    if (!this.busy) {
      if (this.paging.getPage() > 0) {
        this.paging.setPage(this.paging.getPage() - 1);
        this.pagingSubject.next(this.paging);
        this.checkActiveRowId();
      }
    }
  }

  /**
   * Go to the next page.
   */
  public doPageNext(): void {
    if (!this.busy) {
      if (this.paging.getPage() < this.paging.getNumPages() - 1) {
        this.paging.setPage(this.paging.getPage() + 1);
        this.pagingSubject.next(this.paging);
        this.checkActiveRowId();
      }
    }
  }

  /**
   * Go to the last page.
   */
  public doPageLast(): void {
    if (!this.busy) {
      this.paging.setPage(this.paging.getNumPages() - 1);
      this.pagingSubject.next(this.paging);
      this.checkActiveRowId();
    }
  }

  /**
   * After page turns, check if the page has the row that need to be highlighted
   */
  private checkActiveRowId(): void {
    //if a row is selected, and rowId field is specified (usually configured by config)
    if (this.selectedRow && this.rowId) {
      let id = this.selectedRow[this.rowId];
      if (id) {
        this.activateRowById(id);
      }
    }
  }

  /**
   * Concat group by fields to comma delimited words
   * or delimiter delimited words if the deliminator has its default value set.
   *
   * @param {RowGroup} rowGroup
   * @returns {string}
   */
  getGroupDisplay(rowGroup: RowGroup): string {
    let display: string;

    if (this.groupDisplay) {
      display = this.groupDisplay;
      for (let field of this.grouping.fields) {
        display = display.replace(field, rowGroup[field]);
      }
    } else {
      for (let field of this.grouping.fields) {
        if (rowGroup[field] !== null && rowGroup[field] !== "") {
          display = (display) ? display + this.delimiter + rowGroup[field] : rowGroup[field];
        }
      }
    }

    return display;
  }

  /**
   * Sets the selectedRowGroupContainer element.
   *
   * @param {ElementRef} selectedRowGroupContainer
   */
  @ViewChild("selectedRowGroupContainer", {
    read: ElementRef,
    static: false
  }) set selectedRowGroupContainerSetter(selectedRowGroupContainer: ElementRef) {
    if (this.selectedRowGroupContainer && this.setScrollTop) {
      this.selectedRowGroupContainer.scrollTop = (<HTMLElement>this.selectedRowGroupContainer.querySelector("#innerRowGroupContainer")).offsetHeight;
      this.setScrollTop = false;
    }

    if (!this.selectedRowGroupContainer && selectedRowGroupContainer) {
      this.selectedRowGroupContainer = selectedRowGroupContainer.nativeElement;

      let groupSelectedParent: HTMLElement = <HTMLElement>this.selectedRowGroupContainer.closest(".group-selected-parent");
      this.renderer.setStyle(groupSelectedParent, "display", "flex");

      of(null).pipe(delay(50)).subscribe(() => {
        this.renderer.addClass(groupSelectedParent, "active");
        this.innerRowGroupContainer = this.selectedRowGroupContainer.querySelector("#innerRowGroupContainer");
        this.renderer.setStyle(this.selectedRowGroupContainer, "height", this.innerRowGroupContainer.offsetHeight + "px");
        this.renderer.setStyle(this.innerRowGroupContainer, "min-height", (this.innerRowGroupContainer.offsetHeight + 30) + "px");
        this.renderer.setStyle(this.selectedRowGroupContainer, "scrollTop", "1");

        this.addGroupScrollListener();
      });
    }
  }

  /**
   * Allow a custom row click function to control deselection.
   */
  deselectAllRows(): void {
    let es: HTMLElement[] = this.elementRef.nativeElement.querySelectorAll(".row-parent");
    for (let e of es) {
      this.renderer.removeClass(e, "active");
    }
  }

  /**
   * Allow a custom group click function to control deselection.
   */
  deselectAllGroups(): void {
    let es: HTMLElement[] = this.elementRef.nativeElement.querySelectorAll(".group-parent");
    for (let e of es) {
      this.renderer.removeClass(e, "active");
    }
  }

  /**
   * Allow a custom row click function to select a row.
   *
   * @param {string} id
   */
  activateRowById(id: string): void {
    let e: HTMLElement = this.elementRef.nativeElement.querySelector("#row-" + id);
    if (e) {
      this.renderer.addClass(e, "active");
    }
  }

  activateGroupById(id: string): void {
    let e: HTMLElement = this.elementRef.nativeElement.querySelector("#row-group-" + id);
    if (e) {
      this.renderer.addClass(e, "active");
    }
  }

  /**
   * This function will return whether a particular group is expanded or collapsed
   * @param rowGroup
   */
  getGroupExpandedIndicator(rowGroup: RowGroup) {
    if (rowGroup.rgExpanded === true) {
      return "minus";
    } else {
      return "plus";
    }
  }

  //TODo : Handle the case where the user uses a custom groupTemplate
  /**
   * This function will cause SearchListComponent to navigate to a page and highlight the given group item, or
   * the row (in-case of non-grouped list)
   * This
   * @param groupId : This should contain a string to uniquely identify a particular group-display
   * @param rowId : This is the actual rowId for the row to be selected.
   */
  navigateToPageContainingElement(groupId: string, rowId: string) {

    //Introducing the additional check below to address CCRUI-1163.
    if (!this.boundData || this.boundData.length === 0) {
      return;
    }
    // Check if grouped
    let rowGrouped: boolean = this.grouping.fields && this.grouping.fields.length > 0;

    if (rowGrouped) {
      this.groupedDataSet.forEach((rowGroup, index) => {
        let groupDisplay = this.getGroupDisplay(rowGroup);
        if (groupDisplay.includes(groupId)) {
          //Set the page to the index
          if (!this.busy) {
            let page = Math.floor(index / this.paging.getPageSize());
            if (this.paging.getNumPages() - 1 >= page) {
              this.paging.setPage(page);
              this.pagingSubject.next(this.paging);
              //this.defaultGroupClick(null, this, rowGroup);
              let groupSelectedParent = <HTMLElement><unknown>document.getElementById(this.getRowGroupId(rowGroup));
              groupSelectedParent.click();
              this.deselectAllRows();
              this.activateRowById(rowId.replace("row-", ""));
            }
          }
        }
      });
    } else {
      this.boundData.forEach((row, index) => {
        if (this.getRowId(row) === rowId) {
          if (!this.busy) {
            let page = Math.floor(index / this.paging.getPageSize());
            if (this.paging.getNumPages() - 1 >= page) {
              this.paging.setPage(page);
              this.pagingSubject.next(this.paging);
              this.activateRowById(rowId.replace("row-", ""));
            }
          }
        }
      });
    }
  }

  private setResultMessage(data: Object[]) {
    if (this.showResultsCount && data && data.length > 0) {
      this.searchResultMessage = data.length + " result(s)";
    } else {
      this.searchResultMessage = "";
    }
  }
}
