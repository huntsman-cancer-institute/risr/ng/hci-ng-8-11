import {
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  isDevMode,
  Optional,
  Output,
  Renderer2,
  SimpleChanges
} from "@angular/core";

import {BehaviorSubject, Subject} from "rxjs";

import {HciFilterDto, HciSortDto} from "hci-ng-grid-dto";
import {ComponentType} from "@huntsman-cancer-institute/utils";

import {NavComponent} from "../nav.component";
import {NavigationService} from "../../services/navigation.service";
import {NavigationGlobalService} from "../../services/navigation-global.service";
import {HttpClient} from "@angular/common/http";


/**
 * The search list controller renders as a header to one or more search lists.  Only one search list can be shown at a time.
 * The controller features the ability to handle filtering and sorting.  Without the controller, the search list can exist,
 * but it can only handle paging internally.
 */
@ComponentType("SearchListControllerComponent")
@Component(
  {
    selector: "hci-search-list-controller",
    exportAs: "searchListControllerComponent",
    styles: [
      `
        select {
          padding: .25em;
          padding-right: 0em;
          outline-offset: -1px;
          border: 0;
          color: #8A9499;
          font-size: 0.8rem;
          background-color: white;
        }

        .closeable {
        }

        .search-box {
          border: none;
          border-right: 0.1px solid #5B6266;
          padding-left: .5rem !important;
          color: #8A9499;
          font-size: 0.8rem;
        }

        .search-options {
          /*width: 65vw;*/
          border-radius: 10px;
        }

        .search-button {
          width: 30%;
          height: 2rem;
          background-image: linear-gradient(#6DA9C7, #3D7A99);
          margin: 0;
          color: white;
          border: none;
        }

        .global-search {
          align-items: center;
          width: 100%;
          border-radius: 2px;
          border: 0.04em solid #727b80;
        }

        .tab-title-class {
          display: flex;
          height: 50px;
          background: lightgray;
        }

        .tab-pane {
          width: min-content !important;
        }

        .search-content-body {

        }

        .basic-search {

        }

        .advanced-search {

        }

        .search-options.expanded {
          height: fit-content;
        }

        .classification-header {

        }

        .button {

        }

        .alignBtn {

        }

        .advancedSearch-alignBtn {

        }

        .divider {

        }

        .advancedSearch-scroll {

        }

        .backButton .fa-angle-double-left {
          color: white;
        }

        .svg-inline--fas fa-angle-double-left {
          color: white;
        }

        .sort-item {
          display: flex;
          color: black;
        }

        .sort-icon {
          width: 1.5rem;
        }

        .sort-text:hover {
          cursor: pointer;
        }

        .hci-white {
          color: white !important;
        }

        .textarea-search-box {
          align-content: center;
          width: 100%;
          min-height: 30px;
        }

        .multiline-search-button {
          height: 100%;
          max-width: 3rem;
          min-width: 3rem;
        }

        .filter-sorting-panel {
          margin-top: 30px;
        }

      `
    ],
    template:
      `
        <div id="search-title" class="d-flex flex-column">
          <div *ngIf="showTitle" class="d-flex pl-2">
<!--  Comment this out due to no usage
            <div *ngIf="showBackButton" class="mr-auto ml-1 backButton">
              <button (click)="backButtonClicked()">
                <i class="hci-white fas fa-angle-double-left"></i>
              </button>
            </div>
-->
            {{title}}
            <div class="ml-auto mr-2" (click)="toggleShowOptions($event)">
              <i class="fas fa-cog"></i>
            </div>
            <div class="search-options" [class.expanded]="showOptions" [ngStyle]="{'width': (searchOptionsWidth !== '') ? searchOptionsWidth : 'auto'}">
              <div class="flex-grow-1">
                <div *ngIf="showToggleOptions">
                  <div class="card-header-component">
                    <div style="margin-left: auto">
                      <button class="card-header-button btn closeable" (click)="toggleShowOptions($event)">
                        <i class="fas fa-times" card-header-text></i>
                      </button>
                    </div>
                  </div>

                  <div *ngIf="disableLocalFiltering; else display">
                    <nav ngbNav #nav="ngbNav" [(activeId)]="activeTab">
                      <ng-container ngbNavItem [ngbNavItem]="1" class="tab-title-class"
                                    *ngIf="filterByAllowedFields.length > 0">
                        <a ngbNavLink>BASIC SEARCH</a>
                        <ng-template ngbNavContent>
                          <div class="basic-search">
                            <div class="classification-header">
                              {{this.basicSearchHeader}}
                            </div>
                            <div class="row search-content-body">
                              <ng-container *ngFor="let filter of boundFilters; index as i">
                                <div class="col-6">
                                  <div *ngIf="getFieldType(filter.field) === 'input' ">
                                    <mat-form-field appearance="none">
                                      <mat-label>{{getFieldDisplay(filter.field)}}</mat-label>
                                      <input matInput [ngModel]="filter.value"
                                             (ngModelChange)="setFilter($event, filter)"
                                             (keydown)="applyFilters($event)"/>
                                    </mat-form-field>
                                  </div>
                                  <div *ngIf="getFieldType(filter.field) === 'dropdown' ">
                                    <mat-form-field appearance="none">
                                      <mat-label>{{getFieldDisplay(filter.field)}}</mat-label>
                                      <mat-select [ngModel]="filter.value"
                                                  (ngModelChange)="setFilter($event, filter)"
                                                  (keydown)="applyFilters($event)">

                                        <div *ngIf="fields[i] && fields[i].values && fields[i].values[0].display;else display_raw_value">
                                          <mat-option>None</mat-option>
                                          <mat-option *ngFor="let value of fields[i].values"
                                                      [value]="value.id">{{value.display}}</mat-option>
                                        </div>
                                        <ng-template #display_raw_value>
                                          <mat-option>None</mat-option>
                                          <mat-option *ngFor="let value of fields[i].values"
                                                      [value]="value">{{value}}</mat-option>
                                        </ng-template>
                                      </mat-select>
                                    </mat-form-field>
                                  </div>
                                  <div *ngIf="getFieldType(filter.field) === 'date' ">
                                    <mat-form-field appearance="none">
                                      <mat-label>{{getFieldDisplay(filter.field)}}</mat-label>
                                      <input [ngModel]="filter.value"
                                             (ngModelChange)="setFilter($event, filter)"
                                             (keydown)="applyFilters($event)" matInput [matDatepicker]="myPicker"
                                             placeholder="date">
                                      <mat-datepicker-toggle matSuffix [for]="myPicker">
                                        <mat-icon fontSet="fa" fontIcon="fa-calendar-alt fa-lg"></mat-icon>
                                      </mat-datepicker-toggle>
                                      <mat-datepicker #myPicker></mat-datepicker>
                                    </mat-form-field>
                                  </div>
                                </div>
                              </ng-container>
                            </div>
                          </div>
                          <div class="search-msg-holder">
                            <div id="basic-search-msg-holder"></div>
                          </div>

                          <div class="ri-modal-footer ri-full-width">
                            <div class="ri-modal-header-right-container ri-full-width">
                              <button [ngClass]="{'ri-btn-cancel': !this.changed, 'ri-btn-cancel-dirty': this.changed}" [disabled] ="!this.changed" (click)="setupFilters(false)">CLEAR
                              </button>
                              <button [ngClass]="{'ri-btn-save': !this.changed, 'ri-btn-save-dirty': this.changed}"
                                      [disabled]="!this.changed " (click)="applyFilters($event)">SEARCH
                              </button>
                            </div>
                          </div>
                        </ng-template>
                      </ng-container>
                      <ng-container ngbNavItem [ngbNavItem]="2" class="tab-title-class"
                                    *ngIf="advancedSearchFields.length > 0">
                        <a ngbNavLink>ADVANCED SEARCH</a>
                        <ng-template ngbNavContent>
                          <div class="advancedSearch-scroll" id="advancedSearch-scroll-panel">
                            <div class="advancedSearch-panel">
                              <div *ngFor="let param of this.advancedSearch; index as j">
                                <div [ngClass]="{'advanced-search': !this.hasAdvancedSearchVerticalScrollbar, 'advanced-search-scrolled': this.hasAdvancedSearchVerticalScrollbar}">
                                  <div class="advanced-search-classification-header">
                                    {{param.header}}
                                  </div>
                                  <div class="row advanced-search-content-body">
                                    <ng-container>
                                      <div *ngFor="let field of this.param.fields; index as k">
                                        <div class="col-5">
                                          <div *ngIf="getFieldType(field.field) === 'input' ">
                                            <mat-form-field appearance="none">
                                              <mat-label>{{getFieldDisplay(field.field)}}</mat-label>
                                              <input matInput [ngModel]="getFilterForField(field.field).value"
                                                     (ngModelChange)="setFilter($event, getFilterForField(field.field))"
                                                     (keydown)="applyAdvancedSearchFilters($event)"/>
                                            </mat-form-field>
                                          </div>
                                          <div *ngIf="getFieldType(field.field) === 'checkbox'  ">
                                            <mat-checkbox class="checkboxes"
                                                          [(ngModel)]="getFilterForField(field.field).value">
                                              {{getFieldDisplay(field.field)}}
                                            </mat-checkbox>
                                          </div>
                                          <div *ngIf="getFieldType(field.field) === 'dropdown' ">
                                            <mat-form-field appearance="none">
                                              <mat-label>{{getFieldDisplay(field.field)}}</mat-label>
                                              <mat-select [ngModel]="getFilterForField(field.field).value"
                                                          (ngModelChange)="setFilter($event, getFilterForField(field.field))"
                                                          (keydown)="applyAdvancedSearchFilters($event)">
                                                <div *ngIf="this.field.valueURL;else display_raw_value">
                                                  <mat-option>None</mat-option>
                                                  <mat-option *ngFor="let value of this.field.values"
                                                              [value]="value.id">{{value.display}}</mat-option>
                                                </div>
                                                <ng-template #display_raw_value>
                                                  <mat-option>None</mat-option>
                                                  <mat-option *ngFor="let value of this.field.values"
                                                              [value]="value">{{value}}</mat-option>
                                                </ng-template>
                                              </mat-select>
                                            </mat-form-field>
                                          </div>
                                          <div *ngIf="getFieldType(field.field) === 'date' ">
                                            <mat-form-field appearance="none">
                                              <mat-label>{{getFieldDisplay(field.field)}}</mat-label>
                                              <input [ngModel]="getFilterForField(field.field).value"
                                                     (ngModelChange)="setFilter($event, getFilterForField(field.field))"
                                                     (keydown)="applyAdvancedSearchFilters($event)" matInput
                                                     [matDatepicker]="myPicker" placeholder="date">
                                              <mat-datepicker-toggle matSuffix [for]="myPicker">
                                                <mat-icon fontSet="fa" fontIcon="fa-calendar-alt fa-lg"></mat-icon>
                                              </mat-datepicker-toggle>
                                              <mat-datepicker #myPicker></mat-datepicker>
                                            </mat-form-field>
                                          </div>
                                        </div>
                                      </div>
                                    </ng-container>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="search-msg-holder">
                            <div id="advanced-search-msg-holder"></div>
                          </div>

                          <div class="ri-modal-footer ri-full-width">
                            <div class="ri-modal-header-right-container ri-full-width">
                              <button [ngClass]="{'ri-btn-cancel': !this.changed, 'ri-btn-cancel-dirty': this.changed}" [disabled]="!this.changed" (click)="setupFilters(false)">CLEAR
                              </button>
                              <button [ngClass] ="{'ri-btn-save': !this.changed, 'ri-btn-save-dirty': this.changed}"
                                      [disabled]="!this.changed" (click)="applyAdvancedSearchFilters($event)">SEARCH
                              </button>
                            </div>
                          </div>
                        </ng-template>
                      </ng-container>
                    </nav>
                    <div [ngbNavOutlet]="nav"></div>
                  </div>

                  <ng-template #display>
                    <nav ngbNav #nav1="ngbNav" [(activeId)]="activeTab1" [ngStyle]="getCustomStyles('nav')">
                      <ng-container ngbNavItem [ngbNavItem]="1" class="tab-title-class" *ngIf="filterByAllowedFields.length > 0">
                        <a ngbNavLink>FILTER</a>
                        <ng-template ngbNavContent>
                          <div class="basic-search sidebar-filter filter-sorting-panel" [ngStyle]="getCustomStyles('basic-search sidebar-filter filter-sorting-panel')">
                            <div class="row search-content-body" [ngStyle]="getCustomStyles('row search-content-body')">
                              <ng-container *ngFor="let filter of boundFilters; index as i">
                                <div class="col-6" [ngStyle]="getCustomStyles('col-6')">
                                  <div *ngIf="getFieldType(filter.field) === 'input' ">
                                    <mat-form-field appearance="none" [ngStyle]="getCustomStyles('mat-form-field')">
                                      <mat-label>{{getFieldDisplay(filter.field)}}</mat-label>
                                      <input matInput [ngModel]="filter.value"
                                             (ngModelChange)="setFilter($event, filter)"
                                             (keydown)="applyFilters($event)"/>
                                    </mat-form-field>
                                  </div>
                                  <div *ngIf="getFieldType(filter.field) === 'dropdown' ">
                                    <mat-form-field appearance="none" [ngStyle]="getCustomStyles('mat-form-field')">
                                      <mat-label>{{getFieldDisplay(filter.field)}}</mat-label>
                                      <mat-select [ngModel]="filter.value"
                                                  (ngModelChange)="setFilter($event, filter)"
                                                  (keydown)="applyFilters($event)">

                                        <div
                                          *ngIf="fields[i] && fields[i].values && fields[i].values[0].display;else display_raw_value">
                                          <mat-option>None</mat-option>
                                          <mat-option *ngFor="let value of fields[i].values"
                                                      [value]="value.id">{{value.display}}</mat-option>
                                        </div>
                                        <ng-template #display_raw_value>
                                          <mat-option>None</mat-option>
                                          <mat-option *ngFor="let value of fields[i].values"
                                                      [value]="value">{{value}}</mat-option>
                                        </ng-template>
                                      </mat-select>
                                    </mat-form-field>
                                  </div>
                                  <div *ngIf="getFieldType(filter.field) === 'date' ">
                                    <mat-form-field appearance="none" [ngStyle]="getCustomStyles('mat-form-field')">
                                      <mat-label>{{getFieldDisplay(filter.field)}}</mat-label>
                                      <input [ngModel]="filter.value"
                                             (ngModelChange)="setFilter($event, filter)"
                                             (keydown)="applyFilters($event)" matInput [matDatepicker]="myPicker"
                                             placeholder="mm/dd/yyyy">
                                      <mat-datepicker-toggle matSuffix [for]="myPicker">
                                        <mat-icon fontSet="fas" fontIcon="fa-calendar-alt fa-lg"></mat-icon>
                                      </mat-datepicker-toggle>
                                      <mat-datepicker #myPicker></mat-datepicker>
                                    </mat-form-field>
                                  </div>
                                </div>
                              </ng-container>
                            </div>
                          </div>
                          <div class="search-msg-holder" [ngStyle]="getCustomStyles('search-msg-holder')">
                            <div id="basic-search-msg-holder"></div>
                          </div>

                          <div class="ri-modal-footer ri-full-width sidebar-filter">
                            <div class="ri-modal-header-right-container ri-full-width">
                              <button class="ri-btn-cancel ri-btn-cancel-dirty" (click)="setupFilters(true)">RESET
                              </button>
                              <button class="ri-btn-save" [ngClass]="{'ri-btn-save-dirty': this.changed}"
                                      [disabled]="!this.changed " (click)="applyFilters()">FILTER
                              </button>
                            </div>
                          </div>
                        </ng-template>
                      </ng-container>
                      <ng-container ngbNavItem [ngbNavItem]="2" class="tab-title-class" *ngIf="sortByAllowedFields.length > 0">
                        <a ngbNavLink>SORTING</a>
                        <ng-template ngbNavContent>
                          <div class="basic-search sidebar-filter filter-sorting-panel" [ngStyle]="getCustomStyles('basic-search sidebar-filter filter-sorting-panel')">
                            <div class="row search-content-body">
                              <ng-container *ngFor="let sortField of sortByAllowedFields">
                                <div class="col-6 sort-item" [ngStyle]="getCustomStyles('col-6 sort-item')">
                                  <div class="sort-icon" [ngStyle]="getCustomStyles('sort-icon')">
                                    <div *ngIf="isSort(sortField) && isSortAsc(sortField)"
                                         [ngClass]="{'sort-arrow':isFirstSort(sortField)}">
                                      <i class="fas fa-arrow-circle-up fa-lg"></i>
                                    </div>
                                    <div *ngIf="isSort(sortField) && !isSortAsc(sortField)"
                                         [ngClass]="{'sort-arrow':isFirstSort(sortField)}">
                                      <i class="fas fa-arrow-circle-down fa-lg"></i>
                                    </div>
                                  </div>
                                  <div class="sort-text" (click)="setSort($event, sortField)" [ngStyle]="getCustomStyles('sort-text')">
                                    {{getFieldDisplay(sortField)}}
                                  </div>
                                </div>
                              </ng-container>
                            </div>
                          </div>

                          <div class="search-msg-holder">
                            <div id="basic-search-msg-holder"></div>
                          </div>

                          <div class="ri-modal-footer ri-full-width sidebar-filter">
                            <div class="ri-modal-header-right-container ri-full-width">
                            </div>
                          </div>

                        </ng-template>
                      </ng-container>
                    </nav>
                    <div [ngbNavOutlet]="nav1"></div>
                  </ng-template>
                </div>
              </div>
            </div>
          </div>
          <div class="d-flex mb-1 global-search" *ngIf="inputMultiline === false">
            <input [ngModel]="globalSearch" class="search-box" (keyup)="doGlobalSearch($event)" [placeholder]="globalSearchPlaceholder"/>
            <select *ngIf="showQuickSearchOptions" [(ngModel)]="selectedSearchType" (change)="selectSearchType()"
                    id="search-identifier" style="width: 100%; border-radius: 0px; border-color: black; ">
              <option *ngFor="let searchType of searchTypeArray"
                      [value]="searchType">
                {{searchType}}
              </option>
            </select>
            <button class="pl-1 pr-1 search-button" style="height: 100%" (click)="searchButtonClicked()">
              <i class="fas fa-search"></i>
            </button>
          </div>
          <div class="d-flex mb-1 global-search" *ngIf="inputMultiline === true">
            <textarea [ngModel]="globalSearch" class= "search-box textarea-search-box" [ngStyle]="getCustomStyles('search-box textarea-search-box')" (keyup)="doGlobalSearch($event)" [placeholder]="globalSearchPlaceholder"></textarea>
            <select *ngIf="showQuickSearchOptions" [(ngModel)]="selectedSearchType" (change)="selectSearchType()" id="search-identifier" style="width: 100%; border-radius: 0px; border-color: black; ">
              <option *ngFor="let searchType of searchTypeArray"
                      [value]="searchType">
                {{searchType}}
              </option>
            </select>
            <button class="pl-1 pr-1 search-button multiline-search-button" [ngStyle]="getCustomStyles('search-button multiline-search-button')" (click)="searchButtonClicked()">
              <i class="fas fa-search"></i>
            </button>
          </div>
          <div class="d-flex x-auto" *ngIf="hasValidFilters()">
            <div class="d-flex">
              <ng-container *ngFor="let filter of filters">
                <ng-container *ngIf="filter.valid">
                  <div (click)="clearFilter(filter)" class="d-flex pill">
                    <div class="mr-2">
                      {{filter.field}}
                    </div>
                    <div class="close-icon">
                      <i class="fas fa-times-circle fa-xs"></i>
                    </div>
                  </div>
                </ng-container>
              </ng-container>
            </div>
          </div>

          <div *ngIf="searchLists.length > 1" class="d-flex x-hidden list-toggle">
            <mat-button-toggle-group [ngModel]="selectedSearchList" (ngModelChange)="setSelectedSearchList($event)">
              <mat-button-toggle *ngFor="let searchList of searchLists" mat-raised-button-toggle color="primary"
                                 [value]="searchList">
                {{searchList}}
              </mat-button-toggle>
            </mat-button-toggle-group>
          </div>
        </div>
      `
  }
)
export class SearchListControllerComponent extends NavComponent {
  @Input() showTitle: boolean = true;
  @Input() inputMultiline: boolean = false;
  @Input() searchOptionsWidth: string = "";
  @Input() fields: any[] = [];
  @Input() customStyles: any = {};

  @Input() basicSearch: any = {};
  @Input() advancedSearch: any = {};
  @Input() filterByAllowedFields: string[] = [];
  @Input() quickSearchFields: string[] = [];
  @Input() advancedSearchFields: string[] = [];
  @Input() sortByAllowedFields: string[] = [];
  @Input() sorts: HciSortDto[] = [];
  @Input() showQuickSearchOptions: boolean = false;
  @Input() showAdvancedSearchOptions: boolean = false;
  @Input() searchTypeArray: string[] = [];
  @Input() searchType: string = "";
  @Input() searchTypeSelected: string = "";
  @Input() basicSearchHeader: string = "";
  @Input() globalSearchPlaceholder: string = "";
  @Input() globalSearchValidator: any | string = "";
  @Output() searchListChange: EventEmitter<string> = new EventEmitter<string>();

  activeTab = 1;

  activeTab1 = 1;

  boundFilters: HciFilterDto[] = [];
  boundAdvancedSearchFilters: HciFilterDto[] = [];
  updatedAdvanceSearchFilters: HciFilterDto[] = [];

  filtersSubject: Subject<HciFilterDto[]> = new BehaviorSubject<HciFilterDto[]>([]);
  advancedSearchFiltersSubject: Subject<HciFilterDto[]> = new BehaviorSubject<HciFilterDto[]>([]);
  sortsSubject: Subject<HciSortDto[]> = new BehaviorSubject<HciSortDto[]>([]);

  globalSearch: string = "";

  showOptions: boolean = false;
  // showBackButton: boolean = false;
  searchLists: string[] = [];
  selectedSearchList: string;
  selectedSearchListSubject: Subject<string> = new BehaviorSubject<string>(undefined);
  searchStringSubject: Subject<string> = new BehaviorSubject<string>(undefined);
  // backButtonSubject: Subject<any> = new BehaviorSubject<any>(undefined);
  advancedSearchStringSubject: Subject<string> = new BehaviorSubject<string>(undefined);
  selectedSearchType: string;
  selectedSearchTypeSubject: Subject<string> = new BehaviorSubject<string>(undefined);
  performedSearchSubject: Subject<string> = new Subject<string>();
  performedBasicSearchSubject: Subject<string> = new Subject<string>();
  performedAdvancedSearchSubject: Subject<string> = new Subject<string>();
  disableLocalFiltering = false;
  advancedSearchAllowFilter = false;

  allowSearchSubject$: Subject<any> = new BehaviorSubject<any>(undefined);
  searchAllowedSubject$: Subject<any> = new BehaviorSubject<any>(undefined);
  allowToggleSubject$: Subject<any> = new BehaviorSubject<any>(undefined);
  toggleShowSubject$: Subject<boolean> = new BehaviorSubject<any>(undefined);
  cogWheelClickedSubject$: Subject<boolean> = new BehaviorSubject<any>(undefined);
  changed: boolean = false;
  allowSearch: boolean = true;
  allowToggle: boolean = true;
  showToggleOptions: boolean = true;

  getCustomStyles(styles: string): any {
    let customStyleObject: any = "";
    for (let style of styles.split(" ")) {
      let styleObject = this.customStyles[style];
      if (styleObject) {
        customStyleObject = Object.assign({}, customStyleObject, styleObject);
      }
    }

    return customStyleObject;
  }

  constructor(elementRef: ElementRef,
              renderer: Renderer2,
              resolver: ComponentFactoryResolver,
              navigationGlobalService: NavigationGlobalService,
              @Optional() navigationService: NavigationService,
              private http: HttpClient,
              changeDetectorRef: ChangeDetectorRef) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);

    if (!navigationService) {
      this.navigationService = new NavigationService(navigationGlobalService);
    }

    if (this.navigationService.getId() === undefined) {
      this.isRoot = true;
    }
  }

  /**
   * Set data only when the component has fully initialized and is rendered.
   */
  checkInitialized() {
    if (this.afterViewInit) {
      this.initialized = true;
      this.updateConfig(this.config);
      this.postInit();
    }
  }

  doGlobalSearch(event: KeyboardEvent): void {
    let searchString: string = ((<HTMLInputElement>event.target).value).trim();
    if (event.keyCode === 13) {
      // Send a message with search-text
      console.debug("In SearchListController enter was detected");
      this.globalSearch = searchString;
      this.searchStringSubject.next(this.getSearchString(searchString));
      this.searchButtonClicked();
      return;
    }

    if (this.disableLocalFiltering === false) {
      if (this.globalSearch !== searchString) {
        if (!searchString || searchString.length === 0) {
          this.filtersSubject.next([]);
        } else {
          let filterField: string;
          for (let field of this.filterByAllowedFields) {
            filterField = (filterField) ? filterField + " or " + field : field;
          }
          this.filtersSubject.next([new HciFilterDto(filterField, "string", this.getSearchString(searchString), undefined, "L", true)]);
        }
      }
    }
    this.globalSearch = searchString;
  }

  searchButtonClicked() {

    // Validation Check: if invalid, do not search.
    if (this.globalSearchValidator && this.isInvalid()) {
      return;
    }

    this.subscriptions.add(this.allowSearchSubject$.subscribe(val => {
      if (val === "allowSearch") {
        this.allowSearch = true;
      } else {
        if (val === "preventSearch") {
          this.allowSearch = false;
        }
      }
    }));

    this.searchAllowedSubject$.next(this.allowSearch);

    if (this.allowSearch) {
      if (this.globalSearch) {
        this.searchStringSubject.next(this.getSearchString(this.globalSearch));
        if (this.globalSearch.length === 0) {
          this.filtersSubject.next([]);
        } else {
          let filterField: string;
          if (this.quickSearchFields.length > 0) {
            for (let field of this.quickSearchFields) {
              filterField = (filterField) ? filterField + " or " + field : field;
            }
          } else {
            for (let field of this.filterByAllowedFields) {
              filterField = (filterField) ? filterField + " or " + field : field;
            }

          }

          this.filtersSubject.next([new HciFilterDto(filterField, "string", this.getSearchString(this.globalSearch), undefined, "L", true)]);
        }
      }
      this.selectedSearchTypeSubject.next(this.selectedSearchType);
      this.performedSearchSubject.next("searchPerformed");
    } else {
      this.filtersSubject.next([]);
    }
  }

  setSelectedSearchList(selectedSearchList: string): void {
    this.selectedSearchList = selectedSearchList;
    this.selectedSearchListSubject.next(this.selectedSearchList);
    this.searchListChange.emit(selectedSearchList);
  }

  selectSearchType() {
    console.debug("selectSearchType");
    console.debug(this.selectedSearchType);
    this.selectedSearchTypeSubject.next(this.selectedSearchType);
  }

  addSearchList(title: string): void {
    if (isDevMode()) {
      console.debug("SearchListControllerComponent.addSearchList: " + title);
    }

    if (title && this.searchLists.indexOf(title) === -1) {
      this.searchLists.push(title);
    }

    if (this.searchLists.length === 1) {
      this.selectedSearchList = title;
      this.selectedSearchListSubject.next(this.selectedSearchList);
    }
  }

  /**
   * Updates the config with options and re-fetches data.
   *
   * @param config
   */
  updateConfig(config) {
    if (isDevMode()) {
      console.debug("updateConfig");
    }

    if (config) {
      this.config = Object.assign({}, this.config, config);

      if (config.filterByAllowedFields) {
        this.filterByAllowedFields = config.filterByAllowedFields;
      }
      if (config.quickSearchFields) {
        this.quickSearchFields = config.quickSearchFields;
      }
      if (config.advancedSearchFields) {
        this.advancedSearchFields = config.advancedSearchFields;
      }
      if (config.sortByAllowedFields) {
        this.sortByAllowedFields = config.sortByAllowedFields;
      }
      if (config.showQuickSearchOptions) {
        this.showQuickSearchOptions = config.showQuickSearchOptions;
      }
      if (config.inputMultiline) {
        this.inputMultiline = config.inputMultiline;
      }
      if (config.searchOptionsWidth) {
        this.searchOptionsWidth = config.searchOptionsWidth;
      }
      if (config.searchTypeArray) {
        this.searchTypeArray = config.searchTypeArray;
        if (config.searchType) {
          this.selectedSearchType = this.searchTypeArray[this.searchTypeArray.findIndex(x => x === config.searchType)];
        } else {
          this.selectedSearchType = this.searchTypeArray[0];
        }
      }
      if (config.disableLocalFiltering) {
        this.disableLocalFiltering = config.disableLocalFiltering;
      }
      if (config.advancedSearchAllowFilter) {
        this.advancedSearchAllowFilter = config.advancedSearchAllowFilter;
      }
      if (config.selectedSearchString) {
        this.globalSearch = config.selectedSearchString;
      }
      if (config.sorts) {
        this.sorts = config.sorts;
      }
      if (config.searchListChange) {
        this.searchListChange = config.searchListChange;
      }
      if (config.showTitle != null) {
        this.showTitle = config.showTitle;
      }
      // if (config.showBackButton != null) {
      //   this.showBackButton = config.showBackButton;
      // }
      if (config.basicSearchHeader != null) {
        this.basicSearchHeader = config.basicSearchHeader;
      }
      if (config.globalSearchPlaceholder) {
        this.globalSearchPlaceholder = config.globalSearchPlaceholder;
      }
      if (config.globalSearchValidator) {
        this.globalSearchValidator = config.globalSearchValidator;
      }
      if (config.customStyles) {
        this.customStyles = config.customStyles;
      }

      if (config.fields != null) {
        this.fields = config.fields;
        if (this.fields) {
          for (let field of this.fields) {
            if (field.valueURL && !field.values) {
              // We found a value URL for the field and will populate the values now
              this.getValuesFromURL(field);
            }
          }
        }
      }
      if (config.advancedSearch != null) {
        this.advancedSearch = config.advancedSearch;
        if (this.advancedSearch != null) {
          var i;
          for (i = 0; i < this.advancedSearch.length; i++) {
            var j;
            for (j = 0; j < this.advancedSearch[i].fields.length; j++) {
              if (this.advancedSearch[i].fields[j].valueURL && !this.advancedSearch[i].fields[j].values) {
                this.getValuesFromURL(this.advancedSearch[i].fields[j]);
              }
            }
          }
        }
      }
    }
    if (this.sortByAllowedFields.length > 0 && this.sorts.length === 0) {
      this.sorts.push(new HciSortDto(this.sortByAllowedFields[0]));
    }
    this.setupFilters();
    super.updateConfig(config);
  }

  /**
   * Return the value of the objects after making a http call to the valueURL of the field
   *
   * @param {any} field
   * @returns {string[]}
   */
  getValuesFromURL(field: any) {
    let url: string;
    url = field.valueURL;
    this.http.get(url)
      .subscribe(
        data => {
          console.debug("values coming back from" + field.valueURL);
          let values: any[] = JSON.parse(JSON.stringify(data));
          console.debug(values);
          if (values && values.length > 0) {
            field.values = values;
            //Sort the values of the drop down lists befinre
            values.sort((a, b) => {
              if (a.display < b.display) {
                return -1;
              } else {
                return 1;
              }
            });
          }
        },
        error => {
          console.error("Error while getting values for URL" + this.getFieldDisplay(field));
          console.error(error);
        }, () => {
        });
  }

  /**
   * Return the display property of the field if it exists, otherwise, try converting camel case to words.
   *
   * @param {string} fieldName
   * @returns {string}
   */


  getFieldDisplay(fieldName: string): string {
    //this.basicSearch.fields
    for (let field of this.fields) {
      if (field.field === fieldName) {
        return field.display;
      }
    }
    //advancedSearch field display
    let i;
    for (i = 0; i < this.advancedSearch.length; i++) {
      let j;
      for (j = 0; j < this.advancedSearch[i].fields.length; j++) {
        if (this.advancedSearch[i].fields[j].field === fieldName) {
          return this.advancedSearch[i].fields[j].display;
        }
      }
    }

    let display: string = fieldName.replace(/([A-Z])/g, " $1");
    return display.charAt(0).toUpperCase() + display.slice(1);
  }

  /**
   * Return the type property of the field if it exists. Defaults to input
   *
   * @param {string} fieldName
   * @returns {string}
   */
  getFieldType(fieldName: string): string {
    //this.basicSearch.fields
    for (let field of this.fields) {
      if (field.field === fieldName) {
        return field.type;
      }
    }

    let i;
    for (i = 0; i < this.advancedSearch.length; i++) {
      let j;
      for (j = 0; j < this.advancedSearch[i].fields.length; j++) {
        if (this.advancedSearch[i].fields[j].field === fieldName) {
          return this.advancedSearch[i].fields[j].type;
        }
      }
    }
    return "input";
  }

  /**
   * Create filters based upon available fields for filtering.
   */
  setupFilters(apply: boolean = false): void {
    this.boundFilters = [];
    this.boundAdvancedSearchFilters = [];

    for (let filterField of this.filterByAllowedFields) {
      this.boundFilters.push(new HciFilterDto(filterField, "string", undefined, undefined, "L", false));
    }
    for (let filterField of this.advancedSearchFields) {
      this.boundAdvancedSearchFilters.push(new HciFilterDto(filterField, "string", undefined, undefined, "L", false));
    }
    if (apply) {
      this.applyFilters();
      this.applyAdvancedSearchFilters();
    }
    this.changed = false;
  }


  /**
   * Return true if at least one filter is valid.
   *
   * @returns {boolean}
   */
  hasValidFilters(): boolean {
    for (let filter of this.boundFilters) {
      if (filter.valid) {
        return true;
      }
    }
    return false;
  }

  /**
   * Sets the value of a filter.
   *
   * @param {string} value
   * @param {HciFilterDto} filter
   */
  setFilter(value: string, filter: HciFilterDto): void {
    filter.value = value;
    if (value) {
      this.changed = true;
    } else {
      this.changed = false;
      if (!this.disableLocalFiltering) {
        for (let fl of this.boundFilters) {
          if (fl.value) {
            this.changed = true;
          }
        }
      }
    }
  }

  /**
   * Set valid filters to valid and call data.
   */
  applyFilters(event?: KeyboardEvent): void {
    if (event && event.keyCode !== 13 && event.type && event.type !== "click") {
      return;
    }

    for (let filter of this.boundFilters) {
      if (filter.value instanceof Date) {
        filter.valid = true;
      } else if (filter.value && filter.value.length > 0) {
        filter.valid = true;
      } else {
        filter.valid = false;
      }
    }

    this.globalSearch = "";
    this.filtersSubject.next(this.boundFilters);
    //Setting a flag on buttonClick event.
    if (event && (event.keyCode === 13 || event.type === "click")) {
      this.performedBasicSearchSubject.next("basicSearchClicked");
    }
    this.changed = true;
  }


  /**
   * Set valid advanced search filters to valid and call data.
   */

  applyAdvancedSearchFilters(event?: KeyboardEvent): void {
    this.updatedAdvanceSearchFilters = [];
    if (event && event.keyCode !== 13 && event.type && event.type !== "click") {
      return;
    }

    for (let filter of this.boundAdvancedSearchFilters) {
      filter.valid = (filter.value && filter.value.length > 0);
    }

    this.updatedAdvanceSearchFilters = this.boundAdvancedSearchFilters;

    this.globalSearch = "";
    if (this.advancedSearchAllowFilter) {
      this.filtersSubject.next(this.updatedAdvanceSearchFilters);
    } else {
      this.filtersSubject.next([]);
    }
    this.advancedSearchFiltersSubject.next(this.updatedAdvanceSearchFilters);

    if (event && (event.keyCode === 13 || event.type === "click")) {
      this.performedAdvancedSearchSubject.next("advancedSearchClicked");
    }

    this.changed = true;
  }


  /**
   * Clear filter fields and make invalid.
   *
   * @param {HciFilterDto} filter
   */
  clearFilter(filter: HciFilterDto): void {
    for (let i = 0; i < this.boundFilters.length; i++) {
      if (this.boundFilters[i].field === filter.field) {
        this.boundFilters[i].setValue(undefined);
        this.boundFilters[i].setValid(false);
        break;
      }
    }

    this.filtersSubject.next(this.boundFilters);
  }

  /**
   * TODO: If sort in group, reset group data, not overall.
   *
   * @param {MouseEvent} event
   * @param {string} field
   */
  setSort(event: MouseEvent, field: string): void {
    if (this.sorts && this.sorts.length === 1 && this.sorts[0].field === field) {
      // Regular behavior
    } else if (this.sorts && this.sorts.length === 1 && !event.ctrlKey && this.sorts[0].field !== field) {
      this.sorts = [];
    } else if (!event.ctrlKey) {
      this.sorts = [];
    }

    if (this.sorts.length === 0) {
      this.sorts.push(new HciSortDto(field));
    } else {
      let exists: boolean = false;
      for (let sort of this.sorts) {
        if (sort.field === field) {
          sort.asc = !sort.asc;
          exists = true;
          break;
        }
      }

      if (!exists) {
        this.sorts.push(new HciSortDto(field));
      }
    }
    this.sortsSubject.next(this.sorts);
  }

  /**
   * Return true if the sort field is used.
   *
   * @param sortField
   * @returns {boolean}
   */
  isSort(sortField): boolean {
    for (let sort of this.sorts) {
      if (sort.field === sortField) {
        return true;
      }
    }

    return false;
  }

  /**
   * Return true if the sort field is ascending.
   *
   * @param sortField
   * @returns {boolean}
   */
  isSortAsc(sortField): boolean {
    for (let sort of this.sorts) {
      if (sort.field === sortField) {
        return sort.asc;
      }
    }
    return false;
  }

  /**
   * Return true if the field is the first sort field.
   *
   * @param sortField
   * @returns {boolean}
   */
  isFirstSort(sortField): boolean {
    if (!this.sorts || this.sorts.length === 0) {
      return false;
    } else {
      return this.sorts[0].field === sortField;
    }
  }

  /**
   * Toggle the options popout.
   *
   * @param {MouseEvent} event
   */
  toggleShowOptions(event: MouseEvent): void {
    this.cogWheelClickedSubject$.next(true);
    this.subscriptions.add(this.allowToggleSubject$.subscribe(val => {
      if (val === "allowToggle") {
        this.allowToggle = true;
        this.showToggleOptions = true;
      } else {
        if (val === "preventToggle") {
          this.allowToggle = false;
          this.showToggleOptions = false;
        }
      }
    }));

    if (this.showOptions && this.elementRef.nativeElement.querySelector(".search-options").contains(event.target)
      && (!(this.elementRef.nativeElement.querySelector("button.card-header-button.btn.closeable").contains(event.target)))) {
      return;
    }
    let title: HTMLElement = this.elementRef.nativeElement.querySelector("#search-title");

    let options: HTMLElement = title.querySelector(".search-options");

    this.renderer.setStyle(options, "left", (title.offsetLeft + title.offsetWidth) + "px");
    if (!options.closest(".dropdown-menu")) {
      this.renderer.setStyle(options, "top", title.getBoundingClientRect().top + "px");
    }

    this.showOptions = !this.showOptions;
  }

  /**
   * This function would match a HciFilterDto by comparing a provided field-name
   */
  getFilterForField(fieldName: string) {
    return this.boundAdvancedSearchFilters.find(x => x.field === fieldName);
  }

  /**
   * Listener for back button
   */

  // backButtonClicked() { // This should never be called
  //   this.backButtonSubject.next(this.backButtonSubject);
  // }

  /**
   * Listen for document clicks.
   *
   * @param {MouseEvent} event
   * @param {HTMLElement} target
   */
  @HostListener("document:click", ["$event", "$event.target"])
  documentClick(event: MouseEvent, target: HTMLElement) {
    if (this.allowToggle) {
      try {
        if (target.parentElement.getAttribute("class").includes("closeable")) {
          this.showOptions = false;
        }
        if (this.showOptions && !this.elementRef.nativeElement.querySelector("#search-title").contains(target)
          && !target.getAttribute("class").includes("mat")) {
          this.showOptions = false;
        }
        this.toggleShowSubject$.next(this.showOptions);
      } catch (error) {
      }
    }
  }

  isInvalid(): boolean {
    let invalid = false;
    if (this.globalSearchValidator) {
      let searchString = this.getSearchString(this.globalSearch);
      let globalSearchStrList: string[] = searchString.split(",");

      if (typeof this.globalSearchValidator === "string") {
        for (let globalSearchStr of globalSearchStrList) {
          if(!globalSearchStr.match(this.globalSearchValidator)) {
            invalid = true;
            break;
          }
        }
      } else if (typeof this.globalSearchValidator === "object") {
        if (this.globalSearchValidator.required && globalSearchStrList.length === 0) {
          invalid = true;
        }
        if (this.globalSearchValidator.minRange && !isNaN(Number(this.globalSearchValidator.minRange))) {
          for (let globalSearchStr of globalSearchStrList) {
            if((!globalSearchStr && Number(this.globalSearchValidator.minRange) > 0)
              || (globalSearchStr && globalSearchStr.length < Number(this.globalSearchValidator.minRange))) {
              invalid = true;
              break;
            }
          }
        }
        if (this.globalSearchValidator.maxRange && !isNaN(Number(this.globalSearchValidator.maxRange))) {
          for (let globalSearchStr of globalSearchStrList) {
            if(globalSearchStr && globalSearchStr.length > Number(this.globalSearchValidator.maxRange)) {
              invalid = true;
              break;
            }
          }
        }
        if (this.globalSearchValidator.pattern) {
          for (let globalSearchStr of globalSearchStrList) {
            if (!globalSearchStr.match(this.globalSearchValidator.pattern)) {
              invalid = true;
              break;
            }
          }
        }
      } else {
        invalid = true;
      }
    }
    return invalid;
  }

  getSearchString(searchString: string): string {
    searchString = searchString.replace(/\n/g, ",");
    if (searchString.length > 0) {
      let strList = searchString.split(",");
      searchString = "";
      for (let str of strList) {
        str = str.trim();
        searchString = searchString ? (str ? searchString + "," + str : searchString) : str;
      }
    }

    return searchString;
  }

  get hasAdvancedSearchVerticalScrollbar(): boolean {
    const divPanel = document.getElementById("advancedSearch-scroll-panel");
    return divPanel.scrollHeight > divPanel.clientHeight;
  }

}
