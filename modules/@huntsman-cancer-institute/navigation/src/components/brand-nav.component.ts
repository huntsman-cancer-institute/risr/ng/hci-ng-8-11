import {ChangeDetectorRef, Component, ComponentFactoryResolver, ElementRef, Input, Renderer2} from "@angular/core";

import {ComponentType} from "@huntsman-cancer-institute/utils";

import {NavComponent} from "./nav.component";
import {NavigationService} from "../services/navigation.service";

@ComponentType("BrandNavComponent")
@Component({
  selector: "hci-brand",
  template: `
    <div class="hci-brand">
      <a *ngIf="!href && !route" 
          [id]="id + '-a'" class="hci-brand-a {{aClass}}">
        <img *ngIf="imgSrc && !imgRight" [src]="imgSrc" />
        <span *ngIf="title" class="hci-brand-span {{spanClass}}">
          {{title}}
        </span>
        <img *ngIf="imgSrc && imgRight" [src]="imgSrc" />
      </a>
      <a *ngIf="href"
          [id]="id" [href]="href" class="hci-brand-a {{aClass}}">
        <img *ngIf="imgSrc && !imgRight" [src]="imgSrc" />
        <span *ngIf="title" class="hci-brand-span {{spanClass}}">
          {{title}}
        </span>
        <img *ngIf="imgSrc && imgRight" [src]="imgSrc" />
      </a>
      <a *ngIf="route"
          [id]="id"
         routerLink="{{route}}"
         routerLinkActive="active-link"
         [queryParams]="queryParams"
         queryParamsHandling="{{queryParamsHandling}}"
         class="hci-brand-a {{aClass}}">
        <img *ngIf="imgSrc && !imgRight" [src]="imgSrc" />
        <span *ngIf="title" class="hci-brand-span {{spanClass}}">
          {{title}}
        </span>
        <img *ngIf="imgSrc && imgRight" [src]="imgSrc" />
      </a>
    </div>
  `,
  styles: [`

    .hci-brand img {
      height: 36px;
    }

  `],
})
export class BrandNavComponent extends NavComponent {

  @Input() href: string;
  @Input() route: string;
  @Input() queryParams: Object;
  @Input() queryParamsHandling: string = "merge";
  @Input() aClass: string = "navbar-brand mr-0 mr-md-2";
  @Input() imgSrc: string;
  @Input() spanClass: string = "d-none d-md-flex";
  @Input() imgRight: boolean = false;
  @Input() aClick: Function = (() => {});

  constructor(elementRef: ElementRef, renderer: Renderer2, resolver: ComponentFactoryResolver, navigationService: NavigationService, changeDetectorRef: ChangeDetectorRef) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);
  }

  handleAClick(event: Event) {
    this.aClick(this, this.navigationService, event);
  }

  updateConfig(config) {
    super.updateConfig(config);

    if (config.aClass) {
      this.aClass = config.aClass;
    }
    if (config.href) {
      this.href = config.href;
    }
    if (config.route) {
      this.route = config.route;
    }
    if (config.queryParams) {
      this.queryParams = config.queryParams;
    }
    if (config.queryParamsHandling) {
      this.queryParamsHandling = config.queryParamsHandling;
    }
    if (config.rootClass) {
      this.rootClass = config.rootClass;
    }
    if (config.imgSrc) {
      this.imgSrc = config.imgSrc;
    }
    if (config.imgRight !== undefined) {
      this.imgRight = config.imgRight;
    }
    if (config.spanClass) {
      this.spanClass = config.spanClass;
    }
  }
}
