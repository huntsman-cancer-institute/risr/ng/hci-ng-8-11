import {DictionaryMetadataField} from "./dictionary-metadata-field.dto";

export interface DictionaryMetadata {
  className: string;
  displayName: string;
  description: string;
  readOnly: boolean;
  internalOnly: boolean;
  fields: DictionaryMetadataField[];
}
