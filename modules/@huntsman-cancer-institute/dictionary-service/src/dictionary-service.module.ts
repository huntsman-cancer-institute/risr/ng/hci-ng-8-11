/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {ModuleWithProviders, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {DICTIONARY_ENDPOINT, DictionaryService} from "./services/dictionary.service";

/**
 * The module providing the dictionary service.
 *
 * @since 1.0.0
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class DictionaryServiceModule {
  static forRoot(config: any): ModuleWithProviders<DictionaryServiceModule> {
    return {
      providers: [
        {provide: DICTIONARY_ENDPOINT, useValue: config.dictionaryEndpoint},
        DictionaryService
      ],
      ngModule: DictionaryServiceModule
    };
  }
}
