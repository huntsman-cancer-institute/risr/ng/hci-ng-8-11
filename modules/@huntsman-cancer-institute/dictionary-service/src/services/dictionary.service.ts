import {Inject, Injectable, InjectionToken} from "@angular/core";
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";

import {BehaviorSubject, Observable} from "rxjs";

import {DropdownEntry} from "../model/dropdown-entry.dto";
import {DictionaryMetadata} from "../model/dictionary-metadata.dto";

export const DICTIONARY_ENDPOINT = new InjectionToken<string>("dictionaryEndpoint");

/**
 * This service interacts with the dictionary api.
 *
 * The typical dictionary url would be "/api/dictionaries"
 */
@Injectable()
export class DictionaryService {


  constructor(private http: HttpClient, @Inject(DICTIONARY_ENDPOINT) private dictionaryEndpoint: string) {}

  public getDictionaryMetaData(className: string): Observable<DictionaryMetadata> {
    return this.http.get<DictionaryMetadata>(this.dictionaryEndpoint + className + "/metadata");
  }

  public getDictionaryMetaDataLoading(className: string,
                                      onResponse: (data: Object) => any,
                                      onError?: (error) => any,
                                      onFinalize?: () => any): BehaviorSubject<boolean> {

    let loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

    this.http.get<Object>(
      this.dictionaryEndpoint + className + "/metadata").subscribe(
      (data: Object) => {
        loading.next(false);
        onResponse(data);
      },
      (error) => {
        onError(error);
      },
      () => {
        onFinalize();
      }
    );

    return loading;
  }

  /**
   * Return the dictionary metadata based on the class name.
   *
   * @param {string} className
   * @returns {Observable<Object>}
   */
  public getDictionary(className: string): Observable<Object> {
    return this.http.get<Object>(this.dictionaryEndpoint + className);
  }

  /**
   * Return the dictionary data based on the class name.
   *
   * @param {string} className
   * @returns {Observable<Object[]>}
   */
  public getDictionaryEntries(className: string): Observable<Object[]> {
    return this.http.get<Object[]>(this.dictionaryEndpoint + className + "/entries");
  }

  public getDictionaryEntriesAsXML(className: string): Observable<String> {
    let queryParams: HttpParams = new HttpParams().set("format", "xml");

    return this.http.get(this.dictionaryEndpoint + className + "/entries", {observe: "body", params: queryParams, responseType: "text"});
  }

  public addDictionaryEntries(className: string, entries: any[]): Observable<Object[]> {
    return this.http.post<Object[]>(this.dictionaryEndpoint + className + "/entries/add", entries);
  }

  public deleteDictionaryEntries(className: string, entries: any[]): Observable<HttpResponse<any>> {
    return this.http.post<Object[]>(this.dictionaryEndpoint + className + "/entries/delete", entries, { observe: "response" });
  }

  public updateDictionaryEntries(className: string, entries: any[]): Observable<Object[]> {
    return this.http.post<Object[]>(this.dictionaryEndpoint + className + "/entries/update", entries);
  }

  public getDictionaryDropdownEntries(className: string): Observable<DropdownEntry[]> {
    return this.http.get<DropdownEntry[]>(this.dictionaryEndpoint + className + "/dropdown-entries");
  }

  public getDictionaryDropdownEntry(className: string, idName: string, id: number): Observable<DropdownEntry> {
    return this.http.get<DropdownEntry>(this.dictionaryEndpoint + className + "/dropdown-entry",
      { params: new HttpParams().set(idName, <string><any>id) });
  }

  /**
   * Returns a full list of dictionaries.
   *
   * @returns {Observable<Object[]>}
   */
  public getDictionaries(): Observable<Object[]> {
    return this.http.get<Object[]>(this.dictionaryEndpoint);
  }
}
