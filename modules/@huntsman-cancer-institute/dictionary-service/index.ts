/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A barrel file for the dictionary service package.
 *
 * @since 1.0.0
 */
export {DictionaryServiceModule} from "./src/dictionary-service.module";
export {DictionaryService} from "./src/services/dictionary.service";
export {DropdownEntry} from "./src/model/dropdown-entry.dto";

export {DICTIONARY_ENDPOINT} from "./src/services/dictionary.service";
