# Angular Style Module

This module is the central definition for the look and feel for our applications.  There are two source folders, one for
RISR and one for CORE.  All other modules avoid using implicit styling especially regarding coloring and fonts.

The idea here is for an application like CORE, you import RISR styling, then CORE styling, then the application's
specific styling.  Most of the styling we are currently putting in RISR such that all of our applications have a similar
look and feel.

## Structure

All source files are based upon Sassy CSS and build in to minified css.  There is also a common folder where functions
are defined and themes are defined.

The main core.scss and risr.scss import all of the files that build up their styling in order.  The individual files are
broken up by functionality.  For example, the navbar has its own scss file.
