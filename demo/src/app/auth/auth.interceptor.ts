import {Injectable, isDevMode} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";

import {Observable, of} from "rxjs";
import {delay, mergeMap} from "rxjs/operators";
import {CoolLocalStorage} from "@angular-cool/storage";

import {DemoInterceptor} from "../demo.interceptor";

@Injectable()
export class AuthInterceptor extends DemoInterceptor implements HttpInterceptor {

  constructor(private localStorageService: CoolLocalStorage) {
    super();
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (isDevMode()) {
      console.debug("AuthInterceptor.intercept: " + request.url);
    }

    this.delayTime = 0;

    return of(null)
      .pipe(mergeMap(() => {
        if (request.url.match(".*localhost:[0-9]{1-4}/")) {
          if (isDevMode()) {
            console.info(".*localhost:[0-9]{1-4}/");
          }
          return of(new HttpResponse<any>({ status: 200, body: this.localStorageService.getItem("token") }));
        } else if (request.url.match(".*/auth")) {
          if (isDevMode()) {
            console.info(".*/auth");
          }
          return of(new HttpResponse<any>({ status: 200, body: {} }));
        }

        return next.handle(request);
      }))
      .pipe(delay(this.delayTime));
  }

}
