import {Injectable} from "@angular/core";

import {BehaviorSubject, Subject} from "rxjs";

import {RoleEntity} from "@huntsman-cancer-institute/user";

@Injectable()
export class NavigationDemoService {

  userRoles: BehaviorSubject<RoleEntity[]> = new BehaviorSubject<RoleEntity[]>([new RoleEntity("USER")]);

  public getUserRoles(): Subject<RoleEntity[]> {
    return this.userRoles;
  }

  public logout() {
    this.userRoles.next(undefined);
  }

  public login(role: string) {
    this.userRoles.next([new RoleEntity(role)]);
  }
}
