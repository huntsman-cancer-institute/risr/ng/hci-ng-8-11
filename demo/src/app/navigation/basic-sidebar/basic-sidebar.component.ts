import {Component, HostBinding, isDevMode, OnInit, ViewChild} from "@angular/core";

import {BehaviorSubject, of} from "rxjs";
import {delay} from "rxjs/operators";

import {SidebarComponent, LiNavComponent, UlNavComponent} from "@huntsman-cancer-institute/navigation";

@Component({
  selector: "hci-basic-sidebar",
  template: `
    <hci-sidebar>
      <div class="nav-item">Ng-Content A</div>
      <div class="nav-item">Ng-Content B</div>
    </hci-sidebar>
    <div class="outlet-column y-auto m-3">
      <h3 class="mb-4">Basic Sidebar Demo</h3>
      <div class="flex-column flex-shrink-0 mb-4">
        <h4>Badge</h4>
        Item 1 has an icon with a badge and a loading listener.  Click on it to increment the count.  The count takes
        2.5 s to increment.  In that time, the text turns in to a pipe.
      </div>
      <div class="flex-column flex-shrink-0 mb-4">
        <h4>Flex Layout</h4>
        <div class="mb-1">
          This page is an example of how the layout works with flex.
        </div>
        <div class="mb-1">
          The main page is a header and footer with a router outlet in between.  The components that appear in this
          outlet would typically use the class "outlet-column".  This makes the component grow to fit the outlet space
          and displays all children in a column layout.
        </div>
        <div class="mb-1">
          In the navigation component, there is a sub header and a router outlet.  The header takes up its space and
          the component shown in the outlet grows to fit the rest of the space.  This basic sidebar demo component
          is a row layout with two main displays.  First, a sidebar.  Next a main content where this text is shown.
        </div>
        <div class="mb-1">
          The sidebar and content are separate so the two can handle overflow differently.  If there are only a few
          options in the sidebar there is no scrollbar, but if you click on "Toggle Overflow" you will see that you
          can scroll the content without affecting the sidebar.
        </div>
        <div class="mb-3">
          This layout is achieved with Flexbox.  All of the html elements are positioned relative to each other so
          they have an affect on each other.  You can toggle the sidebar on and off or resize it, and when that happens,
          the content panel will adjust itself based upon the room it has left.
        </div>
        <div class="mb-3">
          <button class="btn btn-primary mr-3" (click)="toggleOverflow()">Toggle Overflow</button>
          <button class="btn btn-primary" (click)="toggleSidebar()">Toggle Sidebar</button>
        </div>
        <div class="mb-1">
          <a href="https://medium.freecodecamp.org/understanding-flexbox-everything-you-need-to-know-b4013d4dc9af" target="_blank">Here is a guide to flexbox.</a>
        </div>
      </div>
      <div *ngIf="overflow" style="margin-top: 1000px;">
        Bottom
      </div>
    </div>
  `
})
export class BasicSidebarComponent implements OnInit {

  @HostBinding("class") classlist: string = "outlet-row";

  overflow: boolean = false;
  countSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  countLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  @ViewChild(SidebarComponent, {static: true}) private sidebar: SidebarComponent;

  ngOnInit() {
    this.sidebar.setConfig({
      children: [
        {
          type: UlNavComponent,
          children: [
            {
              type: LiNavComponent,
              title: "Item 1",
              iClass: "fas fa-plus-circle fa-lg",
              iRight: false,
              liClick: () => { this.click(); },
              badgeCount: this.countSubject,
              badgeLoading: this.countLoading
            },
            {
              type: LiNavComponent,
              title: "Item 2"
            },
            {
              type: LiNavComponent,
              title: "Item 3"
            }
          ]
        }
      ]
    });
  }

  click() {
    this.countLoading.next(true);
    of(this.countSubject.getValue() + 1)
        .pipe(delay(2500))
        .subscribe((count: number) => {
      this.countSubject.next(count);
      this.countLoading.next(false);
    });
  }

  toggleOverflow() {
    this.overflow = !this.overflow;
  }

  toggleSidebar() {
    this.sidebar.updateConfig({
      hidden: !this.sidebar.hidden
    });
  }

}
