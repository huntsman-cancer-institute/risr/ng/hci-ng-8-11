import {ChangeDetectorRef, Component, EventEmitter, HostBinding, TemplateRef, ViewChild} from "@angular/core";
import {SafeHtml} from "@angular/platform-browser";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";

import * as prism from "prismjs";

import {SidebarComponent, SearchListControllerComponent, SearchListComponent} from "@huntsman-cancer-institute/navigation";

import {HciDataDto, HciGridDto, HciGroupingDto, HciPagingDto} from "hci-ng-grid-dto";
import {NavigationInterceptor} from '../navigation.interceptor';
import {CoolLocalStorage} from "@angular-cool/storage";

@Component({
  selector: "hci-search-sidebar",
  template: `
    <hci-sidebar></hci-sidebar>
    <div class="outlet-column y-auto">
      <div class="flex-column m-3">
        <h3>Search Sidebar Demo</h3>
        <div class="flex-shrink-0 flex-column">
          <div class="mb-3">
            The search list component loaded in the sidebar uses templates to determine the layout of a group header and
            each item.
          </div>
          <router-outlet></router-outlet>
          <div class="mb-3">
            <button mat-button [matMenuTriggerFor]="searchConfig1" class="mt-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
            <mat-menu #searchConfig1="matMenu" class="prism" xPosition="after" style="min-width: 500px;">
              <div [innerHTML]="config1Html"></div>
            </mat-menu>
          </div>
          <div class="mb-3">
            <h4>Item Template</h4>
            <div [innerHTML]="itemHtml" class="code"></div>
          </div>
          <div class="d-flex p-2 mb-3">
            <div class="d-flex mr-2" style="flex:0 1 20%">Demo Names:
            </div>
            <div #namesSearch="ngbDropdown" ngbDropdown style="flex:0 1 50%" placement="bottom-left"
                 ngbTooltip="{{selectedName?.contactName}}" triggers="manual"
                 #namesTooltip="ngbTooltip" (mouseenter)="namesTooltip.open()"
                 (mouseleave)="namesTooltip.close()">
              <button ngbDropdownToggle id="namesDropdownBtn" class="d-flex btn ngbDropdown-subject">
                <div style="font-size:0.75rem;">{{selectedName?.contactName}}</div>
              </button>
              <div ngbDropdownMenu aria-labelledby="namesDropdown" id="nameNgbDropdownMenu"
                   (mouseenter)="namesTooltip.close()" style="width:100%">
                <hci-search-list-controller id="nameSearchListController" class="filter-sorting-panel"
                                            #controllerComponent="searchListControllerComponent"
                                            [filterByAllowedFields]="['contactName']" [searchOptionsWidth]="'50px'"
                                            [customStyles]="nameCustomStyles"
                                            [sortByAllowedFields]="['contactName']"></hci-search-list-controller>
                <hci-search-list #namesDropdown class="hci-search-list-consent"
                                 [controller]="controllerComponent"></hci-search-list>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <ng-template #demoItemTemplate let-row="row">
      {{row.firstName}} {{row.middleName}}, {{row.lastName}}
    </ng-template>

    <ng-template #namesListItem let-row="row">
      <div class="search-item" (click)=selectName(row)>
        <div class="name">{{row.contactName}}</div>
      </div>
    </ng-template>
  `,
  styles: [`
    .ri-sidebar {
      min-width: 160px;
    }

    .ngbDropdown-subject {
      border: solid;
      border-radius: 1px;
      border-color: lightgrey;
      background-color: white;
      width: 100%;
      color: black;
      align-items:center;
    }

    .ngbDropdown-subject::after {
      margin-left: auto;
    }

    .hci-search-list-consent {
      width:100%;/*max-content*/;
      overflow-y:auto;
      max-height:240px;
    }

  `]
})
export class SearchSidebarDataCallGroupingComponent {

  @HostBinding("class") classList: string = "outlet-row";

  selectedName: any;

  searchListChange: EventEmitter<string> = new EventEmitter<string>();

  overflow: boolean = false;

  itemHtml: SafeHtml;
  itemExample: string = `
    <ng-template #demoItemTemplate let-row="row">
      {{row.firstName}} {{row.middleName}}, {{row.lastName}}
    </ng-template>
  `;

  config1Html: SafeHtml;
  config1String: string = `
    @ViewChild("demoItemTemplate", { read: TemplateRef, static: true }) demoItemTemplate: any;

    this.sidebar.setConfig({
      sizebarSize: 260,
      children: [
        {
          type: "SearchListControllerComponent",
          title: "Demographics",
          fields: [
            {field: "firstName", display: "First Name"},
            {field: "middleName", display: "Middle Name"},
            {field: "lastName", display: "Last Name"}
          ],
          filterByAllowedFields: ["firstName", "middleName", "lastName"],
          sortByAllowedFields: ["firstName", "lastName"],
          globalSearchPlaceholder: "Min 2 Char",
          globalSearchValidator: {minRange: 2, maxRange: 15, required: true, pattern: "^.*[\\\\w\\\\d]{2}.*"},
        },
        {
          type: "SearchListComponent",
          itemTemplate: this.demoItemTemplate,
          id: "search-list-small",
          title: "Demo Small",
          inlineExpand: true,
          dataCall: (gridDto: HciGridDto) => {
            return this.http.post<HciDataDto>("/api/search-sidebar-big-data", gridDto);
          },
          route: (rowGroup: any, row: any) => {
            return ["selected", row.id, row.lastName, row.firstName, row.middleName]
          },
          pageSize: 4,
          groupCacheNumPages: 2,
          groupingFields: ["lastName"],
          onGroupClose: () => {
            this.router.navigate(["."], {relativeTo: this.route});
          }
        },
        {
          type: "SearchListComponent",
          itemTemplate: this.demoItemTemplate,
          id: "search-list-big",
          title: "Demo Big",
          dataCall: (gridDto: HciGridDto) => {
            return this.http.post<HciDataDto>("/api/search-sidebar-random-data", gridDto);
          },
          route: (rowGroup: any, row: any) => {
            return ["selected", row.id, row.lastName, row.firstName, row.middleName]
          },
          pageSize: 4,
          groupPageSize: 5,
          groupCacheNumPages: 2,
          groupingFields: ["lastName"],
          onGroupClose: () => {
            this.router.navigate(["."], {relativeTo: this.route});
          },
          rowId: "id",
          rowClass: "test"
        }
      ]
    });
  `;

  @ViewChild(SidebarComponent, {static: true}) sidebar: SidebarComponent;

  @ViewChild("namesDropdown", {static: true}) private namesDropdown: SearchListComponent;

  @ViewChild("namesListItem", {  read: TemplateRef, static: true }) private namesListItem: any;

  nameList: any[] = [];
  customStyles: any = {};
  nameCustomStyles: any = {
    'filter-sorting-panel': {'margin-top': '20px'},
    'mat-form-field': {
      'width': '10rem',
    },
    'sort-item': {
      'flex': '0 0 100%',
      'max-width': '100%',
    },
    'nav': {
      'padding-left': '5px',
      'padding-right': '5px',
    },
    'basic-search': {
      'width': '238px',
      'margin-left': '5px',
      'margin-right': '5px',
    }
  };

  navigationInterceptor: NavigationInterceptor;

  @ViewChild("demoItemTemplate", { read: TemplateRef, static: true }) demoItemTemplate: any;

  constructor(private changeDetectorRef: ChangeDetectorRef,
              private router: Router,
              private route: ActivatedRoute,
              private coolLocalStorage: CoolLocalStorage,
              private http: HttpClient) {}

  ngAfterViewInit() {
    let advancedSearch = [
      {header: "PATIENT PARAMETERS", fields: [
          {display: "MRN", field: "itsMrn", type: "input"},
          {display: "First Name", field: "firstName", type: "input"},
          {display: "Middle Name", field: "middleName", type: "input"},
          {display: "Last Name", field: "lastName", type: "input"},
          {display: "Gender", field: "gender", type: "dropdown", values: ["F", "M"]},
          {display: "Visit Date", field: "firstVisitDate", type: "date"},
          {display: "Is Decease", field: "isDeceased", type: "checkbox"}
        ]},
      {header: "PATIENT PARAMETERS", fields: [
          {display: "MRN", field: "itsMrn", type: "input"},
          {display: "First Name", field: "firstName", type: "input"},
          {display: "Middle Name", field: "middleName", type: "input"},
          {display: "Last Name", field: "lastName", type: "input"},
          {display: "Gender", field: "gender", type: "dropdown", values: ["F", "M"]},
          {display: "Visit Date", field: "firstVisitDate", type: "date"},
          {display: "Is Decease", field: "isDeceased", type: "checkbox"}
        ]}
    ];

    this.customStyles = {
      'textarea-search-box': {
        'min-height': '55px'
      },
      'search-box': {
        'color': 'red',
        'font-size': '1.8rem'
      },
      // 'basic-search': {
      //   'width': '500px',
      // }

    };


    let advancedSearchFields = ['itsMrn', 'firstName', 'middleName', 'lastName', 'gender', 'isDeceased', 'firstVisitDate'];
    this.navigationInterceptor = new NavigationInterceptor(this.coolLocalStorage)
    this.sidebar.setConfig({
      sizebarSize: 260,
      children: [
        {
          type: SearchListControllerComponent,
          title: "Demographics",
          disableLocalFiltering: false, // Flag for using advanced search or normal filter/sort
          // advancedSearch: advancedSearch,
          // advancedSearchFields: advancedSearchFields,
          basicSearchHeader: "PATIENT",
          fields: [
            {field: "firstName", display: "First Name", type: 'input'},
            {field: "middleName", display: "Middle Name", type: 'input'},
            {field: "lastName", display: "Last Name", type: 'input'}
          ],
          filterByAllowedFields: ["firstName", "middleName", "lastName"], //"firstName", "middleName", "lastName"
          sortByAllowedFields: ["firstName", "lastName"],
          globalSearchPlaceholder: "Min 2 Char",
          globalSearchValidator: {minRange: 2, maxRange: 15, required: true, pattern: "^.*[\\w\\d]{2}.*"},
          inputMultiline: true,
          // advancedSearchAllowFilter: true,
          customStyles: this.customStyles,
          searchListChange: this.searchListChange
        },
        {
          type: SearchListComponent,
          itemTemplate: this.demoItemTemplate,
          id: "search-list-small",
          title: "Demo Small",
          inlineExpand: true,
          showResultsCount: true,
          data: this.navigationInterceptor.origBigData,
          // dataCall: (gridDto: HciGridDto) => {
          //   return this.http.post<HciDataDto>("/api/search-sidebar-big-data", gridDto);
          // },
          route: (rowGroup: any, row: any) => {
            return ["selected", row.id, row.lastName, row.firstName, row.middleName]
          },
          pageSize: 4,
          groupCacheNumPages: 2,
          groupingFields: ["lastName"],
          onGroupClose: () => {
            this.router.navigate(["."], {relativeTo: this.route});
          }
        },
        {
          type: SearchListComponent,
          itemTemplate: this.demoItemTemplate,
          id: "search-list-big",
          title: "Demo Big",
          data: this.navigationInterceptor.origRandomData,
          // dataCall: (gridDto: HciGridDto) => {
          //   return this.http.post<HciDataDto>("/api/search-sidebar-random-data", gridDto);
          // },
          route: (rowGroup: any, row: any) => {
            return ["selected", row.id, row.lastName, row.firstName, row.middleName]
          },
          pageSize: 4,
          groupPageSize: 5,
          groupCacheNumPages: 2,
          groupingFields: ["lastName"],
          onGroupClose: () => {
            this.router.navigate(["."], {relativeTo: this.route});
          },
          rowId: "id",
          rowClass: "test"
        }
      ]
    });

    this.nameList = [
      {contactName: "A Wayne Meikle" , firstName: "A", idContact: 52, lastName: "Meikle", middleName: "Wayne"},
      {contactName: "B John Hope" , firstName: "B", idContact: 53, lastName: "John", middleName: "Hope"}
    ];
    this.namesDropdown.setConfig({
      type: SearchListComponent,
      itemTemplate: this.namesListItem,
      data: this.nameList,
      pageSize: 30
    });

    this.searchListChange.subscribe((searchList: string) => {
      console.info("SearchListChange: " + searchList);
    });

    this.itemHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.itemExample, prism.languages["html"]) + "</code></pre>";
    this.config1Html = "<pre class=\"language-js\"><code #code class=\"language-js\">" + prism.highlight(this.config1String, prism.languages["js"]) + "</code></pre>";
    this.changeDetectorRef.detectChanges();
  }

  selectName(row) {
    this.selectedName = this.nameList.find(c => c.idContact === row.idContact);
  }

}
