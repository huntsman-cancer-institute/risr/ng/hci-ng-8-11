import {ChangeDetectorRef, Component, HostBinding, TemplateRef, ViewChild} from "@angular/core";
import {SafeHtml} from "@angular/platform-browser";

import {BehaviorSubject, of, Subject} from "rxjs";
import {delay} from "rxjs/operators";
import * as prism from "prismjs";

import {SidebarComponent, SearchListComponent} from "@huntsman-cancer-institute/navigation";

@Component({
  selector: "hci-search-sidebar",
  template: `
    <hci-sidebar></hci-sidebar>
    <div class="outlet-column y-auto">
      <div class="flex-column m-3">
        <h3>Search Sidebar Demo</h3>
        <div class="flex-shrink-0 flex-column">
          <div class="mb-3">
            The search list component loaded in the sidebar uses templates to determine the layout of a group header and
            each item.
          </div>
          <div>
            <button (click)="pushDemographic()">Push Demographic</button>
          </div>
          <div>
            <h4>Item Template</h4>
          </div>
          <div [innerHTML]="itemHtml" class="code mb-3"></div>
        </div>
      </div>
    </div>
    
    <ng-template #demoItemTemplate let-row="row">
      <div class="search-item ml-4">
        <div>{{row.first}}, {{row.last}}</div>
      </div>
    </ng-template>
  `
})
export class SearchSidebarDataComponent {

  @HostBinding("class") classList: string = "outlet-row";

  overflow: boolean = false;

  demographics: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  demoLoadingSubject: Subject<boolean>[] = [new BehaviorSubject<boolean>(false)];
  firsts: string[] = ["Bob", "Jane", "Charles", "Dixie", "Mike", "Stacy", "Steve"];
  lasts: string[] = ["Brown", "Black", "Pink", "Orange", "White"];

  itemHtml: SafeHtml;
  itemExample: string = `
    <ng-template #demoItemTemplate let-row="row">
      <div class="search-item pl-4" style="font-size: 1rem;">
        <div>{{row.first}}, {{row.last}}</div>
      </div>
    </ng-template>
  `;

  @ViewChild(SidebarComponent, {static: true}) private sidebar: SidebarComponent;

  @ViewChild("demoItemTemplate", { read: TemplateRef, static: true }) private demoItemTemplate: any;

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.sidebar.setConfig({
      sizebarSize: 260,
      children: [
        {
          type: SearchListComponent,
          itemTemplate: this.demoItemTemplate,
          id: "search-list",
          title: "Demographics",
          dataSubject: this.demographics,
          loadingSubjects: this.demoLoadingSubject
        }
      ]
    });

    this.itemHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.itemExample, prism.languages["html"]) + "</code></pre>";
    this.changeDetectorRef.detectChanges();
  }

  /**
   * After the view has fully loaded, fake a request for data to populate the demographics.
   */
  ngAfterContentInit(): void {
    let temp: any[] = [];

    this.demoLoadingSubject[0].next(true);

    for (var i = 0; i < 50; i++) {
      temp.push({
        id: i,
        first: this.firsts[Math.floor(Math.random() * this.firsts.length)],
        last: this.lasts[Math.floor(Math.random() * this.lasts.length)]
      });
    }

    this.demographics.next(temp);
    this.demoLoadingSubject[0].next(false);
  }

  pushDemographic(): void {
    this.demoLoadingSubject[0].next(true);

    of(null).pipe(delay(1000)).subscribe(n => {
      this.demoLoadingSubject[0].next(false);

      let temp: any[] = this.demographics.getValue();
      temp.push({
        id: temp.length,
        first: "Xavier",
        last: "Black"
      });
      this.demographics.next(temp);
    });
  }

}
