/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, OnInit, ViewChild} from "@angular/core";

import {AppHeaderComponent, NavComponent, NavigationGlobalService, UlNavComponent, LiNavComponent, LiDdNavComponent} from "@huntsman-cancer-institute/navigation";
import {RoleEntity} from "@huntsman-cancer-institute/user";

import {NavigationDemoService} from "./navigation.service";

/**
 * @since 1.2.0
 */
@Component({
  selector: "hci-navigation-demo",
  template: `
    <hci-app-header #subHeader [style.display]="showSubHeader ? 'inherit' : 'none'"></hci-app-header>
    <router-outlet></router-outlet>
  `
})
export class NavigationComponent implements OnInit {

  @HostBinding("class") classlist: string = "outlet-column";

  @ViewChild("subHeader", {static: true}) subHeader: AppHeaderComponent;

  public showSubHeader: boolean = true;

  constructor(private navigationDemoService: NavigationDemoService, private navigationGlobalService: NavigationGlobalService) {}

  ngOnInit() {
    this.subHeader.setConfig({
      id: "nav-sub-header",
      navbarClasses: "sub-header",
      children: [
        {
          type: UlNavComponent, ulClass: "nav-container",
          children: [
            {type: LiNavComponent, liClass: "nav-item", title: "Home", route: "home"},
            {type: LiNavComponent, roleName: "NOT_AUTHC", liClass: "nav-item", title: "You are not logged in"},
            {type: LiNavComponent, roleName: "AUTHC", liClass: "nav-item", title: "Basic Sidebar and Layout", route: "basic-sidebar"},
            {type: LiNavComponent, roleName: "AUTHC", liClass: "nav-item", title: "Components", route: "components"},
            {type: LiNavComponent, roleName: "AUTHC", liClass: "nav-item", title: "Search Data", route: "search-sidebar-data"},
            {type: LiNavComponent, roleName: "AUTHC", liClass: "nav-item", title: "Search Call", route: "search-sidebar-data-call"},
            {type: LiNavComponent, roleName: "AUTHC", liClass: "nav-item", title: "Search Call Grouped", route: "search-sidebar-data-call-grouping"},
            {type: LiDdNavComponent, roleName: "USER", liClass: "nav-item", title: "User Menu", children: [
              {type: LiNavComponent, liClass: "nav-item", title: "Empty"}
            ]},
            {type: LiDdNavComponent, roleName: "ADMIN", liClass: "nav-item", title: "Admin Menu", children: [
                {type: LiNavComponent, liClass: "nav-item", title: "Empty"}
            ]},
            {type: LiDdNavComponent, roleName: "AUTHC", id: "options", liClass: "nav-item", iClass: "hci fa-core", title: "Options", children: [
              this.getOptionDropDown("Option 1", false),
              this.getOptionDropDown("Option 2", false),
              this.getOptionDropDown("Option 3", false),
              {
                type: LiNavComponent, id: "reset", title: "Reset", liClass: "divider-top", aClass: "dropdown-item",
                liClick: (component: NavComponent) => {
                  component.getParent().updateConfig({
                    iClass: "hci fa-consent-solid",
                    title: component.getParent().config.title
                  });
                }
              }
            ]},
          ]
        }
      ]
    });

    this.navigationDemoService.getUserRoles().subscribe((userRoles: RoleEntity[]) => {
      this.subHeader.getNavigationService().setUserRoles(userRoles);
    });
  }

  getOptionDropDown(title: string, dropdownDivider: boolean): any {
    return {
      type: LiNavComponent,
      title: title,
      aClass: "dropdown-item",
      dropdownDivider: dropdownDivider,
      liClick: (component: NavComponent) => {
        component.getParent().setTitle(component.title);
      }
    };
  }

  toggleSubHeader() {
    this.showSubHeader = !this.showSubHeader;
    this.navigationGlobalService.pushNotificationEvent();
  }

}
