import {Component, HostBinding, ViewChild} from "@angular/core";

import {AppHeaderComponent, LiNavComponent, UlNavComponent} from "@huntsman-cancer-institute/navigation";

@Component({
  selector: "hci-navigation-components",
  template: `
    <hci-app-header></hci-app-header>
    <router-outlet></router-outlet>
  `
})
export class ComponentsDemo {

  @HostBinding("class") classlist: string = "outlet-column";

  @ViewChild(AppHeaderComponent, {static: true}) subHeader: AppHeaderComponent;

  ngOnInit(): void {
    this.subHeader.setConfig({
      navbarClasses: "sub-header",
      children: [
        {type: UlNavComponent, children: [
            {type: LiNavComponent, title: "Home", route: "home"},
            {type: LiNavComponent, title: "Search List", route: "search-list"}
        ]}
      ]
    });
  }
}
