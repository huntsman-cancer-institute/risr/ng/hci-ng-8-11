/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {MatCardModule} from "@angular/material/card";
import {MatMenuModule} from "@angular/material/menu";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {NavigationModule} from "@huntsman-cancer-institute/navigation";

import {NavigationComponent} from "./navigation.component";
import {NAVIGATION_ROUTING} from "./navigation.routes";
import {NavigationDemoService} from "./navigation.service";
import {BasicSidebarComponent} from "./basic-sidebar/basic-sidebar.component";
import {NavigationHomeComponent} from "./home/navigation-home.component";
import {SearchSidebarDataComponent} from "./search-sidebar/search-sidebar-data.component";
import {SearchSidebarDataCallComponent} from "./search-sidebar/search-sidebar-data-call.component";
import {SearchSidebarDataCallGroupingComponent} from "./search-sidebar/search-sidebar-data-call-grouping.component";
import {SearchSidebarWithSearchOptions} from "./search-sidebar/search-sidebar-with-search-controller-options";
import {SelectedNameComponent} from "./search-sidebar/selected-name.component";
import {ComponentsDemo} from "./components/components.component";
import {ComponentsSearchListDemo} from "./components/search-list.component";
import {ComponentsHomeDemo} from "./components/home.component";

/**
 * @since 1.0.0
 */
@NgModule({
  imports: [
    NAVIGATION_ROUTING,
    CommonModule,
    FormsModule,
    RouterModule,
    MatCardModule,
    MatMenuModule,
    NgbModule,
    NavigationModule
  ],
  providers: [
    NavigationDemoService
  ],
  declarations: [
    NavigationComponent,
    NavigationHomeComponent,
    BasicSidebarComponent,
    SearchSidebarDataComponent,
    SearchSidebarDataCallComponent,
    SearchSidebarDataCallGroupingComponent,
    SearchSidebarWithSearchOptions,
    SelectedNameComponent,
    ComponentsDemo,
    ComponentsHomeDemo,
    ComponentsSearchListDemo
  ]
})
export class NavigationDemoModule {}
