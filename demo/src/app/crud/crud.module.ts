import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {MiscModule} from "@huntsman-cancer-institute/misc";
import {CrudModule} from "@huntsman-cancer-institute/crud";
import {NavigationModule} from "@huntsman-cancer-institute/navigation";

import {CrudDemoComponent} from "./crud.component";
import {CrudGetDemoComponent} from "./get/get.component";
import {CrudHomeDemoComponent} from "./home/home.component";
import {CrudFormDemoComponent} from "./form/form.component";

/**
 * @since 9.0.0
 */
@NgModule({
  imports: [
    RouterModule.forChild([
      {path: "", component: CrudDemoComponent,
        children: [
          {path: "", redirectTo: "home", pathMatch: "full"},
          {path: "form", component: CrudFormDemoComponent},
          {path: "get", component: CrudGetDemoComponent},
          {path: "home", component: CrudHomeDemoComponent}
        ]}
    ]),
    CommonModule,
    FormsModule,
    NgbModule,
    MiscModule,
    NavigationModule,
    CrudModule
  ],
  declarations: [
    CrudDemoComponent,
    CrudHomeDemoComponent,
    CrudGetDemoComponent,
    CrudFormDemoComponent
  ]
})
export class CrudDemoModule {}
