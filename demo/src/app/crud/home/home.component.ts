/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding} from "@angular/core";

/**
 * @since 9.0.0
 */
@Component({
  selector: "hci-crud-home-demo",
  template: `
    <div class="d-flex flex-column flex-shrink-0 m-3">
      <h4>Create, Read, Update and Delete</h4>
      <div class="d-flex mb-3">
        This demo tests the CRUD API.  For now, a dozen rows for subject name are provided as a test case.  The interceptor
        sets this up on startup and from then on the data can be modified to test the library.
      </div>
      <div class="d-flex mb-3">
        The "Get" route shows two separate calls, the getAll and get for an ID.  The "Grid" route uses getAll to load the
        grid and from then on, grid editing can be used to test PUTs and DELETEs.
      </div>
      <div class="d-flex mb-3">
        Each function has two forms.  Using GET as an example, the first form returns an observable from the http request
        that you subscribe to.  The second form returns a boolean subject which starts as true prior to the request, and
        changes to false when the request completes.  This is used to control loading spinners or anything else that needs
        to know when the request is active.  In this case to fetch the data, you must provide a callback function to the
        GET request.
      </div>
    </div>
  `
})
export class CrudHomeDemoComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

}
