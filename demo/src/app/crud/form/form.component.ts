/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, isDevMode, ViewChild} from "@angular/core";
import {SafeHtml} from "@angular/platform-browser";

import {Subject} from "rxjs";
import * as prism from "prismjs";

import {CrudService, CrudContainerComponent} from "@huntsman-cancer-institute/crud";

/**
 * @since 9.0.0
 */
@Component({
  selector: "hci-crud-form-demo",
  template: `
    <div class="d-flex flex-column flex-shrink-0 m-3">
      <h3 class="mb-3">CRUD Form</h3>
      <div class="d-flex mb-3">
        There are two fundamental differences in how CRUD components may be used, when no data exists and it is being entered
        for the first time such as in a wizard, and when data exists and it is being updated.
        <br />
        As for styling, invalid fields are indicated by a background red.  Fields that are PHI are bordered with orange.
      </div>
      <div class="d-flex mb-3">
        <div class="d-flex flex-column pr-2" style="flex: 0 0 50%; border-right: #3d7a99 2px solid;">
          <div class="d-flex">
            <h4>CRUD Wizard</h4>
            <button type="button" class="ml-3 btn btn-outline-primary top-0" [ngbPopover]="configWizard" popoverTitle="Config" placement="right">Show Config</button>
            <ng-template #configWizard>
              <div [innerHTML]="wizardHtml"></div>
            </ng-template>
          </div>
          <div class="d-flex flex-column mt-3 p-3" style="border: #3d7a99 1px solid; border-radius: 1rem; background-color: #f0faff;">
            <hci-busy [busy]="[busy]"></hci-busy>

            <h4 class="mb-3">Add Subject</h4>
            <hci-crud-container
              #crudWizard
              [ids]="[{key: 'idSystemSource', value: 1}, {key: 'idRecordSource', value: 1}]"
              [saveable]="false"
              [crudGroups]="[
                {
                  display: 'Name', className: 'hci.ri.core.subject.model.Name', mode: 'singular',
                  displayFields: [['title', 'firstName', 'lastName']],
                  styles: [{field: 'title', styles: {'flex': '0 0 15%'}}]
                },
                {
                  display: 'Gender', className: 'hci.ri.core.subject.model.Gender', mode: 'singular',
                  displayFields: [['idGenderType']]
                },
                {
                  display: 'Address', className: 'hci.ri.core.subject.model.Address', mode: 'singular',
                  displayFields: [['idAddressType'], ['address1', 'address2'], ['city', 'state', 'postalCode']]
                }
              ]">
            </hci-crud-container>
            <div class="d-flex justify-content-end">
              <button class="btn btn-green mr-3" (click)="addSubject()">Add</button>
              <button class="btn btn-red">Cancel</button>
            </div>
          </div>
          <button type="button" class="mt-3 btn btn-outline-primary top-0" [ngbPopover]="dataPopup" popoverTitle="Wizard Data" placement="top">Show Data</button>
          <ng-template #dataPopup>
            <pre style="max-width: 50vw; max-height: 50vh; min-width: 50vw;" class="x-auto y-auto">
              <div [innerHTML]="wizardDataHtml"></div>
            </pre>
          </ng-template>
        </div>
        <div class="d-flex flex-column pl-2" style="flex: 0 0 50%;">
          <h4 class="mb-3">CRUD Update</h4>
          <hci-crud-container
            #crudUpdate
            [crudGroups]="[
                {
                  display: 'Name', className: 'hci.ri.core.subject.model.Name', mode: 'singular', dataField: 'namePref',
                  displayFields: [['title', 'firstName', 'lastName']],
                  styles: [{field: 'title', styles: {'flex': '0 0 15%'}}]
                },
                {
                  display: 'Gender', className: 'hci.ri.core.subject.model.Gender', mode: 'singular', dataField: 'genderPref',
                  displayFields: [['idGenderType']]
                },
                {
                  display: 'Address', className: 'hci.ri.core.subject.model.Address', mode: 'singular', dataField: 'addressPref',
                  displayFields: [['idAddressType'], ['address1', 'address2'], ['city', 'state', 'postalCode']]
                }
              ]">
          </hci-crud-container>
        </div>
      </div>
    </div>
  `
})
export class CrudFormDemoComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

  @ViewChild("crudWizard", {static: true}) crudWizard: CrudContainerComponent;
  @ViewChild("crudUpdate", {static: true}) crudUpdate: CrudContainerComponent;

  busy: boolean = false;
  studySubject: any = {};

  wizardHtml: SafeHtml;
  wizard: string = `
            <hci-crud-container
              #crudWizard
              [ids]="[{key: 'idSystemSource', value: 1}, {key: 'idRecordSource', value: 1}]"
              [saveable]="false"
              [crudGroups]="[
                {
                  display: 'Name', className: 'hci.ri.core.subject.model.Name', mode: 'singular',
                  displayFields: [['title', 'firstName', 'lastName']],
                  styles: [{field: 'title', styles: {'flex': '0 0 15%'}}]
                },
                {
                  display: 'Gender', className: 'hci.ri.core.subject.model.Gender', mode: 'singular',
                  displayFields: [['idGenderType']]
                },
                {
                  display: 'Address', className: 'hci.ri.core.subject.model.Address', mode: 'singular',
                  displayFields: [['idAddressType', 'address1', 'address2'], ['city', 'state', 'postalCode']]
                }
              ]">
  `;

  wizardDataHtml: any = "<pre></pre>";

  constructor(private crudService: CrudService) {}

  ngOnInit() {
    this.wizardHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">";
    this.wizardHtml += prism.highlight(this.wizard, prism.languages["html"]);
    this.wizardHtml += "</code></pre>";
  }

  ngAfterViewInit(): void {
    this.crudService.get("hci.ri.core.subject.model.StudySubject", 21).subscribe((data: Object) => {
      console.debug("this.crudService.get StudySubject 21");
      console.debug(data);
      this.crudUpdate.setData(data);
    });
  }

  addSubject(): void {
    this.busy = true;

    this.crudWizard.saveJoin().subscribe(responses => {
      this.busy = false;
      console.debug(responses);

      this.wizardDataHtml = "<pre class=\"language-js\"><code #code class=\"language-js\">";
      this.wizardDataHtml += prism.highlight(JSON.stringify(responses, undefined, 2), prism.languages["javascript"]);
      this.wizardDataHtml += "</code></pre>";
    });
  }
}
