/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {OnInit, Component, HostBinding} from "@angular/core";

import * as prism from "prismjs";

@Component({
  selector: "hci-input-combobox-demo",
  template: `
    <h3>ComboBox</h3>
    <div class="card-group flex-column flex-shrink-0">

      <div class="card">
        <div class="card-header">
          <h4>Single Input</h4>
        </div>

        <div class="card-body">
          <hci-combobox [(ngModel)]="value1"
                        [displayField]="'label'"
                        [valueField]="'id'"
                        [(options)]="data"></hci-combobox>
        </div>

        <div class="card-body">
          <button mat-button [matMenuTriggerFor]="popup1" class="mr-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
          <mat-menu #popup1="matMenu" class="prism" xPosition="after">
              <div [innerHTML]="example1Html"></div>
          </mat-menu>
          Bound Data: {{value1}}
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          <h4>Multi Input</h4>
        </div>

        <div class="card-body">
          <hci-multi-combobox [(ngModel)]="value2"
                              [displayField]="'label'"
                              [valueField]="'id'"
                              [(options)]="data"></hci-multi-combobox>
        </div>

        <div class="card-body">
          <button mat-button [matMenuTriggerFor]="popup2" class="mr-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
          <mat-menu #popup2="matMenu" class="prism" xPosition="after">
              <div [innerHTML]="example2Html"></div>
          </mat-menu>
          Bound Data: {{value2}}
        </div>
      </div>

    </div>
  `,
  styles: [`
  `]
})
export class ComboBoxDemoComponent implements OnInit {

  @HostBinding("class") classlist: string = "outlet-column y-auto m-3";

  public data: Object[] = [
    {id: 1, label: "Test-1"},
    {id: 2, label: "Test-2"},
    {id: 3, label: "Test-3"},
    {id: 4, label: "Test-4"},
    {id: 5, label: "Test-5"},
    {id: 6, label: "Test-6"},
    {id: 7, label: "Test-7"},
    {id: 8, label: "Test-8"},
  ];

  public value1: any;
  public value2: any;

  example1: string = `
    <hci-combobox [(ngModel)]="value1"
                  [displayField]="'label'"
                  [valueField]="'id'"
                  [(options)]="data"></hci-combobox>
  `;
  example1Html: string;

  example2: string = `
    <hci-multi-combobox [(ngModel)]="value1"
                        [displayField]="'label'"
                        [valueField]="'id'"
                        [(options)]="data"></hci-multi-combobox>
  `;
  example2Html: string;

  ngOnInit() {
    this.example1Html = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.example1, prism.languages["html"]) + "</code></pre>";
    this.example2Html = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.example2, prism.languages["html"]) + "</code></pre>";
  }
}
