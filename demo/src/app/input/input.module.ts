/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {MatMenuModule} from "@angular/material/menu";
import {MatCardModule} from "@angular/material/card";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {UlNavComponent, LiNavComponent, LiDdNavComponent, BrandNavComponent, NavigationModule} from "@huntsman-cancer-institute/navigation";
import {DateModule, DropdownModule, InlineModule, SelectModule} from "@huntsman-cancer-institute/input";

import {InputComponent} from "./input.demo";
import {INPUT_ROUTING} from "./input.routes";
import {InputService} from "./input.service";
import {InputHomeComponent} from "./home/input-home.demo";
import {InputInlineComponent} from "./inline/inline.demo";
import {InputDropdownComponent} from "./dropdown/dropdown.demo";
import {InputDateComponent} from "./date/date.demo";
import {SelectDemo} from "./select/select.demo";
import {ComboBoxDemoComponent} from "./combobox/combobox.demo";

/**
 * @since 1.0.0
 */
@NgModule({
  imports: [
    INPUT_ROUTING,
    CommonModule,
    FormsModule,
    RouterModule,
    MatCardModule,
    MatMenuModule,
    NgbModule,
    NavigationModule,
    DropdownModule,
    SelectModule,
    InlineModule,
    DateModule,
    ReactiveFormsModule
  ],
  providers: [
    InputService
  ],
  declarations: [
    InputComponent,
    InputHomeComponent,
    InputDropdownComponent,
    InputDateComponent,
    InputInlineComponent,
    SelectDemo,
    ComboBoxDemoComponent
  ],
  entryComponents: [
    UlNavComponent,
    LiNavComponent,
    LiDdNavComponent,
    BrandNavComponent
  ]
})
export class InputDemoModule {}
