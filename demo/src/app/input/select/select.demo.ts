/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {AfterViewInit, Component, HostBinding} from "@angular/core";

import {of} from "rxjs";
import {delay} from "rxjs/operators";
import {NgForm} from "@angular/forms";

/**
 * @since 11.0.0
 */
@Component({
  selector: "hci-input-dropdown-demo",
  template: `
    <h3>Select</h3>
    <div class="card-group flex-column flex-shrink-0">

      <div class="card">
        <div class="card-header">
          <h4>Native Single Select</h4>
        </div>
        <div class="card-body">
          <div class="d-flex">
            <div class="d-flex flex-column col-md-3">
              Dictionary Classname
              <hci-native-select url="hci.ri.core.subject.model.dictionary.GenderType"
                                 [(ngModel)]="data1"></hci-native-select>
            </div>
            <div class="d-flex flex-column col-md-3">
              Url to Array
              <hci-native-select url="/api/dictionaries/hci.ri.core.subject.model.dictionary.GenderType/dropdown-entries"
                                 [(ngModel)]="data1"></hci-native-select>
            </div>
            <div class="d-flex flex-column col-md-3">
              <div class="d-flex">
                <div class="d-flex flexcolumn col-md-6">
                    Data Array
                </div>
                <div class="d-flex flexcolumn col-md-6">
                  <button type="button" (click)="disabled = !disabled">Toggle Disable dropdown</button>
                </div>
              </div>
              <hci-native-select [entries]="[{id: 2, display: 'F'}, {id: 1, display: 'M'}, {id: 3, display: 'U'}]"
                                 [(ngModel)]="data1" [disabled]="disabled"></hci-native-select>
            </div>
            <div class="d-flex flex-column col-md-3">
              Data Array with 0 Value
              <hci-native-select [entries]="[{id: 0, display: 'Zero'}, {id: 1, display: 'One'}, {id: 2, display: 'Two'}]"
                                 [(ngModel)]="dataZero"></hci-native-select>
            </div>
            <div class="d-flex flex-column col-md-3">
              Url to Array with sorting
              <hci-native-select url="/api/dictionaries/hci.ri.core.subject.model.Organization/entries"
                                 displayKey="dictionarydisplay"
                                 idKey="id"
                                 sortKey="dictionarydisplay"
                                 [(ngModel)]="data4"></hci-native-select>
            </div>
            Bound Data: {{data1}}
          </div>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          <h4>Native Single Select with Filtering</h4>
        </div>
        <div class="card-body">
          <div class="d-flex">
            <div class="d-flex flex-column col-md-3">
              Filter Dropdown
              <hci-native-select url="/api/dictionaries/hci.updb.model.dict.SpanishSourceDict/entries"
                                 displayKey="spanishSource"
                                 idKey="spanishSource"
                                 [(ngModel)]="spanishSource"></hci-native-select>
            </div>
            <div class="d-flex flex-column col-md-3">
              Filters the data in this Dropdown
              <hci-native-select url="/api/dictionaries/hci.updb.model.dict.SpanishTypeDict/entries"
                                 displayKey="definition"
                                 idKey="spanishTypeCode"
                                 filterKey="spanishSource"
                                 [filter]="spanishSource"
                                 [(ngModel)]="spanishTypeCode"></hci-native-select>
            </div>

          </div>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          <h4>Material Single Select</h4>
        </div>
        <div class="card-body">
          <div class="d-flex">
            <div class="d-flex flex-column col-md-3">
              Dictionary Classname
              <hci-md-select label="Gender"
                             url="hci.ri.core.subject.model.dictionary.GenderType"
                             [(ngModel)]="data2"
                             style=""></hci-md-select>
            </div>
            <div class="d-flex flex-column col-md-3">
              Url to Array
              <hci-md-select label="Gender"
                             url="/api/dictionaries/hci.ri.core.subject.model.dictionary.GenderType/dropdown-entries"
                             [(ngModel)]="data2"></hci-md-select>
            </div>
            <div class="d-flex flex-column col-md-3">
              Data Array
              <hci-md-select label="Gender"
                             [entries]="[{id: 2, display: 'F'}, {id: 1, display: 'M'}, {id: 3, display: 'U'}]"
                             [(ngModel)]="data2"></hci-md-select>
            </div>
            Bound Data: {{data2}}
          </div>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          <h4>Material Multi Select</h4>
        </div>
        <div class="card-body">
          <div class="d-flex">
            <div class="d-flex flex-column col-md-3">
              Dictionary Classname
              <hci-md-multi-select label="Gender"
                                   url="hci.ri.core.subject.model.dictionary.GenderType"
                                   [(ngModel)]="data3"></hci-md-multi-select>
            </div>
            <div class="d-flex flex-column col-md-3">
              Url to Array
              <hci-md-multi-select label="Gender"
                                   url="/api/dictionaries/hci.ri.core.subject.model.dictionary.GenderType/dropdown-entries"
                                   [(ngModel)]="data3"></hci-md-multi-select>
            </div>
            <div class="d-flex flex-column col-md-3">
              Data Array
              <hci-md-multi-select label="Gender"
                                   [entries]="[{id: 2, display: 'F'}, {id: 1, display: 'M'}, {id: 3, display: 'U'}]"
                                   [(ngModel)]="data3"></hci-md-multi-select>
            </div>
            Bound Data: {{data3}}
          </div>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          <h4>Form</h4>
        </div>
        <div class="card-body">
          <div class="d-flex">
            <form #myForm="ngForm" (ngSubmit)="onSubmit(myForm)" class="d-flex flex-grow-1 flex-column">
              <div class="d-flex flex-wrap flex-grow-1">
                <div class="d-flex col-md-3">
                  <hci-native-select name="nssGenderType"
                                     [required]="true"
                                     label="Gender"
                                     url="hci.ri.core.subject.model.dictionary.GenderType"
                                     [(ngModel)]="form1nss"></hci-native-select>
                </div>
                <div class="d-flex flex-column col-md-3">
                  <hci-md-select name="mssGenderType"
                                 [required]="true"
                                 label="Gender"
                                 url="hci.ri.core.subject.model.dictionary.GenderType"
                                 [(ngModel)]="form1mss"></hci-md-select>
                </div>
                <div class="d-flex flex-column col-md-3">
                  <hci-md-multi-select name="mmsGenderType"
                                       [required]="true"
                                       label="Gender"
                                       url="hci.ri.core.subject.model.dictionary.GenderType"
                                       [(ngModel)]="form1mms"></hci-md-multi-select>
                </div>
              </div>
              <div class="d-flex flex-wrap flex-grow-1">
                <button type="submit" class="btn btn-success mr-5">Submit</button>
                <div class="">
                  Form Data: {{myForm.form.value | json}}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

    </div>
  `,
  styles: [`

  .test-fix{
  	width: 300px;
  }

  `]
})
export class SelectDemo implements AfterViewInit {

  @HostBinding("class") classlist: string = "outlet-column y-auto m-3";

  data1: any;
  data2: any;
  data3: any[] = [];
  data4: any;
  dataZero: any = 0;

  form1nss: any;
  form1mss: any;
  form1mms: any;

  disabled: boolean = false;

  spanishSource: string = "";
  spanishTypeCode: string = "";

  ngAfterViewInit(): void {
    of(2).pipe(delay(2000)).subscribe((data: any) => {
      this.data1 = data;
      this.data2 = data;
    });
  }

  onSubmit(myForm: NgForm): void {
    console.debug(myForm);
  }
}
