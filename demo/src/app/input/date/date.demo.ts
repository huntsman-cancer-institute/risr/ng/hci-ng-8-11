/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding} from "@angular/core";
import {DateComponent} from "@huntsman-cancer-institute/input";

/**
 * @since 1.2.0
 */
@Component({
  selector: "hci-date-demo",
  template: `
    <div class="d-flex flex-column m-3">
      <h3>Date</h3>
      <div class="card-group flex-column flex-shrink-0">
        <div class="card">
          <div class="card-header">
            <h4>Date Input How To</h4>
          </div>
          <div class="card-body">
            <div class="card-text">
              These components use the ngb datepicker to build <b>non inline</b> variations of datepickers (including a date range picker).
              If you click the check button, type enter, or click outside, then the data saves.  If you click on the cancel button or type
              escape, then the data doesn't save.
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>hci-date (short date format)</h4>
          </div>
          <div class="card-body">
            <hci-date [(inputData)]="dateValue"></hci-date>
          </div>
          <div class="card-body">
            Bound Data: {{ dateValue }}
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>hci-date (long date format)</h4>
          </div>
          <div class="card-body">
            <hci-date [(inputData)]="dateValue2" [dateFormat]="'longDate'"></hci-date>
          </div>
          <div class="card-body">
            Bound Data: {{ dateValue2 }}
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>hci-date-range (long date format)</h4>
          </div>
          <div class="card-body">
            <hci-date-range [startLabel]="startLabel"
                            [endLabel]="endLabel"
                            (inputDataChange)="updateDateRangeValues($event)"
                            [(inputStartData)]="startDateValue"
                            [(inputEndData)]="endDateValue"
                            [dateFormat]="'longDate'">

            </hci-date-range>
          </div>
          <div class="card-body">
            Bound Data: <b>Start</b>: {{ (startDateValue !== null) ? startDateValue : "--------------" }} <b>End</b>:  {{ (endDateValue !==null) ? endDateValue : "--------------" }}
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>hci-date-range (long date format)</h4>
          </div>
          <div class="card-body">
            <div class="d-flex flex-column">
              <hci-date-range [startLabel]="startLabel"
                              [endLabel]="endLabel"
                              (inputDataChange)="updateDateRangeValues2($event)"
                              [(inputStartData)]="startDateValue2"
                              [(inputEndData)]="endDateValue2"
                              [dateFormat]="'longDate'"
                              style="max-width: 300px;">
              </hci-date-range>
            </div>
          </div>
          <div class="card-body">
            Bound Data: <b>Start</b>: {{ (startDateValue2 !== null) ? startDateValue2 : "--------------" }} <b>End</b>:  {{ (endDateValue2 !==null) ? endDateValue2 : "--------------" }}
          </div>
        </div>

        <div style="margin-top: 200px;"></div>
      </div>
    </div>
  `
})
export class InputDateComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

  public dateValue: string = "2018-01-18T12:00-06:00";
  public dateValue2: string = "2011-11-11T12:00-06:00";

  public startDateValue: string = "2009-11-11T12:00-06:00";
  public startDateValue2: string;
  public endDateValue: string = "2018-01-02T12:00-06:00";
  public endDateValue2: string = "2018-01-02T12:00-06:00";

  public startLabel: string = "Start";
  public endLabel: string = "End";

  public updateDateRangeValues(inputData: any[]) {
    if (inputData[0]) {
      this.startDateValue = <string>inputData[0];
    } else {
      this.startDateValue = null;
    }
    if (inputData[1]) {
      this.endDateValue = <string>inputData[1];
    } else {
      this.endDateValue =  null;
    }
  }

  public updateDateRangeValues2(inputData: any[]) {
    if (inputData[0]) {
      this.startDateValue2 = <string>inputData[0];
    } else {
      this.startDateValue2 = null;
    }
    if (inputData[1]) {
      this.endDateValue2 = <string>inputData[1];
    } else {
      this.endDateValue2 =  null;
    }
  }
}
