/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, OnInit} from "@angular/core";

import {of} from "rxjs";
import {delay} from "rxjs/operators";

import {DROPDOWN_TYPE} from "@huntsman-cancer-institute/input";

/**
 * @since 1.2.0
 */
@Component({
  selector: "hci-input-dropdown-demo",
  template: `
    <div class="d-flex flex-column m-3">
      <h3>Dropdown</h3>
      <div class="card-group flex-column flex-shrink-0">

        <div class="card">
          <div class="card-header">
            <h4>Dropdown Input How To</h4>
          </div>
          <div class="card-body">
            <div class="card-text">
              These components are designed to work similar to JIRA Dropdown inputs with typeAhead.  Under normal circumstances they appear as
              plain text.  If you hover over, you have the option of editing the field.  While editing, if you click the
              check button, type enter, or click outside, then the data saves.  If you click on the cancel button or type
              escape, then the data doesn't save.
            </div>
          </div>
        </div>
        
        <div class="card">
          <div class="card-header">
            <h4>Single Input</h4>
          </div>
          <div class="card-body">
            <hci-dropdown [(inputSingleData)]="inputSingleValue"
                          [dataUrl]="'hci.ri.core.organization.dictionary.model.Organization/entries'"
                          [dropdownType]="inputSingle"
                          [placeholder]="'Single Input'"
                          [displayId]="'id'"
                          [displayField]="dropDownDataField"></hci-dropdown>
          </div>
          <div class="card-body">
            Bound Data: {{inputSingleValue["id"]}} : {{inputSingleValue[dropDownDataField]}}
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>Multiple Input</h4>
          </div>
          <div class="card-body">
            <hci-dropdown [(inputMultiData)]="inputMultiValue"
                          [optionData]="dropdownDataSetSingleInput"
                          [dropdownType]="inputMulti"
                          [placeholder]="'Multi Input'"
                          [displayId]="'id'"
                          [displayField]="dropDownDataField"></hci-dropdown>
          </div>
          <div class="card-body">
            Bound Data:
            <li *ngFor="let row of inputMultiValue">{{row["id"]}} : {{row[dropDownDataField]}} </li>
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>Single Select</h4>
          </div>
          <div class="card-body">
            <hci-dropdown [(inputSingleData)]="selectSingleValue"
                          [optionData]="dropdownDataSet"
                          [dropdownType]="selectSingle"
                          [placeholder]="'Single Select'"
                          [displayId]="'id'"
                          [displayField]="dropDownDataField"></hci-dropdown>
          </div>
          <div class="card-body">
            Bound Data: {{selectSingleValue["id"]}} : {{selectSingleValue[dropDownDataField]}}
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>Multiple Select</h4>
          </div>
          <div class="card-body">
            <hci-dropdown [(inputMultiData)]="selectMultiValue"
                          [dataUrl]="'hci.ri.core.organization.dictionary.model.Organization/entries'"
                          [dropdownType]="selectMulti"
                          [placeholder]="'Multi Select'"
                          [displayId]="'id'"
                          [displayField]="dropDownDataField"></hci-dropdown>
          </div>
          <div class="card-body">
            Bound Data:
            <li *ngFor="let row of selectMultiValue">{{row["id"]}} : {{row[dropDownDataField]}} </li>
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>Custom Template Single Select</h4>
          </div>
          <div class="card-body">
            <hci-dropdown [(inputSingleData)]="selectSingleCustomValue"
                          [dataUrl]="'hci.ri.core.organization.dictionary.model.Organization/entries'"
                          [dropdownType]="selectSingleCustom"
                          [placeholder]="'Single Select with id'"
                          [displayId]="'id'"
                          [displayField]="dropDownDataField"></hci-dropdown>
          </div>
          <div class="card-body">
            Bound Data: {{selectSingleCustomValue["id"]}} : {{selectSingleCustomValue[dropDownDataField]}}
          </div>
        </div>

      </div>
    </div>
  `
})
export class InputDropdownComponent implements OnInit {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

  public dropdownDataSet: Object[] = [
    {"id": 1, "dictionarydisplay": "Test-1", "description": "description-test-1"},
    {"id": 2, "dictionarydisplay": "Test-2", "description": "description-test-2"},
    {"id": 3, "dictionarydisplay": "Test-3", "description": "description-test-3"},
    {"id": 4, "dictionarydisplay": "Test-4", "description": "description-test-4"},
    {"id": 5, "dictionarydisplay": "Test-5", "description": "description-test-5"},
    {"id": 6, "dictionarydisplay": "Test-6", "description": "description-test-6"},
    {"id": 7, "dictionarydisplay": "Test-7", "description": "description-test-7"},
    {"id": 8, "dictionarydisplay": "Test-8", "description": "description-test-8"},
  ];

  public dropdownDataSetSingleInput: Object[] = [
    {"id": 1, "dictionarydisplay": "SingleInputTest-1"},
    {"id": 2, "dictionarydisplay": "SingleInputTest-2"},
    {"id": 3, "dictionarydisplay": "SingleInputTest-3"},
  ];

  // The field name of the Dropdwon list data. Modify it from your DropdownObject above.
  public dropDownDataField = "dictionarydisplay";

  //dropdownType: set your dropdown type as one of the follow:
  public inputSingle = DROPDOWN_TYPE.INPUT_SINGLE;
  public inputMulti = DROPDOWN_TYPE.INPUT_MULTI;
  public selectSingle = DROPDOWN_TYPE.SELECT_SINGLE;
  public selectMulti = DROPDOWN_TYPE.SELECT_MULTI;
  public selectSingleCustom = DROPDOWN_TYPE.SELECT_SINGLE_CUSTOM;

  public inputSingleValue: Object;
  public selectSingleValue: Object;
  public selectSingleCustomValue: Object;
  public inputMultiValue: Object[] = [];
  public selectMultiValue: Object[] = [];

  ngOnInit () {
    this.inputSingleValue = this.dropdownDataSetSingleInput[0];
    this.selectSingleValue = this.dropdownDataSet[1];
    this.selectSingleCustomValue = this.dropdownDataSet[3];

    this.inputMultiValue.push(this.dropdownDataSetSingleInput[0]);
    this.inputMultiValue.push(this.dropdownDataSetSingleInput[1]);

    this.selectMultiValue.push(this.dropdownDataSet[3]);
    this.selectMultiValue.push(this.dropdownDataSet[4]);
  }
}
