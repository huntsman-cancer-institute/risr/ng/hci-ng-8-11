/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding} from "@angular/core";

import {Subject} from "rxjs";

/**
 * @since 2.1.0
 */
@Component({
  selector: "hci-busy-demo",
  template: `
    <div class="d-flex flex-column flex-shrink-0 m-3">
      <h3 class="mb-3">Busy</h3>
      <div class="d-flex mb-3">
        <button class="btn btn-primary mr-3" (click)="toggleBusy()">Toggle Busy</button>
        <div ngbDropdown
             placement="bottom-left"
             triggers="manual">
          <button ngbDropdownToggle
                  id="dropdown"
                  class="btn btn-primary">
            Dropdown with Busy
          </button>
          <div ngbDropdownMenu
               aria-labelledby="dropdown"
               id="testdd"
               style="width: 300px; height: 300px;">
            <hci-busy [busy]="[busy]" parentSelector=".dropdown-menu" rootClass="no-background" style="color: black;"></hci-busy>
            Dropdown that contains a busy
          </div>
        </div>
      </div>
      
      <div id="test1" class="flex-grow-1">
        <hci-busy [busy]="[busy]"></hci-busy>
        <h4 class="mb-3">Busy Array</h4>
        <div class="mb-3">
          This content has a busy as well as the above dropdown.  This is to test what happens when both busy components
          overlap.  We want the dropdown to be on top of the busy underneath it.  Dropdowns are by default a z-index of 1010
          and busy is 500.
        </div>
        <div class="mb-3">
          This is also a test of how the busy component auto fits its parent conent.
        </div>
      </div>
      
      <div id="test2" class="flex-grow-1">
        <hci-busy [busySubjects]="[busySubject]"></hci-busy>
        <h4 class="mb-3">Busy Subject Array</h4>
        <div class="mb-3">
          This content has a busy as well as the above dropdown.  This is to test what happens when both busy components
          overlap.  We want the dropdown to be on top of the busy underneath it.  Dropdowns are by default a z-index of 1010
          and busy is 500.
        </div>
        <div class="mb-3">
          This is also a test of how the busy component auto fits its parent conent.
        </div>
      </div>
      
      <div id="test3" class="flex-grow-1">
        <hci-busy [busySubjects]="[busySubject]" rootClass="no-background"></hci-busy>
        <h4 class="mb-3">No Background</h4>
        <div class="mb-3">
          Custom classes added to the .hci-busy selector.
        </div>
      </div>

      <div id="test4" class="d-flex flex-column flex-grow-1">
        <h4 class="mb-3">Parent Selector</h4>
        <div class="flex-grow-0 mb-3">
          <hci-busy [busySubjects]="[busySubject]" parentSelector="#test4"></hci-busy>
          Busy added to div, but want to show busy over that div's parent.
        </div>
      </div>
      
      <div style="height: 12rem;"></div>
    </div>
  `
})
export class BusyDemoComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

  busy: boolean = false;
  busySubject: Subject<boolean> = new Subject<boolean>();

  toggleBusy() {
    this.busy = !this.busy;
    this.busySubject.next(this.busy);
  }

}
