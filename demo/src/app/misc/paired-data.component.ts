import {Component, HostBinding, ViewChild, TemplateRef} from "@angular/core";
import {SafeHtml} from "@angular/platform-browser";

import * as prism from "prismjs";

@Component({
  selector: "hci-paired-data-demo",
  template: `
    <div class="d-flex flex-column flex-shrink-0 m-3">
      <h3 class="mb-3">Paired Data Display</h3>
      <div class="d-flex">
      Simple component to display key value pairs in a given number of columns
      </div>
      <div class="d-flex">
        <button type="button" class="btn btn-outline-primary" [ngbPopover]="configAccord" popoverTitle="Config" placement="right">Show Config</button>
        <ng-template #configAccord>
          <div [innerHTML]="exampleHtml"></div>
        </ng-template>
      </div>
   <div class="card flex-shrink-1">
          <div class="card-header">
            <h4> Nine Key-Value Pairs displayed in 2 columns</h4>
          </div>
          <div class="card-body">

              <hci-paired-data 
                     [numColumns]="2"
                     [header]="header"
                     [dataArray]="dataArray1"
                      id="test-paired-with-2-columns"
                       class="mt-auto mb-0">
              </hci-paired-data>
          </div>
   </div> 
            
   <div class="card flex-shrink-1">
          <div class="card-header">
            <h4> Nine Key-Value Pairs displayed in 3 columns</h4>
          </div>
          <div class="card-body">

              <hci-paired-data 
                     [numColumns]="3"
                     [header]="header"
                     [dataArray]="dataArray1"
                      id="test-paired-with-3-columns"
                       class="mt-auto mb-0">
              </hci-paired-data>
          </div>
   </div> 
    <div class="card flex-shrink-1">
          <div class="card-header">
            <h4> Nine Key-Value Pairs displayed in 3 columns and with custom key and value templates</h4>
          </div>
          <div class="card-body">

              <hci-paired-data 
                     [numColumns]="3"
                     [header]="header"
                     [dataArray]="dataArray2"
                      id="test-paired-with-3-columns-and-custom-templates"
                       class="mt-auto mb-0">
              </hci-paired-data>
          </div>
   </div> 
   </div>
    <!-- Custom templates for key and value row elements -->
   <ng-template #keyTemplate let-row="rowData">
      <div class="d-flex flex-grow-1 pl-2" style="background-color:red;">
        {{row.key}} 
      </div>
    </ng-template>
    
    <ng-template #valueTemplate let-row="rowData">
      <div class="d-flex flex-grow-1 pl-2" style="align-items: center;color:green;font-weight:bold">
        {{row.value}} 
      </div>
      <div class="d-flex ml-2" style="color: black">{{row.value2}}</div>
    </ng-template>
    
  `
})
export class PairedDataDemoComponent {

  @HostBinding("class") classlist: string = "outlet-column";

  exampleHtml: SafeHtml;
  example: string = `
    <div class="outlet-column y-auto m-3">
      <h3 class="mb-4">Paired Data Demo</h3>
      <div class="d-flex flex-shrink-0 mb-4">
        <div class="d-flex flex-column">
          <h4>Key Value pairs displayed in two columns</h4>
     
    <hci-paired-data [numColumns]="numCols1"
                     [header]="header1"
                     [dataArray]="dataArray1"
                      id="test-paired-with-2-columns"
                       class="mt-auto mb-0">
    </hci-paired-data>
    <hci-paired-data [numColumns]="numCols2"
                     [header]="header2"
                     [dataArray]="dataArray2"
                     id="test-paired-with-3-columns"
                       class="mt-auto mb-0">
    </hci-paired-data>
  `;

  @ViewChild("keyTemplate", {read: TemplateRef, static: true}) keyTemplate: TemplateRef<any>;
  @ViewChild("valueTemplate", {read: TemplateRef, static: true}) valueTemplate: TemplateRef<any>;
  
  header = {"title": "Sample Details", "icon": "fa-sample-hex-open"};

  dataArray1 = [{"key": "ID:", "value": "123456"},
                {"key": "Amount:", "value": "1.2 ml"},
                {"key": "Concentration:", "value": "200 ng/ul"},
                {"key": "Sample Type:", "value": "DNA"},
                {"key": "Tissue Type:", "value": "Blood"},
                {"key" : "System Source:", "value": "itBioPath"},
                {"key" : "Custodial Lab:", "value": "HCI - BMP-Blood"},
                {"key" : "Studies", "value": "CTO-TOWARDS (91596)"},
                {"key" : "Primary Subject", "value": "PNS123"}
                ];
                
  dataArray2 = [];
  
  ngOnInit() {
    this.exampleHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">";
    this.exampleHtml += prism.highlight(this.example, prism.languages["html"]);
    this.exampleHtml += "</code></pre>";
   
    // have to define dataArray2 here since only at this point do the keyTemplate and valueTemplate exist
    this.dataArray2 = [{"key": "ID:", "value": "123456", "keyTemplate": this.keyTemplate},
                       {"key": "Amount:", "value": "1.2 ml", "value2": "Cryovial (2.0 ml)", "valueTemplate": this.valueTemplate}, 
                       {"key": "Concentration:", "value": "200 ng/ul"},
                       {"key": "Sample Type:", "value": "DNA"},
                       {"key": "Tissue Type:", "value": "Blood"},
                       {"key" : "System Source:", "value": "itBioPath", "keyTemplate": this.keyTemplate},
                       {"key" : "Custodial Lab:", "value": "HCI - BMP-Blood"},
                       {"key" : "Studies", "value": "CTO-TOWARDS (91596)", "keyTemplate": this.keyTemplate},
                       {"key" : "Primary Subject", "value": "PNS123"}
                      ];
  }
}
