import {Component, HostBinding} from "@angular/core";
import {SafeHtml} from "@angular/platform-browser";

import * as prism from "prismjs";

@Component({
  selector: "hci-accord-nav-demo",
  template: `
    <div class="d-flex flex-column flex-shrink-0 m-3">
      <h3 class="mb-3">Accordion Navigation</h3>
      <div class="d-flex">
        @ng-bootstrap's NgbAccordion can be used; however, if big we want to use the hci-accordion-nav as a way to
        easily navigate the accordion.
      </div>
      <div class="d-flex">
        <button type="button" class="btn btn-outline-primary" [ngbPopover]="configAccord" popoverTitle="Config" placement="right">Show Config</button>
        <ng-template #configAccord>
          <div [innerHTML]="exampleHtml"></div>
        </ng-template>
      </div>
    </div>
    <div class="d-flex flex-column y-hidden">
      <ngb-accordion id="test-accordion" #accordion="ngbAccordion" class="y-auto">
        <ngb-panel id="test-1" title="Test 1">
          <ng-template ngbPanelContent>
            <div class="d-flex flex-wrap">
              <div class="col-md-4">
                #1
              </div>
              <ng-container *ngFor="let n of counter">
                <input type="text" class="col-md-4 mb-2" />
              </ng-container>
            </div>
          </ng-template>
        </ngb-panel>
        
        <ngb-panel id="test-2" title="Test 2">
          <ng-template ngbPanelContent>
            <div class="d-flex flex-wrap">
              <div class="col-md-4">
                #2
              </div>
              <ng-container *ngFor="let n of counter">
                <input type="text" class="col-md-4 mb-2" />
              </ng-container>
            </div>
          </ng-template>
        </ngb-panel>
        
        <ngb-panel id="test-3" title="Test 3">
          <ng-template ngbPanelContent>
            <div class="d-flex flex-wrap">
              <div class="col-md-4">
                #3
              </div>
              <ng-container *ngFor="let n of counter">
                <input type="text" class="col-md-4 mb-2" />
              </ng-container>
            </div>
          </ng-template>
        </ngb-panel>

        <ngb-panel id="test-4" title="Test 4">
          <ng-template ngbPanelContent>
            <div class="d-flex flex-wrap">
              <div class="col-md-4">
                #4
              </div>
              <ng-container *ngFor="let n of counter">
                <input type="text" class="col-md-4 mb-2" />
              </ng-container>
            </div>
          </ng-template>
        </ngb-panel>

        <ngb-panel id="test-5" title="Test 5">
          <ng-template ngbPanelContent>
            <div class="d-flex flex-wrap">
              <div class="col-md-4">
                #5
              </div>
              <ng-container *ngFor="let n of counter">
                <input type="text" class="col-md-4 mb-2" />
              </ng-container>
            </div>
          </ng-template>
        </ngb-panel>
      </ngb-accordion>
      <hci-accordion-nav [accordion]="accordion"
                         accordionId="test-accordion"
                         class="mt-auto mb-0">
      </hci-accordion-nav>
    </div>
  `
})
export class AccordNavDemoComponent {

  @HostBinding("class") classlist: string = "outlet-column";

  exampleHtml: SafeHtml;
  example: string = `
    <ngb-accordion id="test-accordion" #accordion="ngbAccordion" class="y-auto">
      <ngb-panel id="test-1" title="Test 1">
        <ng-template ngbPanelContent>
          ...
        </ng-template>
      </ngb-panel>
    </ngb-accordion>
    <hci-accordion-nav [accordion]="accordion"
                       accordionId="test-accordion"
                       class="mt-auto mb-0">
    </hci-accordion-nav>
  `;

  counter: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

  ngOnInit() {
    this.exampleHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">";
    this.exampleHtml += prism.highlight(this.example, prism.languages["html"]);
    this.exampleHtml += "</code></pre>";
  }

}
