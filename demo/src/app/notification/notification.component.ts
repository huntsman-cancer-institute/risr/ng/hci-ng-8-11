/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding} from "@angular/core";

import {NavigationGlobalService} from "@huntsman-cancer-institute/navigation";
import {NotificationService, Notification, NotificationDropdown} from "@huntsman-cancer-institute/notification";

/**
 * @since 1.2.0
 */
@Component({
  selector: "hci-notification-demo",
  template: `
    <div class="d-flex flex-column flex-shrink-0 m-3">
      <h3 class="mb-3">Notifications</h3>
      <div class="mb-3">
        The notification system can be used to capture exceptions from the backend to display to the user.  This gives
        information about the type of error/warning and provides a visual of the error so it isn't hidden in the console.
      </div>
      <div class="mb-3">
        The user can acknowledge a notification but keep them in a log for future reference.  In a next release we can
        allow an extra info popup that can show details such as the stacktrace which the support team can use to identify
        the issue.
      </div>
      <div>
        <button class="btn btn-primary mr-3" (click)="push('INFO', 'Patient 123 was added.')">Push Info</button>
        <button class="btn btn-primary mr-3" (click)="push('WARN', 'You are not allowed to view this resource.')">Push Warning</button>
        <button class="btn btn-primary mr-3" (click)="push('ERROR', 'There was an exception.')">Push Error</button>
      </div>
    </div>
  `
})
export class NotificationDemoComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

  constructor(private navigationGlobalService: NavigationGlobalService, private notificationService: NotificationService) {}

  ngAfterViewInit() {
    this.navigationGlobalService.getNavigationComponent("header").getComponent("header-right-container").add(
      {type: NotificationDropdown, id: "notification-dd", iClass: "fas fa-bell fa-lg", aClass: "nav-link", ulClass: "dropdown-menu dropdown-menu-right"}
    );
  }

  ngOnDestroy() {
    this.navigationGlobalService.getNavigationComponent("header").getComponent("header-right-container").clear();
  }

  push(type: string, message: string) {
    this.notificationService.addNotification(<Notification>{
      notification: message,
      type: type,
      status: "NEW"
    });
  }
}
