import {Injectable, isDevMode} from "@angular/core";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";

import {Observable, of, throwError} from "rxjs";
import {catchError, delay, mergeMap} from "rxjs/operators";

import {AttributeConfiguration, AttributeValueSet} from "@huntsman-cancer-institute/cod";

import {DemoInterceptor} from "../demo.interceptor";

let DICTIONARIES: any = require("./data/dictionaries.json");
let SECURITY_CONTEXTS: any = require("./data/security-contexts.json");
let CONTEXTS: any = require("./data/contexts.json");
let FILTER1: any = require("./data/filter1.json");
let FILTER2: any = require("./data/filter2.json");

let CFG_70_23: any = require("./data/cfg-70-23.json");
let CFG_70_23_C: any = require("./data/cfg-70-23-c.json");
let CFG_70_23_A: any = require("./data/cfg-70-23-a.json");
let CFG_70_23_AC: any = require("./data/cfg-70-23-ac.json");
let CFG_70_23_D: any = require("./data/cfg-70-23-d.json");
let AVS_156231: any = require("./data/avs-156231.json");

let CFG_88_34: any = require("./data/cfg-88-34.json");
let CFG_88_34_C: any = require("./data/cfg-88-34-c.json");
let CFG_88_34_A: any = require("./data/cfg-88-34-a.json");
let CFG_88_34_AC: any = require("./data/cfg-88-34-ac.json");
let CFG_88_34_D: any = require("./data/cfg-88-34-d.json");
let AVS_536753: any = require("./data/avs-536753.json");

let FACILITY: any[] = [
  {id: "6", display: "UUMC"},
  {id: "7", display: "PVH"},
  {id: "8", display: "LDSH"},
  {id: "9", display: "HCH"}
];

/**
 * Interceptor for mocking COD data.  We load data in to json so we can update it during POSTs.
 *
 * TODO: Add posts and ID generator for mocking identity columns.
 */
@Injectable()
export class CodInterceptor extends DemoInterceptor implements HttpInterceptor {

  // Keep track of highest id so we can mock identity generation.
  id: number = 0;

  avs156231: any = {};
  cfg7023: AttributeConfiguration;

  avs536753: any = {};
  cfg8834: AttributeConfiguration;

  constructor() {
    super();

    let data: any = this.loadData(AVS_156231, CFG_70_23, CFG_70_23_C, CFG_70_23_A, CFG_70_23_AC, CFG_70_23_D);
    this.avs156231 = data.avs;
    this.cfg7023 = data.cfg;

    data = this.loadData(AVS_536753, CFG_88_34, CFG_88_34_C, CFG_88_34_A, CFG_88_34_AC, CFG_88_34_D);
    this.avs536753 = data.avs;
    this.cfg8834 = data.cfg;

    if (isDevMode()) {
      console.debug("CodInterceptor");
      console.debug(this.cfg7023);
      console.debug(this.cfg8834);
    }
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (isDevMode()) {
      console.debug("CodInterceptor.intercept: " + request.url);
    }

    return of(null)
      .pipe(mergeMap(() => {
        if (request.url.match("/api/attr/attribute-value-count")) {
          if (isDevMode()) {
            console.info("GET /api/attr/attribute-value-count");
          }
          return of(new HttpResponse<any>({status: 200, body: 1234}));
        } else if (request.url.match("/api/attr/attribute-dictionary-count")) {
          if (isDevMode()) {
            console.info("GET /api/attr/attribute-dictionary-count");
          }
          return of(new HttpResponse<any>({status: 200, body: 1234}));
        } else if (request.url.match("/api/attr/base-window-dimension")) {
          if (isDevMode()) {
            console.info("GET /api/attr/base-window-dimension");
          }
          return of(new HttpResponse<any>({status: 200, body: {width: 800, height: 500}}));
        } else if (request.url.match("/api/dictionaries/hci.ri.attr.model.AttributeDictionary/entries")) {
          if (isDevMode()) {
            console.info("GET /api/dictionaries/hci.ri.attr.model.AttributeDictionary/entries");
          }
          return of(new HttpResponse<any>({status: 200, body: DICTIONARIES}));
        } else if (request.url.match("/api/dictionaries/hci.ccr.model.Facility/dropdown-entries")) {
          if (isDevMode()) {
            console.info("GET /api/dictionaries/hci.ccr.model.Facility/dropdown-entries");
          }
          return of(new HttpResponse<any>({status: 200, body: FACILITY}));
        } else if (request.method === "GET" && request.url.match("/api/dictionaries/hci.ri.attr.model.AttributeSecurityContext/entries")) {
          if (isDevMode()) {
            console.info("GET /api/dictionaries/hci.ri.attr.model.AttributeSecurityContext/entries");
          }

          return of(new HttpResponse<any>({status: 200, body: SECURITY_CONTEXTS}));
        } else if (request.method === "GET" && request.url.match("/api/dictionaries/hci.ri.attr.model.AttributeContext/entries")) {
          if (isDevMode()) {
            console.info("GET /api/dictionaries/hci.ri.attr.model.AttributeContext/entries");
          }

          return of(new HttpResponse<any>({status: 200, body: CONTEXTS}));
        } else if (request.method === "GET" && request.url.match("/api/dictionaries/hci.ri.attr.model.Filter1/entries")) {
          if (isDevMode()) {
            console.info("GET /api/dictionaries/hci.ri.attr.model.Filter1/entries");
          }

          return of(new HttpResponse<any>({status: 200, body: FILTER1}));
        } else if (request.method === "GET" && request.url.match("/api/dictionaries/hci.ri.attr.model.Filter2/entries")) {
          if (isDevMode()) {
            console.info("GET /api/dictionaries/hci.ri.attr.model.Filter2/entries");
          }

          return of(new HttpResponse<any>({status: 200, body: FILTER2}));
        } else if (request.url.match("/api/attr/configurations/70")) {
          if (isDevMode()) {
            console.info("GET /api/attr/configurations/70");
          }

          return of(new HttpResponse<any>({status: 200, body: this.cfg7023}));
        } else if (request.url.match("/api/attr/configurations/88")) {
          if (isDevMode()) {
            console.info("GET /api/attr/configurations/88");
          }

          return of(new HttpResponse<any>({status: 200, body: this.cfg8834}));
        } else if (request.url.match("/api/attr/configurations") && !request.url.match("/filter/")) {
          if (isDevMode()) {
            console.info("GET /api/attr/configurations");
          }

          if (request.params.has("codeAttributeSecurityContext")) {
            let codeAttributeSecurityContext: string = request.params.get("codeAttributeSecurityContext");
            if (isDevMode()) {
              console.info(codeAttributeSecurityContext);
            }

            if (codeAttributeSecurityContext === "BMT") {
              return of(new HttpResponse<any>({status: 200, body: [this.cfg8834]}));
            } else if (codeAttributeSecurityContext === "MELANOMA") {
              return of(new HttpResponse<any>({status: 200, body: [this.cfg7023]}));
            }
          }

          return of(new HttpResponse<any>({status: 200, body: [this.cfg7023, this.cfg8834]}));
        } else if (request.url.match("/api/attr/configurations/filter/23")) {
          if (isDevMode()) {
            console.info("GET /api/attr/configurations/filter/23");
          }
          return of(new HttpResponse<any>({status: 200, body: this.cfg7023}));
        } else if (request.method === "GET" && request.url.match("/api/attr/attribute-value-set/156231")) {
          if (isDevMode()) {
            console.info("GET /api/attr/attribute-value-set/156231");
          }
          return of(new HttpResponse<any>({status: 200, body: this.avs156231}));
        } else if (request.method === "PUT" && request.url.match("/api/attr/attribute-value-set/156231")) {
          if (isDevMode()) {
            console.info("PUT /api/attr/attribute-value-set/156231");
            console.info(request.body);
          }

          let avs: AttributeValueSet = <AttributeValueSet>request.body;

          for (let av of avs.attributeValues) {
            if (!av.idAttributeValue) {
              av.idAttributeValue = ++this.id;
            }
            if (av.valueLongText && !av.valueLongText.idLongText) {
              av.valueLongText.idLongText = ++this.id;
            }
          }
          this.avs156231 = avs;

          return of(new HttpResponse<any>({status: 200, body: this.avs156231}));
        } else if (request.url.match("/api/attr/configurations/filter/34")) {
          if (isDevMode()) {
            console.info("GET /api/attr/configurations/filter/34");
          }
          return of(new HttpResponse<any>({status: 200, body: this.cfg8834}));
        } else if (request.method === "GET" && request.url.match("/api/attr/attribute-configuration/id/88")) {
          if (isDevMode()) {
            console.info("GET /api/attr/attribute-configuration/id/88");
          }

          return of(new HttpResponse<any>({status: 200, body: this.cfg8834}));
        } else if (request.method === "PUT" && request.url.match("/api/attr/attribute-configuration/id/88")) {
          if (isDevMode()) {
            console.info("PUT /api/attr/attribute-configuration/id/88");
          }

          this.cfg8834 = this.createIds(<AttributeConfiguration>request.body);

          return of(new HttpResponse<any>({status: 200, body: this.cfg8834}));
        } else if (request.method === "GET" && request.url.match("/api/attr/attribute-value-set/536753")) {
          if (isDevMode()) {
            console.info("GET /api/attr/attribute-value-set/536753");
          }
          return of(new HttpResponse<any>({status: 200, body: this.avs536753}));
        } else if (request.method === "PUT" && request.url.match("/api/attr/attribute-value-set/536753")) {
          if (isDevMode()) {
            console.info("PUT /api/attr/attribute-value-set/536753");
            console.info(request.body);
          }

          let avs: AttributeValueSet = <AttributeValueSet>request.body;

          for (let av of avs.attributeValues) {
            if (!av.idAttributeValue) {
              av.idAttributeValue = ++this.id;
            }
            if (av.valueLongText && !av.valueLongText.idLongText) {
              av.valueLongText.idLongText = ++this.id;
            }
          }
          this.avs536753 = avs;

          return of(new HttpResponse<any>({status: 200, body: this.avs536753}));
        } else {
          this.delayTime = 0;
        }

        return next.handle(request)
          .pipe(catchError(response => {
            if (response instanceof HttpErrorResponse) {
              console.warn("Http error", response);
            }

            return throwError(response);
          }));
      }))
      .pipe(delay(this.delayTime));
  }

  /**
   * In order to prevent manually converting sql results in to json.  We store the sql results in individual files for
   * each table store it in to maps, and then populate the configuration entity in the same way hibernate would.
   */
  loadData(avs: any, cfg: any, containers: any[], atrributes: any[], attributeChoices: any[], dictionaries: any[]): any {
    let acMap: Map<number, any> = new Map<number, any>();
    for (let ac of attributeChoices) {
      acMap.set(+ac.idAttributeChoice, ac);

      if (this.id < +ac.idAttributeChoice) {
        this.id = +ac.idAttributeChoice;
      }
    }

    let dMap: Map<number, any> = new Map<number, any>();
    for (let d of dictionaries) {
      dMap.set(+d.idAttributeDictionary, d);

      if (this.id < +d.idAttributeDictionary) {
        this.id = +d.idAttributeDictionary;
      }
    }

    let aMap: Map<number, any> = new Map<number, any>();
    for (let a of atrributes) {
      aMap.set(+a.idAttribute, a);

      if (this.id < +a.idAttribute) {
        this.id = +a.idAttribute;
      }
    }

    let cMap: Map<number, any> = new Map<number, any>();
    for (let c of containers) {
      cMap.set(+c.idAttributeContainer, c);

      if (this.id < +c.idAttributeContainer) {
        this.id = +c.idAttributeContainer;
      }
    }

    aMap.forEach((attribute: any) => {
      attribute.attributes = [];
      attribute.attributeChoices = [];

      acMap.forEach((attributeChoice: any) => {
        if (+attributeChoice.idAttribute === +attribute.idAttribute) {
          attribute.attributeChoices.push(attributeChoice);
        }
      });

      dMap.forEach((attributeDictionary: any) => {
        if (+attributeDictionary.idAttributeDictionary === +attribute.idAttributeDictionary) {
          attribute.attributeDictionary = attributeDictionary;
        }
      });
    });

    aMap.forEach((groupAttribute: any) => {
      groupAttribute.attributes = [];

      aMap.forEach((attribute: any) => {
        if (+groupAttribute.idAttribute === +attribute.idGroupAttribute) {
          //attribute.groupAttribute = groupAttribute;
          groupAttribute.attributes.push(attribute);
        }
      });
    });

    cfg.attributeContainers = [];
    cMap.forEach((attributeContainer: any) => {
      attributeContainer.graphicalAttributes = [];

      aMap.forEach((attribute: any) => {
        if (+attributeContainer.idAttributeContainer === +attribute.idAttributeContainer) {
          attributeContainer.graphicalAttributes.push(attribute);
        }
      });

      if (+cfg.idAttributeConfiguration === +attributeContainer.idAttributeConfiguration) {
        cfg.attributeContainers.push(attributeContainer);
      }

      if (this.id < +cfg.idAttributeConfiguration) {
        this.id = +cfg.idAttributeConfiguration;
      }
    });

    for (let attributeValue of avs.attributeValues) {
      if (attributeValue.valueIdAttributeChoice) {
        attributeValue.valueAttributeChoice = acMap.get(+attributeValue.valueIdAttributeChoice);
      }

      if (this.id < +attributeValue.idAttributeValue) {
        this.id = +attributeValue.idAttributeValue;
      }
    }

    return {
      avs: avs,
      cfg: cfg
    };
  }

  createIds(attributeConfiguration: AttributeConfiguration): AttributeConfiguration {
    if (attributeConfiguration.idAttributeConfiguration === undefined) {
      attributeConfiguration.idAttributeConfiguration = ++this.id;
    }

    for (let attributeContainer of attributeConfiguration.attributeContainers) {
      if (attributeContainer.idAttributeContainer === undefined) {
        attributeContainer.idAttributeContainer = ++this.id;
      }

      for (let attribute of attributeContainer.graphicalAttributes) {
        if (attribute.idAttribute === undefined) {
          attribute.idAttribute = ++this.id;
        }

        if (attribute.attributeChoices) {
          for (let attributeChoice of attribute.attributeChoices) {
            if (attributeChoice.idAttributeChoice === undefined) {
              attributeChoice.idAttributeChoice = ++this.id;
            }
          }
        }
      }
    }

    return attributeConfiguration;
  }
}
