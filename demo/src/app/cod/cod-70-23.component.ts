/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding} from "@angular/core";
import {SafeHtml} from "@angular/platform-browser";

import * as prism from "prismjs";

/**
 * @since 2.1.0
 */
@Component({
  selector: "hci-cfg-70-23",
  template: `
    <div class="d-flex flex-column flex-shrink-0 m-3">
      <h3 class="mb-3">Configure on Demand</h3>
      <div class="d-flex mb-3">
        Here we use a sample configuration with three containers each with a variety of attributes for testing.
        This is using CCR, Melanoma, ACIS-001, Medical Event Lymphatic Surgery on 2012-03-06.
      </div>
      <div class="d-flex mb-3">
        <button type="button" class="btn btn-outline-primary" [ngbPopover]="configCcr" popoverTitle="Config" placement="right">Show Config</button>
        <ng-template #configCcr>
          <div [innerHTML]="ccrExampleHtml"></div>
        </ng-template>
      </div>
      <div class="d-flex flex-grow-1 flex-column mb-3">
        <h4 class="mb-3">Absolute Positioning</h4>
        <hci-attribute-configuration [idAttributeValueSet]="156231">
          <ngb-panel>
            <ng-template ngbPanelTitle>
              Hardcoded Template
            </ng-template>
            <ng-template ngbPanelContent>
              Template of structured data goes here.
            </ng-template>
          </ngb-panel>
        </hci-attribute-configuration>
      </div>
    </div>
  `
})
export class Cod7023DemoComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

  ccrExampleHtml: SafeHtml;
  ccrExample: string = `
    <hci-attribute-configuration [idFilter1]="23"
                                 [idAttributeValueSet]="156231">
    </hci-attribute-configuration>
  `;

  ngOnInit() {
    this.ccrExampleHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">";
    this.ccrExampleHtml += prism.highlight(this.ccrExample, prism.languages["html"]);
    this.ccrExampleHtml += "</code></pre>";
  }

}
