/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, ViewChild} from "@angular/core";

import {AppHeaderComponent, LiNavComponent, UlNavComponent} from "@huntsman-cancer-institute/navigation";

/**
 * @since 2.1.0
 */
@Component({
  selector: "cod-demo",
  template: `
    <hci-app-header #subHeader></hci-app-header>
    <router-outlet></router-outlet>
  `
})
export class CodDemoComponent {

  @HostBinding("class") classlist: string = "outlet-column";

  @ViewChild("subHeader", {static: true}) subHeader: AppHeaderComponent;

  ngOnInit() {
    this.subHeader.setConfig({
      navbarClasses: "sub-header",
      children: [
        {
          type: UlNavComponent, ulClass: "nav-container",
          children: [
            {type: LiNavComponent, liClass: "nav-item", title: "Home", route: "home"},
            {type: LiNavComponent, liClass: "nav-item", title: "COD 70-23", route: "cod-70-23"},
            {type: LiNavComponent, liClass: "nav-item", title: "COD 88-34", route: "cod-88-34"}
          ]
        }
      ]
    });
  }

}
