import {Component, HostBinding} from "@angular/core";

@Component({
  selector: "cod-home",
  template: `
    <div class="d-flex flex-column flex-shrink-1 flex-grow-1 m-0">
      <div class="d-flex flex-column flex-shrink-0 m-3">
        <h3 class="mb-3">Configure on Demand</h3>
        <div class="d-flex">
          This is a demo of configure on demand with two demos from CCR.  Demos will show the current absolute positioning
          as well as a future flex based layout.
        </div>
      </div>
    </div>
  `
})
export class CodHomeComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

}
