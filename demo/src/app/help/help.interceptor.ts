import {Injectable, isDevMode} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";

import {Observable, of, throwError} from "rxjs";
import {delay, mergeMap} from "rxjs/operators";

import {DemoInterceptor} from "../demo.interceptor";

/**
 * @since 9.0.0
 */
@Injectable()
export class HelpInterceptor extends DemoInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (isDevMode()) {
      console.debug("HelpInterceptor.intercept: " + request.url);
    }

    return of(null)
      .pipe(mergeMap(() => {
        if (request.url.match(".*/api/tooltip") && request.method === "GET") {
          // Pulled from GNOMEX ContextSensitiveHelp as an example
          let tooltipHtml = "<TEXTFORMAT LEADING=\"2\"><P ALIGN=\"JUSTIFY\">" +
            "<FONT FACE=\"Open Sans\" LETTERSPACING=\"0\" KERNING=\"0\">The samples grid now " +
            "supports <B>copy and paste</B>.</FONT></P></TEXTFORMAT><TEXTFORMAT LEADING=\"2\"><P ALIGN=\"JUSTIFY\">" +
            "<FONT FACE=\"Open Sans\" LETTERSPACING=\"0\" KERNING=\"0\"></FONT></P></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><P ALIGN=\"JUSTIFY\">" +
            "<FONT FACE=\"Open Sans\" LETTERSPACING=\"0\" KERNING=\"1\">" +
            "There are also several functions in the  <FONT COLOR=\"#0074D9\"><I>right-click context</I> <I>menu</I>" +
            "<I>.</I></FONT></FONT></P></TEXTFORMAT><TEXTFORMAT LEADING=\"2\">" +
            "<P ALIGN=\"JUSTIFY\"><FONT FACE=\"Open Sans\" LETTERSPACING=\"0\" KERNING=\"1\">" +
            "</FONT></P></TEXTFORMAT><TEXTFORMAT LEADING=\"2\"><P ALIGN=\"JUSTIFY\">" +
            "<FONT FACE=\"Open Sans\" LETTERSPACING=\"0\" KERNING=\"1\"><I>Click for more information.</I>" +
            "</FONT></P></TEXTFORMAT>";

          let tooltip: string;

          if (request.url.match(".*/api/tooltip/GNOMEX/42")) {
            tooltip = "You got old 42!";
          } else if (request.url.match(".*/api/tooltip/GNOMEX/21")) {
            tooltip = tooltipHtml;
          } else if (request.url.match(".*/api/tooltip/GNOMEX/13")) {
            tooltip = "Here is your tooltip!";
          }

          let respOpts: HttpResponse<any> = new HttpResponse<any>({
            body: {
              "tooltip": tooltip
            },
            status: 200
          });

          if (tooltip) {
            return of(respOpts);
          } else {
            return throwError(request.url + " Error");
          }
        } else if (request.url.match(".*/api/help") && request.method === "GET") {
          let helpText: string = "Pellentesque dynamic, sit amet venenatis urna cursus eget nunc scelerisque viverra mauris, " +
            "in aliquam. Tincidunt lobortis feugiat vivamus at left eget arcu dictum varius duis at consectetur lorem. " +
            "Vitae elementum curabitur right nunc sed velit dignissim sodales ut eu sem integer vitae. Turpis egestas " +
            "bottom pharetra convallis posuere morbi leo urna, fading at elementum eu, facilisis sed odio morbi quis " +
            "commodo odio. In cursus delayed turpis massa tincidunt dui ut. nunc sed velit dignissim sodales ut eu sem " +
            "integer vitae. Turpis egestas.";

          // Pulled from GNOMEX ContextSensitiveHelp as an example
          let helpHtml: string = "<TEXTFORMAT LEADING=\"2\"><P ALIGN=\"LEFT\">" +
            "<FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "The Samples Grid has been given a functionality makeover.  It now supports <FONT COLOR=\"#FF0000\">" +
            "<B>copy and paste</B></FONT>!  </FONT></P></TEXTFORMAT><TEXTFORMAT LEADING=\"2\"><P ALIGN=\"LEFT\">" +
            "<FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\"></FONT></P></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><P ALIGN=\"LEFT\">" +
            "<FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "Further, there are many functions that can be accessed from the<I> </I><FONT COLOR=\"#000099\">" +
            "<I>right-click context menu</I></FONT>:</FONT></P></TEXTFORMAT><TEXTFORMAT LEADING=\"2\"><LI>" +
            "<FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "Copy row(s) to clipboard - copies the selected row(s) to the clipboard which can then be pasted into the sample" +
            " grid itself or into Excel</FONT></LI></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><LI><FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "Copy grid to clipboard - this will copy the entire grid (including headers) to the clipboard which can then " +
            "easily be pasted into Excel or into a text file</FONT></LI></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><LI><FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "Duplicate row(s) - duplicates the selected row(s) and adds them to the bottom of the list</FONT></LI></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><LI><FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "Add row - adds an empty row at the end of the list</FONT></LI></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><LI><FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "Delete row(s) - removes the selected row(s) from the list</FONT></LI></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><LI><FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "Clear all - clears the list completely </FONT></LI></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><LI><FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "Undo/Redo - undoes the last action <I>performed from the list above </I>or redoes the last action if you have " +
            "already clicked &apos;Undo&apos; and haven&apos;t done any other actions yet</FONT></LI></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><P ALIGN=\"LEFT\"><FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "(To select  rows, click on the row number number column; hold shift or ctrl and click on several row numbers to select multiple rows.)" +
            "</FONT></P></TEXTFORMAT><TEXTFORMAT LEADING=\"2\"><P ALIGN=\"LEFT\">" +
            "<FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\"></FONT></P></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><P ALIGN=\"LEFT\">" +
            "<FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">To <FONT COLOR=\"#FF0000\">" +
            "paste</FONT> into the grid, you just need to click on the grid so that it is selected (this will put a blue " +
            "line around the grid) and then click <FONT COLOR=\"#FF0000\"><B>&apos;ctrl+v&apos;</B></FONT> (the normal key " +
            "sequence for paste).  Data can be copied straight from Excel and then pasted into the grid.  Or, if your data " +
            "is in a tab-delimited txt file, you can copy that and paste into the grid as well.  <FONT COLOR=\"#666666\">" +
            "<I>It is expected that the columns are the same and in the same order as the Samples Grid for it to work correctly.</I>" +
            "</FONT></FONT></P></TEXTFORMAT><TEXTFORMAT LEADING=\"2\">" +
            "<P ALIGN=\"LEFT\"><FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\"></FONT></P></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><P ALIGN=\"LEFT\"><FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "Special fields (like drop downs and multiple select annotations) should paste as you expect.  For example, if " +
            "you want &apos;Option 1&apos; selected from the drop down, just enter the text &apos;Option 1&apos; in your " +
            "spreadsheet before copying.  As long as the text matches (case-insensitive), the correct selection will be " +
            "chosen.  For multiple select annotations, you can just separate selections with a comma.  For example if you " +
            "have &apos;Option 1, Option 3, Option 4&apos; written in a cell, then &apos;Option 1&apos;, &apos;Option " +
            "3&apos;, and &apos;Option 4&apos; will be selected in the corresponding multiple selection box. Just make sure " +
            "that your spelling matches!</FONT></P></TEXTFORMAT><TEXTFORMAT LEADING=\"2\"><P ALIGN=\"LEFT\">" +
            "<FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\"></FONT></P></TEXTFORMAT>" +
            "<TEXTFORMAT LEADING=\"2\"><P ALIGN=\"LEFT\"><FONT FACE=\"Open Sans\" SIZE=\"2\" COLOR=\"#2E2D2C\" LETTERSPACING=\"0\" KERNING=\"0\">" +
            "If you have any questions or comments about the new features for the Sample Grid, don&apos;t hesitate to contact " +
            "megan.bowler@huntsman-cancer-institute.utah.edu. </FONT></P></TEXTFORMAT>";

          let title: string;
          let body: string;

          if (request.url.match(".*/api/help/GNOMEX/84")) {
            if (isDevMode()) {
              console.info("GET " + request.url);
            }
            title = "Help Documentation for old 84";
            body = helpHtml;
          } else if (request.url.match(".*/api/help/GNOMEX/42")) {
            title = "<em>Help Documentation for old 42</em>";
            body = helpText;
          } else if (request.url.match(".*/api/help/GNOMEX/420")) {
            title = "";
            body = helpText;
          } else if (request.url.match(".*/api/help/GNOMEX/13")) {
            title = "Help Documentation";
            body = helpText;
          }

          let respOpts: HttpResponse<any> = new HttpResponse<any>({
            body: {
              "title": title,
              "body": body
            },
            status: 200
          });

          if (body) {
            return of(respOpts);
          } else {
            return throwError("We are currently experiencing issues. Please try back later or call the help desk.");
          }
        } else {
          this.delayTime = 0;
        }

        return next.handle(request);
      }))
      .pipe(delay(this.delayTime));
  }

}
