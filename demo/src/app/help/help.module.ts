/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";

import {HelpModule, TOOLTIP_BASE_ENDPOINT, HELP_BASE_ENDPOINT, HELP_CACHE_SIZE} from "@huntsman-cancer-institute/help";

import {HelpDemoComponent} from "./help.component";

/**
 * @since 9.0.0
 */
@NgModule({
  imports: [
    RouterModule.forChild([
      {path: "", component: HelpDemoComponent}
    ]),
    CommonModule,
    FormsModule,
    RouterModule,
    HelpModule
  ],
  declarations: [
    HelpDemoComponent
  ],
  providers: [
    {provide: HELP_CACHE_SIZE, useValue: 40},
    {provide: TOOLTIP_BASE_ENDPOINT, useValue: "/api/tooltip"},
    {provide: HELP_BASE_ENDPOINT, useValue: "/api/help"}
  ]
})
export class HelpDemoModule {}
