import {Injectable, isDevMode} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";

import {CoolLocalStorage} from "@angular-cool/storage";
import {Observable, of} from "rxjs";
import {delay, mergeMap} from "rxjs/operators";

import {Widget, WidgetAttributeValueSet, WidgetInstance, Dashboard} from "@huntsman-cancer-institute/dashboard";

import {DemoInterceptor} from "../demo.interceptor";

declare var CONTENT_PATH: string;

let WIDGETS: any = require("./data/widgets.json");
let DASHBOARDS_EMPTY: any = require("./data/dashboards-empty.json");
let DASHBOARDS_SIMPLE: any = require("./data/dashboards-simple.json");
let DASHBOARDS_COMPLEX_WIDGETS: any = require("./data/dashboards-complex-widgets.json");
let DASHBOARDS_MANY_WIDGETS: any = require("./data/dashboards-many-widgets.json");
let DASHBOARDS_MULTI_DASHBOARD: any = require("./data/dashboards-multi-dashboard.json");

@Injectable()
export class DashboardInterceptor extends DemoInterceptor implements HttpInterceptor {

  constructor(private localStorage: CoolLocalStorage) {
    super();
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (isDevMode()) {
      console.debug("DashboardInterceptor.intercept: " + request.url);
    }

    let id: number = +this.localStorage.getItem("id");
    if (!id) {
      id = 1000000;
    }

    return of(null)
      .pipe(mergeMap(() => {
        if (request.url.match(".*/api/user-profile/dashboard") && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET .*/api/user-profile/dashboard");
          }

          let dashboardType: string = request.params.get("dashboardType");
          if (dashboardType === "EMPTY") {
            return of(new HttpResponse<any>({ status: 200, body: this.setWidgets(DASHBOARDS_EMPTY, WIDGETS) }));
          } else if (dashboardType === "SIMPLE") {
            return of(new HttpResponse<any>({ status: 200, body: this.setWidgets(DASHBOARDS_SIMPLE, WIDGETS) }));
          } else if (dashboardType === "COMPLEX_WIDGETS") {
            return of(new HttpResponse<any>({ status: 200, body: this.setWidgets(DASHBOARDS_COMPLEX_WIDGETS, WIDGETS) }));
          } else if (dashboardType === "MANY_WIDGETS") {
            return of(new HttpResponse<any>({ status: 200, body: this.setWidgets(DASHBOARDS_MANY_WIDGETS, WIDGETS) }));
          } else if (dashboardType === "MULTI_DASHBOARD") {
            return of(new HttpResponse<any>({ status: 200, body: this.setWidgets(DASHBOARDS_MULTI_DASHBOARD, WIDGETS) }));
          }
        } else if (request.url.match(".*/api/user-profile/user-profile/my") && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET .*/api/user-profile/user-profile/my");
          }
          return of(new HttpResponse<any>({ status: 200, body: {} }));
        } else if (request.url.match(".*/api/user-profile/attribute/common-attributes")) {
          if (isDevMode()) {
            console.debug("GET /api/user-profile/attribute/common-attributes");
          }
          request = request.clone({
            url: CONTENT_PATH + "data/common-attributes.json"
          });
        } else if (request.url.match(new RegExp(".*\\/api\\/user-profile\\/widget\\/component\\/.*")) && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET .*/api/user-profile/widget/component/.*");
            console.debug("JSON " + JSON.stringify(request));
          }

          let component: string = request.url.replace(new RegExp(".*\\/api\\/user-profile\\/widget\\/component\\/"), "");

          for (let widget of WIDGETS) {
            if (widget.component === component) {
              return of(new HttpResponse<any>({ status: 200, body: widget }));
            }
          }
        } else if (request.url.match(".*/api/user-profile/widget/widget-list")
          && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET /api/user-profile/widget/widget-list");
            console.debug("JSON " + request.body);

            console.debug("RESPOND " + JSON.stringify(WIDGETS));
          }
          return of(new HttpResponse<any>({ status: 200, body: WIDGETS }));
        } else if (request.url.match(".*/api/user-profile/widget/widget-categories") && request.method === "GET") {
          request = request.clone({
            url: CONTENT_PATH + "data/widget-categories.json"
          });
        } else if (request.url.match(".*/api/user-profile/widget/dashboard/")
          && request.method === "PUT") {
          if (isDevMode()) {
            console.debug("PUT /api/widget/dashboard/");
            console.debug("JSON " + request.body);
          }

          let dashboard: Dashboard = new Dashboard().deserialize(JSON.parse(request.body));
          dashboard.idDashboard = id++;
          this.localStorage.setItem("id", id.toString());

          return of(new HttpResponse<any>({ status: 200, body: dashboard }));
        } else if (request.url.match(".*/api/user-profile/dashboard/[0-9]+")
          && request.method === "DELETE") {
          if (isDevMode()) {
            console.debug("DELETE /api/user-profile/dashboard/[0-9]+");
            console.debug("JSON " + request.body);
          }

          return of(new HttpResponse<any>({ status: 200, body: {} }));
        } else if (request.url.match(".*/api/user-profile/widget/widget-instance/[0-9]+")
          && request.method === "DELETE") {
          if (isDevMode()) {
            console.debug("DELETE /api/user-profile/widget/widget-instance/[0-9]+");
            console.debug("JSON " + request.body);
          }

          return of(new HttpResponse<any>({ status: 200, body: {} }));
        } else if (request.url.match(".*/api/user-profile/widget/widget-instance/[0-9]+/sort-order") && request.method === "POST") {
          if (isDevMode()) {
            console.debug("POST /api/user-profile/widget/widget-instance/\d+/sort-order");
            console.debug("JSON " + request.body);
          }

          return of(new HttpResponse<any>({ status: 200, body: {} }));
        } else if (request.url.match(".*/api/user-profile/widget/widget-instance/") && request.method === "PUT") {
          if (isDevMode()) {
            console.debug("PUT /api/user-profile/widget/widget-instance/");
            console.debug("JSON " + request.body);
          }

          let widgetInstance: WidgetInstance = new WidgetInstance().deserialize(JSON.parse(request.body));
          if (!widgetInstance.idWidgetInstance || widgetInstance.idWidgetInstance < 0) {
            widgetInstance.idWidgetInstance = id++;
            this.localStorage.setItem("id", id.toString());
          }

          /*let idWidget: number = +request.params.get("idWidget");
          for (let widget of widgets) {
            if (widget.idWidget === idWidget) {
              widgetInstance.widget = widget;
              break;
            }
          }*/

          if (isDevMode()) {
            console.debug("RESPONSE:");
            console.debug(widgetInstance);
          }
          return of(new HttpResponse<any>({ status: 200, body: widgetInstance }));
        } else if (request.url.match(".*/api/user-profile/widget/widget-instance/") && request.method === "POST") {
          if (isDevMode()) {
            console.debug("POST /api/user-profile/widget/widget-instance/");
            console.debug("JSON " + request.body);
          }

          let widgetInstance: WidgetInstance = new WidgetInstance().deserialize(JSON.parse(request.body));

          return of(new HttpResponse<any>({ status: 200, body: widgetInstance }));
        } else if (request.url.match(".*/api/user-profile/widget/attribute-value-set")
          && request.method === "PUT") {
          if (isDevMode()) {
            console.debug("PUT " + request.url);
            console.debug("JSON " + JSON.stringify(request));
          }

          //let idWidgetInstance: number = +request.url.replace(new RegExp(".*\\/api\\/user-profile\\/widget\\/"), "").replace(new RegExp("\\/attribute-value-set"), "");

          let avs: WidgetAttributeValueSet = new WidgetAttributeValueSet().deserialize(JSON.parse(request.body));
          if (!avs.idWidgetAttributeValueSet || avs.idWidgetAttributeValueSet < 0) {
            avs.idWidgetAttributeValueSet = id++;
            this.localStorage.setItem("id", id.toString());
          }

          if (avs.attributeValues) {
            for (let attributeValue of avs.attributeValues) {
              if (!attributeValue.idWidgetAttributeValue || attributeValue.idWidgetAttributeValue < 0) {
                attributeValue.idWidgetAttributeValue = id++;
                this.localStorage.setItem("id", id.toString());
              }
            }
          }

          /*for (let dashboard of userProfile.dashboards) {
            for (let widgetInstance of dashboard.widgetInstances) {
              if (widgetInstance.idWidgetInstance === idWidgetInstance) {
                widgetInstance.attributeValueSet = avs;
              }
            }
          }*/

          if (isDevMode()) {
            console.debug("RESPONSE " + JSON.stringify(avs));
          }

          return of(new HttpResponse<any>({ status: 200, body: avs }));
        } else if (request.url.match(".*/api/.+") && request.url.endsWith("/accruals-per-study") && request.method === "GET") {
          request = request.clone({
            url: CONTENT_PATH + "data/accruals-per-study.json"
          });
        } else if (request.url.match(".*/api/") && request.url.endsWith("/patients-by-treatment-status") && request.method === "GET") {
          request = request.clone({
            url: CONTENT_PATH + "data/patients-by-treatment-status.json"
          });
        } else if (request.url.match(".*/api/") && request.url.endsWith("/patients-by-treatment-status-2") && request.method === "GET") {
          request = request.clone({
            url: CONTENT_PATH + "data/patients-by-treatment-status-2.json"
          });
        } else if (request.url.match(".*/api/") && request.url.endsWith("/accruals-per-month") && request.method === "GET") {
          request = request.clone({
            url: CONTENT_PATH + "data/accruals-per-month.json"
          });
        } else if (request.url.match(".*/api/") && request.url.endsWith("/accruals-per-year") && request.method === "GET") {
          request = request.clone({
            url: CONTENT_PATH + "data/accruals-per-year.json"
          });
        } else if (request.url.match(".*/assets/.+") && request.method === "GET") {
          request = request.clone({
            url: request.url.replace(new RegExp(".*\\/assets\\/"), CONTENT_PATH + "assets/")
          });
        } else if (request.url.match(".*/data/.+") && request.method === "GET") {
          request = request.clone({
            url: request.url.replace(new RegExp(".*\\/data\\/"), CONTENT_PATH + "data/")
          });
        } else if (request.url.match(".*/api/iq/app")
          && request.method === "GET") {
          console.debug("GET " + request.url);

          let studies = [
            { value: "ccr", display: "CCR"},
            { value: "bst", display: "itBioPath"},
            { value: "rsr", display: "RSR"}
          ];

          return of(new HttpResponse<any>({ status: 200, body: studies }));
        } else if (request.url.match(".*/api/iq/queryFolder/.+")
          && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET " + request.url);
          }

          let queryFolders = undefined;

          if (request.url.endsWith("/ccr")) {
            queryFolders = [
              { value: 1, display: "Query Folder 1"}
            ];
          } else if (request.url.endsWith("/bst")) {
            queryFolders = [
              { value: 2, display: "Query Folder 2"}
            ];
          } else {
            queryFolders = [];
          }

          return of(new HttpResponse<any>({ status: 200, body: queryFolders }));
        } else if (request.url.match(".*/api/iq/queryList/.+")
          && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET " + request.url);
          }

          let queryFolders = undefined;

          if (request.url.endsWith("/1")) {
            queryFolders = [
              { value: 1, display: "Query 1"},
              { value: 2, display: "Query 2"},
              { value: 3, display: "Query 3"}
            ];
          } else if (request.url.endsWith("/2")) {
            queryFolders = [
              { value: 4, display: "Query 4"},
              { value: 5, display: "Query 5"},
              { value: 6, display: "Query 6"}
            ];
          } else {
            queryFolders = [];
          }

          return of(new HttpResponse<any>({ status: 200, body: queryFolders }));
        } else if (request.url.match(".*/api/iq/query/.+")
          && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET " + request.url);
          }

          let query = {
            ReportColumns: [],
            ReportList: {
              Report: {
                name: "",
                ReportRow: []
              }
            }
          };

          if (request.url.endsWith("/1")) {
            query.ReportColumns.push({dataField: "col0", name: "col0", caption: "ID"});
            query.ReportColumns.push({dataField: "col1", name: "col1", caption: "Name"});
            query.ReportColumns.push({dataField: "col2", name: "col2", caption: "# Labs"});
            query.ReportColumns.push({dataField: "col3", name: "col3", caption: "# Vitals"});

            query.ReportList.Report.name = "Query 1";
            query.ReportList.Report.ReportRow.push({row: "0", col0: "100", col1: "Bob", col2: "54", col3: "12"});
            query.ReportList.Report.ReportRow.push({row: "1", col0: "101", col1: "Jane", col2: "23", col3: "65"});
            query.ReportList.Report.ReportRow.push({row: "2", col0: "102", col1: "Mike", col2: "43", col3: "25"});
            query.ReportList.Report.ReportRow.push({row: "3", col0: "103", col1: "Amy", col2: "62", col3: "42"});
            query.ReportList.Report.ReportRow.push({row: "4", col0: "104", col1: "Chris", col2: "12", col3: "32"});
          } else if (request.url.endsWith("/4")) {
            query.ReportColumns.push({dataField: "col0", name: "col0", caption: "ID"});
            query.ReportColumns.push({dataField: "col1", name: "col1", caption: "Name"});
            query.ReportColumns.push({dataField: "col2", name: "col2", caption: "# Labs"});
            query.ReportColumns.push({dataField: "col3", name: "col3", caption: "# Vitals"});
            query.ReportColumns.push({dataField: "col4", name: "col4", caption: "# Meds"});

            query.ReportList.Report.name = "Query 2";
            query.ReportList.Report.ReportRow.push({row: "0", col0: "110", col1: "Arty", col2: "98", col3: "99", col4: "12"});
            query.ReportList.Report.ReportRow.push({row: "1", col0: "111", col1: "Jenny", col2: "87", col3: "76", col4: "23"});
            query.ReportList.Report.ReportRow.push({row: "2", col0: "112", col1: "Jimi", col2: "76", col3: "56", col4: "34"});
            query.ReportList.Report.ReportRow.push({row: "3", col0: "113", col1: "Sarge", col2: "78", col3: "87", col4: "45"});
            query.ReportList.Report.ReportRow.push({row: "4", col0: "114", col1: "Bobby", col2: "56", col3: "76", col4: "56"});
          }

          return of(new HttpResponse<any>({ status: 200, body: query }));
        } else {
          this.delayTime = 0;
        }

        return next.handle(request);
      }))
      .pipe(delay(this.delayTime));
  }

  setWidgets(dashboards: Dashboard[], widgets: Widget[]): Dashboard[] {
    for (let dashboard of dashboards) {
      for (let widgetInstance of dashboard.widgetInstances) {
        for (let widget of widgets) {
          if (widgetInstance.widget.idWidget === widget.idWidget) {
            widgetInstance.widget = widget;
            break;
          }
        }
      }
    }

    return dashboards;
  }
}
