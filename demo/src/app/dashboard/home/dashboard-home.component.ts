/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding} from "@angular/core";

/**
 * @since 9.0.0
 */
@Component({
  selector: "hci-dashboard-home-demo",
  template: `
    <div class="d-flex flex-column m-3">
      <h3>Dashboard</h3>
      <div class="mb-3">
        The dashboard is a collection of widgets that can contain any type on information.  The user can be presented
        with a default dashboard, then add/delete/modify widgets in any way.  This demo has several examples of what
        a dashboard might look like.  D3 is used for all of the charting.
      </div>
    </div>
  `
})
export class DashboardHomeComponent {

  @HostBinding("class") classList: string = "outlet-row y-auto";

}
