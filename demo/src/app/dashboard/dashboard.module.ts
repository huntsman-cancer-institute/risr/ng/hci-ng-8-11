import {NgModule, Type} from "@angular/core";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {ColorPickerModule} from "ngx-color-picker";
import {DragulaModule} from "ng2-dragula";

import {NavigationModule} from "@huntsman-cancer-institute/navigation";
import {DashboardModule, USER_PROFILE_ENDPOINT} from "@huntsman-cancer-institute/dashboard";

import {DashboardDemoComponent} from "./dashboard.component";
import {HBarChartWidget} from "./widgets/hbar-chart-widget.component";
import {LogUserHitsWidget} from "./widgets/log-user-hits-widget.component";
import {HomeWidget} from "./widgets/home-widget.component";
import {PieChartWidget} from "./widgets/pie-chart-widget.component";
import {MultiLineChartWidget} from "./widgets/multi-line-chart-widget.component";
import {BarChartWidget} from "./widgets/bar-chart-widget.component";
import {SimpleContainerWidget} from "./widgets/simple-container-widget.component";
import {DashboardDemoService} from "./services/dashboard-demo.service";
import {DashboardHomeComponent} from "./home/dashboard-home.component";
import {SimpleComponent} from "./simple/simple.component";
import {ComplexWidgetsComponent} from "./complex-widgets/complex-widgets.component";
import {ManyWidgetsComponent} from "./many-widgets/many-widgets.component";
import {MultiDashboardComponent} from "./multi-dashboard/multi-dashboard.component";
import {EmptyComponent} from "./empty/empty.component";

/**
 * @since 9.0.0
 */
@NgModule({
  imports: [
    RouterModule.forChild([
      {path: "", component: DashboardDemoComponent,
        children: [
          {path: "", redirectTo: "home", pathMatch: "full"},
          {path: "complex-widgets", component: ComplexWidgetsComponent},
          {path: "empty", component: EmptyComponent},
          {path: "home", component: DashboardHomeComponent},
          {path: "many-widgets", component: ManyWidgetsComponent},
          {path: "multi-dashboard", component: MultiDashboardComponent},
          {path: "simple",  component: SimpleComponent}
        ]}
    ]),
    CommonModule,
    FormsModule,
    NgbModule,
    DashboardModule,
    NavigationModule,
    ColorPickerModule,
    DragulaModule,
  ],
  providers: [
    {provide: USER_PROFILE_ENDPOINT, useValue: "/api/user-profile"},
    DashboardDemoService
  ],
  declarations: [
    DashboardDemoComponent,
    DashboardHomeComponent,
    SimpleComponent,
    EmptyComponent,
    ComplexWidgetsComponent,
    ManyWidgetsComponent,
    MultiDashboardComponent,
    HomeWidget,
    SimpleContainerWidget,
    BarChartWidget,
    HBarChartWidget,
    PieChartWidget,
    MultiLineChartWidget,
    LogUserHitsWidget,
  ],
  entryComponents: [
    HomeWidget,
    SimpleContainerWidget,
    BarChartWidget,
    HBarChartWidget,
    PieChartWidget,
    MultiLineChartWidget,
    LogUserHitsWidget,
  ]
})
export class DashboardDemoModule {}
