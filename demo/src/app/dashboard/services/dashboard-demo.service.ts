import {Injectable, isDevMode} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {CoolLocalStorage} from "@angular-cool/storage";
import {Observable} from "rxjs";
import {map, switchMap} from "rxjs/operators";

import {UserProfile, Widget} from "@huntsman-cancer-institute/dashboard";

@Injectable()
export class DashboardDemoService {
  
  CONTENT_PATH: string = "";

  constructor(private http: HttpClient, private localStorage: CoolLocalStorage) {}

  init(): Observable<boolean> {
    if (isDevMode()) {
      console.debug("init");
    }

    let demo: string = this.localStorage.getItem("demo");
    let userProfile: UserProfile = this.localStorage.getObject("userProfile");
    let widgets: Widget[] = this.localStorage.getObject("widgets");

    return this.http.get(this.CONTENT_PATH + "data/" + demo + "/user-profile.json")
      .pipe(
        map((rawUserProfile: any) => rawUserProfile),
        switchMap((rawUserProfile: any) => {
          userProfile = new UserProfile().deserialize(rawUserProfile);

          return this.http.get(this.CONTENT_PATH + "data/" + demo + "/widgets.json");
        }),
        map((rawWidgets: any) => {
          widgets = new Widget().deserializeArray(rawWidgets);

          let widgetMap: Map<number, Widget> = new Map<number, Widget>();

          for (let widget of widgets) {
            widgetMap.set(widget.idWidget, widget);
          }

          for (var d = 0; d < userProfile.dashboards.length; d++) {
            for (var w = 0; w < userProfile.dashboards[d].widgetInstances.length; w++) {
              let widget: Widget = widgetMap.get(userProfile.dashboards[d].widgetInstances[w].widget.idWidget);
              if (widget !== null) {
                userProfile.dashboards[d].widgetInstances[w].widget = widget;
              }
            }
          }

          this.localStorage.setObject("userProfile", userProfile);
          this.localStorage.setObject("widgets", widgets);

          if (isDevMode()) {
            console.debug("Set userProfile");
            console.debug(userProfile);
            console.debug("Set widgets");
            console.debug(widgets);
          }

          return true;
        })
      );
  }

}
