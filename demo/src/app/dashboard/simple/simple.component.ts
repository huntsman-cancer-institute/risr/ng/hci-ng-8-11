/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, ViewChild} from "@angular/core";

import {DashboardComponent, DASHBOARD_TYPE} from "@huntsman-cancer-institute/dashboard";
import {NavigationGlobalService, LiNavComponent, LiDdNavComponent} from "@huntsman-cancer-institute/navigation";

/**
 * @since 1.0.0
 */
@Component({
  selector: "simple",
  template: `
    <dashboard #dashboard></dashboard>
  `,
  providers: [
    {provide: DASHBOARD_TYPE, useValue: "SIMPLE"}
  ]
})
export class SimpleComponent {

  @HostBinding("class") classList: string = "outlet-row y-auto";

  @ViewChild(DashboardComponent, {static: true}) private dashboard: DashboardComponent;

  constructor(private navigationGlobalService: NavigationGlobalService) {}

  ngAfterViewInit() {
    this.navigationGlobalService.getNavigationComponent("header").getComponent("header-right-container").add(
      {type: LiDdNavComponent, iClass: "fas fa-user-circle fa-lg", aClass: "nav-link", ulClass: "dropdown-menu dropdown-menu-right",
        children: [
          {type: LiNavComponent, title: "Edit Dashboard", aClass: "dropdown-item",
            liClick: () => {
              this.dashboard.setEditMode(true);
            }}
        ]
      }
    );
  }

  ngOnDestroy() {
    this.navigationGlobalService.getNavigationComponent("header").getComponent("header-right-container").clear();
  }

}
