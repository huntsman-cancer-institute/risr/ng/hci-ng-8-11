import {Component, ElementRef, OnInit} from "@angular/core";

import * as d3 from "d3";

import {DashboardService, WidgetComponent} from "@huntsman-cancer-institute/dashboard";
import {ComponentType} from "@huntsman-cancer-institute/utils";

@ComponentType("LogUserHitsWidget")
@Component({
  selector: "log-user-hits-widget",
  template: "",
  styles: [ `
    /deep/ .label {
      font-size: medium;
    }
    /deep/ .axis {
      stroke-width: 1px;
    }
    /deep/ .axis path, .axis line {
      fill: none;
      stroke: #000;
      shape-rendering: crispEdges;
      stroke-width: 1px;
    }
    /deep/ .line {
      fill: none;
      stroke: #00f;
      stroke-width: 1px;
    }
    /deep/ .chart-div {
      padding: 10px;
      background-color: white;
      border-radius: 0px;
      border: lightgray 1px solid;
      margin: 10px 10px;
    }
    /deep/ .ctext {
      font: 8px sans-serif;
      font-weight: bold;
    }
    /deep/ .year.label {
      font: 140 36px "Helvetica Neue";
      fill: #ddd;
    }
    /deep/ .year.label.active {
      fill: #aaa;
    }
    /deep/ .overlay {
      fill: none;
      pointer-events: all;
      cursor: ew-resize;
    }
    /deep/ circle {
      stroke-width: 1px;
    }
    /deep/ .tick > text {
      font-size: medium;
    }
    
  ` ]
})
export class LogUserHitsWidget extends WidgetComponent implements OnInit {

  private appHitUsers: Object[] = [
    { "app": "BST",
      "hits": [[1,71719],[2,90642],[3,112588],[4,103137],[5,95887],[6,79710],[7,75473],[8,81302],[9,82819],[10,68179],[11,61206],[12,67053],[13,95505],[14,81238]],
      "users": [[1,60],[2,66],[3,66],[4,61],[5,73],[6,61],[7,65],[8,67],[9,76],[10,66],[11,71],[12,65],[13,88],[14,84]]
    },
    { "app": "CCR",
      "hits": [[1,68661],[2,62684],[3,58578],[4,54305],[5,66333],[6,80633],[7,64557],[8,67832],[9,72352],[10,89350],[11,59062],[12,73300],[13,54072],[14,65354]],
      "users": [[1,56],[2,46],[3,48],[4,44],[5,41],[6,48],[7,42],[8,50],[9,46],[10,51],[11,39],[12,40],[13,45],[14,41]]
    },
    { "app": "CCRSurvey",
      "hits": [[1,159],[2,4],[3,0],[4,0],[5,8],[6,8],[7,49],[8,4],[9,0],[10,4],[11,8],[12,4],[13,0],[14,0]],
      "users": [[1,1],[2,1],[3,0],[4,0],[5,1],[6,1],[7,1],[8,1],[9,0],[10,1],[11,1],[12,1],[13,0],[14,0]]
    },
    { "app": "CGNQ",
      "hits": [[1,0],[2,0],[3,0],[4,7],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0],[13,0],[14,0]],
      "users": [[1,0],[2,0],[3,0],[4,1],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0],[13,0],[14,0]]
    },
    { "app": "CS Admin",
      "hits": [[1,0],[2,0],[3,0],[4,0],[5,13],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0],[13,0],[14,0]],
      "users": [[1,0],[2,0],[3,0],[4,0],[5,1],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0],[13,0],[14,0]]
    },
    { "app": "CommRequest Manager",
      "hits": [[1,0],[2,18],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0],[13,0],[14,0]],
      "users": [[1,0],[2,1],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0],[13,0],[14,0]]
    },
    { "app": "FHQ",
      "hits": [[1,82],[2,26],[3,28],[4,44],[5,0],[6,298],[7,47],[8,17],[9,0],[10,44],[11,35],[12,0],[13,21],[14,10]],
      "users": [[1,2],[2,3],[3,4],[4,2],[5,0],[6,3],[7,2],[8,1],[9,0],[10,1],[11,2],[12,0],[13,3],[14,2]]
    },
    { "app": "HOPE",
      "hits": [[1,31244],[2,32170],[3,33294],[4,46085],[5,35455],[6,36282],[7,28937],[8,33144],[9,27993],[10,27954],[11,29093],[12,21255],[13,26921],[14,36427]],
      "users": [[1,10],[2,10],[3,10],[4,13],[5,11],[6,12],[7,12],[8,13],[9,10],[10,12],[11,10],[12,11],[13,11],[14,10]]
    },
    { "app": "HRCC Export",
      "hits": [[1,20],[2,66],[3,15],[4,23],[5,17],[6,8],[7,0],[8,0],[9,0],[10,14],[11,0],[12,8],[13,18],[14,0]],
      "users": [[1,1],[2,4],[3,1],[4,1],[5,1],[6,1],[7,0],[8,0],[9,0],[10,1],[11,0],[12,1],[13,2],[14,0]]
    },
    { "app": "Idea",
      "hits": [[1,55],[2,64],[3,80],[4,6],[5,13],[6,70],[7,63],[8,31],[9,354],[10,46],[11,110],[12,131],[13,30],[14,0]],
      "users": [[1,4],[2,4],[3,4],[4,1],[5,2],[6,3],[7,3],[8,2],[9,8],[10,4],[11,5],[12,3],[13,1],[14,0]]
    },
    { "app": "LPI Admin",
      "hits": [[1,44],[2,152],[3,723],[4,199],[5,89],[6,5],[7,41],[8,0],[9,0],[10,79],[11,5],[12,5],[13,58],[14,0]],
      "users": [[1,1],[2,2],[3,2],[4,3],[5,1],[6,1],[7,1],[8,0],[9,0],[10,1],[11,1],[12,1],[13,2],[14,0]]
    },
    { "app": "MetaBuilder",
      "hits": [[1,1042],[2,2592],[3,3028],[4,828],[5,2481],[6,1156],[7,5606],[8,7408],[9,4308],[10,11583],[11,13743],[12,11671],[13,2920],[14,2586]],
      "users": [[1,7],[2,4],[3,5],[4,5],[5,7],[6,6],[7,6],[8,6],[9,8],[10,9],[11,8],[12,8],[13,9],[14,10]]
    },
    { "app": "NCCN",
      "hits": [[1,0],[2,0],[3,0],[4,0],[5,118],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0],[13,0],[14,0]],
      "users": [[1,0],[2,0],[3,0],[4,0],[5,1],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0],[13,0],[14,0]]
    },
    { "app": "Plasmid",
      "hits": [[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,1],[11,0],[12,0],[13,0],[14,0]],
      "users": [[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,1],[11,0],[12,0],[13,0],[14,0]]
    },
    { "app": "PurchAdmin",
      "hits": [[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,10],[9,0],[10,4837],[11,47],[12,0],[13,0],[14,4]],
      "users": [[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,1],[9,0],[10,4],[11,1],[12,0],[13,0],[14,1]]
    },
    { "app": "R Admin",
      "hits": [[1,0],[2,0],[3,15],[4,0],[5,0],[6,0],[7,0],[8,224],[9,30],[10,0],[11,11],[12,113],[13,44],[14,0]],
      "users": [[1,0],[2,0],[3,1],[4,0],[5,0],[6,0],[7,0],[8,1],[9,1],[10,0],[11,1],[12,1],[13,1],[14,0]]
    },
    { "app": "Security",
      "hits": [[1,175],[2,173],[3,342],[4,139],[5,286],[6,203],[7,243],[8,188],[9,217],[10,199],[11,233],[12,75],[13,258],[14,296]],
      "users": [[1,4],[2,3],[3,6],[4,6],[5,7],[6,6],[7,5],[8,3],[9,4],[10,5],[11,2],[12,3],[13,5],[14,4]]
    },
    { "app": "Subject",
      "hits": [[1,49105],[2,51424],[3,60342],[4,51133],[5,41334],[6,51117],[7,36965],[8,49153],[9,49142],[10,54118],[11,63253],[12,56433],[13,65347],[14,68635]],
      "users": [[1,39],[2,47],[3,34],[4,40],[5,47],[6,40],[7,44],[8,57],[9,57],[10,51],[11,54],[12,53],[13,58],[14,62]]
    },
    { "app": "iQ",
      "hits": [[1,2517],[2,2987],[3,9906],[4,10438],[5,10194],[6,10554],[7,9017],[8,10670],[9,10086],[10,11560],[11,9004],[12,6998],[13,13089],[14,11598]],
      "users": [[1,42],[2,46],[3,52],[4,48],[5,54],[6,53],[7,54],[8,57],[9,47],[10,52],[11,49],[12,40],[13,56],[14,58]]
    }
  ];

  protected svg: any;
  protected htmlElement: HTMLElement;
  protected svgParent: any;

  constructor(private element: ElementRef, dashboardService: DashboardService) {
    super(dashboardService);
    this.htmlElement = this.element.nativeElement;
    this.svgParent = d3.select(this.htmlElement);
  }

  ngOnInit() {
    window.onresize = (e) => {
      this.initSvg(this.appHitUsers);
	  };
  }

  ngAfterViewInit() {
    this.initSvg(this.appHitUsers);
  }

  initSvg(appHitUsers: Object[]) {
    this.svgParent.html("");

    function x(d: any) {
      if (d.hits == 0)
        return 1;
      else
        return d.hits;
    }

    function y(d: any) {
      return d.users;
    }

    function radius(d: any) {
      return d.hits;
    }

    function color(d: any) {
      return d.app;
    }

    function key(d: any) {
      return d.app;
    }

    // Positions the dots based on data.
    function gradius(dot: any) {
      dot.attr("r", function (d: any) {
        return radiusScale(radius(d));
      });
    }
    function gposition(dot: any) {
      dot.style("visibility", function(d: any) {
        return "inherit";
      });

      dot.attr("transform", function (d: any) {
        return "translate(" + xScale(x(d)) + "," + yScale(y(d)) + ")";
      });
    }
    function position(dot: any) {
      dot.attr("cx", function (d: any) {
        return xScale(x(d));
      })
        .attr("cy", function (d: any) {
          return yScale(y(d));
        })
        .attr("r", function (d: any) {
          return radiusScale(radius(d));
        });
    }

    // Defines a sort order so that the smallest dots are drawn on top.
    function order(a: any, b: any) {
      return radius(b) - radius(a);
    }

    // After the transition finishes, you can mouseover to change the year.
    function enableInteraction() {
      var yearScale = d3.scaleLinear()
        .domain([1, 14])
        .range([box.x + 10, box.x + box.width - 10])
        .clamp(true);

      // Cancel the current transition, if any.
      svgg.transition().duration(0);

      overlay
        .on("mouseover", mouseover)
        .on("mouseout", mouseout)
        .on("mousemove", mousemove)
        .on("touchmove", mousemove);

      function mouseover() {
        label.classed("active", true);
      }

      function mouseout() {
        label.classed("active", false);
      }

      function mousemove() {
        displayYear(yearScale.invert(d3.mouse(this)[0]));
      }
    }

    // Tweens the entire chart by first tweening the year, and then the data.
    // For the interpolated data, the dots and label are redrawn.
    function tweenYear() {
      var year = d3.interpolateNumber(1, 14);
      return function (t: any) {
        displayYear(year(t));
      };
    }

    // Updates the display to show the specified year.
    function displayYear(year: any) {
      dot.data(interpolateData(year), key).call(gposition).sort(order);

      cdot.data(interpolateData(year), key).call(gradius);

      if (year > 12) {
        label.text("2016-" + (Math.round(year) - 12));
      } else {
        label.text("2015-" + Math.round(year));
      }
    }

    // Interpolates the dataset for the given (fractional) year.
    function interpolateData(year: any) {
      return appHitUsers.map(function (d: any) {
        return {
          app : d.app,
          hits : interpolateValues(d.hits, year),
          users : interpolateValues(d.users, year)
        };
      });
    }

    var bisect = d3.bisector(function (d: any) {
      return d[0];
    });

    // Finds (and possibly interpolates) the value for the specified year.
    function interpolateValues(values: any, year: any) {
      var i = bisect.left(values, year, 0, values.length - 1),
        a = values[i];
      if (i > 0) {
        var b = values[i - 1],
          t = (year - a[0]) / (b[0] - a[0]);
        return a[1] * (1 - t) + b[1] * t;
      }
      return a[1];
    }

    var margin = {
      top : 10,
      right : 20,
      bottom : 20,
      left : 30
    };

    /*var width = this.getParentDimension(this.htmlElement, "width") - margin.left - margin.right;
    var maxHeight = this.getParentDimension(this.htmlElement, "height");
    var height = Math.min(maxHeight - margin.top - margin.bottom, width / 2 - margin.top - margin.bottom);*/
    var width = 400;
    var height = 200;

    this.svg = this.svgParent
      .classed("widget-svg-container", true)
      .append("svg")
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", "0 0 " + width + " " + height);

    var xScale = d3.scaleLog().domain([1, 120000]).range([0, width - margin.left - margin.right]);
    var yScale = d3.scaleLinear().domain([0, 90]).range([height - margin.top - margin.bottom, 0]);

    var radiusScale = d3.scaleLinear().domain([0, 120000]).range([5, 30]);
    var colorScale = d3.scaleOrdinal(d3["schemeCategory20"]);

    // The x & y axes.
    var xAxis = d3.axisBottom(xScale).ticks(5, d3.format(",d"));
    var yAxis = d3.axisLeft(yScale).ticks(5);

    // Create the SVG container and set the origin.
    /*var svgg = this.svg.attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    */

    var svgg = this.svg
      .append("g")
      .classed("widget-svg-content-responsive", true)
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Add the x-axis.
    svgg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + (height - margin.top - margin.bottom) + ")")
      .call(xAxis);
    // Add the y-axis.
    svgg.append("g")
      .attr("class", "y axis")
      .call(yAxis);
    // Add an x-axis label.
    svgg.append("text")
      .attr("class", "x label")
      .attr("text-anchor", "end")
      .attr("x", width)
      .attr("y", height - 6)
      .text("Hits");
    // Add a y-axis label.
    svgg.append("text")
      .attr("class", "y label")
      .attr("text-anchor", "end")
      .attr("y", 6)
      .attr("dy", ".75em")
      .attr("transform", "rotate(-90)")
      .text("Users");
    // Add the year label; the value is set on transition.
    var label = svgg.append("text")
      .attr("class", "year label")
      .attr("text-anchor", "start")
      .attr("y", margin.top + 0)
      .attr("x", margin.left + 0)
      .text("2015-01");

    var dot = svgg.append("g")
      .attr("class", "dots")
      .selectAll(".dot")
      .data(interpolateData(1))
      .enter()
      .append("g")
      .attr("transform", (d: any) => "translate(" + xScale(x(d)) + "," + yScale(y(d)) + ")");

    var cdot = dot.append("circle")
      .attr("stroke", "#444444")
      .attr("stroke-width", "2px")
      .attr("class", "dot")
      .style("fill", (d: any) => {
        return colorScale(color(d));
      })
      .attr("r", (d: any) => radiusScale(radius(d)));

    // Add a title.
    var tdot = dot.append("text")
      .attr("class", "ctext")
      .attr("text-anchor", "middle")
      .attr("transform", "translate(0, 5)")
      .text((d: any) => { return d.app; });
    // Add an overlay for the year label.
    var box = label.node().getBBox();
    var overlay = svgg.append("rect")
      .attr("class", "overlay")
      .attr("x", box.x)
      .attr("y", box.y)
      .attr("width", box.width)
      .attr("height", box.height)
      .attr("fill", "none")
      .on("mouseover", enableInteraction);
    // Start a transition that interpolates the data based on year.

    svgg.transition()
      .duration(15000)
      .ease(d3.easeLinear)
      .tween("inter", tweenYear);

    this.rendered.next(true);
  }

}
