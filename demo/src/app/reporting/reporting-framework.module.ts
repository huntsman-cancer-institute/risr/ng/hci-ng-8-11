import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";

import {ReportingFrameworkDemoComponent} from "./reporting-framework-demo.component";
import {ReportModule} from "@huntsman-cancer-institute/reporting-framework";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReportModule,
    RouterModule.forChild([
      {path: "", component: ReportingFrameworkDemoComponent}
    ])
  ],
  declarations: [
    ReportingFrameworkDemoComponent
  ]
})
export class ReportingFrameworkDemoModule {}
