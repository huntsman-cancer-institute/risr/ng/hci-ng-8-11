import {DemoInterceptor} from "../demo.interceptor";
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {delay, mergeMap} from "rxjs/operators";
import {Injectable, isDevMode} from "@angular/core";

let REPORT_JSON_RESULT: any = require("./data/report-data.json");

let FISCAL_YEAR_DICTIONARY: any = require("./data/fiscal-year.dictionary.json");
let GRANT_APPLICATION_TYPE_DICTIONARY: any = require("./data/grant-application-type.dictionary.json");
let GRANT_STATUS_DICTIONARY: any = require("./data/grant-status.dictionary.json");
let GROUP_DICTIONARY: any = require("./data/group.dictionary.json");
let GROUP_TYPE_DICTIONARY: any = require("./data/group-type.dictionary.json");
let INCLUDED_IN_DICTIONARY: any = require("./data/included-in.dictionary.json");
let PROGRAM_DICTIONARY: any = require("./data/program.dictionary.json");

let XLS_RESULT: any = require("!raw-loader!./data/report-xls.txt");
let HTML_RESULT: any = require("!raw-loader!./data/report-html.txt");
let CSV_RESULT: any = require("!raw-loader!./data/report-csv.txt");

@Injectable()
export class ReportingFrameworkInterceptor extends DemoInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(isDevMode()) {
      console.log("ReportingFrameworkInterceptor.intercept: " + request.url);
    }

    return of(null)
      .pipe(mergeMap( () => {
        if(request.url.match(".*ManageReports.*")) {
          if (isDevMode()) {
            console.info("GET .*ManageReports");
          }
          return of(new HttpResponse<any>({status: 200, body: REPORT_JSON_RESULT }));

        } else if (request.url.match(".*FiscalYear/entries")) {
          if (isDevMode()) {
            console.info("GET .*FiscalYear/entries");
          }
          return of(new HttpResponse<any>({ status: 200, body: FISCAL_YEAR_DICTIONARY }));

        } else if (request.url.match(".*GrantApplicationType/entries")) {
          if (isDevMode()) {
            console.info("GET .*GrantApplicationType/entries");
          }
          return of(new HttpResponse<any>({ status: 200, body: GRANT_APPLICATION_TYPE_DICTIONARY }));

        } else if (request.url.match(".*GrantStatus/entries")) {
          if (isDevMode()) {
            console.info("GET .*GrantStatus/entries");
          }
          return of(new HttpResponse<any>({ status: 200, body: GRANT_STATUS_DICTIONARY }));

        } else if (request.url.match(".*GroupDict/entries")) {
          if (isDevMode()) {
            console.info("GET .*GroupDict/entries");
          }
          return of(new HttpResponse<any>({ status: 200, body: GROUP_DICTIONARY }));

        } else if (request.url.match(".*GroupType/entries")) {
          if (isDevMode()) {
            console.info("GET .*GroupType/entries");
          }
          return of(new HttpResponse<any>({ status: 200, body: GROUP_TYPE_DICTIONARY }));

        } else if (request.url.match(".*IncludedIn/entries")) {
          if (isDevMode()) {
            console.info("GET .*IncludedIn/entries");
          }
          return of(new HttpResponse<any>({ status: 200, body: INCLUDED_IN_DICTIONARY }));

        } else if (request.url.match(".*Program/entries")) {
          if (isDevMode()) {
            console.info("GET .*Program/entries");
          }
          return of(new HttpResponse<any>({ status: 200, body: PROGRAM_DICTIONARY }));

        }  else if (request.url.match(".*reports.Report.*")) {//run any report
          if (isDevMode()) {
            console.info("GET .*reports.Report.*");
          }
          let format = request.params.get("format");

          if(format == "xls") {
            let header = new HttpHeaders({"content-disposition": "attachment; filename=\"test.xls\""});
            return of(new HttpResponse<Blob>({ status: 200, body: XLS_RESULT.default, headers: header }));
          } else if(format == "csv") {
            let header = new HttpHeaders({"content-disposition": "attachment; filename=\"test.csv\""});
            return of(new HttpResponse<Blob>({ status: 200, body: CSV_RESULT.default, headers: header }));
          } else if(format == "pdf") {
            let header = new HttpHeaders({"content-disposition": "attachment; filename=\"test.html\""});
            return of(new HttpResponse<Blob>({ status: 200, body: HTML_RESULT.default, headers: header }));
          } else { //html
            return of(new HttpResponse<Blob>({ status: 200, body: HTML_RESULT.default }));
          }
        } else {
          this.delayTime = 0;
        }
        return next.handle(request);
      }))
      .pipe(delay(this.delayTime));
  }
}
