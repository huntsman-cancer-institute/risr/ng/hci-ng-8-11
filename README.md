# HCI Angular Packages

This project is a mono repo for all RISR associated Angular libraries.  They are intended to create common functionality
and stlying across all applications part of the redesign effort including CORE.

Styling is based off of bootstrap with @ng-bootstrap and the primary component library.

All packages are versioned based upon the root version of hci-ng.  Gulp scripts are used to push this version to all others.
However, each package is published as its own artifact.  Here is a list of all packages in [./modules/@huntsman-cancer-institute](modules/@huntsman-cancer-institute):

1. [@huntsman-cancer-institute/authentication](modules/@huntsman-cancer-institute/authentication) - Authentication involving JWT and SSO.
1. [@huntsman-cancer-institute/cod](modules/@huntsman-cancer-institute/cod) - Basic COD service and generic COD components.
1. [@huntsman-cancer-institute/dashboard](modules/@huntsman-cancer-institute/dashboard) - Dashboard hosts configurable widgets.
1. [@huntsman-cancer-institute/dictionary-service](modules/@huntsman-cancer-institute/dictionary-service) - Service to interact with the backend dictionary api.
1. [@huntsman-cancer-institute/help](modules/@huntsman-cancer-institute/help) - Help
1. [@huntsman-cancer-institute/icon](modules/@huntsman-cancer-institute/icon) - Our own icons based on font awesome 5.
1. [@huntsman-cancer-institute/input](modules/@huntsman-cancer-institute/input) - Custom input components.
1. [@huntsman-cancer-institute/misc](modules/@huntsman-cancer-institute/misc) - Various components or helper classes with no home.
1. [@huntsman-cancer-institute/navigation](modules/@huntsman-cancer-institute/navigation) - General navigation components for dynamic configuration.
1. [@huntsman-cancer-institute/notification](modules/@huntsman-cancer-institute/notification) - Notification
1. [@huntsman-cancer-institute/reporting-framework](modules/@huntsman-cancer-institute/reporting-framework) - Main component for the Reporting Framework
1. [@huntsman-cancer-institute/style](modules/@huntsman-cancer-institute/style) - Centralized style sheets for RISR and CORE.
1. [@huntsman-cancer-institute/study](modules/@huntsman-cancer-institute/study) - Services and components for getting studies and lists of studies.
1. [@huntsman-cancer-institute/user](modules/@huntsman-cancer-institute/user) - User for roles and permissions

## NPM

For setting up npm, see [here](https://ri-confluence.hci.utah.edu/display/STRD/npm+and+Artifactory) for further details.

## Getting Started

First, node must be installed on your machine.  After that you can run the install command on hci-ng:

    npm install

Gulp scripts are used to manage commands from the project root rather than deal with changing directory in each package
and running commands individually.

This command will do through each package in order, do an install on it and copy its built files in to the packages that
rely on it including pushing to the demo.  Installing each module takes a while the first time.

    npm run build-libs -- --install

Then install the demo (this just has to be done when the demo's package.json non @huntsman-cancer-institute dependencies change):

    npm run install-demo

Then to run the demo (then go to http://localhost:3000):

    npm run run-demo-prod

## Versioning

Set the version in the root package.json file.  To update the version in all packages, run the following:

    npm run push-versions

To check to see if your version compares to what is published, run:

    npm run version-check

## Building

Getting started show the command to do a full install and build on every package.  However, if every package already has
had an npm install run, you don't need to do that step again.  To build and push all the packages, do the following:

    npm run build-libs

Also, if you only update a couple packages and want to test them in the demo, you can do the following:

    npm run build-libs -- --packages icon,style

## Publishing

The previous build commands copy each built package in to the packages that depend on it.  This is a way of testing locally
without publishing anything.  However, for the actual publish we don't want to do that and instead install each package
and publish.  That way each package is pulling the published artifact for its build and publish rather than rely on local
copying.
First, we need to bump the version.  Update the version in the root package.json.  To push this change, run

    npm run push-versions
    npm run push-dependencies

The first command pushes the root version to the package.json in each package.  The push-dependencies command pushes
all versions in pushDependencies in the root package.json as well as @huntsman-cancer-institute dependencies to all the packages and demo.

To see what the state of npm artifacts, run the following.  This will show you the latest artifact for each package and
if the current version is published or not.

    npm run check-versions

Since we just bumped the version, it will show every package as out of sync.  But now, run the following command to
publish all libs.

    npm run publish-libs

## Demo

Running the demo assumes that all libaries have been built and pushed to the demo.

### Running Locally (webpack-live)

    npm run run-demo

### Demo on Wildfly (webpack-server)

Assuming the demo can be run using "npm run run-demo", run "npm run run-demo-prod".  In wildfly deployments, create a
directory called "hci-ng-demo.war".  This must be the same as what is specified in webpack.server.js.
Then copy dist/ in to that directory.  In deployments, do a "hci-ng-demo.war.dodeploy" and the demo
will be available from Wildfly.  This could be "http://hci-as-dev.hci.utah.edu/hci-ng-demo".  One note
is that you cannot directly go to a route since there is no real application backing it.

## Other Commands

To view the dependency tree:

    npm run find-dependencies

The following commands accept the "-- --packages arg1,arg2,..." argument to control which packages are affected.

To clean compiled code and node_packages:

    npm run clean-all

To push libs to the demo:

    npm run push-libs

To delete pushed packages in to other packages:

    npm run delete-libs

## Push Externally (to Core)

To test a package in an application, you can build the package and copy that transpiled code directly to the node_modules
of an application.  The generic command is as follows:

    npm run push-libs-dest -- --dest ../core/core-ng

As a shortcut, Core has its own run commands to make it easier.  Also, if you don't specify --packages,
all packages will be copied, so if you only re-build one, use the --packages argument.

    npm run push-core -- --packages user

This copy will be overwritten in core if you do the full build command which runs an npm install.  To avoid that, when you
run core, run the following.  This will still re-package the angular, but won't check what versions are in the node_modules.

    gradle deploy -Pskip-install
